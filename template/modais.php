
<div class="modal fade" id="modal-login" tabindex="-1" role="modal" aria-labelledby="modal-label-3"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-label-3" class="modal-title"><i class="fa fa-lock"></i> FAÇA O SEU LOGIN </h4>
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 p-20">
                        <form class="">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">E-MAIL DE ACESSO</label>
                                    <input class="form-control" id="inputEmail4" placeholder="Insira o seu lgin" type="text">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputPassword4">E-MAIL</label>
                                    <input class="form-control" id="inputPassword4" placeholder="Insira a sua senha"
                                           type="password">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 ">
                        <div role="alert" class="alert alert-warning alert-dismissible">
                            <h4><strong><i class="fa fa-exclamation-circle"></i> Esqueceu a sua senha?</strong> <br /> Não tem problema, <a style="text-decoration: underline;" href="#" class="text-white " name="btn_recuperar_senha">clique aqui</a> para recuperar</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-b" type="button">FECHAR</button>
                <button class="btn btn-outline btn-light btn-azul" type="button">FAZER LOGIN <i class="fa fa-check"></i> </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-recuperar-senha" tabindex="-1" role="modal" aria-labelledby="modal-label-3"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-label-3" class="modal-title"><i class="fa fa-lock"></i> FAÇA O SEU LOGIN </h4>
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 p-20">
                        <form class="">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputEmail4">E-MAIL DE ACESSO</label>
                                    <input class="form-control" id="email-acesso" placeholder="Insira o seu lgin" type="text">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 ">
                        <div role="alert" class="alert alert-info alert-dismissible">
                            <h4><strong><i class="fa fa-exclamation-circle"></i> Lembrou a senha?</strong> <br /> Então faça o seu login  <a style="text-decoration: underline;" href="#" name class="text-white" name="btn-login-recuperar-senha">clicando aqui</a></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-b" type="button">FECHAR</button>
                <button class="btn btn-warning" type="button">RECUPERAR SENHA <i class="fa fa-check"></i> </button>
            </div>
        </div>
    </div>
</div>
