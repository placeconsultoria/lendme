<div  class="call-to-action p-t-20 p-b-40 background-colored mb-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-10">
                <img src="<?= $URL_SITE ?>images/logo-dark.png"   />
                <h4 class="text-light p-l-10">
                    Considerada uma das mais modernas lojas do mundo, a Pedale conta com uma ampla estrutura de showroom e oficina. Somos especializados em SPECIALIZED.
                </h4>
            </div>
            <div class="col-lg-2 text-center p-t-50">
                <h4 class="text-white m-b-0 p-b-0">LIGUE AGORA</h4>
                <a href="tel:+554131218609" class="btn btn-light btn-outline m-t-0 "><i class="fa fa-phone fa-rotate-90"></i> (41) 3121-8609</a>
            </div>
        </div>
    </div>
</div>