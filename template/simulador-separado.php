<section id="simulador">
    <div class="container p-b-0">
        <div class="row">
            <div class="col-lg-5">
                <h2   class="font-title-1 color-azul-b2b">SIMULE AGORA MESMO!</h2>
                <h4  class="font-title-1">Crédito de até <b>50%</b> do valor do seu imóvel e <b>20 anos</b> para pagar</h4>
                <p  class="lead font-paragrafo-1">
                    O empréstimo com garantida de imóvel que você precisa em poucos passos.<br><br>
                    1 - Simule sua necessidade e prazo de pagamento, e confira as condições iniciais.<br>
                    2 - Envie sua documentação de forma simples, online e rápida.<br>
                    3 - O processo de análise é iniciado.<br>
                    4 - Aprovada a solicitação, o crédito é liberado e o dinheiro entra na sua conta.<br>
                </p>
                <div class="row p-t-30">
                    <div class="col-12">
                        <h4 class="font-title-1 color-azul-b2b" >Toda sua jornada acompanhada por um assessor especializado que vai te orientar e negociar
as melhores condições para você.</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 ">
                <div class="row">
                    <div class="col-12  background-grey p-b-10 p-t-20 " style="min-height: 300px;"  >
                        <div id="simulador_step_1">

<!--                            deixar sequencial (valor do imóvel, valor do empréstimo, idade e prazo para pagamento)-->
<!--                            - escala de 10 mil para valor de empréstimo e imóvel-->
<!--                            - no valor de empréstimo o máximo seria 1,5MM (não 3MM)-->
<!--                            - o valor do imóvel tem que ser no mínimo o dobro do valor do empréstimo (acho que isso é API)-->
<!--                            - deixar a escala de renda de 1000 em 1000-->
<!---->
<!--                            Vou prosseguir testando, abs-->

                            <!-- --->
                            <div class="row">
                               

                                <div class="col-lg-6 center text-center m-b-30">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo">QUAL O VALOR DO EMPRÉSTIMO?</h4>
                                    <input id="valor_desejado" type="text" name="valor_desejado" value="" />
                                    <small>Acima de 1.5 milhões, entre em contato.</small>
                                </div>
                                <div class="col-lg-6  center text-center m-b-30">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo">QUAL O VALOR DO SEU IMÓVEL?</h4>
                                    <input id="valor_imovel" name="valor_imovel" type="text" value="" />
                                </div>
                                <div class="col-lg-6 center text-center m-b-20">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo m-b-30">QUAL A SUA IDADE?</h4>
                                    <input id="range_idade" name="range_idade" type="range" min="18"  max="75"  step="1"  value="25"  data-rangeslider >
                                </div>
                                <div class="col-lg-6  center text-center">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo m-b-30">PRAZO PARA O PAGAMENTO DESEJADO?</h4>
                                    <input id="range_prazo" name="range_prazo" type="range" min="5"  max="20"  step="1"  value="60"  data-rangeslider >
                                </div>


                                <div class="col-lg-12 p-t-20 center text-center">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo text-uppercase">Escolha o sistema de amortização (Price ou SAC)
                                    
                                    <button class="btn btn-xs btn-slide btn-light" href="#" data-container="body" data-toggle="popover" data-placement="top" title="Price ou SAC?" data-content="<small><b>Sistema SAC (Sistema de Amortização
Constante)</b> se mantém fixo é o valor da
amortização. Como os juros vão diminuindo com o
passar do tempo, consequentemente a prestação
também diminuirá.<br><br> A <b>Tabela Price</b>, a
somatória entre juros e amortização é sempre
igual. Entretanto, como o saldo devedor decresce,
os juros também decrescem ao passo que a
amortização aumenta, ou seja, amortiza se mais
no final quando comparado ao início do contrato.</small>">
                                    <i class="fa fa-question"></i>
                                    <span>Saiba mais</span>
                                    </button>
                                    
                                    
                                    
                                    
                                    </h4>
                                    <input id="forma_parcela" name="form_parcela" data-switch="true" data-size="small" data-on-color="secondary" checked type="checkbox" data-on-text="TABELA PRICE" data-off-text="TABELA SAC">
                                </div>
                                <div class="col-lg-10 center text-center">
                                    <div class="line m-t-10 m-b-10"></div>
                                    <div class="row valores_parcela">
                                        <div class="col-md-6 text-center">
                                            <h5 class="font-title-1 color-roxo text-uppercase">Valor da 1ª. parcela:</h5>
                                            <button id="r_v_p" type="button" class="btn btn-azul" style="min-width: 200px">R$ 1.500,00</button>
                                        </div>
                                        <div class="col-md-6 text-center">
                                            <h5 class="font-title-1 color-roxo text-uppercase">Custo Efetivo Total:</h5>
                                            <button id="r_cte" type="button" class="btn btn-azul" style="min-width: 200px">2%</button>
                                        </div>
                                    </div>
                                    <div class="row erro_simulador">
                                        <div class="col-lg-12">
                                            <div role="alert" class="alert alert-danger text-left alert-dismissible">
                                                <i class="fa fa-warning"></i> Atenção: <span id="text_error_api"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 m-t-20 m-b-20">
                                    <small>Esta simulação é apenas ilustrativa e não garante o valor final a ser pago pelo tomador do empréstimo nem a aprovação da concessão do crédito através da LendMe.</small>
                                </div>
                                <div class="col-12 text-center" >
                                    <a href="#" id="btn_simulador_direct" class="btn" style="font-size: 18px; line-height: 120%; width: 100%;">Veja aqui as melhores<br>condições para você</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>