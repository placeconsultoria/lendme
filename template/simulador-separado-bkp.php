<section id="simulador">
    <div class="container p-b-0">
        <div class="row">
            <div class="col-lg-5">
                <h2 data-animate="fadeInUp" data-animate-delay="150"  class="font-title-1 color-azul-b2b">SIMULE AGORA MESMO!</h2>
                <h4 data-animate="fadeInUp" data-animate-delay="150"  class="font-title-1">As melhores taxas e prazos para você. Seu empréstimo em poucos passos</h4>
                <p data-animate="fadeInUp" data-animate-delay="200" class="lead font-paragrafo-1">
                    Simule sua necessidade e prazo de pagamento. Um processo de análise é iniciado. Os dados e a documentação são facilmente encaminhados, eletronicamente, aos analistas. Tudo com sigilo total. Aprovada sua solicitação, seu crédito é liberado. Assessores especializados acompanham toda sua jornada
                </p>
                <div class="row p-t-30">
                    <div class="col-12">
                        <h4 class="font-title-1 color-azul-b2b" data-animate="fadeInUp" data-animate-delay="400">Obtenha crédito de até 50% do valor do seu imóvel e 15 anos para quitar</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 ">
                <div class="row">
                    <div class="col-12  background-grey p-b-10 p-t-20 " style="min-height: 300px;" data-animate="fadeInRight" data-animate-delay="500" >
                        <div id="simulador_step_1">

<!--                            deixar sequencial (valor do imóvel, valor do empréstimo, idade e prazo para pagamento)-->
<!--                            - escala de 10 mil para valor de empréstimo e imóvel-->
<!--                            - no valor de empréstimo o máximo seria 1,5MM (não 3MM)-->
<!--                            - o valor do imóvel tem que ser no mínimo o dobro do valor do empréstimo (acho que isso é API)-->
<!--                            - deixar a escala de renda de 1000 em 1000-->
<!---->
<!--                            Vou prosseguir testando, abs-->

                            <!-- --->
                            <div class="row">
                                <div class="col-lg-6  center text-center m-b-30">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo">QUAL O VALOR DO SEU IMÓVEL?</h4>
                                    <input id="valor_imovel" name="valor_imovel" type="text" value="" />
                                </div>

                                <div class="col-lg-6 center text-center m-b-30">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo">QUAL O VALOR DO EMPRÉSTIMO?</h4>
                                    <input id="valor_desejado" type="text" name="valor_desejado" value="" />
                                    <small>Acima de 1.5 milhões, entre em contato.</small>
                                </div>
                                <div class="col-lg-6 center text-center m-b-20">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo m-b-30">QUAL A SUA IDADE?</h4>
                                    <input id="range_idade" name="range_idade" type="range" min="18"  max="72"  step="1"  value="25"  data-rangeslider >
                                </div>
                                <div class="col-lg-6  center text-center">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo m-b-30">PRAZO PARA O PAGAMENTO DESEJADO?</h4>
                                    <input id="range_prazo" name="range_prazo" type="range" min="1"  max="15"  step="1"  value="60"  data-rangeslider >
                                </div>


                                <div class="col-lg-12 p-t-20 center text-center">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo text-uppercase">Escolha o sistema de amortização</h4>
                                    <input id="forma_parcela" name="form_parcela" data-switch="true" data-size="small" data-on-color="secondary" checked type="checkbox" data-on-text="TABELA PRICE" data-off-text="TABELA SAC">
                                </div>
                                <div class="col-lg-10 center text-center">
                                    <div class="line m-t-10 m-b-10"></div>
                                    <div class="row valores_parcela">
                                        <div class="col-md-6 text-center">
                                            <h5 class="font-title-1 color-roxo text-uppercase">Valor da 1ª. parcela:</h5>
                                            <button id="r_v_p" type="button" class="btn btn-azul" style="min-width: 200px">R$ 1.500,00</button>
                                        </div>
                                        <div class="col-md-6 text-center">
                                            <h5 class="font-title-1 color-roxo text-uppercase">Custo Efetivo Total:</h5>
                                            <button id="r_cte" type="button" class="btn btn-azul" style="min-width: 200px">2%</button>
                                        </div>
                                    </div>
                                    <div class="row erro_simulador">
                                        <div class="col-lg-12">
                                            <div role="alert" class="alert alert-danger text-left alert-dismissible">
                                                <i class="fa fa-warning"></i> Atenção: <span id="text_error_api"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 m-t-20 m-b-20" data-animate="fadeInUp" data-animate-delay="1200">
                                    <small>Esta simulação é apenas ilustrativa e não garante o valor final a ser pago pelo tomador do empréstimo nem a aprovação da concessão do crédito através da LendMe.</small>
                                </div>
                                <div class="col-12 text-center" data-animate="fadeInUp" data-animate-delay="800" >
                                    <a href="#" id="btn_simulador_direct" class="btn" style="font-size: 18px; line-height: 120%; width: 100%;">Veja aqui as melhores <br />Condições para você</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>