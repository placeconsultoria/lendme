<?
//require_once("include/template/topbar.php");
//require_once("include/template/header.php");
?>
<div class=" background-colored m-b-0">
    <div class="container p-t-10">

    </div>
</div>

    <main id="body-content">

        <!-- Default Grid Start -->
        <section class="wide-tb-100">
            <div class="container pos-rel">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1 bg-light-gray rounded">
                        <!-- xxx Error Page xxx -->
                        <div class="text-center p-5">
                            <img src="<?=$URL_SITE?>assets/images/404_img.png" alt="Pagina não encontrada 404">
                            <h3 class="h3-sm fw-6 txt-ligt-gray mb-4 mt-5">Ops!! pagina não econtrada <br>pode ser que esta url não existe mais.</h3>
                            <a href="<?=$URL_SITE?>" class="mr-2 mb-3 btn-theme bg-navy-blue icon-left"><i class="icofont-home"></i> Voltar para Inicial</a>
                        </div>
                        <!-- xxx Error Page xxx -->
                    </div>
                </div>

            </div>
        </section>
        <!-- Default Grid End -->
    </main>
<?     require_once("include/template/footer.php"); ?>


<a id="mkdf-back-to-top" href="#" class="off"><i class="icofont-rounded-up"></i></a>
