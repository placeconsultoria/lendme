<link href="https://lendme.com.br/css/plugins.css" rel="stylesheet">
    <link href="https://lendme.com.br/webfonts/bwgradual/fontes1/stylesheet.css" rel="stylesheet">
    <link href="https://lendme.com.br/webfonts/bwgradual/fontes2/stylesheet.css" rel="stylesheet">
    <link href="https://lendme.com.br/css/style.css" rel="stylesheet">
    <link href="https://lendme.com.br/css/responsive.css" rel="stylesheet">
    <link href="https://lendme.com.br/css/adjusts.css" rel="stylesheet">
    <link rel="stylesheet" href="https://lendme.com.br/js/rangerslider/rangeslider.css">
    <link rel="stylesheet" href="https://lendme.com.br/js/ionrangeslider/css/ion.rangeSlider.css"/>
    <link rel="stylesheet" href="https://lendme.com.br/inc/sweetalert/sweetalert.css"/>
    <script type="text/javascript" src="https://lendme.com.br/js/jquery1.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Rubik:500%2C400%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="https://lendme.com.br/js/plugins/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" type="text/css" href="https://lendme.com.br/js/plugins/revolution/fonts/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="https://lendme.com.br/js/plugins/revolution/css/settings.css">
    <style type="text/css">.tiny_bullet_slider .tp-bullet:before{content:" ";  position:absolute;  width:100%;  height:25px;  top:-12px;  left:0px;  background:transparent}</style>
    <style type="text/css">.bullet-bar.tp-bullets{}.bullet-bar.tp-bullets:before{content:" ";position:absolute;width:100%;height:100%;background:transparent;padding:10px;margin-left:-10px;margin-top:-10px;box-sizing:content-box}.bullet-bar .tp-bullet{width:60px;height:3px;position:absolute;background:#aaa;  background:rgba(204,204,204,0.5);cursor:pointer;box-sizing:content-box}.bullet-bar .tp-bullet:hover,.bullet-bar .tp-bullet.selected{background:rgba(204,204,204,1)}.bullet-bar .tp-bullet-image{}.bullet-bar .tp-bullet-title{}</style>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type='text/javascript' src='https://lendme.com.br/js/plugins/revolution/revolution-addons/slicey/js/revolution.addon.slicey.min.js?ver=1.0.0'></script>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="https://lendme.com.br/js/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <link rel="shortcut icon" href="https://lendme.com.br/images/favicon.ico" />
    <script src="https://lendme.com.br/js/jquery03.js"></script>
    <script src="https://lendme.com.br/js/rangerslider/rangeslider.min.js"></script>
    <script src="https://lendme.com.br/js/ion.js"></script>
    <script src="https://lendme.com.br/js/plugins.js"></script>
    <script src="https://lendme.com.br/js/functions.js"></script>
    <script src="https://lendme.com.br/js/mask.js"></script>
    <script src="https://lendme.com.br/js/moment.js"></script>
    <script src="https://lendme.com.br/inc/sweetalert/sweetalert.min.js"></script>


<script>
    $(document).ready(function() {


        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $('.title-banner-mobile').css("font-size","30px").css("line-height","120%");
        }

        $("#btn_simulador_direct").click(function(e) {
            e.preventDefault();
            var Amortizacao = retornaStatusTipoParcela();
            var prazo =  parseFloat($('#range_prazo').val()) * 12;
            var param = {
                "ValorImovel": $("#valor_imovel").val(),
                "ValorFinanciamento": $("#valor_desejado").val(),
                "Idade": $("#range_idade").val(),
                "Prazo": prazo,
                "Amortizacao": Amortizacao
            };
            var json_param = JSON.stringify(param);

            var pre_url = "https://lendme.com.br/";
            $.ajax({
                url: "https://lendme.com.br/ajax.php?acao=simulador",
                headers: {
                    "Content-Type": "application/json"
                },
                type: "POST",
                data: json_param ,
                dataType: "json",
                contentType: false,
                processData: false,
                success: function (data) {
                    if(data.status == true){
                        window.top.location = pre_url+data.link;
                    }
                },
                error: function(e) {

                }

            });

        });




        
        $('#link_redirect').click(function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            window.open(href, '_blank');
        });

        

        

        


        $('#range_idade').rangeslider({
            polyfill : false,
            onInit : function() {
                this.output = $( '<div class="range-output center" />' ).insertAfter( this.$range ).html(this.$element.val()+" anos" );
            },
            onSlide : function( position, value ) {
                this.output.html(value+" anos" );
            }
        });
        $('#range_prazo').rangeslider({
            polyfill : false,
            onInit : function() {
                this.output = $( '<div class="range-output2 center" />' ).insertAfter( this.$range ).html(this.$element.val()+" anos" );
            },
            onSlide : function( position, value ) {
                this.output.html(value+" anos" );
            }
        });
        $("#valor_desejado").ionRangeSlider({
            type: "single",
            grid: true,
            min: 100000,
            max: 1500000,
            from: 1000,
            to: 1000,
            step: 10000,
            prefix: "R$"
        });

        $("#frm_pre_cadastro_renda").ionRangeSlider({
            type: "single",
            grid: true,
            min: 1000,
            max: 100000,
            from: 1000,
            to: 1000,
            step: 1000,
            prefix: "R$"
        });


        $("#valor_imovel").ionRangeSlider({
            type: "single",
            grid: true,
            min: 250000,
            max: 3000000,
            from: 200,
            to: 800,
            step: 10000,
            prefix: "R$"
        });
        $("#range_idade").change(function (e) {
            e.preventDefault();
            var idade = parseFloat($(this).val());
            var prazo = 80 - idade;
            if(prazo > 15){ prazo = 15;}
            $('#range_prazo').attr("max",prazo);
            $('#range_prazo').rangeslider('update', true);
            SimuladorApi();
        });
        $("#valor_imovel").change(function (e) {
            e.preventDefault();
            SimuladorApi();
        });
        $("#valor_desejado").change(function (e) {
            e.preventDefault();
            SimuladorApi();
        });
        $("#range_prazo").change(function (e) {
            e.preventDefault();
            SimuladorApi();
        });
        $('#forma_parcela').on('switchChange.bootstrapSwitch', function (event, state) {
            SimuladorApi();
        });


    });


    function SimuladorApi(){
        var Amortizacao = retornaStatusTipoParcela();
        var prazo =  parseFloat($('#range_prazo').val()) * 12;
        var param = {
            "ValorImovel": $("#valor_imovel").val(),
            "ValorFinanciamento": $("#valor_desejado").val(),
            "Idade": $("#range_idade").val(),
            "Prazo": prazo,
            "Amortizacao": Amortizacao
        };
        var json_param = JSON.stringify(param);

        $.ajax({
            url: "https://api.lendme.com.br/Api/Proposta/Simulador",
            headers: {
                "Content-Type": "application/json"
            },
            type: "POST",
            data: json_param ,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (data) {

                data = data.retorno;
                $("#r_v_p").html(numberToReal(data.ValorParcela));
                $("#r_cte").html(data.CET.toFixed(2)+'%');
                $(".valores_parcela").show();
                $(".erro_simulador").hide();
            },
            error: function(e) {
                console.log(JSON.stringify(e));
                var erros = e.responseText;
                var obj = JSON.parse(erros);
                // alert(obj.Erros);
                // console.log(JSON.stringify(obj.Erros));
                var text_erro = obj.Erros;
                $(".valores_parcela").hide();
                $(".erro_simulador").show();
                $("#text_error_api").html(obj.Erros);
            }

        });
    }

    function retornaStatusTipoParcela(){
        var status = "";
        if($("#forma_parcela").prop("checked") == true){
            status = "price"
        }else{
            status = "sac";
        }
        return status
    }



    function EnviaApiPreCadastro(){

        var nome = $("#frm_pre_cadastro_nome").val();
        var email = $("#frm_pre_cadastro_email").val();
        var celular = $("#frm_pre_cadastro_celular").val();
        var cpf = $("#frm_pre_cadastro_cpf").val();
        var nascimento = $("#frm_pre_cadastro_nascimento").val();
        nascimento = moment(nascimento).format('YYYY-MM-DD');
        var cep = $("#frm_pre_cadastro_cep").val();
        var imovel = $("#frm_pre_cadastro_imovel").val();
        var renda = $("#frm_pre_cadastro_renda").val();
        var Amortizacao = retornaStatusTipoParcela();
        var prazo =  parseFloat($('#range_prazo').val());

        var param = {
            "ValorImovel": $("#valor_imovel").val(),
            "ValorFinanciamento": $("#valor_desejado").val(),
            "Idade": $("#range_idade").val(),
            "Prazo": prazo,
            "Amortizacao": Amortizacao,
            "Nome" : nome,
            "Email" : email,
            "Celular" : retornaNumeros(celular),
            "CPF" : retornaNumeros(cpf),
            "CNPJParceiro" : null,
            "CEP" : retornaNumeros(cep),
            "TipoImovel" : imovel,
            "DataNascimento" : nascimento,
            "RendaMensal" : renda,
            "AceiteTermo" : true,
            "ChecagemIdentidade" : true

        }

        var json_param = JSON.stringify(param);
        $.ajax({
            url: "https://api.lendme.com.br/Api/Proposta/PreCadastro",
            headers: {
                "Content-Type": "application/json"
            },
            type: "POST",
            data: json_param ,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (data) {

                var sucesso = data.retorno.Sucesso;
                var status = data.retorno.Status;

                $("#simulador_step_4").hide();

                if(status == 2 && sucesso == true){
                    $('.input-nome').html(nome);
                    $("#simulador_step_5_ligar_depois").show();
                }else if(status == 3 && sucesso == true){
                    $('.input-nome').html(nome);
                    $("#simulador_step_5_reprovado").show();
                }else if(status == 1 && sucesso == true) {
                    $('.input-nome').html(nome);
                    $("#simulador_step_5_aprovado").show();
                    $("#email_senha_cad").val(email);

                    var token_resp = data.retorno.Token;
                    var valor_parcela_resp =  data.retorno.DadosSimulacao.ValorParcela;
                    var valor_imovel_resp =  data.retorno.DadosSimulacao.ValorImovel;
                    var valor_financamento_resp =  data.retorno.DadosSimulacao.ValorFinanciamento;
                    var prazo_resp =  data.retorno.DadosSimulacao.Prazo;
                    var idade_resp =  data.retorno.DadosSimulacao.Idade;
                    var juros_mensal_resp =  data.retorno.DadosSimulacao.JurosMensal;
                    var amortizacao_resp =  data.retorno.DadosSimulacao.Amortizacao;
                    var ltv_resp =  data.retorno.DadosSimulacao.LVT;
                    var iof_resp =  data.retorno.DadosSimulacao.IOF;
                    var prestacaoTotal_resp =  data.retorno.DadosSimulacao.PrestacaoTotal;

                    $("#token_response").val(token_resp);
                    $("#token_resp").html(token_resp);
                    $("#valor_parcela_resp").html(numberToReal2(valor_parcela_resp));
                    $("#valor_imovel_resp").html(numberToReal2(valor_imovel_resp));
                    $("#valor_financamento_resp").html(numberToReal2(valor_financamento_resp));
                    $("#prazo_resp").html(prazo_resp);
                    $("#idade_resp").html(idade_resp);
                    $("#juros_mensal_resp").html(juros_mensal_resp);
                    $("#amortizacao_resp").html(amortizacao_resp);
                    $("#ltv_resp").html(ltv_resp.toFixed(2));
                    $("#iof_resp").html((numberToReal2(iof_resp)));
                    $("#prestacaoTotal_resp").html(numberToReal2(prestacaoTotal_resp));
                }else if(sucesso != true){

                    $("#simulador_step_4").hide();
                    $("#simulador_cadastro_error").show();
                    $('#btn_step_4').prop("disabled",false).html('Prosseguir <i class="fa fa-arrow-right"></i>');
                }

            },
            error: function(e) {
                console.log(JSON.stringify(e));
                var erros = e.responseText;
                var obj = JSON.parse(erros);
                console.log(JSON.stringify(obj.Erros));
                $("#simulador_step_4").hide();
                $("#simulador_cadastro_error").show();
                $('.text_error_cadastro').html(obj.Erros);
                $('#btn_step_4').prop("disabled",false).html('Prosseguir <i class="fa fa-arrow-right"></i>');

            }

        });

        $(".btn_simulador_prosseguir_1").click(function (e) {
            e.preventDefault();
            $("#simulador_step_5_aprovado").hide();
            $("#simulador_step_5_aprovado_dados").show();
        });

        $(".btn_simulador_prosseguir_2").click(function (e) {
            e.preventDefault();
            $("#simulador_step_5_aprovado_dados").hide();
            $("#simulador_step_5_senha").show();
        });

        $("#btn_simulador_prosseguir_3").click(function (e) {
            e.preventDefault();
            var email  = $("#email_senha_cad").val();
            var senha1 = $("#senha_1_cad").val();
            var senha2 = $("#senha_2_cad").val();
            var token  = $("#token_response").val();

            if(senha1 != senha2){
                swal("Atenção","As duas senhas não conferem.","warning");
                return false;
            }else if(senha1.length < 6 || senha2.length < 6 ){
                swal("Atenção","A senha precisa ter no mínimo 8 dígitos.","warning");
                return false;
            }else{
                var param = {
                    "Email": email,
                    "Token": token,
                    "Senha": senha1
                }
                CadastrarSenhaApi(param);
            }
        });

        //reset_simulador

        $(".reset_simulador").click(function (e) {
            e.preventDefault();
            $("#simulador_cadastro_error").hide();
            $("#simulador_step_1").show();
        });
    }


    function CadastrarSenhaApi(param){
        $("#btn_simulador_prosseguir_3").prop("disabled",true).addClass("btn-warning").html('<i class="fa fa-spinner fa-spin"></i> AGUARDE, PROCESSANDO...');
        var json_param = JSON.stringify(param);
        $.ajax({
            url: "https://api.lendme.com.br/Api/Proposta/AlterarSenha",
            headers: {
                "Content-Type": "application/json"
            },
            type: "POST",
            data: json_param ,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);

                if(data.retorno.Sucesso == true){
                    $("#btn_simulador_prosseguir_3").hide();
                    $(".link_biometria").attr("href",data.retorno.Link);
                    $("#simulador_step_5_senha").hide();
                    $("#simulador_step_5_senha_resp").show();

                    setTimeout(function(){
                        window.open(data.retorno.Link, '_blank');
                    }, 3000);
                }

            },
            error: function(e) {
                console.log(JSON.stringify(e));
                var erros = e.responseText;
                var obj = JSON.parse(erros);
                console.log(JSON.stringify(obj.Erros));
                // var text_erro = obj.Erros;
                // $(".valores_parcela").hide();
                // $(".erro_simulador").show();
                // $("#text_error_api").html(obj.Erros);
            }

        });
    }


    $("#frm_pre_cadastro_celular, #telefone2, #parceiro_telefone").keydown(function () {
        //Recebe o elemento ativo
        var focus = $(document.activeElement);
        //Timeout para pegar o valor do campo depois do evento, sem ele, o valor Ã© testado antes do evento ser finalizado
        setTimeout(function () {
            //Se o campo focado Ã© algum dos 3 campos de telefone, aplica a mÃ¡scara de acordo
            if (focus.attr('id') == "frm_pre_cadastro_celular" || focus.attr('id') == "telefone2"  || focus.attr('id') == "parceiro_telefone") {
                if (focus.val().length <= 14) {
                    focus.unmask();
                    focus.mask("(00) 0000-00009");
                }
                else {
                    focus.unmask();
                    focus.mask("(00) 00000-0000");
                }
            }
        }, 10);
    });

    function emailIsValid (email) {
        return /\S+@\S+\.\S+/.test(email)
    }

    function cpfIsValid(cpf){
        cpf = cpf.replace(/\D/g, '');
        if(cpf.toString().length != 11 || /^(\d)\1{10}$/.test(cpf)) return false;
        var result = true;
        [9,10].forEach(function(j){
            var soma = 0, r;
            cpf.split(/(?=)/).splice(0,j).forEach(function(e, i){
                soma += parseInt(e) * ((j+2)-(i+1));
            });
            r = soma % 11;
            r = (r <2)?0:11-r;
            if(r != cpf.substring(j, j+1)) result = false;
        });
        return result;
    }

    function retornaNumeros(str){
        return str.replace(/[^\d]+/g,'');
    }


    function numberToReal2(numero) {
        var numero = numero.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');
    }

    function numberToReal(numero) {
        var numero = numero.toFixed(2).split('.');
        numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');
    }
</script>
    <style>
        body{
            background-color: #36B6B0!important;
        }
        .range-output{
            font-size: 12px;
            padding-top: 10px;
            font-family: 'bw_gradual_demoregular' !Important;
        }
        .range-output2{
            font-size: 12px;
            padding-top: 10px;
            font-family: 'bw_gradual_demoregular' !Important;
        }
        .bootstrap-switch-container{
            height: 30px!important
        }
    </style>
<section id="simulador" style="background-color: #F7F9FB!important; padding: 0px!important; border: 5px solid #05CAB6; border-radius:20px!important">
    <div class="container p-b-0">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-12  background-grey p-b-10 p-t-20 " style="min-height: 300px;"  >
                        <div id="simulador_step_1">


                            <div class="row">
                               

                                <div class="col-lg-6 center text-center m-b-30">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo">QUAL O VALOR DO EMPRÉSTIMO?</h4>
                                    <input id="valor_desejado" type="text" name="valor_desejado" value="" />
                                    <small>Acima de 1.5 milhões, entre em contato.</small>
                                </div>
                                <div class="col-lg-6  center text-center m-b-30">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo">QUAL O VALOR DO SEU IMÓVEL?</h4>
                                    <input id="valor_imovel" name="valor_imovel" type="text" value="" />
                                </div>
                                <div class="col-lg-6 center text-center m-b-20">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo m-b-30">QUAL A SUA IDADE?</h4>
                                    <input id="range_idade" name="range_idade" type="range" min="18"  max="72"  step="1"  value="25"  data-rangeslider >
                                </div>
                                <div class="col-lg-6  center text-center">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo m-b-30">PRAZO PARA O PAGAMENTO DESEJADO?</h4>
                                    <input id="range_prazo" name="range_prazo" type="range" min="5"  max="15"  step="1"  value="60"  data-rangeslider >
                                </div>


                                <div class="col-lg-12 p-t-20 center text-center">
                                    <h4 style="font-size: 14px;" class="font-title-1 color-roxo text-uppercase">Escolha o sistema de amortização (Price ou SAC)</h4>
                                    <input id="forma_parcela" name="form_parcela" data-switch="true" data-size="small" data-on-color="secondary" checked type="checkbox" data-on-text="TABELA PRICE" data-off-text="TABELA SAC">
                                </div>
                                <div class="col-lg-10 center text-center">
                                    <div class="line m-t-10 m-b-10"></div>
                                    <div class="row valores_parcela">
                                        <div class="col-md-6 text-center">
                                            <h5 class="font-title-1 color-roxo text-uppercase">Valor da 1ª. parcela:</h5>
                                            <button id="r_v_p" type="button" class="btn btn-azul" style="min-width: 200px">R$ 1.500,00</button>
                                        </div>
                                        <div class="col-md-6 text-center">
                                            <h5 class="font-title-1 color-roxo text-uppercase">Custo Efetivo Total:</h5>
                                            <button id="r_cte" type="button" class="btn btn-azul" style="min-width: 200px">2%</button>
                                        </div>
                                    </div>
                                    <div class="row erro_simulador">
                                        <div class="col-lg-12">
                                            <div role="alert" class="alert alert-danger text-left alert-dismissible">
                                                <i class="fa fa-warning"></i> Atenção: <span id="text_error_api"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 m-t-20 m-b-20">
                                    <small>Esta simulação é apenas ilustrativa e não garante o valor final a ser pago pelo tomador do empréstimo nem a aprovação da concessão do crédito através da LendMe.</small>
                                </div>
                                <div class="col-12 text-center" >
                                    <a href="#" id="btn_simulador_direct" class="btn" style="font-size: 18px; line-height: 120%; width: 100%;">Veja aqui as melhores<br>condições para você</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>