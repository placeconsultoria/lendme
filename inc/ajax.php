<?php
    
    switch ($_REQUEST["acao"]){
        case "simulador":
            session_name('LendMe');
            session_start();
            $data = file_get_contents("php://input");
            $json = json_decode($data,true);
            $_SESSION["simulador"]["ValorImovel"] = $json["ValorImovel"];
            $_SESSION["simulador"]["ValorFinanciamento"] = $json["ValorFinanciamento"];
            $_SESSION["simulador"]["Idade"] = $json["Idade"];
            $_SESSION["simulador"]["Prazo"] = $json["Prazo"];
            $_SESSION["simulador"]["Amortizacao"] = $json["Amortizacao"];
            $json_retorno["status"] = true;
            $json_retorno["link"] = $URL_SITE.'simulador/cadastro/';
            echo json_encode($json_retorno);
           break;

        case "contato":

            include_once("funcoes_email.php");
            $dados["nome"] = $_REQUEST["nome"];
            $dados["email"] = $_REQUEST["email"];
            $dados["telefone"] = $_REQUEST["telefone"];
            $dados["cnpj"] = $_REQUEST["cnpj"];
            $dados["mensagem"] = $_REQUEST["mensagem"];
            $send_email_status = contato($dados);
            $json_retorno["status"] = true;
            $json_retorno["codigo"] = 0;
            $json_retorno["status_send_email"] = $send_email_status;
            echo json_encode($json_retorno);

            break;

        case "contato_operacoes":

            include_once("funcoes_email.php");
            $dados["nome"] = $_REQUEST["nome"];
            $dados["email"] = $_REQUEST["email"];
            $dados["telefone"] = $_REQUEST["telefone"];
            $dados["cnpj"] = $_REQUEST["cnpj"];
            $dados["mensagem"] = $_REQUEST["mensagem"];
            $send_email_status = contato_operacoes($dados);
            $json_retorno["status"] = true;
            $json_retorno["codigo"] = 0;
            $json_retorno["status_send_email"] = $send_email_status;
            echo json_encode($json_retorno);

            break;
            
            case "vaga":

            include_once("funcoes_email.php");
            $dados["nome"] = $_REQUEST["nome"];
            $dados["email"] = $_REQUEST["email"];
            $dados["telefone"] = $_REQUEST["telefone"];
            $dados["oportunidade"] = $_REQUEST["oportunidade"];
            $dados["linkedin"] = $_REQUEST["linkedin"];
            $dados["file"] = $_FILES["file"];
            $send_email_status = vaga($dados);
            $json_retorno["status"] = true;
            $json_retorno["codigo"] = 0;
            $json_retorno["status_send_email"] = $send_email_status;
            echo json_encode($json_retorno);

            break;
            
            case "solicitacao_aquisicao":

            include_once("funcoes_email.php");
            $dados["nome"] = $_REQUEST["nome"];
            $dados["nascimento"] = $_REQUEST["nascimento"];
            $dados["cpf"] = $_REQUEST["cpf"];
            $dados["imovel"] = $_REQUEST["imovel"];
            $dados["financiamento"] = $_REQUEST["financiamento"];
            $dados["renda"] = $_REQUEST["renda"];
            $dados["telefone"] = $_REQUEST["telefone"];
            $dados["email"] = $_REQUEST["email"];
            $dados["conheceu"] = $_REQUEST["conheceu"];
            $send_email_status = aquisicao($dados);
            $json_retorno["status"] = true;
            $json_retorno["codigo"] = 0;
            $json_retorno["status_send_email"] = $send_email_status;
            echo json_encode($json_retorno);

            break;

    }
?>