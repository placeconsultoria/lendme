<?php

function get_topo_email(){

$topo_email = '					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<title>PRODUTOS CONTROLADOS</title>
					</head>
					
					<style>
					
					body{
						padding:0;
						margin:0;
						background:#f8fafa;
						padding-top:20px;
						padding-bottom:40px;
						width: 100%;
						text-align: center;
					}
					
					p{
						font-family:Verdana, Geneva, sans-serif;
						font-size:12px;
						color:#666;
						letter-spacing:0.05em;
						line-height:120%;
					}
					
					 <style> .table, th, td {  border: 1px solid black; } </style>
                     
					
					</style>
					<body style="background:#f8fafa; padding-top:20px; padding-bottom:40px; width: 100%; text-align: center;">
					
					<table width="800px" border="0" cellpadding="0" cellspacing="0" align="center" style="margin:auto;">
					
					 <tr style="background:#FFF;">
						<td style="text-align:center; padding-bottom:20px; padding-top:20px;">';
	return $topo_email;
}

function get_topo_emailBKP(){

    $topo_email = '					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<title></title>
					</head>
					
					<style>
					
					body{
						padding:0;
						margin:0;
						background:#f8fafa;
						padding-top:20px;
						padding-bottom:40px;
						width: 100%;
						text-align: center;
					}
					
					p{
						font-family:Verdana, Geneva, sans-serif;
						font-size:12px;
						color:#666;
						letter-spacing:0.05em;
						line-height:120%;
					}
					                     
					
					</style>
					<body style="background:#f8fafa; padding-top:20px; padding-bottom:40px; width: 100%; text-align: center;">
					
					<table width="800px" border="0" cellpadding="0" cellspacing="0" align="center" style="margin:auto;">
					  <tr>
						<td><p style="text-align:center; padding-bottom:20px; font-size:14px; color:#666; display:block; width:100%; letter-spacing:0;" ><strong>Aviso:</strong> por favor, <strong>habilite</strong> ou <strong>aprove</strong> para exibir as imagens deste email para melhor visualização do mesmo.</p></td>
					  </tr>
					  <tr style="background:#FFF;">
						<td width="800px" height="auto" style="text-align:center; padding: 40px;">
						    <img src="https://www.smartcityexpocuritiba.com/images/logo.png" />
						</td>
					  </tr>
					  <tr style="background:#FFF;">
						<td style="text-align:center; padding-bottom:20px; padding-top:20px;">';
    return $topo_email;
}
						
	
function get_footer_email(){						

$footer_email = '		 </td>
					  </tr>
					 
					</table>
					</body>
					</html>';

return $footer_email;					
					
}

function get_footer_emailBKP(){

    $footer_email = '		 </td>
					  </tr>
					 
					</table>
					</body>
					</html>';

    return $footer_email;

}



//

function contato_viagem($dados){
    require_once("PHPMailer/class.phpmailer.php");
    // Inicia a classe PHPMailer
    $mail = new PHPMailer(true);

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP

    try {
        $mail->Host = 'smtp.qeasy.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
        $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
        $mail->Port       = 587; //  Usar 587 porta SMTP
        $mail->Username = 'no-replay@qeasy.com.br'; // Usuário do servidor SMTP (endereço de email)
        $mail->Password = 'sempreebommudar'; // Senha do servidor SMTP (senha do email usado)
        $mail->SetFrom('no-replay@qeasy.com.br', 'Qeasy');
        $mail->AddReplyTo($dados["email"], $dados["nome"]);
        $mail->Subject = utf8_decode('[SMART CITY CURITIBA] - PLANEJE SUA VIAGEM');
        $mail->AddAddress("smartcityexpocuritiba@klas.com.br","klas");
        $mail->AddCC('contato@smartcityexpocuritiba.com', 'smartcityexpocuritiba');

        $corpo_email = '

                           
		 	              
		 	               <p style="display:block; width:80%; padding-top: 30px; height:auto;  margin: auto;  font-weight:normal; font-size:25px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
								TIVEMOS UM NOVO CONTATO PELO SITE: <br /> PLANEJE SUA VIAGEM
						  </p>
                            
						 
						   
						  <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							  DADOS DO USUÁRIO:
						  </p>
							
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     <strong>NOME:</strong> '.$dados["nome"].' <br/>
						     <strong>E-MAIL:</strong> '.$dados["email"].' <br/>
						     <strong>TELEFONE: </strong>'.$dados["telefone"].' <br/>
						   
						  </p>
                          
						  <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							   DADOS DA VIAGEM:
							</p>
						   
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     <strong>CIDADE ORIGE,:</strong> '.$dados["cidade_origem"].'<br/>
						     <strong>PRECISA DE HOTEL?</strong> '.$dados["hotel"].' <br />
						     <strong>PRECISA DE AREO?</strong> '.$dados["aero"].' <br />
						     
						 </p>
						 
						<br /><br /><br />
						  
                         
							
		 ';

        $topo_email = get_topo_emailBKP();
        $footer_email = get_footer_emailBKP();
        $email = utf8_decode($topo_email.$corpo_email.$footer_email);
        $mail->IsHTML(true); //enviar em HTML
        //Define o corpo do email
        $mail->MsgHTML($email);
        $mail->Send();

        //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
        $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
    }

    return $erro;
}


function contato_expositor($dados){
    require_once("PHPMailer/class.phpmailer.php");
    // Inicia a classe PHPMailer
    $mail = new PHPMailer(true);

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP

    try {
        $mail->Host = 'smtp.qeasy.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
        $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
        $mail->Port       = 587; //  Usar 587 porta SMTP
        $mail->Username = 'no-replay@qeasy.com.br'; // Usuário do servidor SMTP (endereço de email)
        $mail->Password = 'sempreebommudar'; // Senha do servidor SMTP (senha do email usado)
        $mail->SetFrom('no-replay@qeasy.com.br', 'Qeasy');
        $mail->AddReplyTo($dados["email"], $dados["nome"]);
        $mail->Subject = utf8_decode('[SMART CITY CURITIBA] - QUERO SER UM EXPOSITOR');
        $mail->AddAddress('contato@smartcityexpocuritiba.com',"smartcityexpocuritiba");
        //$mail->AddCC('contato@smartcityexpocuritiba.com', 'smartcityexpocuritiba');

        $corpo_email = '

                           
		 	              
		 	              <p style="display:block; width:80%; padding-top: 30px; height:auto;  margin: auto;  font-weight:normal; font-size:25px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
								TIVEMOS UM NOVO CONTATO PELO SITE: <br /> QUER SER UM EXPOSITOR?
						  </p>
                          
                          <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							  DADOS DO USUÁRIO:
						  </p>
							
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     <strong>NOME:</strong> '.$dados["nome"].' <br/>
						     <strong>E-MAIL:</strong> '.$dados["email"].' <br/>
						     <strong>TELEFONE: </strong>'.$dados["telefone"].' <br/>
						     <strong>EMPRESA: </strong>'.$dados["empresa"].' <br/>
						   
						  </p>
                          
						  <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							 MENSAGEM:
							</p>
						   
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     '.$dados["mensagem"].'
						     
						 </p>
						 
						<br /><br /><br />
						  
                         
							
		 ';

        $topo_email = get_topo_emailBKP();
        $footer_email = get_footer_emailBKP();
        $email = utf8_decode($topo_email.$corpo_email.$footer_email);
        $mail->IsHTML(true); //enviar em HTML
        //Define o corpo do email
        $mail->MsgHTML($email);
        $mail->Send();

        //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
        $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
    }

    return $erro;
}

function contato_duvida($dados){
    require_once("PHPMailer/class.phpmailer.php");
    // Inicia a classe PHPMailer
    $mail = new PHPMailer(true);

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP

    try {
        $mail->Host = 'smtp.qeasy.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
        $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
        $mail->Port       = 587; //  Usar 587 porta SMTP
        $mail->Username = 'no-replay@qeasy.com.br'; // Usuário do servidor SMTP (endereço de email)
        $mail->Password = 'sempreebommudar'; // Senha do servidor SMTP (senha do email usado)
        $mail->SetFrom('no-replay@qeasy.com.br', 'Qeasy');
        $mail->AddReplyTo($dados["email"], $dados["nome"]);
        $mail->Subject = utf8_decode('[SMART CITY CURITIBA] - DÚVIDA');
        $mail->AddAddress('contato@smartcityexpocuritiba.com',"smartcityexpocuritiba");
        //$mail->AddAddress('leonardo@qeasy.com.br',"smartcityexpocuritiba");
        //$mail->AddCC('contato@smartcityexpocuritiba.com', 'smartcityexpocuritiba');

        $corpo_email = '

                           
		 	              
		 	              <p style="display:block; width:80%; padding-top: 30px; height:auto;  margin: auto;  font-weight:normal; font-size:25px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
								TIVEMOS UM NOVO CONTATO PELO SITE: <br /> FIQUEI COM DÚVIDA!
						  </p>
                          
                          <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							  DADOS DO USUÁRIO:
						  </p>
							
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     <strong>NOME:</strong> '.$dados["nome"].' <br/>
						     <strong>E-MAIL:</strong> '.$dados["email"].' <br/>
						     <strong>TELEFONE: </strong>'.$dados["telefone"].' <br/>
						     <strong>EMPRESA: </strong>'.$dados["empresa"].' <br/>
						   
						  </p>
                          
						  <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							 MENSAGEM:
							</p>
						   
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     '.$dados["mensagem"].'
						     
						 </p>
						 
						<br /><br /><br />
						  
                         
							
		 ';

        $topo_email = get_topo_emailBKP();
        $footer_email = get_footer_emailBKP();
        $email = utf8_decode($topo_email.$corpo_email.$footer_email);
        $mail->IsHTML(true); //enviar em HTML
        //Define o corpo do email
        $mail->MsgHTML($email);
        $mail->Send();

        //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
        $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
    }

    return $erro;
}


function seja_patrocinador($dados){
    require_once("PHPMailer/class.phpmailer.php");
    // Inicia a classe PHPMailer
    $mail = new PHPMailer(true);

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP

    try {
        $mail->Host = 'smtp.qeasy.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
        $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
        $mail->Port       = 587; //  Usar 587 porta SMTP
        $mail->Username = 'no-replay@qeasy.com.br'; // Usuário do servidor SMTP (endereço de email)
        $mail->Password = 'sempreebommudar'; // Senha do servidor SMTP (senha do email usado)
        $mail->SetFrom('no-replay@qeasy.com.br', 'Qeasy');
        $mail->AddReplyTo($dados["email"], $dados["nome"]);
        $mail->Subject = utf8_decode('[SMART CITY CURITIBA] - QUERO SER UM PATROCINADOR');
        $mail->AddAddress('contato@smartcityexpocuritiba.com',"smartcityexpocuritiba");
        //$mail->AddAddress('leonardo@qeasy.com.br',"smartcityexpocuritiba");
        $mail->AddCC('comercial@smartcityexpocuritiba.com', 'comercial');

        $corpo_email = '

                           
		 	              
		 	              <p style="display:block; width:80%; padding-top: 30px; height:auto;  margin: auto;  font-weight:normal; font-size:25px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
								TIVEMOS UM NOVO CONTATO PELO SITE: <br /> QUERO SER UM PATROCINADOR!
						  </p>
                          
                          <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							  DADOS DO USUÁRIO:
						  </p>
							
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     <strong>NOME:</strong> '.$dados["nome"].' <br/>
						     <strong>E-MAIL:</strong> '.$dados["email"].' <br/>
						     <strong>TELEFONE: </strong>'.$dados["telefone"].' <br/>
						     <strong>EMPRESA: </strong>'.$dados["empresa"].' <br/>
						   
						  </p>
                          
						  <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							 MENSAGEM:
							</p>
						   
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     '.$dados["mensagem"].'
						     
						 </p>
						 
						<br /><br /><br />
						  
                         
							
		 ';

        $topo_email = get_topo_emailBKP();
        $footer_email = get_footer_emailBKP();
        $email = utf8_decode($topo_email.$corpo_email.$footer_email);
        $mail->IsHTML(true); //enviar em HTML
        //Define o corpo do email
        $mail->MsgHTML($email);
        $mail->Send();

        //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
        $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
    }

    return $erro;
}



function assessoria($dados){
    require_once("PHPMailer/class.phpmailer.php");
    // Inicia a classe PHPMailer
    $mail = new PHPMailer(true);

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP

    try {
        $mail->Host = 'smtp.qeasy.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
        $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
        $mail->Port       = 587; //  Usar 587 porta SMTP
        $mail->Username = 'no-replay@qeasy.com.br'; // Usuário do servidor SMTP (endereço de email)
        $mail->Password = 'sempreebommudar'; // Senha do servidor SMTP (senha do email usado)
        $mail->SetFrom('no-replay@qeasy.com.br', 'Qeasy');
        $mail->AddReplyTo($dados["email"], $dados["nome"]);
        $mail->Subject = utf8_decode('[SMART CITY CURITIBA] - ASSESSORIA DE IMPRENSA');
        $mail->AddAddress('andre@talkcomunicacao.com.br',"andre");
        //$mail->AddAddress('leonardo@qeasy.com.br',"smartcityexpocuritiba");
        $mail->AddCC('comercial@smartcityexpocuritiba.com', 'comercial');

        $corpo_email = '

                           
		 	              
		 	              <p style="display:block; width:80%; padding-top: 30px; height:auto;  margin: auto;  font-weight:normal; font-size:25px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
								TIVEMOS UM NOVO CONTATO PELO SITE: <br /> ASSESSORIA DE IMPRENSA.
						  </p>
                          
                          <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							  DADOS DO USUÁRIO:
						  </p>
							
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     <strong>NOME:</strong> '.$dados["nome"].' <br/>
						     <strong>E-MAIL:</strong> '.$dados["email"].' <br/>
						     <strong>TELEFONE: </strong>'.$dados["telefone"].' <br/>
						     <strong>EMPRESA: </strong>'.$dados["empresa"].' <br/>
						   
						  </p>
                          
						  <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							 MENSAGEM:
							</p>
						   
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     '.$dados["mensagem"].'
						     
						 </p>
						 
						<br /><br /><br />
						  
                         
							
		 ';

        $topo_email = get_topo_emailBKP();
        $footer_email = get_footer_emailBKP();
        $email = utf8_decode($topo_email.$corpo_email.$footer_email);
        $mail->IsHTML(true); //enviar em HTML
        //Define o corpo do email
        $mail->MsgHTML($email);
        $mail->Send();

        //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
        $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
    }

    return $erro;
}


function contato($dados){
    require_once("PHPMailer/class.phpmailer.php");
    // Inicia a classe PHPMailer
    $mail = new PHPMailer(true);

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP

    try {
        $mail->Host = 'smtp.qeasy.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
        $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
        $mail->Port       = 587; //  Usar 587 porta SMTP
        $mail->Username = 'no-replay@qeasy.com.br'; // Usuário do servidor SMTP (endereço de email)
        $mail->Password = 'sempreebommudar'; // Senha do servidor SMTP (senha do email usado)
        $mail->SetFrom('no-replay@qeasy.com.br', 'Qeasy');
        $mail->AddReplyTo($dados["email"], $dados["nome"]);
        $mail->Subject = utf8_decode('[SMART CITY CURITIBA] - CONTATO');
        $mail->AddAddress('contato@smartcityexpocuritiba.com',"contato");
        //$mail->AddAddress('leonardo@qeasy.com.br',"smartcityexpocuritiba");
        //$mail->AddCC('contato@smartcityexpocuritiba.com', 'contato');

        $corpo_email = '

                           
		 	              
		 	              <p style="display:block; width:80%; padding-top: 30px; height:auto;  margin: auto;  font-weight:normal; font-size:25px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
								TIVEMOS UM NOVO CONTATO PELO SITE: <br /> CONTATO
						  </p>
                          
                          <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							  DADOS DO USUÁRIO:
						  </p>
							
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     <strong>NOME:</strong> '.$dados["nome"].' <br/>
						     <strong>E-MAIL:</strong> '.$dados["email"].' <br/>
						     <strong>TELEFONE: </strong>'.$dados["telefone"].' <br/>
						     <strong>EMPRESA: </strong>'.$dados["empresa"].' <br/>
						   
						  </p>
                          
						  <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							 MENSAGEM:
							</p>
						   
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     '.$dados["mensagem"].'
						     
						 </p>
						 
						<br /><br /><br />
						  
                         
							
		 ';

        $topo_email = get_topo_emailBKP();
        $footer_email = get_footer_emailBKP();
        $email = utf8_decode($topo_email.$corpo_email.$footer_email);
        $mail->IsHTML(true); //enviar em HTML
        //Define o corpo do email
        $mail->MsgHTML($email);
        $mail->Send();

        //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
        $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
    }

    return $erro;
}


function voluntario($dados){
    require_once("PHPMailer/class.phpmailer.php");
    // Inicia a classe PHPMailer
    $mail = new PHPMailer(true);

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP

    try {
        $mail->Host = 'smtp.qeasy.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
        $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
        $mail->Port       = 587; //  Usar 587 porta SMTP
        $mail->Username = 'no-replay@qeasy.com.br'; // Usuário do servidor SMTP (endereço de email)
        $mail->Password = 'sempreebommudar'; // Senha do servidor SMTP (senha do email usado)
        $mail->SetFrom('no-replay@qeasy.com.br', 'Qeasy');
        $mail->AddReplyTo($dados["email"], $dados["nome"]);
        $mail->Subject = utf8_decode('[SMART CITY CURITIBA] - SEJA UM VOLUNTARIO');
        $mail->AddAddress('contato@smartcityexpocuritiba.com',"contato");
        //$mail->AddAddress('leonardo@qeasy.com.br',"smartcityexpocuritiba");
       // $mail->AddCC('contato@smartcityexpocuritiba.com', 'contato');

        $corpo_email = '

                           
		 	              
		 	              <p style="display:block; width:80%; padding-top: 30px; height:auto;  margin: auto;  font-weight:normal; font-size:25px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
								TIVEMOS UM NOVO CONTATO PELO SITE: <br /> SEJA UM VOLUNTARIO
						  </p>
                          
                          <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							  DADOS DO USUÁRIO:
						  </p>
							
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     <strong>NOME:</strong> '.$dados["nome"].' <br/>
						     <strong>E-MAIL:</strong> '.$dados["email"].' <br/>
						     <strong>TELEFONE: </strong>'.$dados["telefone"].' <br/>
						     <strong>CPF: </strong>'.$dados["cpf"].' <br/>
						   
						  </p>
                          
						  <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							 MENSAGEM:
							</p>
						   
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     '.$dados["mensagem"].'
						     
						 </p>
						 
						<br /><br /><br />
						  
                         
							
		 ';

        $topo_email = get_topo_emailBKP();
        $footer_email = get_footer_emailBKP();
        $email = utf8_decode($topo_email.$corpo_email.$footer_email);
        $mail->IsHTML(true); //enviar em HTML
        //Define o corpo do email
        $mail->MsgHTML($email);
        $mail->Send();

        //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
        $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
    }

    return $erro;
}


function paletrante($dados){
    require_once("PHPMailer/class.phpmailer.php");
    // Inicia a classe PHPMailer
    $mail = new PHPMailer(true);

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP

    try {
        $mail->Host = 'smtp.qeasy.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
        $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
        $mail->Port       = 587; //  Usar 587 porta SMTP
        $mail->Username = 'no-replay@qeasy.com.br'; // Usuário do servidor SMTP (endereço de email)
        $mail->Password = 'sempreebommudar'; // Senha do servidor SMTP (senha do email usado)
        $mail->SetFrom('no-replay@qeasy.com.br', 'Qeasy');
        $mail->AddReplyTo($dados["email"], $dados["nome"]);
        $mail->Subject = utf8_decode('[SMART CITY CURITIBA] - SEJA UM PALETRANTE');
        $mail->AddAddress('contato@smartcityexpocuritiba.com',"contato");
        //$mail->AddAddress('leonardo@qeasy.com.br',"smartcityexpocuritiba");
        //$mail->AddCC('contato@smartcityexpocuritiba.com', 'contato');

        $corpo_email = '

                           
		 	              
		 	              <p style="display:block; width:80%; padding-top: 30px; height:auto;  margin: auto;  font-weight:normal; font-size:25px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
								TIVEMOS UM NOVO CONTATO PELO SITE: <br /> SEJA UM PALETRANTE
						  </p>
                          
                          <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							  DADOS DO USUÁRIO:
						  </p>
							
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     <strong>NOME:</strong> '.$dados["nome"].' <br/>
						     <strong>E-MAIL:</strong> '.$dados["email"].' <br/>
						     <strong>TELEFONE: </strong>'.$dados["telefone"].' <br/>
						     <strong>EMPRESA: </strong>'.$dados["empresa"].' <br/>
						   
						  </p>
                          
						  <p style="display:block; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:20px; font-family:Arial, Helvetica, sans-serif; color: #009d52;  text-align: left" class="h1">
							 MENSAGEM:
							</p>
						   
						  <p style="display:block; line-height: 145%; padding-top: 20px; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:16px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						     '.$dados["mensagem"].'
						     
						 </p>
						 
						<br /><br /><br />
						  
                         
							
		 ';

        $topo_email = get_topo_emailBKP();
        $footer_email = get_footer_emailBKP();
        $email = utf8_decode($topo_email.$corpo_email.$footer_email);
        $mail->IsHTML(true); //enviar em HTML
        //Define o corpo do email
        $mail->MsgHTML($email);
        $mail->Send();

        //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
        $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
    }

    return $erro;
}
	
	
?>