<nav>
    <ul>
    
        <li class="dropdown"> <a href="#">PRODUTOS</a>
            <ul class="dropdown-menu">
                <li><a href="<?= $URL_SITE ?>home-equity/">HOME EQUITY</a></li>
                <li> <a href="<?= $URL_SITE ?>aquisicao/">AQUISIÇÃO</a> </li>
                <li><a href="<?= $URL_SITE ?>operacoes-estruturadas/">OPERAÇÕES ESTRUTURADAS</a></li>
            </ul>
        </li>        
        <li><a href="<?= $URL_SITE ?>institucional/quem-somos/">QUEM SOMOS</a></li>
        <li><a href="<?= $URL_SITE ?>agentes/seja-nosso-agente/">SEJA AGENTE</a></li>
        <li><a href="<?= $URL_SITE ?>contato/fale-conosco/">CONTATO</a></li>
        <li><a href="#" name="btn_login">ENTRAR</a></li>
    </ul>
</nav>