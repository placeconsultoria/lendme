<?php 
header("Access-Control-Allow-Origin: *");
?>
<!doctype html>
<html lang="pt-BR">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Qeasy" />
    <meta name="description" content="LendMe é a única fintech especializada em home equity que faz registros em Blockchain, trazendo muito mais segurança e tecnologia para seus empréstimos.">
    <title>LendMe - Plataforma de Home Equity segura baseada em Blockchain</title>
    <link href="<?=$URL_SITE?>css/plugins.css" rel="stylesheet">
    <link href="<?=$URL_SITE?>webfonts/bwgradual/fontes1/stylesheet.css" rel="stylesheet">
    <link href="<?=$URL_SITE?>webfonts/bwgradual/fontes2/stylesheet.css" rel="stylesheet">
    <link href="<?=$URL_SITE?>css/style.css" rel="stylesheet">
    <link href="<?=$URL_SITE?>css/responsive.css" rel="stylesheet">
    <link href="<?=$URL_SITE?>css/adjusts.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=$URL_SITE?>js/rangerslider/rangeslider.css">
    <link rel="stylesheet" href="<?=$URL_SITE?>js/ionrangeslider/css/ion.rangeSlider.css"/>
    <link rel="stylesheet" href="<?=$URL_SITE?>inc/sweetalert/sweetalert.css"/>
    <script type="text/javascript" src="<?=$URL_SITE?>js/jquery1.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Rubik:500%2C400%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="<?=$URL_SITE?>js/plugins/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" type="text/css" href="<?=$URL_SITE?>js/plugins/revolution/fonts/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="<?=$URL_SITE?>js/plugins/revolution/css/settings.css">
    <style type="text/css">.tiny_bullet_slider .tp-bullet:before{content:" ";  position:absolute;  width:100%;  height:25px;  top:-12px;  left:0px;  background:transparent}</style>
    <style type="text/css">.bullet-bar.tp-bullets{}.bullet-bar.tp-bullets:before{content:" ";position:absolute;width:100%;height:100%;background:transparent;padding:10px;margin-left:-10px;margin-top:-10px;box-sizing:content-box}.bullet-bar .tp-bullet{width:60px;height:3px;position:absolute;background:#aaa;  background:rgba(204,204,204,0.5);cursor:pointer;box-sizing:content-box}.bullet-bar .tp-bullet:hover,.bullet-bar .tp-bullet.selected{background:rgba(204,204,204,1)}.bullet-bar .tp-bullet-image{}.bullet-bar .tp-bullet-title{}</style>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type='text/javascript' src='<?=$URL_SITE?>js/plugins/revolution/revolution-addons/slicey/js/revolution.addon.slicey.min.js?ver=1.0.0'></script>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <link rel="shortcut icon" href="<?=$URL_SITE?>images/favicon.ico" />
    <style>
        .range-output{
            font-size: 12px;
            padding-top: 10px;
            font-family: 'bw_gradual_demoregular' !Important;
        }
        .range-output2{
            font-size: 12px;
            padding-top: 10px;
            font-family: 'bw_gradual_demoregular' !Important;
        }
    </style>
    <!-- Global site tag (gtag.js) - Google Analytics 
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169699447-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-169699447-1');
    </script>-->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5DK96MK');</script>
    <!-- End Google Tag Manager -->
    <script type="text/javascript">
(function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobal
ObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(
arguments)};e[e.visitorGlobalObjectAlias].l=(new
Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("sc
ript")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/dif
fuser/diffuser.js","vgo");
vgo('setAccount', '252748609');
vgo('setTrackByDefault', true);
vgo('process');
</script>
<script src="https://www.googleoptimize.com/optimize.js?id=OPT-TP8KDT5"></script>
<script type="text/javascript">
    (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
    vgo('setAccount', '252748609');
    vgo('setTrackByDefault', true);

    vgo('process');
</script>
<!-- COOKIE LGPD -->
<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="6769d7fc-1fad-40cf-97b0-d883e294b7e1" data-blockingmode="auto" type="text/javascript"></script>
<!-- RD STATION -->
<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/a38d36c7-5819-4445-8011-1f4e30e90be3-loader.js" ></script>
</head>




