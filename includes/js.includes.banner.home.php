<link rel="stylesheet" type="text/css" href="<?=$URL_SITE?>js/plugins/revolution/css/settings.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?=$URL_SITE?>js/plugins/revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="<?=$URL_SITE?>js/plugins/revolution/css/navigation.css">
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?=$URL_SITE?>js/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript">


    var tpj=jQuery;
    tpj.noConflict();
    var revapi6;
    tpj(document).ready(function() {

        tpj('a[name="btn_login_open"]').click(function (e){
            e.preventDefault();
            tpj("#modal-login").modal("show");
            // $("#modal-login").modal("show");
        });

        tpj('a[name="btn_recuperar_senha"]').click(function (e) {
            e.preventDefault();
            tpj("#modal-login").modal("hide");
            tpj("#modal-recuperar-senha").modal("show");
        });

        tpj('a[name="btn-login-recuperar-senha"]').click(function (e) {
            e.preventDefault();
            tpj("#modal-recuperar-senha").modal("hide");
            tpj("#modal-login").modal("show");
        });

        //btn-login-recuperar-senha


        if(tpj("#rev_slider_6_1").revolution == undefined){
            revslider_showDoubleJqueryError("#rev_slider_6_1");
        }else{
            revapi6 = tpj("#rev_slider_6_1").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/plugins/revolution/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                    keyboardNavigation:"off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation:"off",
                    onHoverStop:"on",
                    arrows: {
                        style:"hades",
                        enable:true,
                        hide_onmobile:true,
                        hide_under:0,
                        hide_onleave:true,
                        hide_delay:200,
                        hide_delay_mobile:1200,
                        tmp:'<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div></div>',
                        left: {
                            h_align:"left",
                            v_align:"center",
                            h_offset:40,
                            v_offset:0
                        },
                        right: {
                            h_align:"right",
                            v_align:"center",
                            h_offset:40,
                            v_offset:0
                        }
                    }
                },
                responsiveLevels:[1240,1024,778,480],
                visibilityLevels:[1240,1024,778,480],
                gridwidth:[1170,1024,778,480],
                gridheight:[700,768,960,720],
                lazyType:"none",
                shadow:0,
                spinner:"spinner2",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                disableProgressBar:"on",
                hideThumbsOnMobile:"on",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:481,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                    simplifyAll:"off",
                    nextSlideOnWindowFocus:"off",
                    disableFocusListener:false,
                }
            });
        }
    });	/*ready*/
</script>