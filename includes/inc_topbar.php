<div id="topbar" class="topbar-transparent dark d-none d-xl-block d-lg-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="top-menu">
                    <li><a href="#"><i class="fa fa-phone fa-rotate-90"></i> 0800 759 1000</a></li>
                    <li><a href="mailto:meajuda@lendme.com.br"><i class="fa fa-envelope"></i> meajuda@lendme.com.br</a></li>
                </ul>
            </div>
            <div class="col-md-6 d-none d-sm-block">
                <div class="social-icons social-icons-colored-hover">
                    <ul>
                        <li class="social-linkedin"><a href="https://linkedin.com/company/lendmeoficial" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                        <li class="social-instagram"><a href="https://www.instagram.com/lendmeoficial/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li class="social-facebook"><a href="https://www.facebook.com/lendmeoficial" target="_blank"><i class="fab fa-facebook"></i></a></li>
                        <li class="social-youtube"><a href="https://www.youtube.com/channel/UChuDsNikFNMRj9HekCVcy2w" target="_blank"><i class="fab fa-youtube"></i></a></li>
                        <li class="social-twitter"><a href="https://twitter.com/lendmeoficial" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li class="social-pinterest"><a href="https://br.pinterest.com/lendmeoficial/" target="_blank"><i class="fab fa-pinterest"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
