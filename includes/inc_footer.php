
<div class="modal fade" id="modal_login" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title font-paragrafo-1 text-uppercase color-roxo" id="modal-label">Entre na Plataforma:</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="https://plataforma.lendme.com.br/Login/LoginExterno" method="post" target="_self" name="login">
                            <div class="form-group">
                                <label>EMAIL:</label>
                                <input type="text" id="frm_login_email" name="Login" class="form-control" placeholder="Insira o seu email...">
                            </div>
                            <div class="form-group m-b-5">
                                <label >SENHA:</label>
                                <input  id="frm_login_senha" name="Password" type="password" class="form-control" placeholder="Insira a sua senha...">
                            </div>
                            <div class="form-group form-inline text-left">
                                <div class="form-check">
                                    <label>
                                        <input type="checkbox" name="RememberMe"><small class="m-l-10"> Lembrar de mim</small>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 box_error_login" style="display: none;">
                                    <div role="alert" class="alert alert-danger alert-dismissible">
                                        Erro: Usuário e/ou Senha inválidos, tente novamente.
                                    </div>
                                </div>
                            </div>
                            <p class="small">Esqueceu a senha? <a href="https://plataforma.lendme.com.br/Login/RecoverPassword" target="_blank">Recuperar senha</a></p>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btn_fazer_login" type="button" class="btn btn-outline btn-light btn-azul ">FAZER LOGIN</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="login.reset();">FECHAR</button>
                
            </div>
        </div>
    </div>
</div>

<footer id="footer">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="widget">
                        <div class="widget-title">
                            <img src="<?= $URL_SITE?>images/logo.png" class="img-fluid" />
                            
                            
                            
                            
                        </div>
                        <div class="social-icons social-icons-border float-left m-t-20">
                <ul>
                  <li class="social-linkedin"><a href="https://linkedin.com/company/lendmeoficial" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                        <li class="social-instagram"><a href="https://www.instagram.com/lendmeoficial/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li class="social-facebook"><a href="https://www.facebook.com/lendmeoficial" target="_blank"><i class="fab fa-facebook"></i></a></li>
                        <li class="social-youtube"><a href="https://www.youtube.com/channel/UChuDsNikFNMRj9HekCVcy2w" target="_blank"><i class="fab fa-youtube"></i></a></li>
                        <li class="social-twitter"><a href="https://twitter.com/lendmeoficial" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li class="social-pinterest"><a href="https://br.pinterest.com/lendmeoficial/" target="_blank"><i class="fab fa-pinterest"></i></a></li>
                </ul>
              </div>
                        
                        
                    </div>

                </div>
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="widget">
                                <ul class="list">
                                    <li><a href="<?= $URL_SITE ?>home-equity/">HOME EQUITY</a></li>
                                    <li><a style="font-size: 12px;" href="<?= $URL_SITE ?>operacoes-estruturadas/">OPERAÇÕES ESTRUTURADAS</a></li>
                                    <li><a style="font-size: 12px;" href="<?= $URL_SITE ?>aquisicao/">AQUISIÇÃO</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="widget">
                                <ul class="list">
                                    <li><a href="<?= $URL_SITE ?>institucional/quem-somos/">QUEM SOMOS</a></li>
                                    <li><a href="<?= $URL_SITE ?>agentes/seja-nosso-agente/">SEJA NOSSO AGENTE</a></li>
                                    <li><a href="<?= $URL_SITE ?>contato/trabalhe-conosco/">TRABALHE CONOSCO</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="widget">
                                <ul class="list">
                                    <li><a href="<?=$URL_SITE?>termos-de-uso/">TERMOS DE USO</a></li>
                                    <li><a href="<?= $URL_SITE ?>politica-de-privacidade/">POLÍTICA DE PRIVACIDADE</a></li></ul>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="widget">
                                <ul class="list">
                                    <li><a href="<?= $URL_SITE ?>blog/materias/">BLOG</a></li>
                                    <li><a href="<?= $URL_SITE ?>contato/fale-conosco/">CONTATO</a></li>
                                    <li><a href="#" name="btn_login">LOGIN</a></li>
                                    <li><a href="<?= $URL_SITE ?>ajuda/perguntas-frequentes/">AJUDA</a></li>
                                </ul>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <div class="col-lg-2">
                    
                            <div class="icon-box effect small center" style="margin-top: 0!important">
          <img src="<?= $URL_SITE?>images/seloNivelI.png" class="img-fluid"/>
          
          <p align="center" style="font-size: 8px">Somos membro da Associação Brasileira de Fintechs e fazemos parte do Programa Fintech Segura, pelo qual 
asseguramos todas as boas práticas recomendadas para segurança e privacidade de dados.
</p>
        </div>
                            
                            
                        

                </div>
                
                
                
            </div>
        </div>
    </div>
    <div class="copyright-content">
        <div class="container">
            <div class="row p-b-50 p-t-50">
                <div class="col-md-12">
                    <p class=" font-paragrafo-1">A LendMe não é instituição financeira nem concede empréstimo diretamente. Atualmente a LendMe atua como correspondente bancário da BMP MONEY PLUS SOCIEDADE DE CRÉDITO DIRETO S.A., Instituição financeira, inscrita no CNPJ/ME sob n° 34.337.707/0001-00, com sede na Avenida Paulista, nº 1.765, 1º andar, CEP 01311-200, São Paulo – SP ("BMP MONEY PLUS") e da MONEY PLUS SCMEPP LTDA., Instituição financeira, inscrita no CNPJ/ME sob o nº 11.581.339/0001-45, com sede na Av. Paulista, nº 1.765, 1º andar, CEP nº 01311-200, São Paulo – SP.</p>
                    <p class=" font-paragrafo-1">Não solicitamos nenhum tipo de depósito ou pagamento antecipado para a aprovação do empréstimo solicitado.</p>
                    <p class=" font-paragrafo-1">Informações adicionais sobre o Empréstimo com Garantia Imobiliária: mínimo de 60 meses e máximo de 240 meses - Exemplo R$ 100.000,00 para pagar em 240 meses, valor da 1a prestação de R$1.044,78, taxa de juros ao mês de 0,81% + IPCA - Sistemas de Amortização Price - CET 11,71%</p>
                </div>
            </div>
            <div class="copyright-text text-center">&copy; <?php echo date('Y');?> LendMe -  Todos os Direitos são reservados</div>
        </div>
    </div>
</footer>
<script>!function(p,n,o){if(!(o=n.getElementById("brz-external-popup")))return(o=n.createElement("script")).async=1,o.id="brz-external-popup",o.src="https://s3.amazonaws.com/brizy.cloud/popups/popup.js",o.onload=e,void n.body.appendChild(o);function e(n,e){p.brzExternalPopup?p.brzExternalPopup("https://passionfruit3721047.brizy.site"):(n=o.onload,o.onload=function(){n(),p.brzExternalPopup("https://passionfruit3721047.brizy.site")})}e()}(window,document);</script>