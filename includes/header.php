<?php require_once("includes/inc_topbar.php"); ?>
<header id="header" data-transparent="true" data-fullwidth="true" class="header-transparent dark">
    <div class="header-wrap">
        <div class="container">
            <div id="logo" style="float: left!important">
                <a href="<?= $URL_SITE ?>home/" class="logo" data-src-dark="<?= $URL_SITE ?>images/logo-dark2.png">
                    <img src="<?= $URL_SITE ?>images/logo.png" alt="LendMe" style="height: 80px!important">
                </a>
            </div>
            <div id="mainMenu-trigger">
                <button class="lines-button x"> <span class="lines"></span> </button>
            </div>
            <div id="mainMenu">
                <div class="container">
                    <?  require_once("includes/inc_nav2.php");  ?>
                </div>
            </div>
        </div>
    </div>
</header>

