<?php


define("URL_ADMIN", '');
session_name('LendMe');
session_start();
header("Content-Type: text/html; charset=UTF-8",true);
date_default_timezone_set('America/Sao_Paulo');
$PATH_file = str_replace(DIRECTORY_SEPARATOR, "/", dirname(dirname(__FILE__)))."/";
define(URL_FILE,str_replace("/;", "/", $PATH_file));
define("URL_ERP",str_replace("//", "/", $PATH_file). "erp/");
define("URL_JS","");
define("NOME_SOFTWARE","LENDME");
// Caminho via browser / internet
define("URL_SITE","http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']). "");
ini_set("display_errors", true);
/*ini_set('xdebug.max_nesting_level', 3000);*/
//error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once('classes/AutoLoad.php');
