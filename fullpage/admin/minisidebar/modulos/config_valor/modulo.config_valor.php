<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/config_valor/classe.config_valor.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{


	case "cadastrar_config_valor":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {

		    $id_departamento = $_REQUEST['id_departamento'];
		    $id_certificacao = $_REQUEST['id_certificacao'];

		    $objConfigValor = new ConfigValor($pdo);
			$objConfigValor->setIdDepartamento($id_departamento);
			$objConfigValor->setIdCertificacao($id_certificacao);
			$objConfigValor->setTitulo($_REQUEST['titulo']);
			$objConfigValor->setValidade($_REQUEST['validade']);
			$objConfigValor->setTaxa($_REQUEST['taxa']);
			$objConfigValor->setHonorario($_REQUEST['honorario']);
			$objConfigValor->setTipo($_REQUEST['tipo']);
			$novoId = $objConfigValor->Adicionar();


			$msg["codigo"] = 0;
			$msg["mensagem"] = "Configuração de valor adicionada com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.config_valor.php";
		break;



	case "atualizar_config_valor":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objConfigValor = new ConfigValor($pdo);
			$objConfigValor->setId($_REQUEST['id']);
			$objConfigValor->setIdCertificacao($_REQUEST['id_certificacao']);
			$objConfigValor->setTitulo($_REQUEST['titulo']);
			$objConfigValor->setValidade($_REQUEST['validade']);
			$objConfigValor->setTaxa($_REQUEST['taxa']);
			$objConfigValor->setHonorario($_REQUEST['honorario']);
			$objConfigValor->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Configuração alterada com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.config_valor.php";
		break;



	case "deletar_config_valor":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objConfigValor = new ConfigValor($pdo);
			$objConfigValor->setId($_REQUEST["id"]);
			$objConfigValor->Remover();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "DELETADO COM SUCESSO!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.config_valor.php";
		break;





}
