

<?php

define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/atividades_pr_pf/classe.atividades_pr_pf.php");
include_once(URL_FILE . "modulos/config_valor/classe.config_valor.php");

$id_departamento = $_GET["id_departamento"];

$objConfigValor = new ConfigValor();
$objConfigValor->setIdDepartamento($id_departamento);
$listar = $objConfigValor->ListarPorDepartamento();




if($id_departamento < 3){
    $processoatitividade = "PROCESSO:";
}else{
    $processoatitividade = "ATIVIDADE:";
}




$url_edit = "edit_config_valor.php?id_departamento=".$id_departamento.'&id=';





?>
<div class="col-md-12">
    <div class="table-responsive">
        <table id="dataTable<?=$id_departamento ?>" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>CERTIFICAÇÃO:</th>
                <th><?= $processoatitividade; ?></th>
                <th>VALIDADE:</th>
                <th>TAXA:</th>
                <th>HONORÁRIO:</th>
                <th>AÇÕES</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $x = 0;
            if(count($listar)> 0){
                foreach($listar as $linha){

                    if($linha["validade"]>0 && strpos($linha["validade"],"/") == 0){
                        $validade = $linha["validade"]." meses";
                    }else if($linha["validade"]>0 && strpos($linha["validade"],"/") > 0){
                        $validade = 'até '.$linha["validade"]." de cada ano";
                    }else{
                        $validade = "Não se aplica";
                    }

                    if($linha["taxa"] > 0){
                        $taxa = 'R$: '.number_format($linha["taxa"], 2, ',', '.');
                    }else{
                        $taxa = "Não se aplica";
                    }

                    if($linha["honorario"] > 0){
                        $honorario = 'R$: '.number_format($linha["honorario"], 2, ',', '.');
                    }else{
                       $honorario = "Não se aplica";
                    }

                    if($linha["estado_sigla"] != ""){
                        $estado = "(".$linha["estado_sigla"].") ";
                    }else{
                        $estado = "";
                    }

                    echo '
                             <tr>
                                <td>'.$estado.' '.$linha["certificacao"].'</td>
                                <td>'.$linha["titulo"].'</td>
                                <td>'.$validade.'</td>
                                <td>'.$taxa.'</td>
                                <td>'.$honorario.'</td>
                                <td>
                                   <a href="'.$url_edit.$linha["id"].'" target="_blank" class="btn btn-warning"> 
                                   GERENCIAR <i class="fa fa-pencil"></i>
                                   </a> 
                                </td>
                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>

<script src="../../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable<?=$id_departamento ?>').DataTable();
    });
</script>
