<div class="alert alert-warning" role="alert"><i class="fa fa-info-circle"></i> <strong>Atenção:</strong> Para este departamento, os valores são por <u>atividade</u>.</div>


<div class="row form-group">
    <div class="col-md-4">
        <label for="validade">ATIVIDADE:</label>
        <input type="text" name="titulo[]"  id="processo"  class="form-control" placeholder=""/>
    </div>
    <div class="col-md-2">
        <label for="validade"><i class="fa fa-clock-o"></i> VALIDADE:</label>
        <input type="text" name="validade[]"  id="validade"  class="form-control" value="<?=$linha['validade'];?>"/>
    </div>
    <div class="col-md-3">
        <label for="taxa"><i class="fa fa-money"></i> TAXAS:</label>
        <input type="text" name="taxa[]"  id="taxa"  class="form-control mask-valor text-right" value="<?=$linha['taxa'];?>"/>
    </div>
    <div class="col-md-3">
        <label for="honorario"><i class="fa fa-money"></i> HONORÁRIOS:</label>
        <input type="text" name="honorario[]"  id="honorario"  class="form-control mask-valor text-right" value="<?=$linha['honorario'];?>"/>
    </div>
</div>


<div class="add_form_linha">

</div>
<hr />
<div class="row form-group">
    <div class="col-md-12 text-center">
        <a href="#" name="add_linha_tipo_processo" class="btn btn-sm btn-warning"><i class=" fa fa-plus"></i> Adicionar linha </a>
    </div>
</div>

<script type="text/javascript">
    $(function()
    {
        cont=0;
        $('.mask-valor').mask('#.##0,00', { reverse: true });
        $('a[name="add_linha_tipo_processo"]').click(function (e) {
            e.preventDefault();
            cont++;
            var clone = '  <div id="row-linha-ajax-'+cont+'" class="row form-group">' +
                '                <div class="col-md-4">' +
                '                    <input type="text" name="titulo[]" class="form-control">' +
                '                </div>' +
                '                <div class="col-md-2">' +
                '                     <input type="text" name="validade[]"  id="validade"  class="form-control" />\n'+
                '                </div>'+
                '                <div class="col-md-3">' +
                '                     <input type="text" name="taxa[]"  id="taxa"  class="form-control mask-valor text-right"/>\n'+
                '                </div>'+
                '                <div class="col-md-3">' +
                '                     <input type="text" name="honorario[]"  id="honorario"  class="form-control mask-valor text-right"/>\n'+
                '                </div>'+
                '           </div>';
            $('.add_form_linha').append(clone);
            $('.mask-valor').mask('#.##0,00', { reverse: true });
        });
    });


</script>