<?
class ConfigValor
{
	private $id;
	private $id_departamento;
	private $id_certificacao;
	private $titulo;
	private $validade;
	private $taxa;
	private $honorario;
	private $tipo;
	private $id_estado;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdDepartamento($arg)
	{
		$this->id_departamento = $arg;
	}
 	
	public function getIdDepartamento()
	{
		return $this->id_departamento;
	}
 	
	public function setIdCertificacao($arg)
	{
		$this->id_certificacao = $arg;
	}
 	
	public function getIdCertificacao()
	{
		return $this->id_certificacao;
	}
 	
	public function setTitulo($arg)
	{
		$this->titulo = $arg;
	}
 	
	public function getTitulo()
	{
		return $this->titulo;
	}
 	
	public function setValidade($arg)
	{
		$this->validade = $arg;
	}
 	
	public function getValidade()
	{
		return $this->validade;
	}
 	
	public function setTaxa($arg)
	{
		//$this->taxa = $arg;
        $this->taxa = str_replace(",",".",str_replace(".","",$arg));
	}
 	
	public function getTaxa()
	{
		return $this->taxa;
	}
 	
	public function setHonorario($arg)
	{
		//$this->honorario = $arg;
        $this->honorario = str_replace(",",".",str_replace(".","",$arg));
	}
 	
	public function getHonorario()
	{
		return $this->honorario;
	}
 	
	public function setTipo($arg)
	{
		$this->tipo = $arg;
	}
 	
	public function getTipo()
	{
		return $this->tipo;
	}


    public function setidEstado($arg)
    {
        $this->id_estado = $arg;
    }

    public function getIdEstado()
    {
        return $this->id_estado;
    }
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO config_valor SET';
		if ($this->getIdDepartamento() != "") $sql .= " id_departamento = ?";
		if ($this->getIdCertificacao() != "") $sql .= " ,id_certificacao = ?";
		if ($this->getTitulo() != "") $sql .= " ,titulo = ?";
		if ($this->getValidade() != "") $sql .= " ,validade = ?";
		if ($this->getTaxa() != "") $sql .= " ,taxa = ?";
		if ($this->getHonorario() != "") $sql .= " ,honorario = ?";
		if ($this->getTipo() != "") $sql .= " ,tipo = ? ";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		if ($this->getIdCertificacao() != "") $stmt->bindParam(++$x,$this->getIdCertificacao(),PDO::PARAM_INT);
		if ($this->getTitulo() != "") $stmt->bindParam(++$x,$this->getTitulo(),PDO::PARAM_STR);
		if ($this->getValidade() != "") $stmt->bindParam(++$x,$this->getValidade(),PDO::PARAM_STR);
		if ($this->getTaxa() != "") $stmt->bindParam(++$x,$this->getTaxa(),PDO::PARAM_STR);
		if ($this->getHonorario() != "") $stmt->bindParam(++$x,$this->getHonorario(),PDO::PARAM_STR);
		if ($this->getTipo() != "") $stmt->bindParam(++$x,$this->getTipo(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE config_valor SET ';
		if ($this->getIdCertificacao() != "") $sql .= "id_certificacao = ?";
		if ($this->getTitulo() != "") $sql .= ",titulo = ?";
		if ($this->getValidade() != "") $sql .= ",validade = ?";
		if ($this->getTaxa() != "") $sql .= ",taxa = ?";
		if ($this->getHonorario() != "") $sql .= ",honorario = ?";
        $sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getIdCertificacao() != "") $stmt->bindParam(++$x,$this->getIdCertificacao(),PDO::PARAM_INT);
		if ($this->getTitulo() != "") $stmt->bindParam(++$x,$this->getTitulo(),PDO::PARAM_STR);
		if ($this->getValidade() != "") $stmt->bindParam(++$x,$this->getValidade(),PDO::PARAM_STR);
		if ($this->getTaxa() != "") $stmt->bindParam(++$x,$this->getTaxa(),PDO::PARAM_STR);
		if ($this->getHonorario() != "") $stmt->bindParam(++$x,$this->getHonorario(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover()
	{
		$pdo = $this->getConexao();
		$sql = "DELETE FROM config_valor WHERE id = ?";
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE config_valor.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM config_valor
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				config_valor.*
			FROM config_valor
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY config_valor.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

    public function ListarPorDepartamento()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                        config_valor.*, certificacao.certificacao, certificacao.certificacao, estados.sigla AS estado_sigla
                FROM config_valor 
                INNER JOIN certificacao ON (certificacao.id = config_valor.id_certificacao) 
                LEFT JOIN estados ON (certificacao.id_estado = estados.id)
                WHERE config_valor.id_departamento = ? ORDER BY  config_valor.id ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdDepartamento(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function ListarPorDepartamentoPoliciaCivil()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                        config_valor.*, certificacao.certificacao, certificacao.certificacao, estados.sigla AS estado_sigla, certificacao.id as id_certificacao
                FROM config_valor 
                INNER JOIN certificacao ON (certificacao.id = config_valor.id_certificacao) 
                LEFT JOIN estados ON (certificacao.id_estado = estados.id)
                WHERE config_valor.id_departamento = ? AND certificacao.id_estado = ?  ORDER BY certificacao.certificacao, config_valor.titulo ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdDepartamento(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getIdEstado(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM config_valor WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function ListarPorDepartamentoFront()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                        config_valor.*, certificacao.certificacao, certificacao.certificacao, estados.sigla AS estado_sigla
                FROM config_valor 
                INNER JOIN certificacao ON (certificacao.id = config_valor.id_certificacao) 
                LEFT JOIN estados ON (certificacao.id_estado = estados.id)
                WHERE config_valor.id_departamento = ? AND config_valor.id_certificacao = ?  ORDER BY  config_valor.id ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdDepartamento(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getIdCertificacao(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function ListarTableOrcamento()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                        config_valor.*, certificacao.certificacao, certificacao.certificacao, estados.sigla AS estado_sigla
                FROM config_valor 
                INNER JOIN certificacao ON (certificacao.id = config_valor.id_certificacao) 
                LEFT JOIN estados ON (certificacao.id_estado = estados.id)
                WHERE config_valor.id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->Fetch();
    }

    public function ListarTableOrcamentoPDF($id)
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                        config_valor.*, certificacao.certificacao, certificacao.certificacao, estados.sigla AS estado_sigla
                FROM config_valor 
                INNER JOIN certificacao ON (certificacao.id = config_valor.id_certificacao) 
                LEFT JOIN estados ON (certificacao.id_estado = estados.id)
                WHERE config_valor.id = '$id'";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->Fetch();
    }
}
