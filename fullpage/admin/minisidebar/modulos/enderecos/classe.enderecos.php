<?
class Enderecos
{
	private $id;
	private $logradouro;
	private $numero;
	private $complemento;
	private $cep;
	private $bairro;
	private $id_cidade;
	private $id_estado;
	private $caixa_postal;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setLogradouro($arg)
	{
		$this->logradouro = $arg;
	}
 	
	public function getLogradouro()
	{
		return $this->logradouro;
	}
 	
	public function setNumero($arg)
	{
		$this->numero = $arg;
	}
 	
	public function getNumero()
	{
		return $this->numero;
	}
 	
	public function setComplemento($arg)
	{
		$this->complemento = $arg;
	}
 	
	public function getComplemento()
	{
		return $this->complemento;
	}
 	
	public function setCep($arg)
	{
		$this->cep = $arg;
	}
 	
	public function getCep()
	{
		return $this->cep;
	}
 	
	public function setBairro($arg)
	{
		$this->bairro = $arg;
	}
 	
	public function getBairro()
	{
		return $this->bairro;
	}
 	
	public function setIdCidade($arg)
	{
		$this->id_cidade = $arg;
	}
 	
	public function getIdCidade()
	{
		return $this->id_cidade;
	}
 	
	public function setIdEstado($arg)
	{
		$this->id_estado = $arg;
	}
 	
	public function getIdEstado()
	{
		return $this->id_estado;
	}

    public function setCaixaPostal($arg)
    {
        $this->caixa_postal = $arg;
    }

    public function getCaixaPostal()
    {
        return $this->caixa_postal;
    }
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO enderecos SET ';
		if ($this->getLogradouro() != "") $sql .= " logradouro = ?";
		if ($this->getNumero() != "") $sql .= ",numero = ?";
		if ($this->getComplemento() != "") $sql .= ",complemento = ?";
		if ($this->getCep() != "") $sql .= ",cep = ?";
		if ($this->getBairro() != "") $sql .= ",bairro = ?";
		if ($this->getIdCidade() != "") $sql .= ",id_cidade = ?";
		if ($this->getIdEstado() != "") $sql .= ",id_estado = ?";
        if ($this->getCaixaPostal() != "") $sql .= ",caixa_postal = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getLogradouro() != "") $stmt->bindParam(++$x,$this->getLogradouro(),PDO::PARAM_STR);
		if ($this->getNumero() != "") $stmt->bindParam(++$x,$this->getNumero(),PDO::PARAM_STR);
		if ($this->getComplemento() != "") $stmt->bindParam(++$x,$this->getComplemento(),PDO::PARAM_STR);
		if ($this->getCep() != "") $stmt->bindParam(++$x,$this->getCep(),PDO::PARAM_STR);
		if ($this->getBairro() != "") $stmt->bindParam(++$x,$this->getBairro(),PDO::PARAM_STR);
		if ($this->getIdCidade() != "") $stmt->bindParam(++$x,$this->getIdCidade(),PDO::PARAM_INT);
		if ($this->getIdEstado() != "") $stmt->bindParam(++$x,$this->getIdEstado(),PDO::PARAM_INT);
        if ($this->getCaixaPostal() != "") $stmt->bindParam(++$x,$this->getCaixaPostal(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE enderecos SET ';
		if ($this->getLogradouro() != "") $sql .= "logradouro = ?";
		if ($this->getNumero() != "") $sql .= ",numero = ?";
		if ($this->getComplemento() != "") $sql .= ",complemento = ?";
		if ($this->getCep() != "") $sql .= ",cep = ?";
		if ($this->getBairro() != "") $sql .= ",bairro = ?";
		if ($this->getIdCidade() != "") $sql .= ",id_cidade = ?";
		if ($this->getIdEstado() != "") $sql .= ",id_estado = ?";
        if ($this->getCaixaPostal() != "") $sql .= ",caixa_postal = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getLogradouro() != "") $stmt->bindParam(++$x,$this->getLogradouro(),PDO::PARAM_STR);
		if ($this->getNumero() != "") $stmt->bindParam(++$x,$this->getNumero(),PDO::PARAM_STR);
		if ($this->getComplemento() != "") $stmt->bindParam(++$x,$this->getComplemento(),PDO::PARAM_STR);
		if ($this->getCep() != "") $stmt->bindParam(++$x,$this->getCep(),PDO::PARAM_STR);
		if ($this->getBairro() != "") $stmt->bindParam(++$x,$this->getBairro(),PDO::PARAM_STR);
		if ($this->getIdCidade() != "") $stmt->bindParam(++$x,$this->getIdCidade(),PDO::PARAM_INT);
		if ($this->getIdEstado() != "") $stmt->bindParam(++$x,$this->getIdEstado(),PDO::PARAM_INT);
        if ($this->getCaixaPostal() != "") $stmt->bindParam(++$x,$this->getCaixaPostal(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM enderecos WHERE id IN({$lista})";
		$sql = "UPDATE enderecos SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE enderecos.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM enderecos
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				enderecos.*
			FROM enderecos
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY enderecos.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM enderecos WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}
}
