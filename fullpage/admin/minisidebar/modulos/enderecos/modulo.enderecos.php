<?


define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/enderecos/classe.enderecos.php");
$app_comando = $_REQUEST["acao"];


switch($app_comando)
{


	case "adicionar_enderecos":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objEnderecos = new Enderecos($pdo);
			$objEnderecos->setLogradouro($_REQUEST['logradouro']);
			$objEnderecos->setNumero($_REQUEST['numero']);
			$objEnderecos->setComplemento($_REQUEST['complemento']);
			$objEnderecos->setCep($_REQUEST['cep']);
			$objEnderecos->setBairro($_REQUEST['bairro']);
			$objEnderecos->setIdCidade($_REQUEST['id_cidade']);
			$objEnderecos->setIdEstado($_REQUEST['id_estado']);
			$novoId = $objEnderecos->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.enderecos.php";
		break;



	case "atualizar_enderecos":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objEnderecos = new Enderecos($pdo);
			$objEnderecos->setId($_REQUEST['id']);
			$objEnderecos->setLogradouro($_REQUEST['logradouro']);
			$objEnderecos->setNumero($_REQUEST['numero']);
			$objEnderecos->setComplemento($_REQUEST['complemento']);
			$objEnderecos->setCep($_REQUEST['cep']);
			$objEnderecos->setBairro($_REQUEST['bairro']);
			$objEnderecos->setIdCidade($_REQUEST['id_cidade']);
			$objEnderecos->setIdEstado($_REQUEST['id_estado']);
			$objEnderecos->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.enderecos.php";
		break;


	case "deletar_enderecos":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objEnderecos = new Enderecos($pdo);
			$objEnderecos->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.enderecos.php";
		break;


}
