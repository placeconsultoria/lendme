<form id="frm_enderecos">
			<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="logradouro"> <?=RTL_LOGRADOURO?></label>
			<input type="text" name="logradouro"  id="logradouro" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="numero"> <?=RTL_NUMERO?></label>
			<input type="text" name="numero"  id="numero" maxlength="255" class="form-control  " value="<?=$linha['numero'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="complemento"> <?=RTL_COMPLEMENTO?></label>
			<input type="text" name="complemento"  id="complemento" maxlength="255" class="form-control  " value="<?=$linha['complemento'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="cep"> <?=RTL_CEP?></label>
			<input type="text" name="cep"  id="cep" maxlength="20" class="form-control  " value="<?=$linha['cep'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="bairro"> <?=RTL_BAIRRO?></label>
			<input type="text" name="bairro"  id="bairro" maxlength="100" class="form-control  " value="<?=$linha['bairro'];?>"/>
		</div>
	</div>
</form>
<?
include_once("modulos/enderecos/template/js.frm.enderecos.php");
?>
