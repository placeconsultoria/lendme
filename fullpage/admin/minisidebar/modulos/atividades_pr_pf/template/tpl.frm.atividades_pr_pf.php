<?php
define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/departamento/classe.departamento.php");
    $objDepartamento = new Departamento($pdo);
    $departamentos = $objDepartamento->Listar();
    if($_GET["id"] != ""){

        $acao = "edit_atividade";
    }else{
        $acao = "cad_atividade";
    }

?>
<form id="frm_atividades_pr_pf">
        <input type="hidden" name="acao" value="<?= $acao ?>" />
		<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
        <div class="row form-group">

            <div class="col-md-4">
                <label for="id_departamento">SELECIONE O DEPARTAMENTO:</label>
                <select id="id_departamento" name="id_departamento" class="form-control">
                    <?php
                    echo "<option value='0'>--SELECIONE--</option>";
                    foreach ($departamentos AS $departamento){
                        if($departamento["id"] != "3"){
                            echo '<option value="'.$departamento["id"].'">'.$departamento["departamento"].'</option>';
                        }
                    }
                    ?>
                </select>
            </div>

            <div id="load_include_form" class="col-md-8"></div>
        </div>
</form>



<script>
    $(document).ready(function () {
        $("#id_departamento").change(function () {
            var id_departamento = $(this).val();
            var url = "modulos/atividades_pr_pf/template/tpl.frm.atividades_pr_pf.includes.php?id_departamento="+id_departamento;

            $("#load_include_form").load(url);


        });
    });
</script>

