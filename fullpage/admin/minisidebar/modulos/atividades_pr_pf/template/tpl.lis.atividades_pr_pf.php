

<?php

define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/atividades_pr_pf/classe.atividades_pr_pf.php");
include_once(URL_FILE . "modulos/departamento/classe.departamento.php");

$objAtividades = new AtividadesPrPf();
$listar = $objAtividades->Listar();



$url_edit = "edit_atividade.php?id=";





?>
<div class="col-md-12">
    <div class="table-responsive">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th width="15%">DEPARTAMENTO:</th>
                <th>ATIVIDADE:</th>
                <th width="5%">TR:</th>
                <th width="20%">AÇÕES</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $x = 0;
            if(count($listar)> 0){
                foreach($listar as $linha){
                    $objDepartamento = NEW Departamento();
                    $color = $objDepartamento->returnColor($linha["id_departamento"]);
                    if($linha["tr"] == 1 AND $linha["id_departamento"] == 1){
                        $tr = '<h3><span class="badge badge-success"><i class="fa fa-check"></i> </span> </h3>';
                    }else{
                        $tr = "";
                    }
                    echo '
                             <tr>
                                <td><h1 class="badge badge-'.$color.'">'.$linha["departamento"].'</h1></td>
                                <td>'.$linha["atividade"].'</td>
                                <td>'.$tr.'</td>
                                <td>
                                   <a href="'.$url_edit.$linha["id"].'" class="btn btn-warning"> 
                                   GERENCIAR <i class="fa fa-pencil"></i>
                                   </a> 
                                </td>
                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>

<script src="../../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>
