<script type="text/javascript">
$(document).ready(function(){
	$('#AdicionarRegistro').click(function()
	{
		DialogFormulario({
			urlConteudo: "index_xml.php?app_modulo=atividades_pr_pf&app_comando=frm_adicionar_atividades_pr_pf",
			titulo: "<?=RTL_ADICIONAR_ATIVIDADES_PR_PF?>",
			width: "50vw",
			closeable: true,
			botoes: [{
				item: "<button type='button'></button>",
				event: "click",
				btnclass: "btn btn-sm btn-primary",
				btntext:  " <?=ROTULO_SALVAR?>",
				callback: function( event ){ ExecutarAtividadesPrPf(event.data, "index_xml.php?app_modulo=atividades_pr_pf&app_comando=adicionar_atividades_pr_pf"); }
			}]
		});
	});
	$('#ExcluirRegistro').click(function()
	{
		var checked = $("input[name='lista[]']:checked").length;
		if(checked > 0)
		{
			var values = [];
			$.each($("input[name='lista[]']:checked"), function() {
				values.push($(this).val());
			});
			ConfirmBootStrap('<?= TXT_CONFIRME_DELETE_REGISTROS?> <br>ID´s ('+values+')','<?= TXT_ATENCAO?>',values,ExcluirRegistros,'',4);
		}
		else
		{
				toastr.warning('<?= TXT_ERRO_NENHUM_REGISTRO_SELECIONADO?>','<?= TXT_ATENCAO?>');
		}
	});
	$('[data-toggle="tooltip"]').tooltip();
	$("#busca").keypress(function (e) {
		if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
			AtualizarGridAtividadesPrPf("",$("#busca").val());
			return false;
		} else {
			return true;
		}
	});
});

function ExcluirRegistro(id)
{
	var ids = [];
	ids.push(id);
	ConfirmBootStrap("<?= TXT_CONFIRME_DELETE_REGISTROS?> \nID´s ("+values+")", "<?= ROTULO_ATENCAO?>", values, ExcluirRegistros,"",4);
}

function ModificarAtividadesPrPf(id)
{
	DialogFormulario({
		urlConteudo: "index_xml.php?app_modulo=atividades_pr_pf&app_comando=frm_atualizar_atividades_pr_pf&app_codigo="+id,
		titulo: "<?=RTL_MODIFICAR_ATIVIDADES_PR_PF?>",
		width: "50vw",
		closeable: true,
		botoes: [{
			item: "<button type='button'></button>",
			event: "click",
			btnclass: "btn btn-sm btn-primary",
			btntext:  " <?=ROTULO_SALVAR?>",
			callback: function( event ){ ExecutarAtividadesPrPf(event.data, "index_xml.php?app_modulo=atividades_pr_pf&app_comando=atualizar_atividades_pr_pf"); }
		}]
	});
}

function ExcluirRegistros(dados)
{
	$.post('index_xml.php?app_modulo=atividades_pr_pf&app_comando=deletar_atividades_pr_pf',
		{
			registros:dados
		},
		function(response)
		{
			if(response['codigo'] == 0)
			{
				toastr.success('<?=TXT_CONFIRMACAO_DELETE_REGISTROS?>', '<?=ROTULO_SUCESSO?>');
				AtualizarGridAtividadesPrPf(0,"");
			}
			else
			{
				toastr.warning('<?=TXT_ERRO_DELETAR_REGISTROS?>', '<?=ROTULO_ATENCAO?>');
			}
		}, 'json'
	);
}
</script>