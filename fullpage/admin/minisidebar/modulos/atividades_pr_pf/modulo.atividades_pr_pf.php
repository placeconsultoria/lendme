<?
define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/atividades_pr_pf/classe.atividades_pr_pf.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{

    case "cad_atividade":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objAtividadesPrPf = new AtividadesPrPf($pdo);
			$objAtividadesPrPf->setIdDepartamento($_REQUEST['id_departamento']);
			$objAtividadesPrPf->setAtividade($_REQUEST['atividade']);
			$objAtividadesPrPf->setDataCadastro($_REQUEST['data_cadastro']);
			if(isset($_REQUEST["tr"])){
			    $tr = 1;
            }else{
			    $tr = 0;
            }
            $objAtividadesPrPf->setTr($tr);
			$novoId = $objAtividadesPrPf->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Atividade adicionada com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.atividades_pr_pf.php";
		break;



	case "editar_atividade":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objAtividadesPrPf = new AtividadesPrPf($pdo);
			$objAtividadesPrPf->setId($_REQUEST['id']);
			$objAtividadesPrPf->setIdDepartamento($_REQUEST['id_departamento']);
			$objAtividadesPrPf->setAtividade($_REQUEST['atividade']);
            if(isset($_REQUEST["tr"])){
                $tr = 1;
            }else{
                $tr = 0;
            }
            $objAtividadesPrPf->setTr($tr);
			$objAtividadesPrPf->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Atividade editada com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.atividades_pr_pf.php";
		break;


	case "deltar":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objAtividadesPrPf = new AtividadesPrPf($pdo);
			$objAtividadesPrPf->Remover($_REQUEST['id']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Atividade deletada com sucesso";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.atividades_pr_pf.php";
		break;


}
