<?
class AtividadesPrPf
{
	private $id;
	private $id_departamento;
	private $atividade;
	private $data_cadastro;
	private $excluido;
    private $tr;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdDepartamento($arg)
	{
		$this->id_departamento = $arg;
	}
 	
	public function getIdDepartamento()
	{
		return $this->id_departamento;
	}
 	
	public function setAtividade($arg)
	{
		$this->atividade = $arg;
	}
 	
	public function getAtividade()
	{
		return $this->atividade;
	}
 	
	public function setDataCadastro($arg)
	{
		$this->data_cadastro = $arg;
	}
 	
	public function getDataCadastro()
	{
		return $this->data_cadastro;
	}
 	
	public function setExcluido($arg)
	{
		$this->excluido = $arg;
	}
 	
	public function getExcluido()
	{
		return $this->excluido;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}


    public function setTr($arg)
    {
        $this->tr = $arg;
    }

    public function getTr()
    {
        return $this->tr;
    }
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO atividades_pr_pf SET data_cadastro = NOW() ';
		if ($this->getIdDepartamento() != "") $sql .= ",id_departamento = ?";
		if ($this->getAtividade() != "") $sql .= ",atividade = ?";
        if ($this->getTr() != "") $sql .= ",tr = ?";
        $stmt = $pdo->prepare($sql);
		if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		if ($this->getAtividade() != "") $stmt->bindParam(++$x,$this->getAtividade(),PDO::PARAM_STR);
        if ($this->getTr() != "") $stmt->bindParam(++$x,$this->getTr(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE atividades_pr_pf SET ';
		if ($this->getAtividade() != "") $sql .= " atividade = ?";
		if ($this->getDataCadastro() != "") $sql .= ",data_cadastro = ?";
        if ($this->getTr() != "") $sql .= ",tr = ? ";
        if ($this->getIdDepartamento() != "") $sql .= ",id_departamento = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getAtividade() != "") $stmt->bindParam(++$x,$this->getAtividade(),PDO::PARAM_STR);
		if ($this->getDataCadastro() != "") $stmt->bindParam(++$x,$this->getDataCadastro(),PDO::PARAM_STR);
        if ($this->getTr() != "") $stmt->bindParam(++$x,$this->getTr(),PDO::PARAM_INT);
        if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}

	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$sql = "UPDATE atividades_pr_pf SET excluido = NOW() WHERE id = '$lista'";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}


	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM atividades_pr_pf WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                  atividades_pr_pf.*, departamento.departamento
                FROM atividades_pr_pf
                INNER JOIN departamento ON (atividades_pr_pf.id_departamento = departamento.id)
                WHERE atividades_pr_pf.excluido IS NULL 
                ORDER BY departamento.departamento, atividades_pr_pf.atividade ASC 
               
                 ";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function ListarFrontDepartamento()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                  atividades_pr_pf.*, departamento.departamento
                FROM atividades_pr_pf
                INNER JOIN departamento ON (atividades_pr_pf.id_departamento = departamento.id)
                WHERE atividades_pr_pf.id_departamento = ? AND atividades_pr_pf.excluido IS NULL 
                ORDER BY departamento.departamento, atividades_pr_pf.atividade ASC 
               
                 ";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdDepartamento(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function ListarAtividadeOrdamento()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                  atividades_pr_pf.*
                FROM atividades_pr_pf
                WHERE atividades_pr_pf.id = ?  
               
                 ";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function ListarAtividadeOrdamentoPF($id)
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                  atividades_pr_pf.*
                FROM atividades_pr_pf
                WHERE atividades_pr_pf.id = '$id'  
               
                 ";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}
