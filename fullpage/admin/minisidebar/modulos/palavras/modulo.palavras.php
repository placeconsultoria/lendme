<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/palavras/classe.palavras.php");
$app_comando = $_REQUEST["acao"];


switch($app_comando)
{

	case "adicionar_palavra":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPalavras = new Palavras($pdo);
			$objPalavras->setTitulo($_REQUEST['titulo']);
			$objPalavras->setIdCategoria($_REQUEST['id_categoria']);
			$objPalavras->setTipo($_REQUEST['tipo']);
            $objPalavras->setTipo2($_REQUEST['tipo2']);
			$objPalavras->setUrl($_REQUEST['url']);
			$objPalavras->setConteudo($_REQUEST['conteudo']);
			$novoId = $objPalavras->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Adicionado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.palavras.php";
		break;


	case "atualizar_palavra":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPalavras = new Palavras($pdo);
			$objPalavras->setId($_REQUEST['id']);
			$objPalavras->setTitulo($_REQUEST['titulo']);
			$objPalavras->setIdCategoria($_REQUEST['id_categoria']);
			$objPalavras->setTipo($_REQUEST['tipo']);
            $objPalavras->setTipo2($_REQUEST['tipo2']);
			$objPalavras->setUrl($_REQUEST['url']);
			$objPalavras->setConteudo($_REQUEST['conteudo']);
			$objPalavras->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Análise de palavra editada com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.palavras.php";
		break;



	case "deletar_palavras":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPalavras = new Palavras($pdo);
			$objPalavras->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.palavras.php";
		break;

	case "ajax_listar_palavras":
		$template = "tpl.lis.palavras.php";
		break;

	case "palavras_pdf":
		$template = "tpl.lis.palavras.pdf.php";
		break;

	case "palavras_xlsx":
		$template = "tpl.lis.palavras.xlsx.php";
		break;

	case "palavras_print":
		$template = "tpl.lis.palavras.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["palavras"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("palavras");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.palavras.php";
		break;
}
