<?
class Palavras
{
	private $id;
	private $titulo;
	private $id_categoria;
	private $tipo;
    private $tipo2;
	private $url;
	private $conteudo;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setTitulo($arg)
	{
		$this->titulo = $arg;
	}
 	
	public function getTitulo()
	{
		return $this->titulo;
	}
 	
	public function setIdCategoria($arg)
	{
		$this->id_categoria = $arg;
	}
 	
	public function getIdCategoria()
	{
		return $this->id_categoria;
	}
 	
	public function setTipo($arg)
	{
		$this->tipo = $arg;
	}
 	
	public function getTipo()
	{
		return $this->tipo;
	}


    public function setTipo2($arg)
    {
        $this->tipo2 = $arg;
    }

    public function getTipo2()
    {
        return $this->tipo2;
    }



    public function setUrl($arg)
	{
		$this->url = $arg;
	}
 	
	public function getUrl()
	{
		return $this->url;
	}
 	
	public function setConteudo($arg)
	{
		$this->conteudo = $arg;
	}
 	
	public function getConteudo()
	{
		return $this->conteudo;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO palavras SET ';
		if ($this->getTitulo() != "") $sql .= " titulo = ?";
		if ($this->getIdCategoria() != "") $sql .= ",id_categoria = ?";
		if ($this->getTipo() != "") $sql .= ",tipo = ?";
        if ($this->getTipo2() != "") $sql .= ",tipo2 = ?";
		if ($this->getUrl() != "") $sql .= ",url = ?";
		if ($this->getConteudo() != "") $sql .= ",conteudo = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getTitulo() != "") $stmt->bindParam(++$x,$this->getTitulo(),PDO::PARAM_STR);
		if ($this->getIdCategoria() != "") $stmt->bindParam(++$x,$this->getIdCategoria(),PDO::PARAM_INT);
		if ($this->getTipo() != "") $stmt->bindParam(++$x,$this->getTipo(),PDO::PARAM_INT);
        if ($this->getTipo2() != "") $stmt->bindParam(++$x,$this->getTipo2(),PDO::PARAM_INT);
		if ($this->getUrl() != "") $stmt->bindParam(++$x,$this->getUrl(),PDO::PARAM_STR);
		if ($this->getConteudo() != "") $stmt->bindParam(++$x,$this->getConteudo(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE palavras SET ';
		if ($this->getTitulo() != "") $sql .= "titulo = ?";
		if ($this->getIdCategoria() != "") $sql .= ",id_categoria = ?";
		if ($this->getTipo() != "") $sql .= ",tipo = ?";
        if ($this->getTipo2() != "") $sql .= ",tipo2 = ?";
		if ($this->getUrl() != "") $sql .= ",url = ?";
		if ($this->getConteudo() != "") $sql .= ",conteudo = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getTitulo() != "") $stmt->bindParam(++$x,$this->getTitulo(),PDO::PARAM_STR);
		if ($this->getIdCategoria() != "") $stmt->bindParam(++$x,$this->getIdCategoria(),PDO::PARAM_INT);
		if ($this->getTipo() != "") $stmt->bindParam(++$x,$this->getTipo(),PDO::PARAM_INT);
        if ($this->getTipo2() != "") $stmt->bindParam(++$x,$this->getTipo2(),PDO::PARAM_INT);
		if ($this->getUrl() != "") $stmt->bindParam(++$x,$this->getUrl(),PDO::PARAM_STR);
		if ($this->getConteudo() != "") $stmt->bindParam(++$x,$this->getConteudo(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM palavras WHERE id IN({$lista})";
		$sql = "UPDATE palavras SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}



	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM palavras WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT palavras.*,
                       categoria_palavaras.categoria, categoria_palavaras.cor, categoria_palavaras.icone
                FROM palavras
                INNER JOIN categoria_palavaras ON categoria_palavaras.id = palavras.id_categoria 
                ORDER BY id_categoria, titulo";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function returnTipo2($tipo){
	    if($tipo == "1"){
	        $return = "AVISO";
        }else{
            $return = "ALERTA";
        }

        return $return;
    }

    public function returnTipo1($tipo){
        if($tipo == "1"){
            $return = "URL";
        }else{
            $return = "CONTEÚDO";
        }

        return $return;
    }

    public function retornaSelected($valor, $banco){
	    if($valor == $banco) echo " selected ";
    }

    public function ListarAlertas()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT palavras.*,
                       categoria_palavaras.categoria, categoria_palavaras.cor, categoria_palavaras.icone
                FROM palavras
                INNER JOIN categoria_palavaras ON categoria_palavaras.id = palavras.id_categoria
                WHERE  palavras.tipo2 = '2'
                ORDER BY id_categoria, titulo";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }




}
