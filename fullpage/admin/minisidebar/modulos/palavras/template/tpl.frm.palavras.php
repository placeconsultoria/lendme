<?php
    define('URL_FILE',"../../../");
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/palavras/classe.palavras.php");
    include_once(URL_FILE . "modulos/categoria_palavaras/classe.categoria_palavaras.php");

    $objPalavras = new Palavras();

    if($_GET["token"] != ""){

        $objPalavras->setId($_GET["token"]);
        $linha = $objPalavras->Editar();
        $acao = "atualizar_palavra";

        if($linha["tipo"] == 1){
            $stylenone1 = '';
            $stylenone2 = 'style="display: none;"';
        }else{
            $stylenone2 = '';
            $stylenone1 = 'style="display: none;"';
        }

    }else{
        $acao = "adicionar_palavra";
        $linha["tipo"] = 0;
        $linha["tipo2"] = 0;
        $stylenone2 = 'style="display: none;"';
        $stylenone1 = 'style="display: none;"';
    }

    $ObjCategoriaPlavras = new CategoriaPalavaras();
    $comboCategorias = $ObjCategoriaPlavras->GetCombo();
    $onchangeTipo = " onchange=\"MudaFormulario(this.value)\"";


?>
<form id="frm_palavras">
			<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
            <input type="hidden" name="acao" id="acao" value="<?= $acao ?>" />
	<div class="row form-group">
        <div class="col-md-3">
            <label for="id_categoria">CATEGORIA</label>
            <select id="id_categoria" name="id_categoria" class="form-control" >
                <?php
                if(!isset($linha["id"])){
                    echo '<option value="">--SELECIONE--</option>';
                }
                if(count($comboCategorias)> 0){
                    foreach($comboCategorias as $categoria){
                        if($categoria["id"] == $linha["id_categoria"]){
                            $sel = "selected";
                        }else{
                            $sel = "";
                        }

                        echo '<option value="'.$categoria["id"].'" '.$sel.' class="text-uppercase text-'.$categoria["cor"].' ">'.$categoria["categoria"].'</option>';
                    }
                }

                ?>
            </select>
        </div>
        <div class="col-md-5">
			<label for="titulo">TÍTULO</label>
			<input type="text" name="titulo"  id="titulo" maxlength="255" class="form-control validar-obrigatorio " value="<?=$linha['titulo'];?>"/>
		</div>

        <div class="col-md-2">
            <label for="tipo">TIPO</label>
            <select name="tipo" id="tipo" class="form-control" <?= $onchangeTipo; ?> >
                <option value="">--SEL--</option>
                <option value="1" <? $objPalavras->retornaSelected("1", $linha["tipo"]) ;?>>URL/LINK EXTERNO</option>
                <option value="2" <? $objPalavras->retornaSelected("2", $linha["tipo"]) ;?>>ABRIR CONTEÚDO</option>
            </select>
        </div>

        <div class="col-md-2">
            <label for="tipo2">TIPO 2</label>
            <select name="tipo2" id="tipo2" class="form-control">
                <option value="">--SEL--</option>
                <option value="1" <? $objPalavras->retornaSelected("1", $linha["tipo2"]) ;?>>AVISO</option>
                <option value="2" <? $objPalavras->retornaSelected("2", $linha["tipo2"]) ;?>>ALERTA</option>
            </select>
        </div>
	</div>

    <div class="row form-group">

        <div class="col-md-12 1tipoconteudo tipoconteudo" <?= $stylenone1; ?> >
            <label for="titulo">INSIRA A URL</label>
            <input PLACEHOLDER="http://" type="text" name="url"  id="url" maxlength="255" class="form-control validar-obrigatorio " value="<?= $linha["url"];?>"/>
        </div>

        <div class="col-12 2tipoconteudo tipoconteudo"  <?= $stylenone2; ?> >
            <textarea name="conteudo" class="textarea_editor form-control" rows="25" placeholder="Insira o conteúdo"><?= $linha["conteudo"]; ?></textarea>
        </div>


    </div>

    <hr />
	<div class="row form-group">
        <div class="col-12">
            <p><strong>Atenção:</strong> Após o cadastro básico você irá poder adicionar as palavras para ativar o filtro nas análises.</p>

        </div>
	</div>


</form>

<script>

    $(document).ready(function () {

       $('select[name="tipo"]').change(function () {
           var id = $(this).val();
           var classe = '.'+id+'tipoconteudo';
           $('.tipoconteudo').hide();
           $(classe).show();
       });
        $('.textarea_editor').wysihtml5();
    });




</script>
