<?php
define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/categoria_palavaras/classe.categoria_palavaras.php");
include_once(URL_FILE . "modulos/palavras/classe.palavras.php");
include_once(URL_FILE . "modulos/palavras_chaves/classe.palavras_chaves.php");


$objPalavras = new Palavras($pdo);
$listar = $objPalavras->Listar();

$url_edit = "edit_palavra.php?id=";



?>


<div class="col-md-12">
    <div class="table-responsive">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>CATEGORIA</th>
                <th>TÍTULO</th>
                <th>PALAVRAS</th>
                <th>GERENCIAR</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(count($listar) > 0){
                foreach($listar as $linha){
                    $id = $linha["id"];

                    $objPalavrasChaves = new PalavrasChaves();
                    $objPalavrasChaves->setIdPalavra($id);
                    $count = $objPalavrasChaves->totalPalavra();

                    echo '
                             <tr>
                                <td><span class="btn-sm btn-'.$linha["cor"].'"><i class="'.$linha["icone"].'"></i> '.$linha["categoria"].'</span></td>
                                <td>'.$linha["titulo"].'</td>
                                <td>'.$count["total"].'</td>
                                <td><a href="'.$url_edit.$id.'" class="btn btn-warning"> GERENCIAR <i class="fa fa-pencil"></i></a> </td>

                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>



<script src="../../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>