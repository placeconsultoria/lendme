<?
class Departamento
{
	private $id;
	private $departamento;
	private $tipo_preco;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setDepartamento($arg)
	{
		$this->departamento = $arg;
	}
 	
	public function getDepartamento()
	{
		return $this->departamento;
	}


    public function setTipoPreco($arg)
    {
        $this->tipo_preco = $arg;
    }

    public function getTipoPreco()
    {
        return $this->tipo_preco;
    }
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO departamento SET ';
		if ($this->getDepartamento() != "") $sql .= "departamento = ?";
        if ($this->getTipoPreco() != "") $sql .= ",tipo_preco = ?";
        $stmt = $pdo->prepare($sql);
		if ($this->getDepartamento() != "") $stmt->bindParam(++$x,$this->getDepartamento(),PDO::PARAM_STR);
        if ($this->getTipoPreco() != "") $stmt->bindParam(++$x,$this->getTipoPreco(),PDO::PARAM_INT);

        $stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE departamento SET ';
		if ($this->getDepartamento() != "") $sql .= "departamento = ?";
        if ($this->getTipoPreco() != "") $sql .= ",tipo_preco = ?";
        $sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getDepartamento() != "") $stmt->bindParam(++$x,$this->getDepartamento(),PDO::PARAM_STR);
        if ($this->getTipoPreco() != "") $stmt->bindParam(++$x,$this->getTipoPreco(),PDO::PARAM_INT);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover()
	{
		$pdo = $this->getConexao();
		$sql = "DELETE FROM departamento WHERE id = ?";
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}



	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM departamento WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM departamento ORDER BY ID ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function  GetTipoDepartamento()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT tipo_preco FROM departamento where id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        $tipo = $stmt->fetch();
        return $tipo["tipo_preco"];
    }

    public function returnColor($id)
    {
        if($id == 1){
            $color = "success";
        }else if($id == 2){
            $color = "warning";
        }else if($id == 3){
            $color = "default";
        }
        return $color;
    }




}
