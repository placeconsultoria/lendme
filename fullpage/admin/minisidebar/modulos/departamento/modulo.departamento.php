<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/departamento/classe.departamento.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{


	case "add_departamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objDepartamento = new Departamento($pdo);
			$objDepartamento->setDepartamento($_REQUEST['departamento']);
            $objDepartamento->setTipoPreco($_REQUEST['tipo_preco']);
			$novoId = $objDepartamento->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "DEPARTAMENTO CADASTRADO";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.departamento.php";
		break;

	case "edit_departamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objDepartamento = new Departamento($pdo);
			$objDepartamento->setId($_REQUEST['id']);
			$objDepartamento->setDepartamento($_REQUEST['departamento']);
            $objDepartamento->setTipoPreco($_REQUEST['tipo_preco']);
			$objDepartamento->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "DEPARTAMENTO ATUALIZADO COM SUCESSO!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.departamento.php";
		break;



	case "deltar_departamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objDepartamento = new Departamento($pdo);
			$objDepartamento->setId($_REQUEST["id"]);
			$objDepartamento->Remover();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Deltado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.departamento.php";
		break;


}
