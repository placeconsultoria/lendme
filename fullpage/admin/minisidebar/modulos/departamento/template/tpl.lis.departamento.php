<?php
    define('URL_FILE',"../../../");
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/departamento/classe.departamento.php");

    $objDepartamento = new Departamento($pdo);
    $listar = $objDepartamento->Listar();
    $url_edit = "edit_departamento.php?id=";

?>


<div class="col-md-12">
    <div class="table-responsive">
        <table  class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>DEPARTAMENTO:</th>
                <th width="20%">AÇÕES</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $x = 0;
            if(count($listar)> 0){
                foreach($listar as $linha){
                        echo '
                             <tr>
                                <td>'.$linha["departamento"].'</td>
                                <td>
                                   <a href="'.$url_edit.$linha["id"].'" class="btn btn-warning"> 
                                   GERENCIAR <i class="fa fa-pencil"></i>
                                   </a> 
                                </td>
                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>


