<?php
    define('URL_FILE',"../../../");
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/departamento/classe.departamento.php");



    if($_GET["id"] != ""){
        $objDepartamento = new Departamento($pdo);
        $objDepartamento->setId($_GET["id"]);
        $linha = $objDepartamento->Editar();
        $acao = "edit_departamento";
    }else{
        $acao = "add_departamento";
    }

?>

<form id="frm_departamento">
	<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
    <input type="hidden" name="acao" value="<?= $acao ?>" />
	<div class="row form-group">
		<div class="col-md-8">
			<label for="departamento">DEPARTAMENTO</label>
			<input type="text" name="departamento"  id="departamento" maxlength="255" class="form-control  " value="<?=$linha['departamento'];?>"/>
		</div>
        <div class="col-md-4">
            <label for="tipo_preco">PRECIFICAÇÃO</label>
            <select name="tipo_preco" id="tipo_preco" class="form-control">
               <option value="1" <? if($linha["tipo_preco"] == "1") echo "selected"; ?>>PREÇO ÚNICO</option>
               <option value="2" <? if($linha["tipo_preco"] == "2") echo "selected"; ?>>PREÇO POR ESTADO</option>
            </select>
        </div>
	</div>
</form>
