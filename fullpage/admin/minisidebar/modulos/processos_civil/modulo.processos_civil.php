<?


define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/processos_civil/classe.processos_civil.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{

	case "cadastrar_processos_civil":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objProcessosCivil = new ProcessosCivil($pdo);
			$objProcessosCivil->setProcesso($_REQUEST['processo']);
			$novoId = $objProcessosCivil->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Processo cadastrado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.processos_civil.php";
		break;



	case "editar_processos_civil":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objProcessosCivil = new ProcessosCivil($pdo);
			$objProcessosCivil->setId($_REQUEST['id']);
			$objProcessosCivil->setProcesso($_REQUEST['processo']);
			$objProcessosCivil->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Processo alterado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.processos_civil.php";
		break;



	case "deltar":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objProcessosCivil = new ProcessosCivil($pdo);
			$objProcessosCivil->Remover($_REQUEST['id']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Processo deletado";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.processos_civil.php";
		break;


}
