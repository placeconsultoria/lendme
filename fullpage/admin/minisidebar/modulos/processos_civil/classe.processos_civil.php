<?
class ProcessosCivil
{
	private $id;
	private $processo;
	private $data_cadastro;
	private $excluido;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setProcesso($arg)
	{
		$this->processo = $arg;
	}
 	
	public function getProcesso()
	{
		return $this->processo;
	}
 	
	public function setDataCadastro($arg)
	{
		$this->data_cadastro = $arg;
	}
 	
	public function getDataCadastro()
	{
		return $this->data_cadastro;
	}
 	
	public function setExcluido($arg)
	{
		$this->excluido = $arg;
	}
 	
	public function getExcluido()
	{
		return $this->excluido;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO processos_civil SET data_cadastro = now() ';
		if ($this->getProcesso() != "") $sql .= ",processo = ?";
        $stmt = $pdo->prepare($sql);
		if ($this->getProcesso() != "") $stmt->bindParam(++$x,$this->getProcesso(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE processos_civil SET ';
		if ($this->getProcesso() != "") $sql .= "processo = ?";
        $sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getProcesso() != "") $stmt->bindParam(++$x,$this->getProcesso(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		//$sql = "DELETE FROM processos_civil WHERE id IN({$lista})";
		$sql = "UPDATE processos_civil SET excluido = UTC_TIMESTAMP() WHERE id = '$lista' ";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}


    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM processos_civil WHERE excluido IS NULL ORDER BY id ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM processos_civil WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}


    public function EditarPDF($id)
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM processos_civil WHERE id = '$id'";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetch();
    }
}
