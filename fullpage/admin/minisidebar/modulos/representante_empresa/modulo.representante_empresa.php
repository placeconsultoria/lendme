<?

switch($app_comando)
{
	case "frm_adicionar_representante_empresa":
		$template = "tpl.frm.representante_empresa.php";
		break;

	case "adicionar_representante_empresa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objRepresentanteEmpresa = new RepresentanteEmpresa($pdo);
			$objRepresentanteEmpresa->setIdEmpresa($_REQUEST['id_empresa']);
			$objRepresentanteEmpresa->setNome($_REQUEST['nome']);
			$objRepresentanteEmpresa->setCargo($_REQUEST['cargo']);
			$objRepresentanteEmpresa->setPai($_REQUEST['pai']);
			$objRepresentanteEmpresa->setMae($_REQUEST['mae']);
			$objRepresentanteEmpresa->setRg($_REQUEST['rg']);
			$objRepresentanteEmpresa->setCpf($_REQUEST['cpf']);
			$objRepresentanteEmpresa->setOrgaoRg($_REQUEST['orgao_rg']);
			$objRepresentanteEmpresa->setNascimento($_REQUEST['nascimento']);
			$objRepresentanteEmpresa->setNacionalidade($_REQUEST['nacionalidade']);
			$objRepresentanteEmpresa->setNaturalidade($_REQUEST['naturalidade']);
			$objRepresentanteEmpresa->setEstadoNacimento($_REQUEST['estado_nacimento']);
			$objRepresentanteEmpresa->setEstadoCivil($_REQUEST['estado_civil']);
			$objRepresentanteEmpresa->setProfissao($_REQUEST['profissao']);
			$objRepresentanteEmpresa->setGrauInstrucao($_REQUEST['grau_instrucao']);
			$objRepresentanteEmpresa->setIdEndereco($_REQUEST['id_endereco']);
			$novoId = $objRepresentanteEmpresa->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.representante_empresa.php";
		break;

	case "frm_atualizar_representante_empresa" :
		$representante_empresa = new RepresentanteEmpresa();
		$representante_empresa->setId($_REQUEST["app_codigo"]);
		$linha = $representante_empresa->Editar();
		$template = "tpl.frm.representante_empresa.php";
		break;

	case "atualizar_representante_empresa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objRepresentanteEmpresa = new RepresentanteEmpresa($pdo);
			$objRepresentanteEmpresa->setId($_REQUEST['id']);
			$objRepresentanteEmpresa->setIdEmpresa($_REQUEST['id_empresa']);
			$objRepresentanteEmpresa->setNome($_REQUEST['nome']);
			$objRepresentanteEmpresa->setCargo($_REQUEST['cargo']);
			$objRepresentanteEmpresa->setPai($_REQUEST['pai']);
			$objRepresentanteEmpresa->setMae($_REQUEST['mae']);
			$objRepresentanteEmpresa->setRg($_REQUEST['rg']);
			$objRepresentanteEmpresa->setCpf($_REQUEST['cpf']);
			$objRepresentanteEmpresa->setOrgaoRg($_REQUEST['orgao_rg']);
			$objRepresentanteEmpresa->setNascimento($_REQUEST['nascimento']);
			$objRepresentanteEmpresa->setNacionalidade($_REQUEST['nacionalidade']);
			$objRepresentanteEmpresa->setNaturalidade($_REQUEST['naturalidade']);
			$objRepresentanteEmpresa->setEstadoNacimento($_REQUEST['estado_nacimento']);
			$objRepresentanteEmpresa->setEstadoCivil($_REQUEST['estado_civil']);
			$objRepresentanteEmpresa->setProfissao($_REQUEST['profissao']);
			$objRepresentanteEmpresa->setGrauInstrucao($_REQUEST['grau_instrucao']);
			$objRepresentanteEmpresa->setIdEndereco($_REQUEST['id_endereco']);
			$objRepresentanteEmpresa->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.representante_empresa.php";
		break;

	case "listar_representante_empresa":
		$template = "tpl.geral.representante_empresa.php";
		break;

	case "deletar_representante_empresa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objRepresentanteEmpresa = new RepresentanteEmpresa($pdo);
			$objRepresentanteEmpresa->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.representante_empresa.php";
		break;

	case "ajax_listar_representante_empresa":
		$template = "tpl.lis.representante_empresa.php";
		break;

	case "representante_empresa_pdf":
		$template = "tpl.lis.representante_empresa.pdf.php";
		break;

	case "representante_empresa_xlsx":
		$template = "tpl.lis.representante_empresa.xlsx.php";
		break;

	case "representante_empresa_print":
		$template = "tpl.lis.representante_empresa.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["representante_empresa"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("representante_empresa");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.representante_empresa.php";
		break;
}
