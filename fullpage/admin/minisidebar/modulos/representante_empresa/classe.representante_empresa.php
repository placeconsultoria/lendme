<?
class RepresentanteEmpresa
{
	private $id;
	private $id_empresa;
	private $nome;
	private $cargo;
	private $pai;
	private $mae;
	private $rg;
	private $cpf;
	private $orgao_rg;
	private $nascimento;
	private $nacionalidade;
	private $naturalidade;
	private $estado_nacimento;
	private $estado_civil;
	private $profissao;
	private $grau_instrucao;
	private $id_endereco;
    private $telefone;
    private $celular;
    private $email;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdEmpresa($arg)
	{
		$this->id_empresa = $arg;
	}
 	
	public function getIdEmpresa()
	{
		return $this->id_empresa;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setCargo($arg)
	{
		$this->cargo = $arg;
	}
 	
	public function getCargo()
	{
		return $this->cargo;
	}
 	
	public function setPai($arg)
	{
		$this->pai = $arg;
	}
 	
	public function getPai()
	{
		return $this->pai;
	}
 	
	public function setMae($arg)
	{
		$this->mae = $arg;
	}
 	
	public function getMae()
	{
		return $this->mae;
	}
 	
	public function setRg($arg)
	{
		$this->rg = $arg;
	}
 	
	public function getRg()
	{
		return $this->rg;
	}
 	
	public function setCpf($arg)
	{
		$this->cpf = $arg;
	}
 	
	public function getCpf()
	{
		return $this->cpf;
	}
 	
	public function setOrgaoRg($arg)
	{
		$this->orgao_rg = $arg;
	}
 	
	public function getOrgaoRg()
	{
		return $this->orgao_rg;
	}
 	
	public function setNascimento($arg)
	{
		$this->nascimento = $arg;
	}
 	
	public function getNascimento()
	{
		return $this->nascimento;
	}
 	
	public function setNacionalidade($arg)
	{
		$this->nacionalidade = $arg;
	}
 	
	public function getNacionalidade()
	{
		return $this->nacionalidade;
	}
 	
	public function setNaturalidade($arg)
	{
		$this->naturalidade = $arg;
	}
 	
	public function getNaturalidade()
	{
		return $this->naturalidade;
	}
 	
	public function setEstadoNacimento($arg)
	{
		$this->estado_nacimento = $arg;
	}
 	
	public function getEstadoNacimento()
	{
		return $this->estado_nacimento;
	}
 	
	public function setEstadoCivil($arg)
	{
		$this->estado_civil = $arg;
	}
 	
	public function getEstadoCivil()
	{
		return $this->estado_civil;
	}
 	
	public function setProfissao($arg)
	{
		$this->profissao = $arg;
	}
 	
	public function getProfissao()
	{
		return $this->profissao;
	}
 	
	public function setGrauInstrucao($arg)
	{
		$this->grau_instrucao = $arg;
	}
 	
	public function getGrauInstrucao()
	{
		return $this->grau_instrucao;
	}
 	
	public function setIdEndereco($arg)
	{
		$this->id_endereco = $arg;
	}
 	
	public function getIdEndereco()
	{
		return $this->id_endereco;
	}

    public function setTelefone($arg)
    {
        $this->telefone = $arg;
    }

    public function getTelefone()
    {
        return $this->telefone;
    }

    public function setCelular($arg)
    {
        $this->celular = $arg;
    }

    public function getCelular()
    {
        return $this->celular;
    }

    public function setEmail($arg)
    {
        $this->email = $arg;
    }

    public function getEmail()
    {
        return $this->email;
    }
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO representante_empresa SET id = ?';
		if ($this->getIdEmpresa() != "") $sql .= ",id_empresa = ?";
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getCargo() != "") $sql .= ",cargo = ?";
		if ($this->getPai() != "") $sql .= ",pai = ?";
		if ($this->getMae() != "") $sql .= ",mae = ?";
		if ($this->getRg() != "") $sql .= ",rg = ?";
		if ($this->getCpf() != "") $sql .= ",cpf = ?";
		if ($this->getOrgaoRg() != "") $sql .= ",orgao_rg = ?";
		if ($this->getNascimento() != "") $sql .= ",nascimento = ?";
		if ($this->getNacionalidade() != "") $sql .= ",nacionalidade = ?";
		if ($this->getNaturalidade() != "") $sql .= ",naturalidade = ?";
		if ($this->getEstadoNacimento() != "") $sql .= ",estado_nacimento = ?";
		if ($this->getEstadoCivil() != "") $sql .= ",estado_civil = ?";
		if ($this->getProfissao() != "") $sql .= ",profissao = ?";
		if ($this->getGrauInstrucao() != "") $sql .= ",grau_instrucao = ?";
		if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";
        if ($this->getTelefone() != "") $sql .= ",telefone = ?";
        if ($this->getCelular() != "") $sql .= ",celular = ?";
        if ($this->getEmail() != "") $sql .= ",email = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdEmpresa() != "") $stmt->bindParam(++$x,$this->getIdEmpresa(),PDO::PARAM_INT);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getCargo() != "") $stmt->bindParam(++$x,$this->getCargo(),PDO::PARAM_STR);
		if ($this->getPai() != "") $stmt->bindParam(++$x,$this->getPai(),PDO::PARAM_STR);
		if ($this->getMae() != "") $stmt->bindParam(++$x,$this->getMae(),PDO::PARAM_STR);
		if ($this->getRg() != "") $stmt->bindParam(++$x,$this->getRg(),PDO::PARAM_STR);
		if ($this->getCpf() != "") $stmt->bindParam(++$x,$this->getCpf(),PDO::PARAM_STR);
		if ($this->getOrgaoRg() != "") $stmt->bindParam(++$x,$this->getOrgaoRg(),PDO::PARAM_STR);
		if ($this->getNascimento() != "") $stmt->bindParam(++$x,$this->getNascimento(),PDO::PARAM_STR);
		if ($this->getNacionalidade() != "") $stmt->bindParam(++$x,$this->getNacionalidade(),PDO::PARAM_STR);
		if ($this->getNaturalidade() != "") $stmt->bindParam(++$x,$this->getNaturalidade(),PDO::PARAM_STR);
		if ($this->getEstadoNacimento() != "") $stmt->bindParam(++$x,$this->getEstadoNacimento(),PDO::PARAM_INT);
		if ($this->getEstadoCivil() != "") $stmt->bindParam(++$x,$this->getEstadoCivil(),PDO::PARAM_INT);
		if ($this->getProfissao() != "") $stmt->bindParam(++$x,$this->getProfissao(),PDO::PARAM_STR);
		if ($this->getGrauInstrucao() != "") $stmt->bindParam(++$x,$this->getGrauInstrucao(),PDO::PARAM_STR);
		if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
        if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
        if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
        if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = '
		UPDATE representante_empresa SET 
			id = ?';
		if ($this->getIdEmpresa() != "") $sql .= ",id_empresa = ?";
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getCargo() != "") $sql .= ",cargo = ?";
		if ($this->getPai() != "") $sql .= ",pai = ?";
		if ($this->getMae() != "") $sql .= ",mae = ?";
		if ($this->getRg() != "") $sql .= ",rg = ?";
		if ($this->getCpf() != "") $sql .= ",cpf = ?";
		if ($this->getOrgaoRg() != "") $sql .= ",orgao_rg = ?";
		if ($this->getNascimento() != "") $sql .= ",nascimento = ?";
		if ($this->getNacionalidade() != "") $sql .= ",nacionalidade = ?";
		if ($this->getNaturalidade() != "") $sql .= ",naturalidade = ?";
		if ($this->getEstadoNacimento() != "") $sql .= ",estado_nacimento = ?";
		if ($this->getEstadoCivil() != "") $sql .= ",estado_civil = ?";
		if ($this->getProfissao() != "") $sql .= ",profissao = ?";
		if ($this->getGrauInstrucao() != "") $sql .= ",grau_instrucao = ?";
		if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";
        if ($this->getTelefone() != "") $sql .= ",telefone = ?";
        if ($this->getCelular() != "") $sql .= ",celular = ?";
        if ($this->getEmail() != "") $sql .= ",email = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdEmpresa() != "") $stmt->bindParam(++$x,$this->getIdEmpresa(),PDO::PARAM_INT);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getCargo() != "") $stmt->bindParam(++$x,$this->getCargo(),PDO::PARAM_STR);
		if ($this->getPai() != "") $stmt->bindParam(++$x,$this->getPai(),PDO::PARAM_STR);
		if ($this->getMae() != "") $stmt->bindParam(++$x,$this->getMae(),PDO::PARAM_STR);
		if ($this->getRg() != "") $stmt->bindParam(++$x,$this->getRg(),PDO::PARAM_STR);
		if ($this->getCpf() != "") $stmt->bindParam(++$x,$this->getCpf(),PDO::PARAM_STR);
		if ($this->getOrgaoRg() != "") $stmt->bindParam(++$x,$this->getOrgaoRg(),PDO::PARAM_STR);
		if ($this->getNascimento() != "") $stmt->bindParam(++$x,$this->getNascimento(),PDO::PARAM_STR);
		if ($this->getNacionalidade() != "") $stmt->bindParam(++$x,$this->getNacionalidade(),PDO::PARAM_STR);
		if ($this->getNaturalidade() != "") $stmt->bindParam(++$x,$this->getNaturalidade(),PDO::PARAM_STR);
		if ($this->getEstadoNacimento() != "") $stmt->bindParam(++$x,$this->getEstadoNacimento(),PDO::PARAM_INT);
		if ($this->getEstadoCivil() != "") $stmt->bindParam(++$x,$this->getEstadoCivil(),PDO::PARAM_INT);
		if ($this->getProfissao() != "") $stmt->bindParam(++$x,$this->getProfissao(),PDO::PARAM_STR);
		if ($this->getGrauInstrucao() != "") $stmt->bindParam(++$x,$this->getGrauInstrucao(),PDO::PARAM_STR);
		if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM representante_empresa WHERE id IN({$lista})";
		$sql = "UPDATE representante_empresa SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE representante_empresa.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM representante_empresa
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				representante_empresa.*
			FROM representante_empresa
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY representante_empresa.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM representante_empresa WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}
}
