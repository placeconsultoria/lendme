<?
define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/clientes/classe.clientes.php");
$app_comando = $_REQUEST["acao"];
switch($app_comando)
{


	case "adicionar_clientes":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objClientes = new Clientes($pdo);
			$objClientes->setNome($_REQUEST['nome']);
			$objClientes->setTelefone($_REQUEST['telefone']);
            $objClientes->setTelefone2($_REQUEST['telefone2']);
            $objClientes->setPasta($_REQUEST['pasta']);
            $objClientes->setCnpj($_REQUEST['cnpj']);
            $objClientes->setIdCidade($_REQUEST['id_cidade']);
            $objClientes->setIdEstado($_REQUEST['id_estado']);
			$novoId = $objClientes->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Cliente cadastrado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.clientes.php";
		break;


	case "atualizar_clientes":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objClientes = new Clientes($pdo);
			$objClientes->setId($_REQUEST['id']);
			$objClientes->setNome($_REQUEST['nome']);
			$objClientes->setTelefone($_REQUEST['telefone']);
            $objClientes->setTelefone2($_REQUEST['telefone2']);
            $objClientes->setPasta($_REQUEST['pasta']);
            $objClientes->setCnpj($_REQUEST['cnpj']);
            $objClientes->setIdCidade($_REQUEST['id_cidade']);
            $objClientes->setIdEstado($_REQUEST['id_estado']);
			$objClientes->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Cliente editado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.clientes.php";
		break;


	case "deletar_cliente":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objClientes = new Clientes($pdo);
			$objClientes ->setId($_REQUEST["id"]);
			$objClientes->Remover();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "OK";
            $msg["url"] = "clientes.php";

            $pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.clientes.php";
		break;


}
