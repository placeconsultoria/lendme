<?php
define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/clientes/classe.clientes.php");



$objClientes = new Clientes($pdo);
$listar = $objClientes->Listar();

$url_edit = "edit_cliente.php?id=";



?>


<div class="col-md-12">
    <div class="table-responsive">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>CLIENTE</th>
                <th>PASTA</th>
                <th>CNPJ</th>
                <th>TELEFONES</th>
                <th>CIDADE</th>
                <th>GERENCIAR</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $x = 0;
            if(count($listar)> 0){
                foreach($listar as $linha){
                    $id = $linha["id"];

                    echo '
                             <tr>
                                <td>'.$linha["nome"].'</td>
                                <td>'.$linha["pasta"].'</td>
                                <td>'.$linha["cnpj"].'</td>
                                <td>'.$linha["telefone"].' '.$linha["telefone2"].'</td>
                                 <td class="text-uppercase">'.$linha["cidade"].'/'.$linha["estado"].'</td>
                                <td><a href="'.$url_edit.$id.'" class="btn btn-warning"> GERENCIAR <i class="fa fa-pencil"></i></a> </td>
                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>



<script src="../../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>