<script type="text/javascript">
$(function()
{
});

/*
 * Executa o post do formulário
 * */
function ExecutarClientes(dialog, url)
{
	if (ValidateForm($("#frm_clientes"))) {
		// ao clicar em salvar enviando dados por post via AJAX
		$.post(url,
			$("#frm_clientes").serialize(),
			// pegando resposta do retorno do post
			function (response)
			{
				if (response["codigo"] == 0) {
					dialog.close();
					toastr.success(response["mensagem"], "<?=ROTULO_MENSAGEM?>");
					AtualizarGridClientes(0, "");
				} else {
					toastr.warning(response["mensagem"], "<?=ROTULO_MENSAGEM?>");
				}
			}
			, "json" // definindo retorno para o formato json
		);
	}
}


function FiltrarCidades(id)
{
    var options = [];
    $.getJSON("index_xml.php?app_modulo=cidades&app_comando=filtrar_cidades&app_codigo="+id, function(result) {
        options.push('<option value="">-- Selecione uma cidade --</option>');
        for (var i = 0; i < result.length; i++)
        {
            options.push('<option value="',
                result[i].id, '">',
                result[i].nome, '</option>');
        }
        $("#id_cidade").html(options.join(''));
        //$("#id_cidade").selectpicker('refresh');
    });
}

</script>
