<?php
    define('URL_FILE',"../../../");
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/clientes/classe.clientes.php");
    include_once(URL_FILE . "modulos/estados/classe.estados.php");
    include_once(URL_FILE . "modulos/cidades/classe.cidades.php");

    if($_GET["id"] != ""){
        $objCliente = new Clientes($pdo);
        $objCliente->setId($_GET["id"]);
        $linha = $objCliente->Editar();

        $acao = "atualizar_clientes";
    }else{
        $acao = "adicionar_clientes";
    }


    $onchangeEstado = " onchange=\"FiltrarCidades(this.value)\"";

 ?>
<form id="frm_clientes">
			<input type="hidden" name="id"  id="id" value="<?=$linha['id'];?>"/>
            <input type="hidden" name="acao" value="<?php echo $acao ?>" />
	<div class="row form-group">

        <div class="col-md-4 ">
            <label for="nome">PASTA</label>
            <input type="text" name="pasta"  id="pasta" maxlength="255" class="form-control validar-obrigatorio " value="<?=$linha['pasta'];?>"/>
        </div>

		<div class="col-md-4">
			<label for="nome">EMPRESA/CLIENTE</label>
			<input type="text" name="nome"  id="nome" maxlength="255" class="form-control validar-obrigatorio " value="<?=$linha['nome'];?>"/>
		</div>

        <div class="col-md-4">
            <label for="nome">CNPJ</label>
            <input type="text" name="cnpj"  id="cnpj" class="form-control validar-obrigatorio " value="<?=$linha['cnpj'];?>" onKeyUp="mascaraTexto(event,'99.999.999/9999-99')" maxlength="18" onkeypress="return SomenteNumero(event);" />
        </div>

	</div>

    <div class="row form-group">

        <div class="col-md-3">
            <label for="telefone">TELEFONE 1</label>
            <input type="text" name="telefone"  id="telefone" class="form-control validar-obrigatorio " value="<?=$linha['telefone'];?>" onKeyUp="mascaraTexto(event,'(99)9999-9999')" maxlength="13" onkeypress="return SomenteNumero(event);"/>
        </div>
        <div class="col-md-3">
            <label for="telefone">TELEFONE 2</label>
            <input type="text" name="telefone2"  id="telefone2"  class="form-control validar-obrigatorio " value="<?=$linha['telefone2'];?> " onKeyUp="mascaraTexto(event,'(99)99999-9999')" maxlength="14" onkeypress="return SomenteNumero(event);"/>
        </div>

        <div class="col-md-2">
            <label for="id_estado">ESTADO</label>
            <select name="id_estado" id="id_estado" class="form-control" <?= $onchangeEstado; ?>>

                <?php

                if($linha["id_estado"] == "") echo '<option value="">--SEL--</option>';
                $objEstado = new Estados();
                $estados = $objEstado->GerarSelectEstados();
                if(@count($estados) > 0){
                    foreach($estados as $estado){
                       if($estado["id_estado"] == $linha["id_estado"]){  $sel = "selected";  }else{ $sel = "";  }
                       echo '<option value="'.$estado['id_estado'].'" '.$sel.'>'.$estado['sigla'].'</option>';
                    }
                }
                ?>
            </select>
        </div>

        <div class="col-md-4">
            <label for="id_cidade">CIDADE</label>
            <select name="id_cidade" id="id_cidade" class="form-control">


                <?php

                if($linha["id_cidade"] != ""){

                    $objCidades = new Cidades();
                    $objCidades->setIdEstado($linha["id_estado"]);
                    $cidades = $objCidades->ComboCidade();

                    if(@count($cidades) > 0){
                        foreach($cidades as $cidade){
                            if($cidade["id"] == $linha["id_cidade"]){  $sel = "selected";  }else{ $sel = "";  }
                            echo '<option value="'.$cidade['id'].'" '.$sel.'>'.$cidade['nome'].'</option>';
                        }
                    }

                }else{
                    echo  '<option value="">-- AGUARDE --</option>';
                }

                ?>

            </select>
        </div>

    </div>

</form>

<script>

    function FiltrarCidades(id)
    {
        var options = [];
        $.getJSON("modulos/cidades/modulo.cidades.php?acao=filtrar_cidades&app_codigo="+id, function(result) {
            options.push('<option value="">-- Selecione uma cidade --</option>');
            for (var i = 0; i < result.length; i++)
            {
                options.push('<option value="',
                    result[i].id, '">',
                    result[i].nome, '</option>');
            }
            $("#id_cidade").html(options.join(''));
            //$("#id_cidade").selectpicker('refresh');
        });
    }
</script>

