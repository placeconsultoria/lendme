<?
class Clientes
{
	private $id;
	private $nome;
	private $telefone;
    private $telefone2;
    private $pasta;
    private $id_estado;
    private $id_cidade;
    private $cnpj;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setTelefone($arg)
	{
		$this->telefone = $arg;
	}
 	
	public function getTelefone()
	{
		return $this->telefone;
	}

    public function setTelefone2($arg)
    {
        $this->telefone2 = $arg;
    }

    public function getTelefone2()
    {
        return $this->telefone2;
    }

     public function setPasta($arg)
     {
         $this->pasta = $arg;
     }

    public function getPasta()
    {
        return $this->pasta;
    }

     public function setIdEstado($arg)
     {
         $this->id_estado = $arg;
     }

    public function getIdEstado()
    {
        return $this->id_estado;
    }

    public function setIdCidade($arg)
    {
        $this->id_cidade = $arg;
    }

    public function getIdCidade()
    {
        return $this->id_cidade;
    }

    public function setCnpj($arg)
    {
        $this->cnpj = $arg;
    }

    public function getCnpj()
    {
        return $this->cnpj;
    }
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO clientes SET ';
		if ($this->getNome() != "") $sql .= "nome = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
        if ($this->getTelefone2() != "") $sql .= ",telefone2 = ?";
        if ($this->getPasta() != "") $sql .= ",pasta = ?";
        if ($this->getCnpj() != "") $sql .= ",cnpj = ?";
        if ($this->getIdCidade() != "") $sql .= ",id_cidade = ?";
        if ($this->getIdEstado() != "") $sql .= ",id_estado = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
        if ($this->getTelefone2() != "") $stmt->bindParam(++$x,$this->getTelefone2(),PDO::PARAM_STR);
        if ($this->getPasta() != "") $stmt->bindParam(++$x,$this->getPasta(),PDO::PARAM_STR);
        if ($this->getCnpj() != "") $stmt->bindParam(++$x,$this->getCnpj(),PDO::PARAM_STR);
        if ($this->getIdCidade() != "") $stmt->bindParam(++$x,$this->getIdCidade(),PDO::PARAM_INT);
        if ($this->getIdEstado() != "") $stmt->bindParam(++$x,$this->getIdEstado(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE clientes SET ';
		if ($this->getNome() != "") $sql .= "nome = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
        if ($this->getTelefone2() != "") $sql .= ",telefone2 = ?";
        if ($this->getPasta() != "") $sql .= ",pasta = ?";
        if ($this->getCnpj() != "") $sql .= ",cnpj = ?";
        if ($this->getIdCidade() != "") $sql .= ",id_cidade = ?";
        if ($this->getIdEstado() != "") $sql .= ",id_estado = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
        if ($this->getTelefone2() != "") $stmt->bindParam(++$x,$this->getTelefone2(),PDO::PARAM_STR);
        if ($this->getPasta() != "") $stmt->bindParam(++$x,$this->getPasta(),PDO::PARAM_STR);
        if ($this->getCnpj() != "") $stmt->bindParam(++$x,$this->getCnpj(),PDO::PARAM_STR);
        if ($this->getIdCidade() != "") $stmt->bindParam(++$x,$this->getIdCidade(),PDO::PARAM_INT);
        if ($this->getIdEstado() != "") $stmt->bindParam(++$x,$this->getIdEstado(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover()
	{
		$pdo = $this->getConexao();

		$sql = "DELETE FROM clientes WHERE id = ?";
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}



	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT *
                    FROM clientes
                WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}



    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT clientes.*,
                       cidades.nome as cidade,
                       estados.sigla as estado
                      FROM clientes 
                 INNER JOIN cidades ON cidades.id = clientes.id_cidade
                 INNER JOIN estados ON estados.id = clientes.id_estado     
                 ORDER BY nome asc";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function GetCombo()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT clientes.*,
                       cidades.nome as cidade,
                       estados.sigla as estado
                      FROM clientes 
                 INNER JOIN cidades ON cidades.id = clientes.id_cidade
                 INNER JOIN estados ON estados.id = clientes.id_estado     
                 ORDER BY nome asc";

        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
