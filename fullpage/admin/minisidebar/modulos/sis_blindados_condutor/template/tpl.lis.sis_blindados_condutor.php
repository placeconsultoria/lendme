<?php

define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/sis_blindados_condutor/classe.sis_blindados_condutor.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");
include_once(URL_FILE . "modulos/cidades/classe.cidades.php");


$id_cadastro = $_GET["id"];
$objCondutores = new SisBlindadosCondutor();
$objCondutores->setIdCadastro($id_cadastro);
$resulCondutores = $objCondutores->GetCondutoresCadastro();

$objEstados = NEW Estados();
$resulComboEstados = $objEstados->GerarSelectEstados();

if(@count($resulCondutores) > 0) {
    foreach ($resulCondutores as $linha) {

        echo '
            <div class="row form-group">
                <div class="col-md-12">
                    <h4><span class="badge badge-inverse">CONDUTOR:</span> </h4>
                </div>
                <div class="col-md-3">
                    <label  for="contudor_nome"> NOME DO CONDUTOR:</label>
                    <input required type="text" name="nome"  id="contudor_nome" maxlength="255" class="form-control  " value="'.$linha["nome"].'"/>
                </div>
                <div class="col-md-3">
                    <label for="contudor_nascimento"> NASCIMENTO:</label>
                    <input required type="text" name="nascimento"  id="contudor_nascimento"  class="form-control  mask-date" value="'.Conexao::PrepararDataPHP($linha["nascimento"]).'" onkeyup="mascaraTexto(event,\'99/99/9999\')" maxlength="10" />
                </div>
                <div class="col-md-3">
                    <label for="contudor_pai"> NOME DO PAI:</label>
                    <input required type="text" name="pai"  id="contudor_pai" maxlength="255" class="form-control  " value="'.$linha["pai"].'"/>
                </div>
                <div class="col-md-3">
                    <label for="contudor_mae"> NOME DO MÃE:</label>
                    <input required type="text" name="mae"  id="contudor_mae" maxlength="255" class="form-control  " value="'.$linha["mae"].'"/>
                </div>
        
        
            </div>
        
            <div class="row form-group">
                <div class="col-md-3">
                    <label for="condutor_cpf"> CPF:</label>
                    <input required type="text"  name="cpf"  id="condutor_cpf" class="form-control  " value="'.$linha["cpf"].'"  onkeyup="mascaraTexto(event,\'999.999.999-99\')" maxlength="14"/>
                </div>
                <div class="col-md-3">
                    <label for="condutor_rg">RG/RNE:</label>
                    <input required type="text" name="rg"  id="condutor_rg" maxlength="255" class="form-control  " value="'.$linha["rg"].'"/>
                </div>
        
                <div class="col-md-3">
                    <label for="condutor_emissor"> ORGÃO EMISSOR:</label>
                    <input required type="text" name="emissor"  id="condutor_emissor"  class="form-control " value="'.$linha["emissor"].'"/>
                </div>
                <div class="col-md-3">
                    <label for="condutor_emissao"> DATA EMISSÃO:</label>
                    <input required type="text" name="emissao"  id="condutor_emissao"  class="form-control  mask-date" value="'.Conexao::PrepararDataPHP($linha["emissao"]).'" onkeyup="mascaraTexto(event,\'99/99/9999\')" maxlength="10" />
                </div>
        
            </div>
            
            <hr />
        ';
    }
}


?>


