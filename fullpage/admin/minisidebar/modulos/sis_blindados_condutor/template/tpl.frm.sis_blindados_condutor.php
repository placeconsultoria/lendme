<form id="frm_sis_blindados_condutor">
			<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="nome"> <?=RTL_NOME?></label>
			<input type="text" name="nome"  id="nome" maxlength="255" class="form-control  " value="<?=$linha['nome'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="nascimento"> <?=RTL_NASCIMENTO?></label>
			<input type="text" name="nascimento"  id="nascimento"  class="form-control  mask-date" value="<?=$linha['nascimento'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="pai"> <?=RTL_PAI?></label>
			<input type="text" name="pai"  id="pai" maxlength="255" class="form-control  " value="<?=$linha['pai'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="mae"> <?=RTL_MAE?></label>
			<input type="text" name="mae"  id="mae" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="cpf"> <?=RTL_CPF?></label>
			<input type="text" name="cpf"  id="cpf" maxlength="20" class="form-control  " value="<?=$linha['cpf'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="rg"> <?=RTL_RG?></label>
			<input type="text" name="rg"  id="rg" maxlength="20" class="form-control  " value="<?=$linha['rg'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="emissor"> <?=RTL_EMISSOR?></label>
			<input type="text" name="emissor"  id="emissor" maxlength="100" class="form-control  " value="<?=$linha['emissor'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="emissao"> <?=RTL_EMISSAO?></label>
			<input type="text" name="emissao"  id="emissao"  class="form-control  mask-date" value="<?=$linha['emissao'];?>"/>
		</div>
	</div>
</form>
<?
include_once("modulos/sis_blindados_condutor/template/js.frm.sis_blindados_condutor.php");
?>
