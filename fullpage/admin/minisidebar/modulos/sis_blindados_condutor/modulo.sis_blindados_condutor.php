<?


define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/sis_blindados_condutor/classe.sis_blindados_condutor.php");
include_once(URL_FILE . "modulos/enderecos/classe.enderecos.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{


	case "adicionar_cadastro_condutores":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objSisBlindadosCondutor = new SisBlindadosCondutor($pdo);
			$objSisBlindadosCondutor->setNome($_REQUEST['nome']);
			$objSisBlindadosCondutor->setNascimento(Conexao::PrepararDataBD($_REQUEST['nascimento']));
			$objSisBlindadosCondutor->setPai($_REQUEST['pai']);
			$objSisBlindadosCondutor->setMae($_REQUEST['mae']);
			$objSisBlindadosCondutor->setCpf($_REQUEST['cpf']);
			$objSisBlindadosCondutor->setRg($_REQUEST['rg']);
			$objSisBlindadosCondutor->setEmissor($_REQUEST['emissor']);
			$objSisBlindadosCondutor->setEmissao(Conexao::PrepararDataBD($_REQUEST['emissao']));
			$objSisBlindadosCondutor->setIdCadastro($_REQUEST['id_cadastro']);
			$novoId = $objSisBlindadosCondutor->Adicionar();
			$msg["codigo"] = 0;
            $msg["id_cadastro"] = $_REQUEST['id_cadastro'];
			$msg["mensagem"] = "Sucesso";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_condutor.php";
		break;

	case "frm_atualizar_sis_blindados_condutor" :
		$sis_blindados_condutor = new SisBlindadosCondutor();
		$sis_blindados_condutor->setId($_REQUEST["app_codigo"]);
		$linha = $sis_blindados_condutor->Editar();
		$template = "tpl.frm.sis_blindados_condutor.php";
		break;

	case "atualizar_sis_blindados_condutor":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objSisBlindadosCondutor = new SisBlindadosCondutor($pdo);
			$objSisBlindadosCondutor->setId($_REQUEST['id']);
			$objSisBlindadosCondutor->setNome($_REQUEST['nome']);
			$objSisBlindadosCondutor->setNascimento($_REQUEST['nascimento']);
			$objSisBlindadosCondutor->setPai($_REQUEST['pai']);
			$objSisBlindadosCondutor->setMae($_REQUEST['mae']);
			$objSisBlindadosCondutor->setCpf($_REQUEST['cpf']);
			$objSisBlindadosCondutor->setRg($_REQUEST['rg']);
			$objSisBlindadosCondutor->setEmissor($_REQUEST['emissor']);
			$objSisBlindadosCondutor->setEmissao($_REQUEST['emissao']);
			$objSisBlindadosCondutor->setIdCadastro($_REQUEST['id_cadastro']);
			$objSisBlindadosCondutor->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_condutor.php";
		break;



	case "deletar_sis_blindados_condutor":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objSisBlindadosCondutor = new SisBlindadosCondutor($pdo);
			$objSisBlindadosCondutor->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_condutor.php";
		break;




}
