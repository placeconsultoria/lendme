<?
class SisBlindadosCondutor
{
	private $id;
	private $nome;
	private $nascimento;
	private $pai;
	private $mae;
	private $cpf;
	private $rg;
	private $emissor;
	private $emissao;
	private $id_cadastro;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setNascimento($arg)
	{
		$this->nascimento = $arg;
	}
 	
	public function getNascimento()
	{
		return $this->nascimento;
	}
 	
	public function setPai($arg)
	{
		$this->pai = $arg;
	}
 	
	public function getPai()
	{
		return $this->pai;
	}
 	
	public function setMae($arg)
	{
		$this->mae = $arg;
	}
 	
	public function getMae()
	{
		return $this->mae;
	}
 	
	public function setCpf($arg)
	{
		$this->cpf = $arg;
	}
 	
	public function getCpf()
	{
		return $this->cpf;
	}
 	
	public function setRg($arg)
	{
		$this->rg = $arg;
	}
 	
	public function getRg()
	{
		return $this->rg;
	}
 	
	public function setEmissor($arg)
	{
		$this->emissor = $arg;
	}
 	
	public function getEmissor()
	{
		return $this->emissor;
	}
 	
	public function setEmissao($arg)
	{
		$this->emissao = $arg;
	}
 	
	public function getEmissao()
	{
		return $this->emissao;
	}
 	
	public function setIdCadastro($arg)
	{
		$this->id_cadastro = $arg;
	}
 	
	public function getIdCadastro()
	{
		return $this->id_cadastro;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO sis_blindados_condutor SET ';
		if ($this->getNome() != "") $sql .= "nome = ?";
		if ($this->getNascimento() != "") $sql .= ",nascimento = ?";
		if ($this->getPai() != "") $sql .= ",pai = ?";
		if ($this->getMae() != "") $sql .= ",mae = ?";
		if ($this->getCpf() != "") $sql .= ",cpf = ?";
		if ($this->getRg() != "") $sql .= ",rg = ?";
		if ($this->getEmissor() != "") $sql .= ",emissor = ?";
		if ($this->getEmissao() != "") $sql .= ",emissao = ?";
		if ($this->getIdCadastro() != "") $sql .= ",id_cadastro = ?";
		$stmt = $pdo->prepare($sql);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getNascimento() != "") $stmt->bindParam(++$x,$this->getNascimento(),PDO::PARAM_STR);
		if ($this->getPai() != "") $stmt->bindParam(++$x,$this->getPai(),PDO::PARAM_STR);
		if ($this->getMae() != "") $stmt->bindParam(++$x,$this->getMae(),PDO::PARAM_STR);
		if ($this->getCpf() != "") $stmt->bindParam(++$x,$this->getCpf(),PDO::PARAM_STR);
		if ($this->getRg() != "") $stmt->bindParam(++$x,$this->getRg(),PDO::PARAM_STR);
		if ($this->getEmissor() != "") $stmt->bindParam(++$x,$this->getEmissor(),PDO::PARAM_STR);
		if ($this->getEmissao() != "") $stmt->bindParam(++$x,$this->getEmissao(),PDO::PARAM_STR);
		if ($this->getIdCadastro() != "") $stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = '
		UPDATE sis_blindados_condutor SET 
			id = ?';
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getNascimento() != "") $sql .= ",nascimento = ?";
		if ($this->getPai() != "") $sql .= ",pai = ?";
		if ($this->getMae() != "") $sql .= ",mae = ?";
		if ($this->getCpf() != "") $sql .= ",cpf = ?";
		if ($this->getRg() != "") $sql .= ",rg = ?";
		if ($this->getEmissor() != "") $sql .= ",emissor = ?";
		if ($this->getEmissao() != "") $sql .= ",emissao = ?";
		if ($this->getIdCadastro() != "") $sql .= ",id_cadastro = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getNascimento() != "") $stmt->bindParam(++$x,$this->getNascimento(),PDO::PARAM_STR);
		if ($this->getPai() != "") $stmt->bindParam(++$x,$this->getPai(),PDO::PARAM_STR);
		if ($this->getMae() != "") $stmt->bindParam(++$x,$this->getMae(),PDO::PARAM_STR);
		if ($this->getCpf() != "") $stmt->bindParam(++$x,$this->getCpf(),PDO::PARAM_STR);
		if ($this->getRg() != "") $stmt->bindParam(++$x,$this->getRg(),PDO::PARAM_STR);
		if ($this->getEmissor() != "") $stmt->bindParam(++$x,$this->getEmissor(),PDO::PARAM_STR);
		if ($this->getEmissao() != "") $stmt->bindParam(++$x,$this->getEmissao(),PDO::PARAM_STR);
		if ($this->getIdCadastro() != "") $stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM sis_blindados_condutor WHERE id IN({$lista})";
		$sql = "UPDATE sis_blindados_condutor SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE sis_blindados_condutor.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM sis_blindados_condutor
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				sis_blindados_condutor.*
			FROM sis_blindados_condutor
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY sis_blindados_condutor.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM sis_blindados_condutor WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function GetCondutoresCadastro()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM sis_blindados_condutor WHERE id_cadastro = ? order by nome";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdCadastro(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
