<form id="frm_Audio">
			<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="name">* <?=RTL_NAME?></label>
			<input type="text" name="name"  id="name" maxlength="255" class="form-control validar-obrigatorio " value="<?=$linha['name'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="name_in_bucket">* <?=RTL_NAME_IN_BUCKET?></label>
			<input type="text" name="name_in_bucket"  id="name_in_bucket" maxlength="255" class="form-control validar-obrigatorio " value="<?=$linha['name_in_bucket'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="transcription">* <?=RTL_TRANSCRIPTION?></label>
			<textarea class="form-control validar-obrigatorio " name="transcription"  id="transcription" placeholder="Insisra o texto" ><?=$linha['transcription'];?></textarea>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="json">* <?=RTL_JSON?></label>
			<textarea class="form-control validar-obrigatorio " name="json"  id="json" placeholder="Insisra o texto" ><?=$linha['json'];?></textarea>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="created_at">* <?=RTL_CREATED_AT?></label>
			<input type="text" name="created_at"  id="created_at"  class="form-control validar-obrigatorio mask-datetime" value="<?=$linha['created_at'];?>"/>
		</div>
	</div>
</form>
<?
include_once("modulos/Audio/template/js.frm.Audio.php");
?>
