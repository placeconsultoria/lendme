<?
class Audio
{
	private $id;
	private $name;
	private $name_in_bucket;
	private $transcription;
	private $json;
	private $created_at;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setName($arg)
	{
		$this->name = $arg;
	}
 	
	public function getName()
	{
		return $this->name;
	}
 	
	public function setNameInBucket($arg)
	{
		$this->name_in_bucket = $arg;
	}
 	
	public function getNameInBucket()
	{
		return $this->name_in_bucket;
	}
 	
	public function setTranscription($arg)
	{
		$this->transcription = $arg;
	}
 	
	public function getTranscription()
	{
		return $this->transcription;
	}
 	
	public function setJson($arg)
	{
		$this->json = $arg;
	}
 	
	public function getJson()
	{
		return $this->json;
	}
 	
	public function setCreatedAt($arg)
	{
		$this->created_at = $arg;
	}
 	
	public function getCreatedAt()
	{
		return $this->created_at;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = '
		INSERT INTO Audio SET 
			id = ?';
		if ($this->getName() != "") $sql .= ",name = ?";
		if ($this->getNameInBucket() != "") $sql .= ",name_in_bucket = ?";
		if ($this->getTranscription() != "") $sql .= ",transcription = ?";
		if ($this->getJson() != "") $sql .= ",json = ?";
		if ($this->getCreatedAt() != "") $sql .= ",created_at = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getName() != "") $stmt->bindParam(++$x,$this->getName(),PDO::PARAM_STR);
		if ($this->getNameInBucket() != "") $stmt->bindParam(++$x,$this->getNameInBucket(),PDO::PARAM_STR);
		if ($this->getTranscription() != "") $stmt->bindParam(++$x,$this->getTranscription(),PDO::PARAM_STR);
		if ($this->getJson() != "") $stmt->bindParam(++$x,$this->getJson(),PDO::PARAM_STR);
		if ($this->getCreatedAt() != "") $stmt->bindParam(++$x,$this->getCreatedAt(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE Audio SET id = ?';
		if ($this->getName() != "") $sql .= ",name = ?";
		if ($this->getNameInBucket() != "") $sql .= ",name_in_bucket = ?";
		if ($this->getTranscription() != "") $sql .= ",transcription = ?";
		if ($this->getJson() != "") $sql .= ",json = ?";
		if ($this->getCreatedAt() != "") $sql .= ",created_at = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getName() != "") $stmt->bindParam(++$x,$this->getName(),PDO::PARAM_STR);
		if ($this->getNameInBucket() != "") $stmt->bindParam(++$x,$this->getNameInBucket(),PDO::PARAM_STR);
		if ($this->getTranscription() != "") $stmt->bindParam(++$x,$this->getTranscription(),PDO::PARAM_STR);
		if ($this->getJson() != "") $stmt->bindParam(++$x,$this->getJson(),PDO::PARAM_STR);
		if ($this->getCreatedAt() != "") $stmt->bindParam(++$x,$this->getCreatedAt(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM Audio WHERE id IN({$lista})";
		$sql = "UPDATE Audio SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE Audio.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM Audio
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				Audio.*
			FROM Audio
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY Audio.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM Audio WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}


    public function ListAllResumoDashboard()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id, name, transcription, created_at FROM Audio  ORDER BY ID DESC";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function ListarAudioUnic()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM Audio WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function verificaPalavra($palavra){
        $pdo = $this->getConexao();
        $palavra2 = utf8_encode($palavra);
        $palavra3 = utf8_decode($palavra);
        $sql = "SELECT count(id) as total FROM Audio WHERE id = ? and (transcription LIKE '%$palavra%' OR transcription LIKE '%$palavra2%' OR transcription LIKE '%$palavra3%') ";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        $tot =  $stmt->fetch();
        return  $tot["total"];
    }



}
