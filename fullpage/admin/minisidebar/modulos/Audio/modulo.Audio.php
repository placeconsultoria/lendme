<?

switch($app_comando)
{
	case "frm_adicionar_Audio":
		$template = "tpl.frm.Audio.php";
		break;

	case "adicionar_Audio":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objAudio = new Audio($pdo);
			$objAudio->setName($_REQUEST['name']);
			$objAudio->setNameInBucket($_REQUEST['name_in_bucket']);
			$objAudio->setTranscription($_REQUEST['transcription']);
			$objAudio->setJson($_REQUEST['json']);
			$objAudio->setCreatedAt($_REQUEST['created_at']);
			$novoId = $objAudio->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.Audio.php";
		break;

	case "frm_atualizar_Audio" :
		$Audio = new Audio();
		$Audio->setId($_REQUEST["app_codigo"]);
		$linha = $Audio->Editar();
		$template = "tpl.frm.Audio.php";
		break;

	case "atualizar_Audio":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objAudio = new Audio($pdo);
			$objAudio->setId($_REQUEST['id']);
			$objAudio->setName($_REQUEST['name']);
			$objAudio->setNameInBucket($_REQUEST['name_in_bucket']);
			$objAudio->setTranscription($_REQUEST['transcription']);
			$objAudio->setJson($_REQUEST['json']);
			$objAudio->setCreatedAt($_REQUEST['created_at']);
			$objAudio->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.Audio.php";
		break;

	case "listar_Audio":
		$template = "tpl.geral.Audio.php";
		break;

	case "deletar_Audio":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objAudio = new Audio($pdo);
			$objAudio->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.Audio.php";
		break;

	case "ajax_listar_Audio":
		$template = "tpl.lis.Audio.php";
		break;

	case "Audio_pdf":
		$template = "tpl.lis.Audio.pdf.php";
		break;

	case "Audio_xlsx":
		$template = "tpl.lis.Audio.xlsx.php";
		break;

	case "Audio_print":
		$template = "tpl.lis.Audio.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["Audio"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("Audio");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.Audio.php";
		break;
}
