<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/voce_sabia_site/classe.voce_sabia_site.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{

	case "cad_voce_sabia_site":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objVoceSabiaSite = new VoceSabiaSite($pdo);
			$objVoceSabiaSite->setIdProduto($_REQUEST['id_produto']);
			$objVoceSabiaSite->setTexto($_REQUEST['texto']);
			$novoId = $objVoceSabiaSite->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Você sabia cadastrado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Você sabia editado com sucesso!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.voce_sabia_site.php";
		break;



	case "atualizar_voce_sabia_site":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objVoceSabiaSite = new VoceSabiaSite($pdo);
			$objVoceSabiaSite->setId($_REQUEST['id']);
			$objVoceSabiaSite->setIdProduto($_REQUEST['id_produto']);
			$objVoceSabiaSite->setTexto($_REQUEST['texto']);
			$objVoceSabiaSite->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.voce_sabia_site.php";
		break;



	case "deletar_voce_sabia":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objVoceSabiaSite = new VoceSabiaSite($pdo);
			$objVoceSabiaSite->setId($_REQUEST["id"]);
			$objVoceSabiaSite->Remover();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.voce_sabia_site.php";
		break;


}
