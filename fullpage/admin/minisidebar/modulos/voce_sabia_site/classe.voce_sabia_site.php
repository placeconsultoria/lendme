<?
class VoceSabiaSite
{
	private $id;
	private $id_produto;
	private $texto;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdProduto($arg)
	{
		$this->id_produto = $arg;
	}
 	
	public function getIdProduto()
	{
		return $this->id_produto;
	}
 	
	public function setTexto($arg)
	{
		$this->texto = $arg;
	}
 	
	public function getTexto()
	{
		return $this->texto;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO voce_sabia_site SET ';
		if ($this->getIdProduto() != "") $sql .= " id_produto = ?";
		if ($this->getTexto() != "") $sql .= ",texto = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdProduto() != "") $stmt->bindParam(++$x,$this->getIdProduto(),PDO::PARAM_INT);
		if ($this->getTexto() != "") $stmt->bindParam(++$x,$this->getTexto(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE voce_sabia_site SET ';
		if ($this->getIdProduto() != "") $sql .= "id_produto = ?";
		if ($this->getTexto() != "") $sql .= ",texto = ?";
        $sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getIdProduto() != "") $stmt->bindParam(++$x,$this->getIdProduto(),PDO::PARAM_INT);
		if ($this->getTexto() != "") $stmt->bindParam(++$x,$this->getTexto(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover()
	{
		$pdo = $this->getConexao();
		$sql = "DELETE FROM voce_sabia_site WHERE id = ?";
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE voce_sabia_site.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM voce_sabia_site
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				voce_sabia_site.*
			FROM voce_sabia_site
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY voce_sabia_site.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM voce_sabia_site WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function ListVoceSabiaProduto()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM voce_sabia_site WHERE id_produto = ? order by id asc";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdProduto(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
