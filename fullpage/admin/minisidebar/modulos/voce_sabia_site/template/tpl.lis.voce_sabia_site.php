<?php

define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/voce_sabia_site/classe.voce_sabia_site.php");

$objVoceSabiaSite = NEW VoceSabiaSite();
$objVoceSabiaSite->setIdProduto($_GET["id_produto"]);
$resul_voce_sabia_site = $objVoceSabiaSite->ListVoceSabiaProduto();

?>

<div class="col-md-12">
    <div class="table-responsive">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th><small>#</small></th>
                <th><small>TEXTO</small></th>
                <th><small>AÇÕES</small></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $cont = 0;
            if(count($resul_voce_sabia_site) > 0){
                foreach($resul_voce_sabia_site as $linha){
                    $cont++;
                    $id = $linha["id"];
                    echo '<tr>
                                <td>'.$cont.'</td>
                                <td>'.$linha["texto"].'</td>
                                <td><a href="'.$id.'" class="btn btn-danger" name="deletar_voce_sabia_site"> DELETAR <i class="fa fa-trash"></i></a> </td>
                          </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>

<script>
    $(document).ready(function(){

        $('a[name="deletar_voce_sabia_site"]').click(function (event) {
            event.preventDefault();
            var id = $(this).attr("href");
            swal("AGUARDE","ESTAMOS PROCESSANDO...");
            var url = "modulos/voce_sabia_site/modulo.voce_sabia_site.php?acao=deletar_voce_sabia&id="+id;
            $.post(url,
                // pegando resposta do retorno do post
                function (response)
                {

                    if (response["codigo"] == 0) {
                        swal("SUCESSO!","Item excluido com sucesso!");
                        load_conteudo_voce_sabia();

                    } else {
                        swal("ERRO!","Detalhe: "+response["debug"],"error");
                    }
                }
                , "json" // definindo retorno para o formato json
            );

        });

        function load_conteudo_voce_sabia(){
            $("#load_voce_sabia_site").load("modulos/voce_sabia_site/template/tpl.lis.voce_sabia_site.php?id_produto=<?= $_GET["id_produto"] ?>");
        }

    });
</script>



