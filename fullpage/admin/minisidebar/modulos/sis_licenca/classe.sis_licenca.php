<?
class SisLicenca
{
	private $id;
	private $id_cadastro;
	private $civil_licenca;
	private $civil_protocolo;
	private $civil_data_protocolo;
	private $civil_numero_licenca;
	private $civil_validade_licenca;
	private $civil_valor_licenca;
	private $eb_licenca;
	private $eb_protocolo_cr;
	private $eb_data_protocolo_cr;
	private $eb_licenca_numero;
	private $eb_validade_cr;
	private $eb_valor_cr;
	private $detran_entrada;
	private $detran_expedicao;
	private $detran_valor;
	private $inmmetro_inspecao_validade;
	private $inmmetro_expedicao;
	private $inmmetro_expedicao_valor;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdCadastro($arg)
	{
		$this->id_cadastro = $arg;
	}
 	
	public function getIdCadastro()
	{
		return $this->id_cadastro;
	}
 	
	public function setCivilLicenca($arg)
	{
		$this->civil_licenca = $arg;
	}
 	
	public function getCivilLicenca()
	{
		return $this->civil_licenca;
	}
 	
	public function setCivilProtocolo($arg)
	{
		$this->civil_protocolo = $arg;
	}
 	
	public function getCivilProtocolo()
	{
		return $this->civil_protocolo;
	}
 	
	public function setCivilDataProtocolo($arg)
	{
		$this->civil_data_protocolo = $arg;
	}
 	
	public function getCivilDataProtocolo()
	{
		return $this->civil_data_protocolo;
	}
 	
	public function setCivilNumeroLicenca($arg)
	{
		$this->civil_numero_licenca = $arg;
	}
 	
	public function getCivilNumeroLicenca()
	{
		return $this->civil_numero_licenca;
	}
 	
	public function setCivilValidadeLicenca($arg)
	{
		$this->civil_validade_licenca = $arg;
	}
 	
	public function getCivilValidadeLicenca()
	{
		return $this->civil_validade_licenca;
	}
 	
	public function setCivilValorLicenca($arg)
	{
		$this->civil_valor_licenca = $arg;
	}
 	
	public function getCivilValorLicenca()
	{
		return $this->civil_valor_licenca;
	}
 	
	public function setEbLicenca($arg)
	{
		$this->eb_licenca = $arg;
	}
 	
	public function getEbLicenca()
	{
		return $this->eb_licenca;
	}
 	
	public function setEbProtocoloCr($arg)
	{
		$this->eb_protocolo_cr = $arg;
	}
 	
	public function getEbProtocoloCr()
	{
		return $this->eb_protocolo_cr;
	}
 	
	public function setEbDataProtocoloCr($arg)
	{
		$this->eb_data_protocolo_cr = $arg;
	}
 	
	public function getEbDataProtocoloCr()
	{
		return $this->eb_data_protocolo_cr;
	}
 	
	public function setEbLicencaNumero($arg)
	{
		$this->eb_licenca_numero = $arg;
	}
 	
	public function getEbLicencaNumero()
	{
		return $this->eb_licenca_numero;
	}
 	
	public function setEbValidadeCr($arg)
	{
		$this->eb_validade_cr = $arg;
	}
 	
	public function getEbValidadeCr()
	{
		return $this->eb_validade_cr;
	}
 	
	public function setEbValorCr($arg)
	{
		$this->eb_valor_cr = $arg;
	}
 	
	public function getEbValorCr()
	{
		return $this->eb_valor_cr;
	}
 	
	public function setDetranEntrada($arg)
	{
		$this->detran_entrada = $arg;
	}
 	
	public function getDetranEntrada()
	{
		return $this->detran_entrada;
	}
 	
	public function setDetranExpedicao($arg)
	{
		$this->detran_expedicao = $arg;
	}
 	
	public function getDetranExpedicao()
	{
		return $this->detran_expedicao;
	}
 	
	public function setDetranValor($arg)
	{
		$this->detran_valor = $arg;
	}
 	
	public function getDetranValor()
	{
		return $this->detran_valor;
	}
 	
	public function setInmmetroInspecaoValidade($arg)
	{
		$this->inmmetro_inspecao_validade = $arg;
	}
 	
	public function getInmmetroInspecaoValidade()
	{
		return $this->inmmetro_inspecao_validade;
	}
 	
	public function setInmmetroExpedicao($arg)
	{
		$this->inmmetro_expedicao = $arg;
	}
 	
	public function getInmmetroExpedicao()
	{
		return $this->inmmetro_expedicao;
	}
 	
	public function setInmmetroExpedicaoValor($arg)
	{
		$this->inmmetro_expedicao_valor = $arg;
	}
 	
	public function getInmmetroExpedicaoValor()
	{
		return $this->inmmetro_expedicao_valor;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO sis_licenca SET ';
		if ($this->getIdCadastro() != "") $sql .= "id_cadastro = ?";
		if ($this->getCivilLicenca() != "") $sql .= ",civil_licenca = ?";
		if ($this->getCivilProtocolo() != "") $sql .= ",civil_protocolo = ?";
		if ($this->getCivilDataProtocolo() != "") $sql .= ",civil_data_protocolo = ?";
		if ($this->getCivilNumeroLicenca() != "") $sql .= ",civil_numero_licenca = ?";
		if ($this->getCivilValidadeLicenca() != "") $sql .= ",civil_validade_licenca = ?";
		if ($this->getCivilValorLicenca() != "") $sql .= ",civil_valor_licenca = ?";
		if ($this->getEbLicenca() != "") $sql .= ",eb_licenca = ?";
		if ($this->getEbProtocoloCr() != "") $sql .= ",eb_protocolo_cr = ?";
		if ($this->getEbDataProtocoloCr() != "") $sql .= ",eb_data_protocolo_cr = ?";
		if ($this->getEbLicencaNumero() != "") $sql .= ",eb_licenca_numero = ?";
		if ($this->getEbValidadeCr() != "") $sql .= ",eb_validade_cr = ?";
		if ($this->getEbValorCr() != "") $sql .= ",eb_valor_cr = ?";
		if ($this->getDetranEntrada() != "") $sql .= ",detran_entrada = ?";
		if ($this->getDetranExpedicao() != "") $sql .= ",detran_expedicao = ?";
		if ($this->getDetranValor() != "") $sql .= ",detran_valor = ?";
		if ($this->getInmmetroInspecaoValidade() != "") $sql .= ",inmmetro_inspecao_validade = ?";
		if ($this->getInmmetroExpedicao() != "") $sql .= ",inmmetro_expedicao = ?";
		if ($this->getInmmetroExpedicaoValor() != "") $sql .= ",inmmetro_expedicao_valor = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdCadastro() != "") $stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_INT);
		if ($this->getCivilLicenca() != "") $stmt->bindParam(++$x,$this->getCivilLicenca(),PDO::PARAM_STR);
		if ($this->getCivilProtocolo() != "") $stmt->bindParam(++$x,$this->getCivilProtocolo(),PDO::PARAM_STR);
		if ($this->getCivilDataProtocolo() != "") $stmt->bindParam(++$x,$this->getCivilDataProtocolo(),PDO::PARAM_STR);
		if ($this->getCivilNumeroLicenca() != "") $stmt->bindParam(++$x,$this->getCivilNumeroLicenca(),PDO::PARAM_STR);
		if ($this->getCivilValidadeLicenca() != "") $stmt->bindParam(++$x,$this->getCivilValidadeLicenca(),PDO::PARAM_STR);
		if ($this->getCivilValorLicenca() != "") $stmt->bindParam(++$x,$this->getCivilValorLicenca(),PDO::PARAM_STR);
		if ($this->getEbLicenca() != "") $stmt->bindParam(++$x,$this->getEbLicenca(),PDO::PARAM_STR);
		if ($this->getEbProtocoloCr() != "") $stmt->bindParam(++$x,$this->getEbProtocoloCr(),PDO::PARAM_STR);
		if ($this->getEbDataProtocoloCr() != "") $stmt->bindParam(++$x,$this->getEbDataProtocoloCr(),PDO::PARAM_STR);
		if ($this->getEbLicencaNumero() != "") $stmt->bindParam(++$x,$this->getEbLicencaNumero(),PDO::PARAM_STR);
		if ($this->getEbValidadeCr() != "") $stmt->bindParam(++$x,$this->getEbValidadeCr(),PDO::PARAM_STR);
		if ($this->getEbValorCr() != "") $stmt->bindParam(++$x,$this->getEbValorCr(),PDO::PARAM_STR);
		if ($this->getDetranEntrada() != "") $stmt->bindParam(++$x,$this->getDetranEntrada(),PDO::PARAM_STR);
		if ($this->getDetranExpedicao() != "") $stmt->bindParam(++$x,$this->getDetranExpedicao(),PDO::PARAM_STR);
		if ($this->getDetranValor() != "") $stmt->bindParam(++$x,$this->getDetranValor(),PDO::PARAM_STR);
		if ($this->getInmmetroInspecaoValidade() != "") $stmt->bindParam(++$x,$this->getInmmetroInspecaoValidade(),PDO::PARAM_STR);
		if ($this->getInmmetroExpedicao() != "") $stmt->bindParam(++$x,$this->getInmmetroExpedicao(),PDO::PARAM_STR);
		if ($this->getInmmetroExpedicaoValor() != "") $stmt->bindParam(++$x,$this->getInmmetroExpedicaoValor(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE sis_licenca SET';
		if ($this->getIdCadastro() != "") $sql .= "id_cadastro = ?";
		if ($this->getCivilLicenca() != "") $sql .= ",civil_licenca = ?";
		if ($this->getCivilProtocolo() != "") $sql .= ",civil_protocolo = ?";
		if ($this->getCivilDataProtocolo() != "") $sql .= ",civil_data_protocolo = ?";
		if ($this->getCivilNumeroLicenca() != "") $sql .= ",civil_numero_licenca = ?";
		if ($this->getCivilValidadeLicenca() != "") $sql .= ",civil_validade_licenca = ?";
		if ($this->getCivilValorLicenca() != "") $sql .= ",civil_valor_licenca = ?";
		if ($this->getEbLicenca() != "") $sql .= ",eb_licenca = ?";
		if ($this->getEbProtocoloCr() != "") $sql .= ",eb_protocolo_cr = ?";
		if ($this->getEbDataProtocoloCr() != "") $sql .= ",eb_data_protocolo_cr = ?";
		if ($this->getEbLicencaNumero() != "") $sql .= ",eb_licenca_numero = ?";
		if ($this->getEbValidadeCr() != "") $sql .= ",eb_validade_cr = ?";
		if ($this->getEbValorCr() != "") $sql .= ",eb_valor_cr = ?";
		if ($this->getDetranEntrada() != "") $sql .= ",detran_entrada = ?";
		if ($this->getDetranExpedicao() != "") $sql .= ",detran_expedicao = ?";
		if ($this->getDetranValor() != "") $sql .= ",detran_valor = ?";
		if ($this->getInmmetroInspecaoValidade() != "") $sql .= ",inmmetro_inspecao_validade = ?";
		if ($this->getInmmetroExpedicao() != "") $sql .= ",inmmetro_expedicao = ?";
		if ($this->getInmmetroExpedicaoValor() != "") $sql .= ",inmmetro_expedicao_valor = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdCadastro() != "") $stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_INT);
		if ($this->getCivilLicenca() != "") $stmt->bindParam(++$x,$this->getCivilLicenca(),PDO::PARAM_STR);
		if ($this->getCivilProtocolo() != "") $stmt->bindParam(++$x,$this->getCivilProtocolo(),PDO::PARAM_STR);
		if ($this->getCivilDataProtocolo() != "") $stmt->bindParam(++$x,$this->getCivilDataProtocolo(),PDO::PARAM_STR);
		if ($this->getCivilNumeroLicenca() != "") $stmt->bindParam(++$x,$this->getCivilNumeroLicenca(),PDO::PARAM_STR);
		if ($this->getCivilValidadeLicenca() != "") $stmt->bindParam(++$x,$this->getCivilValidadeLicenca(),PDO::PARAM_STR);
		if ($this->getCivilValorLicenca() != "") $stmt->bindParam(++$x,$this->getCivilValorLicenca(),PDO::PARAM_STR);
		if ($this->getEbLicenca() != "") $stmt->bindParam(++$x,$this->getEbLicenca(),PDO::PARAM_STR);
		if ($this->getEbProtocoloCr() != "") $stmt->bindParam(++$x,$this->getEbProtocoloCr(),PDO::PARAM_STR);
		if ($this->getEbDataProtocoloCr() != "") $stmt->bindParam(++$x,$this->getEbDataProtocoloCr(),PDO::PARAM_STR);
		if ($this->getEbLicencaNumero() != "") $stmt->bindParam(++$x,$this->getEbLicencaNumero(),PDO::PARAM_STR);
		if ($this->getEbValidadeCr() != "") $stmt->bindParam(++$x,$this->getEbValidadeCr(),PDO::PARAM_STR);
		if ($this->getEbValorCr() != "") $stmt->bindParam(++$x,$this->getEbValorCr(),PDO::PARAM_STR);
		if ($this->getDetranEntrada() != "") $stmt->bindParam(++$x,$this->getDetranEntrada(),PDO::PARAM_STR);
		if ($this->getDetranExpedicao() != "") $stmt->bindParam(++$x,$this->getDetranExpedicao(),PDO::PARAM_STR);
		if ($this->getDetranValor() != "") $stmt->bindParam(++$x,$this->getDetranValor(),PDO::PARAM_STR);
		if ($this->getInmmetroInspecaoValidade() != "") $stmt->bindParam(++$x,$this->getInmmetroInspecaoValidade(),PDO::PARAM_STR);
		if ($this->getInmmetroExpedicao() != "") $stmt->bindParam(++$x,$this->getInmmetroExpedicao(),PDO::PARAM_STR);
		if ($this->getInmmetroExpedicaoValor() != "") $stmt->bindParam(++$x,$this->getInmmetroExpedicaoValor(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM sis_licenca WHERE id IN({$lista})";
		$sql = "UPDATE sis_licenca SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}


	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM sis_licenca WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function VerificaSeTemCadastro()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT count(id) as total FROM sis_licenca WHERE id_cadastro = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdCadastro(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
    }

}
