<?php
$numeroRegistros      = 20000;
$numeroInicioRegistro = 0;
$busca                = $_REQUEST["busca"];

$objSisLicenca = new SisLicenca();
$listar = $objSisLicenca->ListarPaginacao($_SESSION['usuario']['id_grupo'],$numeroRegistros,$numeroInicioRegistro,$busca,$filtro,$ordem);

$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_ID];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CIVIL_LICENCA];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CIVIL_PROTOCOLO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CIVIL_DATA_PROTOCOLO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CIVIL_NUMERO_LICENCA];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CIVIL_VALIDADE_LICENCA];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CIVIL_VALOR_LICENCA];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_EB_LICENCA];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_EB_PROTOCOLO_CR];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_EB_DATA_PROTOCOLO_CR];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_EB_LICENCA_NUMERO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_EB_VALIDADE_CR];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_EB_VALOR_CR];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_DETRAN_ENTRADA];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_DETRAN_EXPEDICAO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_DETRAN_VALOR];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_INMMETRO_INSPECAO_VALIDADE];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_INMMETRO_EXPEDICAO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_INMMETRO_EXPEDICAO_VALOR];

$x = 0;
if (count($listar[0]) > 0) {
	foreach ($listar[0] as $linha) {
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["id"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["civil_licenca"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["civil_protocolo"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["civil_data_protocolo"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["civil_numero_licenca"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["civil_validade_licenca"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["civil_valor_licenca"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["eb_licenca"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["eb_protocolo_cr"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["eb_data_protocolo_cr"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["eb_licenca_numero"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["eb_validade_cr"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["eb_valor_cr"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["detran_entrada"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["detran_expedicao"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["detran_valor"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["inmmetro_inspecao_validade"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["inmmetro_expedicao"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["inmmetro_expedicao_valor"],"class"=> "uppercase"];
		$x++;
	}
}
//Componente::FiltrarRelatorioConfiguracao($dados_coluna, $dados_linha, $_SESSION["configuracao_usuario"]["sis_licenca"]);

$filtro[ROTULO_LISTAGEM] = RTL_SIS_LICENCA;
//$filtro[ROTULO_RELATORIO] = RTL_SIS_LICENCA;
if ($_REQUEST["id_grupo"] > 0) {
	$filtro[ROTULO_GRUPO] = $_REQUEST["nome_grupo"];
}
if ($_REQUEST["id_veiculo"] > 0) {
	$filtro[ROTULO_VEICULO] = $_REQUEST["nome_veiculo"];
}
$filtro[ROTULO_DATA_INICIAL] = $_REQUEST["data_hora_inicio"];
$filtro[ROTULO_DATA_FINAL]   = $_REQUEST["data_hora_fim"];

$tabela                 = new GerarTabelaPrint();
$tabela->buscaAtiva     = false;
$tabela->nome           = "";
$tabela->totalRegistros = $listar[1]->total;
$tabela->dados          = $dados_linha;
$tabela->center         = $center;
$tabela->colunas        = $dados_coluna;
$tabela->botaoAdicionar = false;
$tabela->botao          = false;
$tabela->paginacao      = false;
$tabela->filtro         = $filtro;
echo $tabela->CriarTabela();
