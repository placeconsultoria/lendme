<form id="frm_sis_licenca">
			<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="civil_licenca"> <?=RTL_CIVIL_LICENCA?></label>
			<input type="text" name="civil_licenca"  id="civil_licenca" maxlength="255" class="form-control  " value="<?=$linha['civil_licenca'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="civil_protocolo"> <?=RTL_CIVIL_PROTOCOLO?></label>
			<input type="text" name="civil_protocolo"  id="civil_protocolo" maxlength="100" class="form-control  " value="<?=$linha['civil_protocolo'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="civil_data_protocolo"> <?=RTL_CIVIL_DATA_PROTOCOLO?></label>
			<input type="text" name="civil_data_protocolo"  id="civil_data_protocolo"  class="form-control  mask-date" value="<?=$linha['civil_data_protocolo'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="civil_numero_licenca"> <?=RTL_CIVIL_NUMERO_LICENCA?></label>
			<input type="text" name="civil_numero_licenca"  id="civil_numero_licenca" maxlength="100" class="form-control  " value="<?=$linha['civil_numero_licenca'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="civil_valor_licenca"> <?=RTL_CIVIL_VALOR_LICENCA?></label>
			<input type="text" name="civil_valor_licenca"  id="civil_valor_licenca"  class="form-control  " value="<?=$linha['civil_valor_licenca'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="eb_licenca"> <?=RTL_EB_LICENCA?></label>
			<input type="text" name="eb_licenca"  id="eb_licenca" maxlength="255" class="form-control  " value="<?=$linha['eb_licenca'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="eb_protocolo_cr"> <?=RTL_EB_PROTOCOLO_CR?></label>
			<input type="text" name="eb_protocolo_cr"  id="eb_protocolo_cr" maxlength="100" class="form-control  " value="<?=$linha['eb_protocolo_cr'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="eb_data_protocolo_cr"> <?=RTL_EB_DATA_PROTOCOLO_CR?></label>
			<input type="text" name="eb_data_protocolo_cr"  id="eb_data_protocolo_cr"  class="form-control  mask-date" value="<?=$linha['eb_data_protocolo_cr'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="eb_licenca_numero"> <?=RTL_EB_LICENCA_NUMERO?></label>
			<input type="text" name="eb_licenca_numero"  id="eb_licenca_numero" maxlength="100" class="form-control  " value="<?=$linha['eb_licenca_numero'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="eb_valor_cr"> <?=RTL_EB_VALOR_CR?></label>
			<input type="text" name="eb_valor_cr"  id="eb_valor_cr"  class="form-control  " value="<?=$linha['eb_valor_cr'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="detran_entrada"> <?=RTL_DETRAN_ENTRADA?></label>
			<input type="text" name="detran_entrada"  id="detran_entrada"  class="form-control  mask-date" value="<?=$linha['detran_entrada'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="detran_expedicao"> <?=RTL_DETRAN_EXPEDICAO?></label>
			<input type="text" name="detran_expedicao"  id="detran_expedicao"  class="form-control  mask-date" value="<?=$linha['detran_expedicao'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="detran_valor"> <?=RTL_DETRAN_VALOR?></label>
			<input type="text" name="detran_valor"  id="detran_valor"  class="form-control  " value="<?=$linha['detran_valor'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="inmmetro_expedicao"> <?=RTL_INMMETRO_EXPEDICAO?></label>
			<input type="text" name="inmmetro_expedicao"  id="inmmetro_expedicao"  class="form-control  mask-date" value="<?=$linha['inmmetro_expedicao'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="inmmetro_expedicao_valor"> <?=RTL_INMMETRO_EXPEDICAO_VALOR?></label>
			<input type="text" name="inmmetro_expedicao_valor"  id="inmmetro_expedicao_valor"  class="form-control  " value="<?=$linha['inmmetro_expedicao_valor'];?>"/>
		</div>
	</div>
</form>
<?
include_once("modulos/sis_licenca/template/js.frm.sis_licenca.php");
?>
