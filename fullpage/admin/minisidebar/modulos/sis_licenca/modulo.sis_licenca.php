<?php

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/sis_licenca/classe.sis_licenca.php");
include_once(URL_FILE . "modulos/enderecos/classe.enderecos.php");
$app_comando = $_REQUEST["acao"];


switch($app_comando)
{


	case "gestao_licencas":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {

            $objVerifica = new SisLicenca();
            $objVerifica->setIdCadastro($_REQUEST['id_cadastro']);
            $verifica = $objVerifica->VerificaSeTemCadastro();
            $verifica = $verifica["total"];

			$objSisLicenca = new SisLicenca($pdo);
			$objSisLicenca->setIdCadastro($_REQUEST['id_cadastro']);
			$objSisLicenca->setCivilLicenca($_REQUEST['civil_licenca']);
			$objSisLicenca->setCivilProtocolo($_REQUEST['civil_protocolo']);
			$objSisLicenca->setCivilDataProtocolo($_REQUEST['civil_data_protocolo']);
			$objSisLicenca->setCivilNumeroLicenca($_REQUEST['civil_numero_licenca']);
			$objSisLicenca->setCivilValidadeLicenca($_REQUEST['civil_validade_licenca']);
			$objSisLicenca->setCivilValorLicenca($_REQUEST['civil_valor_licenca']);
			$objSisLicenca->setEbLicenca($_REQUEST['eb_licenca']);
			$objSisLicenca->setEbProtocoloCr($_REQUEST['eb_protocolo_cr']);
			$objSisLicenca->setEbDataProtocoloCr($_REQUEST['eb_data_protocolo_cr']);
			$objSisLicenca->setEbLicencaNumero($_REQUEST['eb_licenca_numero']);
			$objSisLicenca->setEbValidadeCr($_REQUEST['eb_validade_cr']);
			$objSisLicenca->setEbValorCr($_REQUEST['eb_valor_cr']);
			$objSisLicenca->setDetranEntrada($_REQUEST['detran_entrada']);
			$objSisLicenca->setDetranExpedicao($_REQUEST['detran_expedicao']);
			$objSisLicenca->setDetranValor($_REQUEST['detran_valor']);
			$objSisLicenca->setInmmetroInspecaoValidade($_REQUEST['inmmetro_inspecao_validade']);
			$objSisLicenca->setInmmetroExpedicao($_REQUEST['inmmetro_expedicao']);
			$objSisLicenca->setInmmetroExpedicaoValor($_REQUEST['inmmetro_expedicao_valor']);

            if($verifica == 0){
                $novoId = $objSisLicenca->Adicionar();
            }else{
                $objSisLicenca->Modificar();
            }

			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_licenca.php";
		break;



	case "atualizar_sis_licenca":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objSisLicenca = new SisLicenca($pdo);
			$objSisLicenca->setId($_REQUEST['id']);
			$objSisLicenca->setIdCadastro($_REQUEST['id_cadastro']);
			$objSisLicenca->setCivilLicenca($_REQUEST['civil_licenca']);
			$objSisLicenca->setCivilProtocolo($_REQUEST['civil_protocolo']);
			$objSisLicenca->setCivilDataProtocolo($_REQUEST['civil_data_protocolo']);
			$objSisLicenca->setCivilNumeroLicenca($_REQUEST['civil_numero_licenca']);
			$objSisLicenca->setCivilValidadeLicenca($_REQUEST['civil_validade_licenca']);
			$objSisLicenca->setCivilValorLicenca($_REQUEST['civil_valor_licenca']);
			$objSisLicenca->setEbLicenca($_REQUEST['eb_licenca']);
			$objSisLicenca->setEbProtocoloCr($_REQUEST['eb_protocolo_cr']);
			$objSisLicenca->setEbDataProtocoloCr($_REQUEST['eb_data_protocolo_cr']);
			$objSisLicenca->setEbLicencaNumero($_REQUEST['eb_licenca_numero']);
			$objSisLicenca->setEbValidadeCr($_REQUEST['eb_validade_cr']);
			$objSisLicenca->setEbValorCr($_REQUEST['eb_valor_cr']);
			$objSisLicenca->setDetranEntrada($_REQUEST['detran_entrada']);
			$objSisLicenca->setDetranExpedicao($_REQUEST['detran_expedicao']);
			$objSisLicenca->setDetranValor($_REQUEST['detran_valor']);
			$objSisLicenca->setInmmetroInspecaoValidade($_REQUEST['inmmetro_inspecao_validade']);
			$objSisLicenca->setInmmetroExpedicao($_REQUEST['inmmetro_expedicao']);
			$objSisLicenca->setInmmetroExpedicaoValor($_REQUEST['inmmetro_expedicao_valor']);
			$objSisLicenca->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_licenca.php";
		break;



}
