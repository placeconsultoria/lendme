<?php

@session_start();
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
define('URL_FILE',"../../../");
//require_once('inc/inc.sessao.php');
//equire_once('inc/funcoes.php');
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");
$objComboProdutos = NEW ProdutosControlados();
$resulComboProdutos = $objComboProdutos->ComboProdutosControlados();

$objGrupoEmpresa = NEW GrupoEmpresa();
$resulComboGrupo = $objGrupoEmpresa->GetCombo();

$objEstados = NEW Estados();
$resulComboEstados = $objEstados->GerarSelectEstados();


?>


<form id="frm_cad_cadastro_cliente">
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">INFORMAÇÕES COMPLEMENTARES:</span> </h4>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-6">
            <label for="contato_nome_1">GUIA:</label>
            <select name="guia" class="form-control">
                <option value="0"></option>
                <option value="1">BOLETO LICENSER CONSULTORIA</option>
                <option value="2">NOTA FISCAL LICENSER CONSULTORIA</option>
                <option value="3">NOTA FISCAL LICENSER SERVIÇOS</option>
                <option value="4">NOTA FISCAL MAIS  BOLETO LICENSER CONSULTORIA</option>
                <option value="4">NOTA FISCAL MAIS BOLETO LICENSER SERVIÇOS</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="contato_nome_1"> REAJUSTADO EM/DATA:</label>
            <input type="text" name="contato_data_nascimento[]"  id="contato_data_nascimento_1"  class="form-control  mask-date" value="<?=$linha['data_nascimento'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>
        <div class="col-md-3">
            <label for="contato_nome_1">PRÓXIMO REAJUSTE/DATA:</label>
            <input type="text" name="contato_data_nascimento[]"  id="contato_data_nascimento_1"  class="form-control  mask-date" value="<?=$linha['data_nascimento'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>

    </div>
    <div class="row form-group">
        <div class="col-md-3">
            <label for="contato_data_nascimento_1">PROCURAÇÃO VALIDADE:</label>
            <input type="text" name="contato_data_nascimento[]"  id="contato_data_nascimento_1"  class="form-control  mask-date" value="<?=$linha['data_nascimento'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>
        <div class="col-md-3">
            <label for="contato_data_nascimento_1">INDICADO POR:</label>
            <input type="text" name="nome"  id="" maxlength="255" class="form-control  " value="<?=$linha['ewewewew'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="contato_data_nascimento_1">CLIENTE DESTE:</label>
            <input type="text" name="nome"  id="" maxlength="255" class="form-control  " value="<?=$linha['ewewewew'];?>"/>
        </div>
    </div>
    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">OBSERVAÇÕES:</span> </h4>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <label for="contato_nome_1"> Digite alguma informações extra/complementar:</label>
            <textarea name="" rows="3" class="form-control"></textarea>
        </div>
    </div>



    <div class="row form-group">
        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-warning">EDITAR INFORMAÇÕES EXTRAS</button>
        </div>
    </div>





</form>


