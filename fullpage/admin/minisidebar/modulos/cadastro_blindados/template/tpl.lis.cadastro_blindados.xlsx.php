<?php
$numeroRegistros      = 20000;
$numeroInicioRegistro = 0;
$busca                = $_REQUEST["busca"];

$objCadastroBlindados = new CadastroBlindados();
$listar = $objCadastroBlindados->ListarPaginacao($_SESSION['usuario']['id_grupo'],$numeroRegistros,$numeroInicioRegistro,$busca,$filtro,$ordem);

$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_ID];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_PASTA];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CIVIL];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_EXERCITO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_DETRAN];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_INSPECAO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CONCESSAO_REVALIDACAO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_TIPO_CPF_CNPJ];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CLIENTE];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CPF_CNPJ];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_INSCRICAO_ESTADUAL];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CNAE];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_ATIVIDADE_PRINCIPAL];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CONTATO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_TELEFONE];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CELULAR];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_EMAIL];

$x = 0;
if (count($listar[0]) > 0) {
	foreach ($listar[0] as $linha) {
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["id"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["pasta"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["civil"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["exercito"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["detran"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["inspecao"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["concessao_revalidacao"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["tipo_cpf_cnpj"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["cliente"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["cpf_cnpj"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["inscricao_estadual"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["CNAE"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["atividade_principal"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["contato"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["telefone"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["celular"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["email"],"class"=> "uppercase"];
		$x++;
	}
}
//Componente::FiltrarRelatorioConfiguracao($dados_coluna, $dados_linha, $_SESSION["configuracao_usuario"]["cadastro_blindados"]);

$filtro[ROTULO_LISTAGEM] = RTL_CADASTRO_BLINDADOS;
//$filtro[ROTULO_RELATORIO] = RTL_CADASTRO_BLINDADOS;
if ($_REQUEST["id_grupo"] > 0) {
	$filtro[ROTULO_GRUPO] = $_REQUEST["nome_grupo"];
}
if ($_REQUEST["id_veiculo"] > 0) {
	$filtro[ROTULO_VEICULO] = $_REQUEST["nome_veiculo"];
}
$filtro[ROTULO_DATA_INICIAL] = $_REQUEST["data_hora_inicio"];
$filtro[ROTULO_DATA_FINAL]   = $_REQUEST["data_hora_fim"];

$tabela               = new GerarTabelaXml();
$tabela->dados        = $dados_linha;
$tabela->colunas      = $dados_coluna;
$tabela->filtro       = $filtro;
$tabela->logo_cliente = $_SESSION["logo_cliente"];
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment;filename=".RTL_CADASTRO_BLINDADOS.".xls");
header("Cache-Control: max-age=0");
$tabela->CriarTabelaClasse();
