<?php

@session_start();
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
define('URL_FILE',"../../../");
//require_once('inc/inc.sessao.php');
//equire_once('inc/funcoes.php');
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");
$objComboProdutos = NEW ProdutosControlados();
$resulComboProdutos = $objComboProdutos->ComboProdutosControlados();

$objGrupoEmpresa = NEW GrupoEmpresa();
$resulComboGrupo = $objGrupoEmpresa->GetCombo();

$objEstados = NEW Estados();
$resulComboEstados = $objEstados->GerarSelectEstados();


?>


<form id="frm_licencas">
    <input type="hidden" name="acao" value="gestao_licencas" />
    <input type="hidden" name="id_cadastro" value="<?= $_GET["id"] ?>" />

    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">LICENÇA: ALVARÁ POLÍCIA CIVIL</span> </h4>
        </div>
    </div>

    <div class="row form-group pb-0 mb-0">
        <div class="col-md-8">
            <input type="text" name="civil_licenca"  id="" maxlength="255" class="form-control  " value="<?=$linha['civil_licenca'];?>"/>
        </div>
    </div>

    <div class="row form-group p-t-10">
        <div class="col-md-3">
            <label for="civil_protocolo"> PROTOCOLO DA LICENÇA:</label>
            <input type="text" name="civil_protocolo"  id="" maxlength="255" class="form-control  " value="<?=$linha['civil_protocolo'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="civil_data_protocolo">DATA DO PROTOCOLO:</label>
            <input type="text" name="civil_data_protocolo"  id="civil_data_protocolo"  class="form-control  mask-date" value="<?=$linha['civil_data_protocolo'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>
        <div class="col-md-3">
            <label for="civil_numero_licenca"> NÚMERO DA LICENÇA:</label>
            <input type="text" name="civil_numero_licenca"  id="" maxlength="255" class="form-control  " value="<?=$linha['civil_numero_licenca'];?>"/>
        </div>

        <div class="col-md-2">
            <label for="civil_validade_licenca"> VALIDADE DA LICENÇA:</label>
            <input type="text" name="civil_validade_licenca"  id="civil_validade_licenca"  class="form-control" value="<?=$linha['civil_validade_licenca'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>


        <div class="col-md-2">
            <label for="civil_valor_licenca"> VALOR DO LICENÇA:</label>
            <input type="text" name="civil_valor_licenca"  id="civil_valor_licenca"  class="form-control" value="<?=$linha['civil_valor_licenca'];?>" />
        </div>


    </div>
    <hr />

    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-success">LICENÇA EXÉRCITO BRASILEIRO:</span> </h4>
        </div>
    </div>

    <div class="row form-group pb-0 mb-0">
        <div class="col-md-8">
            <input type="text" name="eb_licenca"  id="" maxlength="255" class="form-control  " value="<?=$linha['eb_licenca'];?>"/>
        </div>
    </div>

    <div class="row form-group p-t-10">
        <div class="col-md-3">
            <label for="cr_protocolo"> PROTOCOLO DO CR:</label>
            <input type="text" name="eb_protocolo_cr"  id="" maxlength="255" class="form-control  " value="<?=$linha['eb_protocolo_cr'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="cr_data_protocolo">CR DATA PROTOCOLO:</label>
            <input type="text" name="eb_data_protocolo_cr"  id="eb_data_protocolo_cr"  class="form-control" value="<?=$linha['eb_data_protocolo_cr'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>
        <div class="col-md-3">
            <label for="eb_licenca_numero">LICENÇA CR NÚMERO:</label>
            <input type="text" name="eb_licenca_numero"  id="" maxlength="255" class="form-control  " value="<?=$linha['eb_licenca_numero'];?>"/>
        </div>

        <div class="col-md-2">
            <label for="cr_validade"> VALIDADE DO CR:</label>
            <input type="text" name="eb_validade_cr"  id="eb_validade_cr"  class="form-control  mask-date" value="<?=$linha['eb_validade_cr'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>
        <div class="col-md-2">
            <label for="cr_valor"> VALOR DO CR:</label>
            <input type="text" name="eb_valor_cr"  id="eb_valor_cr"  class="form-control" value="<?=$linha['eb_valor_cr'];?>" />
        </div>
    </div>

    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-info">DETRAN:</span> </h4>
        </div>
    </div>

    <div class="row form-group">

        <div class="col-md-2">
            <label for="detran_entrada"> DETRAN ENTRADA:</label>
            <input type="text" name="detran_entrada"  id="detran_entrada"  class="form-control " value="<?=$linha['detran_entrada'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>
        <div class="col-md-4">
            <label for="detran_expedicao">DETRAN EXPEDIÇÃO:</label>
            <input type="text" name="detran_expedicao"  id="" class="form-control  " value="<?=$linha['detran_expedicao'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10"/>
        </div>

        <div class="col-md-2">
            <label for="detran_valor">DETRAN VALOR:</label>
            <input type="text" name="detran_valor"  id="detran_valor"  class="form-control  mask-date" value="<?=$linha['detran_valor'];?>" />
        </div>
    </div>

    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-warning">INMETRO:</span> </h4>
        </div>
    </div>

    <div class="row form-group">

        <div class="col-md-3">
            <label for="inmmetro_expedicao">INMETRO INSPEÇÃO EXPEDIÇÃO:</label>
            <input type="text" name="inmmetro_expedicao"  id="inmmetro_expedicao"  class="form-control " value="<?=$linha['inmmetro_expedicao'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>
        <div class="col-md-4">
            <label for="inmmetro_inspecao_validade">INMETRO INSPEÇÃO VALIDADE:</label>
            <input type="text" name="inmmetro_inspecao_validade"  id="" maxlength="255" class="form-control  " value="<?=$linha['inmmetro_inspecao_validade'];?>"/>
        </div>

        <div class="col-md-3">
            <label for="inmmetro_inspecao_valor">INMETRO INSPEÇÃO VALOR:</label>
            <input type="text" name="inmmetro_inspecao_valor"  id="inmmetro_inspecao_valor"  class="form-control  " value="<?=$linha['inmmetro_inspecao_valor'];?>" />
        </div>
    </div>

    <hr />

    <div class="row form-group">
        <div class="col-md-12 text-center">
            <button type="submit" class="btn btn-warning">EDITAR LICENÇAS</button>
        </div>
    </div>





</form>


