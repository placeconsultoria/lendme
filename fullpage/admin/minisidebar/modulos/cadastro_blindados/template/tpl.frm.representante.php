<?php

@session_start();
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
define('URL_FILE',"../../../");
//require_once('inc/inc.sessao.php');
//equire_once('inc/funcoes.php');
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");
include_once(URL_FILE . "modulos/cidades/classe.cidades.php");

$objGrupoEmpresa = NEW GrupoEmpresa();
$resulComboGrupo = $objGrupoEmpresa->GetCombo();

$objEstados = NEW Estados();
$resulComboEstados = $objEstados->GerarSelectEstados();

if($resulCadastroBlindado["tipo_cpf_cnpj"] == 2){
    $btn_coppy = '<a href="'.$_GET["id"].'" name="consulta_dados_cpf_proprietario" class="btn btn-inverse btn-sm"><i class="fa fa-copy" ></i></a>';
}else{
    $btn_coppy = '';

}


?>




<div class="col-md-12 text-center">
    <button id="add_representante" name="add_representante" type="button" class="btn btn-inverse">ADICIONAR REPRESENTANTE</button>
</div>

<hr />
<form id="frm_cad_cadastro_representante" style="display: none;">
    <input type="hidden" name="acao" value="adicionar_sis_blindados_representante" />
    <input type="hidden" name="id_cadastro" value="<?= $_GET["id"] ?>" />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">REPRESENTANTE:</span> </h4>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-3">
            <label for="representante_nome"> NOME: <?= $btn_coppy ?></label>
            <input required type="text" name="nome"  id="representante_nome" maxlength="255" class="form-control  " value="<?=$linha['aeaea'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_cpf"> CPF:</label>
            <input required type="text" name="cpf"  id="representante_cpf"onkeyup="mascaraTexto(event,'999.999.999-99')" maxlength="14"  class="form-control " value="<?=$linha['representante_cpf'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_rg"> RG/RNE:</label>
            <input required type="text" name="rg"  id="representante_rg" maxlength="45" class="form-control  " value="<?=$linha['rg'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_emissor"> ORGÃO EMISSOR:</label>
            <input required type="text" name="emissor"  id="representante_emissor" maxlength="100" class="form-control  " value="<?=$linha['orgao_rg'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_data_emissao"> DATA DE EMISSÃO:</label>
            <input required type="text" name="data_emissao"  id="representante_data_emissao"  class="form-control  " value="<?=$linha['cpf'];?>"  onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10"   />
        </div>

        <div class="col-md-1">
            <label for="representante_nascimento"> NASCIMENTO:</label>
            <input required type="text" name="nascimento"  id="representante_nascimento" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" class="form-control  " value="<?=$linha['cpf'];?>"/>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-3">
            <label for="representante_pai">PAI:</label>
            <input required type="text" name="pai"  id="representante_pai" maxlength="255" class="form-control  " value="<?=$linha['pai'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="representante_mae">MÃE:</label>
            <input required type="text" name="mae"  id="representante_mae" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_nacionalidade">NACIONALIDADE:</label>
            <input required type="text" name="nacionalidade"  id="representante_nacionalidade" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_naturalidade">NATURALIDADE:</label>
            <input required type="text" name="naturalidade"  id="representante_naturalidade" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_estado_nascimento">ESTADO NASCIMENTO:</label>
            <select id="representante_estado_nascimento" name="estado_nascimento" class="form-control">
                <?php foreach ($resulComboEstados as $linha){ echo '<option value="'.$linha["id_estado"].'">'.$linha["sigla"].'</option>'; } ?>

            </select>

        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-2">
            <label for="representante_estado_civil">ESTADO CIVIL:</label>
            <select required name="estado_civil" id="representante_estado_civil" class="form-control">
                <option value="0">--SELECIONE--</option>
                <option value="1">CASADO(A)</option>
                <option value="2">DIVORCIADO(A)</option>
                <option value="3">UNIÃO ESTÁVEL</option>
                <option value="4">SOLTEIRO(A)</option>
                <option value="5">VIÚVO(A)</option>
            </select>
        </div>
        <div class="col-md-2">
            <label  for="representante_grau_instrucao"> GRAU DE INSTRUÇÃO:</label>
            <select name="grau_instrucao" id="representante_grau_instrucao" class="form-control">
                <option value="0">--SELECIONE--</option>
                <option value="1">ENSINO FUNDAMENTAL COMPLETO</option>
                <option value="2">ENSINO FUNDAMENTAL INCOMPLETO</option>
                <option value="3">ENSIMO MÉDIO COMPLETO</option>
                <option value="4">ENSIMO MÉDIO INCOMPLETO</option>
                <option value="5">ENSINO SUPERIOR COMPLETO</option>
                <option value="6">ENSINO SUPERIOR INCOMPLETO</option>
            </select>
        </div>
        <div class="col-md-1">
            <label for="representante_profissao"> PROFISSÃO:</label>
            <input required type="text" name="profissao"  id="representante_profissao" maxlength="100" class="form-control  " value="<?=$linha['profissao'];?>"/>
        </div>
        <div class="col-md-1">
            <label for="representante_cargo"> CARGO:</label>
            <input required type="text" name="cargo"  id="representante_cargo" maxlength="100" class="form-control  " value="<?=$linha['profissao'];?>"/>
        </div>

        <div class="col-md-2">
            <label for="representante_telefone"> TELEFONE:</label>
            <input required type="text" name="telefone"  id="representante_telefone" maxlength="100" class="form-control mask_telefone  " value="<?=$linha['grau_instrucao'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_celular"> CELULAR:</label>
            <input required type="text" name="celular"  id="representante_celular" maxlength="100" class="form-control mask-telefone "  value="<?=$linha['grau_instrucao'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_email"> E-MAIL:</label>
            <input  required type="text" name="email"  id="representante_email" maxlength="100" class="form-control  " value="<?=$linha['grau_instrucao'];?>"/>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-1">
            <label for="cep_representante"> CEP:</label>
            <input required type="text" name="cep"  id="cep_representante" onkeyup="mascaraTexto(event,'99999-999')" maxlength="9" class="form-control  " value=""/>
        </div>
        <div class="col-md-3">
            <label for="logradouro_representante"> LOGRADOURO:</label>
            <input required type="text" name="logradouro"  id="logradouro_representante" maxlength="255" class="form-control  " value=""/>
        </div>

        <div class="col-md-1">
            <label for="numero_representante"> NÚMERO:</label>
            <input required type="text" name="numero"  id="numero_representante" maxlength="255" class="form-control  " value=""/>
        </div>

        <div class="col-md-1">
            <label for="complemento_representante"> COMPLEMENTO:</label>
            <input type="text" name="complemento"  id="complemento_representante" maxlength="255" class="form-control  " value=""/>
        </div>


        <div class="col-md-2">
            <label for="bairro_representante"> BAIRRO:</label>
            <input type="text" name="bairro"  id="bairro__representante" maxlength="255" class="form-control  " value=""/>
        </div>

        <div class="col-md-1">
            <label for="id_estado_representante"> ESTADO:</label>
            <select id="id_estado_representante" name="id_estado" class="form-control">
                <option value="0">--</option>
                <?php foreach ($resulComboEstados as $linha){ echo '<option value="'.$linha["id_estado"].'">'.$linha["sigla"].'</option>'; } ?>
            </select>
        </div>
        <div class="col-md-2">
            <label for="id_cidade_representante"> CIDADE:</label>
            <select id="id_cidade_representante" name="id_cidade" class="form-control">
                <option>--SELECIONE UM ESTADO--</option>
            </select>
        </div>

        <div class="col-md-1">
            <label for="caixa_postal_representante">CAIXA POSTAL:</label>
            <input type="text" name="caixa_postal"  id="caixa_postal_representante" maxlength="255" class="form-control  " value=""/>
        </div>

        <div class="clearfix"></div>
        <div class="col-md-12 text-center m-t-40">
            <button id="btn_add_reprensentante" type="submit" class="btn btn-success">ADICIONAR REPRESENTANTE</button>
        </div>

    </div>


    <hr />





</form>


<div id="load_list_representantes">
    <?php include_once("modulos/sis_blindados_representante/template/tpl.lis.sis_blindados_representante.php"); ?>

</div>



