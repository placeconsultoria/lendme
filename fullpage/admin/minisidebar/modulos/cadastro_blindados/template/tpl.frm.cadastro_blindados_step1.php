<?php

@session_start();
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
define('URL_FILE',"../../../");
//require_once('inc/inc.sessao.php');
//equire_once('inc/funcoes.php');
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/grupo_empresa_blindados/classe.grupo_empresa_blindados.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");

$objGrupoEmpresa = NEW GrupoEmpresaBlindados();
$resulComboGrupo = $objGrupoEmpresa->GetCombo();


$objEstados = NEW Estados();
$resulComboEstados = $objEstados->GerarSelectEstados();


?>


<form id="frm_cad_cadastro_blindado">
    <input type="hidden" name="acao" value="adicionar_cadastro_blindados" />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h5><span class="badge badge-inverse">INFORMAÇÕES INICIAIS PARA CONTROLE INTERNO:</span></h5>
        </div>
    </div>
    <input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
    <div class="row form-group">


        <div class="col-md-3">
            <div class="form-group pb-0 mb-0 row">
                <label for="pasta" class="col-6 col-form-label">CONTROLE: <span class="load_pasta"></span> </label>
                <div class="col-4 text-left">
                    <input id="pasta" class="form-control" name="pasta" type="text" required value="" >
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group pb-0 mb-0 row">
                <label for="pasta_antiga" class="col-6 col-form-label">PASTA ANTIGA: <span class="load_pasta_antiga"></span></label>
                <div class="col-4 text-left">
                    <input id="pasta_antiga" class="form-control" name="pasta_antiga" type="text"  value="" >
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group pb-0 mb-0 row">
                <label for="grupo" class="col-5 col-form-label">GRUPO/FAMÍLIA:</label>
                <div class="col-7 text-left">
                    <select  name="id_grupo" id="id_grupo" class="form-control">
                        <option value="0">--SELECIONE--</option>
                        <?php foreach ($resulComboGrupo as $linha){ echo '<option class="text-uppercase" value="'.$linha["id"].'">'.$linha["grupo"].'</option>'; } ?>

                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <button name="btn_add_novo_grupo" type="button" class="btn btn-info">ADICIONAR</button>
        </div>

    </div>


    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">INFORMAÇÕES DO PROPRIETÁRIO:</span> </h4>
        </div>
    </div>

    <hr />

    <div class="row form-group">



        <div class="col-md-6">
            <div class="form-group pb-0 mb-0 row">
                <label for="tipo_cpf_cnpj" class="col-6 col-form-label">SELECIONE O TIPO DO CADASTRO:</label>
                <div class="col-4 text-left">
                    <select required name="tipo_cpf_cnpj" id="tipo_cpf_cnpj" class="form-control">
                        <option VALUE="2" selected>PESSOA FÍSICA</option>
                        <option VALUE="1" >PESSOA JURÍDICA</option>
                    </select>
                </div>
            </div>
        </div>

    </div>

    <hr />

    <div id="load_frm_tipo_cadastro">
        <?php require_once("tpl.frm.cadastro_blindados_step1_cpf.php"); ?>
    </div>

    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">ENDEREÇO DO PROPRIETÁRIO:</span> </h4>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-1">
            <label for="cep"> CEP:</label>
            <input type="text" required name="cep"  id="cep" onkeyup="mascaraTexto(event,'99999-999')" maxlength="9" class="form-control  " value="<?=$linha['cep'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="logradouro"> LOGRADOURO:</label>
            <input type="text" required name="logradouro"  id="logradouro" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
        </div>
        <div class="col-md-1">
            <label for="numero"> NÚMERO:</label>
            <input type="text" required name="numero"  id="numero" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
        </div>
        <div class="col-md-1">
            <label for="complemento"> COMP.:</label>
            <input type="text"  name="complemento"  id="complemento" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="bairro"> BAIRRO:</label>
            <input type="text" required name="bairro"  id="bairro" maxlength="255" class="form-control  " value="<?=$linha['bairro'];?>"/>
        </div>
        <div class="col-md-1">
            <label for="id_estado"> ESTADO:</label>
            <select id="id_estado" name="id_estado" class="form-control">
                <option value="0">--</option>
                <?php foreach ($resulComboEstados as $linha){ echo '<option value="'.$linha["id_estado"].'">'.$linha["sigla"].'</option>'; } ?>
            </select>
        </div>
        <div class="col-md-2">
            <label for="id_cidade"> CIDADE:</label>
            <select id="id_cidade" name="id_cidade" class="form-control">
                <option>--SELECIONE UM ESTADO--</option>
            </select>
        </div>

        <div class="col-md-1">
            <label for="caixa_postal">CAIXA POSTAL:</label>
            <input type="text" name="caixa_postal"  id="caixa_postal" maxlength="255" class="form-control  " value=""/>
        </div>

    </div>
    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">CONTATO DO PROPRIETÁRIO:</span> </h4>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-4">
            <label for="contato"> CONTATO: <a id="copy_name" href="#" class="btn btn-inverse btn-sm"><i class="fa fa-copy"></i> </a> </label>
            <input type="text" required name="contato"  id="contato" maxlength="255" class="form-control  " value="<?=$linha['conato_licenca'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="telefone"> TELEFONE:</label>
            <input type="text" required name="telefone"  id="telefone" maxlength="45" class="form-control mask-telefone  " value="<?=$linha['telefone_licenca'];?>"/>
        </div>
        <div class="col-md-5">
            <label for="email"> E-MAIL:</label>
            <input type="text" required name="email"  id="email" maxlength="255" class="form-control  " value="<?=$linha['email_licenca'];?>"/>
        </div>
    </div>
    <hr />

    <div class="row">
        <div class="col-md-12 text-center">
            <button id="btnsubmit" type="submit" class="btn btn-success">CADASTRAR PRIMEIRA ETAPA</button>
        </div>
    </div>

</form>





