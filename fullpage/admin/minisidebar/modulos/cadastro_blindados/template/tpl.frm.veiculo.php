<?php

@session_start();
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
define('URL_FILE',"../../../");
//require_once('inc/inc.sessao.php');
//equire_once('inc/funcoes.php');
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/sis_blindados_veiculo/classe.sis_blindados_veiculo.php");


$objVeiculo = new SisBlindadosVeiculo();
$objVeiculo->setIdCadastro($_REQUEST['id']);
$resul_veiculos = $objVeiculo->GetInfo();


?>


<form id="frm_veiculo">
    <input type="hidden" name="acao" value="gestao_veiculos" />
    <input type="hidden" name="id_cadastro" value="<?= $_GET["id"] ?>" />

    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">INFORMAÇÕES DO VEÍCULO:</span> </h4>
        </div>
    </div>
    <hr />
    <div class="row form-group">

        <div class="col-md-6">
            <label for="veiculo_marca_modelo"> MARCA/MODELO:</label>
            <input required type="text" name="modelo"  id="veiculo_marca_modelo"  class="form-control " value="<?=$resul_veiculos['modelo'];?>"  />
        </div>
        <div class="col-md-2">
            <label for="veiculo_placa"> PLACA:</label>
            <input required type="text" name="placa"  id="veiculo_placa"  class="form-control  "value="<?=$resul_veiculos['placa'];?>"/>
        </div>
        <div class="col-md-4">
            <label for="veiculo_chassi"> CHASSI:</label>
            <input required type="text" name="chassi"  id="veiculo_chassi" maxlength="255" class="form-control  " value="<?=$resul_veiculos['chassi'];?>"/>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-2">
            <label for="veiculo_renavam"> RENAVAM: <span class="detranload"></span> </label>
            <input type="text" name="renavam"  id="veiculo_renavam" maxlength="255" class="form-control  " value="<?=$resul_veiculos['renavam'];?>"/>
        </div>
        <div class="col-md-4">
            <label for="veiculo_tipo_especie"> TIPO ESPÉCIE:</label>
            <input type="text" name="tipo_especie"  id="veiculo_tipo_especie"  class="form-control  mask-date" value="<?=$resul_veiculos['tipo_especie'];?>"  />
        </div>

        <div class="col-md-1">
            <label for="veiculo_ano"> ANO FAB/MOD:</label>
            <input type="text" name="ano"  id="veiculo_ano"  class="form-control  mask-date" value="<?=$resul_veiculos['ano'];?>" onkeyup="mascaraTexto(event,'9999/9999')" maxlength="9" />
        </div>

        <div class="col-md-3">
            <label for="veiculo_combustivel"> COMBUSTÍVEL:</label>
            <input type="text" name="combustivel"  id="veiculo_combustivel"  class="form-control " value="<?=$resul_veiculos['combustivel'];?>" />
        </div>

        <div class="col-md-1">
            <label for="veiculo_cor"> COR:</label>
            <input type="text" name="cor"  id="veiculo_cor"  class="form-control" value="<?=$resul_veiculos['cor'];?>" />
        </div>

        <div class="col-md-1">
            <label for="veiculo_blindagem">BLINDAGEM:</label>
            <select name="blindagem" id="veiculo_blindagem" class="form-control">
                <option value="0" <? if($resul_veiculos["blindagem"] == 0) echo  "selected"; ?>></option>
                <option value="1" <? if($resul_veiculos["blindagem"] == 1) echo  "selected"; ?>>I</option>
                <option value="2" <? if($resul_veiculos["blindagem"] == 2) echo  "selected"; ?>>II-A</option>
                <option value="3" <? if($resul_veiculos["blindagem"] == 3) echo  "selected"; ?>>II</option>
                <option value="4" <? if($resul_veiculos["blindagem"] == 4) echo  "selected"; ?>>III-A</option>
                <option value="5" <? if($resul_veiculos["blindagem"] == 5) echo  "selected"; ?>>III</option>
                <option value="6" <? if($resul_veiculos["blindagem"] == 6) echo  "selected"; ?>>IV</option>
            </select>
        </div>


    </div>



    <div class="row form-group">
        <div class="col-md-12 text-center">
                <button id="btn_send_form_veiculo" typeof="submit" class="btn btn-warning"><i class="mdi mdi-car"></i> EDITAR VEÍCULO</button>
        </div>
    </div>





</form>



