<?php

@session_start();
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
define('URL_FILE',"../../../");
//require_once('inc/inc.sessao.php');
//equire_once('inc/funcoes.php');
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");
include_once(URL_FILE . "modulos/cidades/classe.cidades.php");

$objGrupoEmpresa = NEW GrupoEmpresa();
$resulComboGrupo = $objGrupoEmpresa->GetCombo();

$objEstados = NEW Estados();
$resulComboEstados = $objEstados->GerarSelectEstados();


?>


<div class="col-md-12 text-center">
    <button id="add_contato_escritorio" name="add_contato_escritorio" type="button" class="btn btn-inverse">ADICIONAR CONTATO DO ESCRITÓRIO</button>
</div>

<form id="frm_cad_contato_escritorio" style="display: none;">
    <input type="hidden" name="acao" value="adicionar_sis_blindados_contato_escritorio" />
    <input type="hidden" name="id_cadastro" value="<?= $_GET["id"] ?>" />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">CONTATO ESCRITÓRIO:</span> </h4>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-3">
            <label for="nome_contato_escritorio"> NOME DO CONTATO:</label>
            <input type="text" name="nome"  id="nome_contato_escritorio" maxlength="255" class="form-control  " value="<?=$linha['ewewewew'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="telefone_contato_escritorio">TELEFONE:</label>
            <input type="text" name="telefone"  id="telefone_contato_escritorio" maxlength="20" class="form-control mask_telefone " value="<?=$linha['telefone'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="celular_contato_escritorio"> CELULAR:</label>
            <input type="text" name="celular"  id="celular_contato_escritorio" maxlength="20" class="form-control mask_telefone  " value="<?=$linha['celular'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="email_contato_escritorio"> CONTATO E-MAIL:</label>
            <input type="text" name="email"  id="email_contato_escritorio" maxlength="255" class="form-control  " value="<?=$linha['email'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="nascimento"> NASCIMENTO:</label>
            <input type="text" name="nascimento"  id="nascimento"  class="form-control  mask-date" value="<?=$linha['data_nascimento'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>


    </div>
    <div class="row form-group">
        <div class="col-md-1">
            <label for="cep_contato_escritorio"> CEP:</label>
            <input type="text" name="cep"  id="cep_contato_escritorio" onkeyup="mascaraTexto(event,'99999-999')" maxlength="9" class="form-control  " value="<?=$linha['cep'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="logradouro_contato_escritorio"> LOGRADOURO:</label>
            <input type="text" name="logradouro"  id="logradouro_contato_escritorio" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
        </div>

        <div class="col-md-1">
            <label for="numero_contato_escritorio"> NÚMERO:</label>
            <input type="text" name="numero"  id="numero_contato_escritorio" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
        </div>

        <div class="col-md-1">
            <label for="complemento_contato_escritorio"> COMPLEMENTO:</label>
            <input type="text" name="complemento"  id="complemento_contato_escritorio" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
        </div>


        <div class="col-md-2">
            <label for="bairro_contato_escritorio"> BAIRRO:</label>
            <input type="text" name="bairro"  id="bairro_contato_escritorio" maxlength="255" class="form-control  " value="<?=$linha['bairro'];?>"/>
        </div>

        <div class="col-md-1">
            <label for="id_estado_contato_escritorio"> ESTADO:</label>
            <select id="id_estado_contato_escritorio" name="id_estado" class="form-control">
                <option value="0">--</option>
                <?php foreach ($resulComboEstados as $linha){ echo '<option value="'.$linha["id_estado"].'">'.$linha["sigla"].'</option>'; } ?>
            </select>
        </div>
        <div class="col-md-2">
            <label for="id_cidade_contato_escritorio"> CIDADE:</label>
            <select id="id_cidade_contato_escritorio" name="id_cidade" class="form-control">
                <option>--SELECIONE UM ESTADO--</option>
            </select>
        </div>

        <div class="col-md-1">
            <label for="caixa_postal_contato_escritorio">CAIXA POSTAL:</label>
            <input type="text" name="caixa_postal"  id="caixa_postal_contato_escritorio" maxlength="255" class="form-control  " value="<?=$linha['caixa_postal'];?>"/>
        </div>


        <div class="clearfix"></div>
        <div class="col-md-12 text-center m-t-40 p-t-20">
           <button id="btn_add_contato_escritorio" type="submit" class="btn btn-success">ADICIONAR CONTATO DO ESCRITÓRIO</button>
        </div>

       






    </div>




    <hr />





</form>





<div id="load_list_contato_escritorio">
    <?php include_once("modulos/sis_blindados_contato_escritorio/template/tpl.lis.sis_blindados_contato_escritorio.php"); ?>

</div>

