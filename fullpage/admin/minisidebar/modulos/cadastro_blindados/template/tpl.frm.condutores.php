<?php

@session_start();
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
define('URL_FILE',"../../../");
//require_once('inc/inc.sessao.php');
//equire_once('inc/funcoes.php');
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");
$objComboProdutos = NEW ProdutosControlados();
$resulComboProdutos = $objComboProdutos->ComboProdutosControlados();

$objGrupoEmpresa = NEW GrupoEmpresa();
$resulComboGrupo = $objGrupoEmpresa->GetCombo();

$objEstados = NEW Estados();
$resulComboEstados = $objEstados->GerarSelectEstados();


?>

<div class="col-md-12 text-center">
    <button id="add_condutor" name="add_condutor" type="button" class="btn btn-inverse">ADICIONAR CONDUTOR</button>
</div>

<hr />



<form id="frm_cad_condutor" style="display: none;">
    <input type="hidden" name="acao" value="adicionar_cadastro_condutores" />
    <input type="hidden" name="id_cadastro" value="<?= $_GET["id"] ?>" />



    <div class="row form-group">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">CONDUTOR:</span>  <a href="<?= $_GET["id"] ?>" name="copy_representante" class="btn btn-inverse"><i class="fa fa-copy"></i> </a> </h4>
        </div>
        <div class="col-md-3">
            <label  for="contudor_nome"> NOME DO CONDUTOR:</label>
            <input required type="text" name="nome"  id="contudor_nome" maxlength="255" class="form-control  " value="<?=$linha['ewewewew'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="contudor_nascimento"> NASCIMENTO:</label>
            <input required type="text" name="nascimento"  id="contudor_nascimento"  class="form-control  mask-date" value="<?=$linha['data_nascimento'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>
        <div class="col-md-3">
            <label for="contudor_pai"> NOME DO PAI:</label>
            <input required type="text" name="pai"  id="contudor_pai" maxlength="255" class="form-control  " value="<?=$linha['ewewewew'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="contudor_mae"> NOME DO MÃE:</label>
            <input required type="text" name="mae"  id="contudor_mae" maxlength="255" class="form-control  " value="<?=$linha['ewewewew'];?>"/>
        </div>


    </div>

    <div class="row form-group">
        <div class="col-md-3">
            <label for="condutor_cpf"> CPF:</label>
            <input required type="text"  name="cpf"  id="condutor_cpf" class="form-control  " value="<?=$resulCadastroBlindado['cpf'];?>"  onkeyup="mascaraTexto(event,'999.999.999-99')" maxlength="14"/>
        </div>
        <div class="col-md-3">
            <label for="condutor_rg">RG/RNE:</label>
            <input required type="text" name="rg"  id="condutor_rg" maxlength="255" class="form-control  " value="<?=$linha['ewewewew'];?>"/>
        </div>

        <div class="col-md-3">
            <label for="condutor_emissor"> ORGÃO EMISSOR:</label>
            <input required type="text" name="emissor"  id="condutor_emissor"  class="form-control " value="<?=$linha['data_nascimento'];?>"  />
        </div>
        <div class="col-md-3">
            <label for="condutor_emissao"> DATA EMISSÃO:</label>
            <input required type="text" name="emissao"  id="condutor_emissao"  class="form-control  mask-date" value="<?=$linha['data_nascimento'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>

    </div>



    <div class="col-md-12 text-center">
        <button id="btn_add_condutor" type="submit" class="btn btn-success">ADICIONAR CONDUTOR</button>
    </div>



    <hr />





</form>


<div id="load_list_condutores">
    <?php include_once("modulos/sis_blindados_condutor/template/tpl.lis.sis_blindados_condutor.php"); ?>

</div>


