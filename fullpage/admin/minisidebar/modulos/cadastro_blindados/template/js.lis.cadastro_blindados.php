<script type="text/javascript">
$(document).ready(function(){
	$('#AdicionarRegistro').click(function()
	{
		DialogFormulario({
			urlConteudo: "index_xml.php?app_modulo=cadastro_blindados&app_comando=frm_adicionar_cadastro_blindados",
			titulo: "<?=RTL_ADICIONAR_CADASTRO_BLINDADOS?>",
			width: "50vw",
			closeable: true,
			botoes: [{
				item: "<button type='button'></button>",
				event: "click",
				btnclass: "btn btn-sm btn-primary",
				btntext:  " <?=ROTULO_SALVAR?>",
				callback: function( event ){ ExecutarCadastroBlindados(event.data, "index_xml.php?app_modulo=cadastro_blindados&app_comando=adicionar_cadastro_blindados"); }
			}]
		});
	});
	$('#ExcluirRegistro').click(function()
	{
		var checked = $("input[name='lista[]']:checked").length;
		if(checked > 0)
		{
			var values = [];
			$.each($("input[name='lista[]']:checked"), function() {
				values.push($(this).val());
			});
			ConfirmBootStrap('<?= TXT_CONFIRME_DELETE_REGISTROS?> <br>ID´s ('+values+')','<?= TXT_ATENCAO?>',values,ExcluirRegistros,'',4);
		}
		else
		{
				toastr.warning('<?= TXT_ERRO_NENHUM_REGISTRO_SELECIONADO?>','<?= TXT_ATENCAO?>');
		}
	});
	$('[data-toggle="tooltip"]').tooltip();
	$("#busca").keypress(function (e) {
		if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
			AtualizarGridCadastroBlindados("",$("#busca").val());
			return false;
		} else {
			return true;
		}
	});
});

function ExcluirRegistro(id)
{
	var ids = [];
	ids.push(id);
	ConfirmBootStrap("<?= TXT_CONFIRME_DELETE_REGISTROS?> \nID´s ("+values+")", "<?= ROTULO_ATENCAO?>", values, ExcluirRegistros,"",4);
}

function ModificarCadastroBlindados(id)
{
	DialogFormulario({
		urlConteudo: "index_xml.php?app_modulo=cadastro_blindados&app_comando=frm_atualizar_cadastro_blindados&app_codigo="+id,
		titulo: "<?=RTL_MODIFICAR_CADASTRO_BLINDADOS?>",
		width: "50vw",
		closeable: true,
		botoes: [{
			item: "<button type='button'></button>",
			event: "click",
			btnclass: "btn btn-sm btn-primary",
			btntext:  " <?=ROTULO_SALVAR?>",
			callback: function( event ){ ExecutarCadastroBlindados(event.data, "index_xml.php?app_modulo=cadastro_blindados&app_comando=atualizar_cadastro_blindados"); }
		}]
	});
}

function ExcluirRegistros(dados)
{
	$.post('index_xml.php?app_modulo=cadastro_blindados&app_comando=deletar_cadastro_blindados',
		{
			registros:dados
		},
		function(response)
		{
			if(response['codigo'] == 0)
			{
				toastr.success('<?=TXT_CONFIRMACAO_DELETE_REGISTROS?>', '<?=ROTULO_SUCESSO?>');
				AtualizarGridCadastroBlindados(0,"");
			}
			else
			{
				toastr.warning('<?=TXT_ERRO_DELETAR_REGISTROS?>', '<?=ROTULO_ATENCAO?>');
			}
		}, 'json'
	);
}
</script>