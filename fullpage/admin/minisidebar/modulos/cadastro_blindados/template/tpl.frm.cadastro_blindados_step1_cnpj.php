<div class="row form-group">

    <div class="col-md-3">
        <label for="cpf_cnpj"> CNPJ: <span class="cnpjload"></span> </label>
        <input type="text" required name="cpf_cnpj"  id="cpf_cnpj" class="form-control  " value="<?=$resulCadastroBlindado['cpf_cnpj'];?>"  onkeyup="mascaraTexto(event,'99.999.999/9999-99')" maxlength="18"/>
    </div>

    <div class="col-md-5">
        <label for="nome">EMPRESA:</label>
        <input type="text" required name="nome"  id="nome" maxlength="255" class="form-control  " value="<?=$resulCadastroBlindado['nome'];?>"/>
    </div>

    <div class="col-md-4">
        <label for="inscricao_estadual"> INSCRIÇÃO ESTADUAL:</label>
        <input type="text" required name="inscricao_estadual"  id="inscricao_estadual" maxlength="100" class="form-control  " value="<?=$resulCadastroBlindado['inscricao_estadual'];?>"/>
    </div>

</div>
<!--<div class="row form-group">-->
<!--    <div class="col-md-9">-->
<!--        <label for="atividade_principal"> ATIVIDADE PRINCIPAL:</label>-->
<!--        <input type="text" required name="atividade_principal"  id="atividade_principal" maxlength="255" class="form-control  " value="--><?//=$resulCadastroBlindado['atividade_principal'];?><!--"/>-->
<!--    </div>-->
<!--    <div class="col-md-3">-->
<!--        <label for="CNAE"> CNAE</label>-->
<!--        <input type="text" required name="CNAE"  id="CNAE" maxlength="45" class="form-control" value="--><?//=$resulCadastroBlindado['cnae'];?><!--"/>-->
<!--    </div>-->
<!--</div>-->


<script>
    $("#cpf_cnpj").change(function () {
        var cnpj = $(this).val();
        $(".cnpjload").html('<i class="fa fa-spin fa-spinner text-warning"></i>');
        $.getJSON("modulos/cadastro_blindados/modulo.cadastro_blindados.php?acao=consulta_cnpj&cnpj=" + cnpj, function (result) {
            var nome_empresa = result.nome_empresa;
            if(nome_empresa != null){
                var options = [];
                $(".cnpjload").html('<i class="fa fa-check text-success"></i>');
                $("#nome").val(nome_empresa);
                $("#inscricao_estadual").val(result.atividade_principal_codigo+" "+result.atividade_principal);
                $("#cep").val(result.endereco_cep);
                $("#logradouro").val(result.logradouro);
                $("#numero").val(result.logradouro_numero);
                $("#complemento").val(result.endereco_complemento);
                $("#bairro").val(result.endereco_bairro);
                var municipio = result.endereco_municipio;
                var id_estado = getEstados(result.endereco_uf);
                if(result.inscricao_estadual == "-"){
                    $("#inscricao_estadual").val("ISENTO");
                }else{
                    $("#inscricao_estadual").val(result.inscricao_estadual);
                }


                $('#id_estado').val(id_estado);

                $.getJSON("modulos/cidades/modulo.cidades.php?acao=filtrar_cidades&app_codigo="+id_estado, function(result2) {
                    options.push('<option value="">-- SELECIONE UMA CIDADE--</option>');



                    for (var i = 0; i < result2.length; i++)
                    {
                        options.push('<option value="', result2[i].id, '">',  result2[i].nome,'</option>');
                    }

                    $("#id_cidade").html(options.join(''));

                    $.getJSON('modulos/cidades/modulo.cidades.php?acao=cidade_id&app_codigo='+municipio, function(result3) {
                        $('#id_cidade').val(result3.id);
                    });
                },'json');


            }else{
                $(".cnpjload").html('<i class="fa fa-close text-danger"></i>');
                swal("ERRO!","Não encontramos este CNPJ","error");
            }

        }, 'json');

    });
</script>
