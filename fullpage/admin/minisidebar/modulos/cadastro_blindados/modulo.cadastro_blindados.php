<?
define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/cadastro_blindados/classe.cadastro_blindados.php");
include_once(URL_FILE . "modulos/enderecos/classe.enderecos.php");
$app_comando = $_REQUEST["acao"];


function xmlToArray($xml, $options = array()) {
    $defaults = array(
        'namespaceSeparator' => ':', // você pode querer que isso seja algo diferente de um cólon
        'attributePrefix' => '@',    // para distinguir entre os nós e os atributos com o mesmo nome
        'alwaysArray' => array(),    // array de tags que devem sempre ser array
        'autoArray' => true,         // só criar arrays para as tags que aparecem mais de uma vez
        'textContent' => '$',        // chave utilizada para o conteúdo do texto de elementos
        'autoText' => true,          // pular chave "textContent" se o nó não tem atributos ou nós filho
        'keySearch' => false,        // pesquisa opcional e substituir na tag e nomes de atributos
        'keyReplace' => false        // substituir valores por valores acima de busca
    );
    $options = array_merge($defaults, $options);
    $namespaces = $xml->getDocNamespaces();
    $namespaces[''] = null; // adiciona namespace base(vazio)

    // Obtém os atributos de todos os namespaces
    $attributesArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
            // Substituir caracteres no nome do atributo
            if ($options['keySearch']) $attributeName =
                str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
            $attributeKey = $options['attributePrefix']
                . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                . $attributeName;
            $attributesArray[$attributeKey] = (string)$attribute;
        }
    }

    // Obtém nós filhos de todos os namespaces
    $tagsArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->children($namespace) as $childXml) {
            // Recursividade em nós filho
            $childArray = xmlToArray($childXml, $options);
            list($childTagName, $childProperties) = each($childArray);

            // Substituir caracteres no nome da tag
            if ($options['keySearch']) $childTagName =
                str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
            // Adiciona um prefixo namespace, se houver
            if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

            if (!isset($tagsArray[$childTagName])) {
                // Só entra com esta chave
                // Testa se as tags deste tipo deve ser sempre matrizes, não importa a contagem de elementos
                $tagsArray[$childTagName] =
                    in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                        ? array($childProperties) : $childProperties;
            } elseif (
                is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                === range(0, count($tagsArray[$childTagName]) - 1)
            ) {
                $tagsArray[$childTagName][] = $childProperties;
            } else {
                $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
            }
        }
    }

    // Obtém o texto do nó
    $textContentArray = array();
    $plainText = trim((string)$xml);
    if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

    $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
        ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

    // Retorna o nó como array
    return array(
        $xml->getName() => $propertiesArray
    );
}


switch($app_comando)
{

    case "get_info":
        $objCadastroBlindados = new CadastroBlindados($pdo);
        $objCadastroBlindados->setId($_REQUEST["id"]);
        $resul = $objCadastroBlindados->getFullInfo();
        echo json_encode($resul);
        break;
    case "consulta_pasta":

        $objCadastroBlindados = new CadastroBlindados($pdo);
        $objCadastroBlindados->setPasta($_REQUEST['pasta']);
        $resul = $objCadastroBlindados->VerificaPasta();
        $total = $resul["total"];

        $msg["total"] = $resul["total"];
        echo json_encode($msg);


        break;

    case "consulta_pasta_antiga":

        $objCadastroBlindados = new CadastroBlindados($pdo);
        $objCadastroBlindados->setPastaAntiga($_REQUEST['pasta_antiga']);
        $resul = $objCadastroBlindados->VerificaPastaAntiga();
        $total = $resul["total"];

        $msg["total"] = $resul["total"];
        echo json_encode($msg);

        break;

    case "editar_cadastro_blindados":

        $pdo = new Conexao();
        $pdo->beginTransaction();
        try {
            $objEnderecos = new Enderecos();
            $objEnderecos->setId($_REQUEST['id_endereco']);
            $objEnderecos->setLogradouro($_REQUEST['logradouro']);
            $objEnderecos->setNumero($_REQUEST['numero']);
            $objEnderecos->setComplemento($_REQUEST['complemento']);
            $objEnderecos->setCep($_REQUEST['cep']);
            $objEnderecos->setBairro($_REQUEST['bairro']);
            $objEnderecos->setIdCidade($_REQUEST['id_cidade']);
            $objEnderecos->setIdEstado($_REQUEST['id_estado']);
            $objEnderecos->Modificar();

            $objCadastroBlindados = new CadastroBlindados($pdo);
            $objCadastroBlindados->setId($_REQUEST['id']);
            $objCadastroBlindados->setPasta($_REQUEST['pasta']);
            $objCadastroBlindados->setPastaAntiga($_REQUEST['pasta_antiga']);
            $objCadastroBlindados->setIdGrupo($_REQUEST['id_grupo']);
            $objCadastroBlindados->setTipoCpfCnpj($_REQUEST['tipo_cpf_cnpj']);
            $objCadastroBlindados->setNome($_REQUEST['nome']);
            $objCadastroBlindados->setCpfCnpj($_REQUEST['cpf_cnpj']);
            $objCadastroBlindados->setInscricaoEstadual($_REQUEST['inscricao_estadual']);
            $objCadastroBlindados->setContato($_REQUEST['contato']);
            $objCadastroBlindados->setTelefone($_REQUEST['telefone']);
            $objCadastroBlindados->setCelular($_REQUEST['celular']);
            $objCadastroBlindados->setEmail($_REQUEST['email']);

            $objCadastroBlindados->setIdEndereco($novoIdEndereco);
            $novoId = $objCadastroBlindados->Modificar_novo();

            $msg["codigo"] = "0";
            $msg["mensagem"] = "Alterado com sucesso";
            $pdo->commit();
        } catch (Exception $e) {
            $msg["codigo"] = 1;
            $msg["mensagem"] = "ERRO";
            $msg["debug"] = $e->getMessage();
            $pdo->rollBack();
        }
        echo json_encode($msg);
        $template = "ajax.cadastro_blindados.php";
        break;

        break;

	case "adicionar_cadastro_blindados":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
            $objEnderecos = new Enderecos();
            $objEnderecos->setLogradouro($_REQUEST['logradouro']);
            $objEnderecos->setNumero($_REQUEST['numero']);
            $objEnderecos->setComplemento($_REQUEST['complemento']);
            $objEnderecos->setCep($_REQUEST['cep']);
            $objEnderecos->setBairro($_REQUEST['bairro']);
            $objEnderecos->setIdCidade($_REQUEST['id_cidade']);
            $objEnderecos->setIdEstado($_REQUEST['id_estado']);
            $objEnderecos->setCaixaPostal($_REQUEST['caixa_postal']);
            $novoIdEndereco = $objEnderecos->Adicionar();

			$objCadastroBlindados = new CadastroBlindados($pdo);
			$objCadastroBlindados->setPasta($_REQUEST['pasta']);
            $objCadastroBlindados->setPastaAntiga($_REQUEST['pasta_antiga']);
			$objCadastroBlindados->setIdGrupo($_REQUEST['id_grupo']);
			$objCadastroBlindados->setTipoCpfCnpj($_REQUEST['tipo_cpf_cnpj']);
			$objCadastroBlindados->setNome($_REQUEST['nome']);
			$objCadastroBlindados->setCpfCnpj($_REQUEST['cpf_cnpj']);
			$objCadastroBlindados->setInscricaoEstadual($_REQUEST['inscricao_estadual']);
			$objCadastroBlindados->setContato($_REQUEST['contato']);
			$objCadastroBlindados->setTelefone($_REQUEST['telefone']);
			$objCadastroBlindados->setCelular($_REQUEST['celular']);
			$objCadastroBlindados->setEmail($_REQUEST['email']);

			$objCadastroBlindados->setIdEndereco($novoIdEndereco);
			$novoId = $objCadastroBlindados->Adicionar_novo();

			$msg["codigo"] = $novoId;
			$msg["mensagem"] = "Cadastrado com sucesso";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.cadastro_blindados.php";
		break;

	case "frm_atualizar_cadastro_blindados" :
		$cadastro_blindados = new CadastroBlindados();
		$cadastro_blindados->setId($_REQUEST["app_codigo"]);
		$linha = $cadastro_blindados->Editar();
		$template = "tpl.frm.cadastro_blindados.php";
		break;

	case "atualizar_cadastro_blindados":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCadastroBlindados = new CadastroBlindados($pdo);
			$objCadastroBlindados->setId($_REQUEST['id']);
			$objCadastroBlindados->setPasta($_REQUEST['pasta']);
			$objCadastroBlindados->setCivil($_REQUEST['civil']);
			$objCadastroBlindados->setExercito($_REQUEST['exercito']);
			$objCadastroBlindados->setDetran($_REQUEST['detran']);
			$objCadastroBlindados->setInspecao($_REQUEST['inspecao']);
			$objCadastroBlindados->setConcessaoRevalidacao($_REQUEST['concessao_revalidacao']);
			$objCadastroBlindados->setTipoCpfCnpj($_REQUEST['tipo_cpf_cnpj']);
			$objCadastroBlindados->setCliente($_REQUEST['cliente']);
			$objCadastroBlindados->setCpfCnpj($_REQUEST['cpf_cnpj']);
			$objCadastroBlindados->setInscricaoEstadual($_REQUEST['inscricao_estadual']);
			$objCadastroBlindados->setCNAE($_REQUEST['CNAE']);
			$objCadastroBlindados->setAtividadePrincipal($_REQUEST['atividade_principal']);
			$objCadastroBlindados->setContato($_REQUEST['contato']);
			$objCadastroBlindados->setTelefone($_REQUEST['telefone']);
			$objCadastroBlindados->setCelular($_REQUEST['celular']);
			$objCadastroBlindados->setEmail($_REQUEST['email']);
			$objCadastroBlindados->setIdEndereco($_REQUEST['id_endereco']);
			$objCadastroBlindados->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.cadastro_blindados.php";
		break;

	case "listar_cadastro_blindados":
		$template = "tpl.geral.cadastro_blindados.php";
		break;

	case "deletar_cadastro_blindados":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCadastroBlindados = new CadastroBlindados($pdo);
			$objCadastroBlindados->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.cadastro_blindados.php";
		break;

	case "ajax_listar_cadastro_blindados":
		$template = "tpl.lis.cadastro_blindados.php";
		break;

	case "cadastro_blindados_pdf":
		$template = "tpl.lis.cadastro_blindados.pdf.php";
		break;

	case "cadastro_blindados_xlsx":
		$template = "tpl.lis.cadastro_blindados.xlsx.php";
		break;

	case "cadastro_blindados_print":
		$template = "tpl.lis.cadastro_blindados.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["cadastro_blindados"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("cadastro_blindados");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.cadastro_blindados.php";
		break;


    case "consulta_cnpj":

        $cnpj = preg_replace("/[^0-9]/", "",$_REQUEST["cnpj"]);
        $url = "https://www.roveri.inf.br/consultas/cnpj.php?cnpj=$cnpj&token=BE35A276-E905-24B7-807B-EE22AF9BDB67";
        $send = file_get_contents($url);
        $xml = simplexml_load_string($send);
        $arrayXML = xmlToArray($xml);

        $nome_empresa = $arrayXML["infCNPJ"]["xNome"];
        $logradouro = $arrayXML["infCNPJ"]["end"]["xLgr"];
        $endereco_numero = $arrayXML["infCNPJ"]["end"]["nro"];
        $endereco_complemento = $arrayXML["infCNPJ"]["end"]["xCpl"];
        $endereco_cep = $arrayXML["infCNPJ"]["end"]["CEP"];
        $endereco_bairro = $arrayXML["infCNPJ"]["end"]["xBairro"];
        $endereco_municipio = $arrayXML["infCNPJ"]["end"]["xMun"];
        $endereco_uf = $arrayXML["infCNPJ"]["end"]["UF"];
        $atividade_principal_codigo = $arrayXML["infCNPJ"]["ativPrinc"]["xCod"];
        $atividade_principal= $arrayXML["infCNPJ"]["ativPrinc"]["xDesc"];


        $endereco_cep = substr($endereco_cep,0,5)."-".substr($endereco_cep,5,3);
        $resposta["nome_empresa"] = $nome_empresa;
        $resposta["logradouro"] = $logradouro;
        $resposta["logradouro_numero"] = $endereco_numero;
        $resposta["endereco_complemento"] = $endereco_complemento;
        $resposta["endereco_cep"] = $endereco_cep;
        $resposta["endereco_bairro"] = $endereco_bairro;
        $resposta["endereco_municipio"] = $endereco_municipio;
        $resposta["endereco_uf"] = $endereco_uf;
        $resposta["atividade_principal_codigo"] = $atividade_principal_codigo;
        $resposta["atividade_principal"] = $atividade_principal;


        $url2 = "https://www.roveri.inf.br/consultas/sintegra.php?cnpj=$cnpj&UF=PR&token=BE35A276-E905-24B7-807B-EE22AF9BDB67";
        $send2 = file_get_contents($url2);
        $xml2 = simplexml_load_string($send2);
        $arrayXML2 = xmlToArray($xml2);

        $resposta["inscricao_estadual"] = substr($arrayXML2["infSintegra"]["IE"],0,7)."-".substr($arrayXML2["infSintegra"]["IE"],8,2);

        echo json_encode($resposta);




        break;

    case "consulta_renavam":

        //$objCadastroBlindados = new CadastroBlindados();
        //$resul_xml = $objCadastroBlindados->api_detran_pr($url);

        $renavam = $_REQUEST["renavam"];
        $url = "https://www.roveri.inf.br/consultas/renavam.php?renavam=$renavam&UF=PR&token=BE35A276-E905-24B7-807B-EE22AF9BDB67";
        $send = file_get_contents($url);
        $xml = simplexml_load_string($send);
        $arrayXML = xmlToArray($xml);

        $marca_modelo = $arrayXML["infVeiculo"]["modelo"];
        $placa = $arrayXML["infVeiculo"]["placa"];
        $chassi = $arrayXML["infVeiculo"]["chassi"];
        $anoFab = $arrayXML["infVeiculo"]["anoFabricacao"];
        $anoMod = $arrayXML["infVeiculo"]["anoModelo"];
        $combustivel =  $arrayXML["infVeiculo"]["combustivel"];
        $cor =  $arrayXML["infVeiculo"]["cor"];
        $especie =  $arrayXML["infVeiculo"]["especie"];
        $tipo =  $arrayXML["infVeiculo"]["tipo"];


        $resposta["marca_modelo"] = $marca_modelo;
        $resposta["placa"] = $placa;
        $resposta["chassi"] = $chassi;
        $resposta["ano_fab"] = $anoFab;
        $resposta["ano_mod"] = $anoMod;
        $resposta["combustivel"] = $combustivel;
        $resposta["cor"] = $cor;
        $resposta["especie"] = $especie;
        $resposta["tipo"] = $tipo;


        echo json_encode($resposta);



        break;
}
