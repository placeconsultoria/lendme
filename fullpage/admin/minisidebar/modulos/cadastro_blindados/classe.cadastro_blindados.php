<?
class CadastroBlindados
{
	private $id;
	private $pasta;
	private $pasta_antiga;
	private $id_grupo;
	private $tipo_cpf_cnpj;
	private $nome;
	private $cpf_cnpj;
	private $inscricao_estadual;
	private $contato;
	private $telefone;
	private $celular;
	private $email;
	private $id_endereco;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setPasta($arg)
	{
		$this->pasta = $arg;
	}
 	
	public function getPasta()
	{
		return $this->pasta;
	}


    public function setPastaAntiga($arg)
    {
        $this->pasta_antiga = $arg;
    }

    public function getPastaAntiga()
    {
        return $this->pasta_antiga;
    }

 	
	public function setIdGrupo($arg)
	{
		$this->id_grupo = $arg;
	}
 	
	public function getIdGrupo()
	{
		return $this->id_grupo;
	}
 	
	public function setCivil($arg)
	{
		$this->civil = $arg;
	}
 	

	public function setTipoCpfCnpj($arg)
	{
		$this->tipo_cpf_cnpj = $arg;
	}
 	
	public function getTipoCpfCnpj()
	{
		return $this->tipo_cpf_cnpj;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setCpfCnpj($arg)
	{
		$this->cpf_cnpj = $arg;
	}
 	
	public function getCpfCnpj()
	{
		return $this->cpf_cnpj;
	}
 	
	public function setInscricaoEstadual($arg)
	{
		$this->inscricao_estadual = $arg;
	}
 	
	public function getInscricaoEstadual()
	{
		return $this->inscricao_estadual;
	}

 	
	public function setContato($arg)
	{
		$this->contato = $arg;
	}
 	
	public function getContato()
	{
		return $this->contato;
	}
 	
	public function setTelefone($arg)
	{
		$this->telefone = $arg;
	}
 	
	public function getTelefone()
	{
		return $this->telefone;
	}
 	
	public function setCelular($arg)
	{
		$this->celular = $arg;
	}
 	
	public function getCelular()
	{
		return $this->celular;
	}
 	
	public function setEmail($arg)
	{
		$this->email = $arg;
	}
 	
	public function getEmail()
	{
		return $this->email;
	}
 	
	public function setIdEndereco($arg)
	{
		$this->id_endereco = $arg;
	}
 	
	public function getIdEndereco()
	{
		return $this->id_endereco;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}


    public function Adicionar_novo()
    {
        $pdo = $this->getConexao();
        $sql = 'INSERT INTO sis_blindados_cadastro SET ';
        if ($this->getPasta() != "") $sql .= " pasta = ?";
        if ($this->getPastaAntiga() != "") $sql .= " ,pasta_antiga = ?";
        if ($this->getIdGrupo() != "") $sql .= ",id_grupo = ?";
        if ($this->getTipoCpfCnpj() != "") $sql .= ",tipo_cpf_cnpj = ?";
        if ($this->getNome() != "") $sql .= ",nome = ?";
        if ($this->getCpfCnpj() != "") $sql .= ",cpf_cnpj = ?";
        if ($this->getInscricaoEstadual() != "") $sql .= ",inscricao_estadual = ?";
        if ($this->getContato() != "") $sql .= ",contato = ?";
        if ($this->getTelefone() != "") $sql .= ",telefone = ?";
        if ($this->getCelular() != "") $sql .= ",celular = ?";
        if ($this->getEmail() != "") $sql .= ",email = ?";
        if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";

        $stmt = $pdo->prepare($sql);
        if ($this->getPasta() != "") $stmt->bindParam(++$x,$this->getPasta(),PDO::PARAM_STR);
        if ($this->getPastaAntiga() != "") $stmt->bindParam(++$x,$this->getPastaAntiga(),PDO::PARAM_STR);
        if ($this->getIdGrupo() != "") $stmt->bindParam(++$x,$this->getIdGrupo(),PDO::PARAM_INT);
        if ($this->getTipoCpfCnpj() != "") $stmt->bindParam(++$x,$this->getTipoCpfCnpj(),PDO::PARAM_INT);
        if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
        if ($this->getCpfCnpj() != "") $stmt->bindParam(++$x,$this->getCpfCnpj(),PDO::PARAM_STR);
        if ($this->getInscricaoEstadual() != "") $stmt->bindParam(++$x,$this->getInscricaoEstadual(),PDO::PARAM_STR);
        if ($this->getContato() != "") $stmt->bindParam(++$x,$this->getContato(),PDO::PARAM_STR);
        if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
        if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
        if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
        if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
        $stmt->execute();
        return $pdo->lastInsertId() ;
    }




    public function Modificar_novo()
    {
        $pdo = $this->getConexao();
        $sql = 'UPDATE sis_blindados_cadastro SET ';
        if ($this->getPasta() != "") $sql .= " pasta = ?";
        if ($this->getPastaAntiga() != "") $sql .= " ,pasta_antiga = ?";
        if ($this->getIdGrupo() != "") $sql .= ",id_grupo = ?";
        if ($this->getTipoCpfCnpj() != "") $sql .= ",tipo_cpf_cnpj = ?";
        if ($this->getNome() != "") $sql .= ",nome = ?";
        if ($this->getCpfCnpj() != "") $sql .= ",cpf_cnpj = ?";
        if ($this->getInscricaoEstadual() != "") $sql .= ",inscricao_estadual = ?";
        if ($this->getContato() != "") $sql .= ",contato = ?";
        if ($this->getTelefone() != "") $sql .= ",telefone = ?";
        if ($this->getCelular() != "") $sql .= ",celular = ?";
        if ($this->getEmail() != "") $sql .= ",email = ?";
        if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";
        $sql .= ' WHERE id = ?';
        $stmt = $pdo->prepare($sql);

        if ($this->getPasta() != "") $stmt->bindParam(++$x,$this->getPasta(),PDO::PARAM_STR);
        if ($this->getPastaAntiga() != "") $stmt->bindParam(++$x,$this->getPastaAntiga(),PDO::PARAM_STR);
        if ($this->getIdGrupo() != "") $stmt->bindParam(++$x,$this->getIdGrupo(),PDO::PARAM_INT);
        if ($this->getTipoCpfCnpj() != "") $stmt->bindParam(++$x,$this->getTipoCpfCnpj(),PDO::PARAM_INT);
        if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
        if ($this->getCpfCnpj() != "") $stmt->bindParam(++$x,$this->getCpfCnpj(),PDO::PARAM_STR);
        if ($this->getInscricaoEstadual() != "") $stmt->bindParam(++$x,$this->getInscricaoEstadual(),PDO::PARAM_STR);
        if ($this->getContato() != "") $stmt->bindParam(++$x,$this->getContato(),PDO::PARAM_STR);
        if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
        if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
        if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
        if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        return $pdo->lastInsertId() ;
    }

	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE cadastro_blindados SET id = ?';
		if ($this->getPasta() != "") $sql .= ",pasta = ?";
		if ($this->getIdGrupo() != "") $sql .= ",id_grupo = ?";
		if ($this->getCivil() != "") $sql .= ",civil = ?";
		if ($this->getExercito() != "") $sql .= ",exercito = ?";
		if ($this->getDetran() != "") $sql .= ",detran = ?";
		if ($this->getInspecao() != "") $sql .= ",inspecao = ?";
		if ($this->getConcessaoRevalidacao() != "") $sql .= ",concessao_revalidacao = ?";
		if ($this->getTipoCpfCnpj() != "") $sql .= ",tipo_cpf_cnpj = ?";
		if ($this->getCliente() != "") $sql .= ",cliente = ?";
		if ($this->getCpfCnpj() != "") $sql .= ",cpf_cnpj = ?";
		if ($this->getInscricaoEstadual() != "") $sql .= ",inscricao_estadual = ?";
		if ($this->getCNAE() != "") $sql .= ",CNAE = ?";
		if ($this->getAtividadePrincipal() != "") $sql .= ",atividade_principal = ?";
		if ($this->getContato() != "") $sql .= ",contato = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getCelular() != "") $sql .= ",celular = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getPasta() != "") $stmt->bindParam(++$x,$this->getPasta(),PDO::PARAM_STR);
		if ($this->getIdGrupo() != "") $stmt->bindParam(++$x,$this->getIdGrupo(),PDO::PARAM_INT);
		if ($this->getCivil() != "") $stmt->bindParam(++$x,$this->getCivil(),PDO::PARAM_INT);
		if ($this->getExercito() != "") $stmt->bindParam(++$x,$this->getExercito(),PDO::PARAM_INT);
		if ($this->getDetran() != "") $stmt->bindParam(++$x,$this->getDetran(),PDO::PARAM_INT);
		if ($this->getInspecao() != "") $stmt->bindParam(++$x,$this->getInspecao(),PDO::PARAM_INT);
		if ($this->getConcessaoRevalidacao() != "") $stmt->bindParam(++$x,$this->getConcessaoRevalidacao(),PDO::PARAM_INT);
		if ($this->getTipoCpfCnpj() != "") $stmt->bindParam(++$x,$this->getTipoCpfCnpj(),PDO::PARAM_INT);
		if ($this->getCliente() != "") $stmt->bindParam(++$x,$this->getCliente(),PDO::PARAM_STR);
		if ($this->getCpfCnpj() != "") $stmt->bindParam(++$x,$this->getCpfCnpj(),PDO::PARAM_STR);
		if ($this->getInscricaoEstadual() != "") $stmt->bindParam(++$x,$this->getInscricaoEstadual(),PDO::PARAM_STR);
		if ($this->getCNAE() != "") $stmt->bindParam(++$x,$this->getCNAE(),PDO::PARAM_STR);
		if ($this->getAtividadePrincipal() != "") $stmt->bindParam(++$x,$this->getAtividadePrincipal(),PDO::PARAM_STR);
		if ($this->getContato() != "") $stmt->bindParam(++$x,$this->getContato(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}


	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM cadastro_blindados WHERE id IN({$lista})";
		$sql = "UPDATE cadastro_blindados SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE cadastro_blindados.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM cadastro_blindados
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				cadastro_blindados.*
			FROM cadastro_blindados
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY cadastro_blindados.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM cadastro_blindados WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function VerificaPasta()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT count(id) as total FROM sis_blindados_cadastro WHERE pasta = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getPasta(),PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function VerificaPastaAntiga()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT count(id) as total FROM sis_blindados_cadastro WHERE pasta_antiga = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getPastaAntiga(),PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function getFullInfo()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT
                    sis_blindados_cadastro.*,
                     enderecos.id as id_endereco, enderecos.logradouro, enderecos.numero, enderecos.complemento, enderecos.cep, enderecos.bairro, enderecos.id_cidade, enderecos.id_estado, enderecos.caixa_postal 
                FROM sis_blindados_cadastro    
                INNER JOIN enderecos ON (sis_blindados_cadastro.id_endereco = enderecos.id)
                WHERE sis_blindados_cadastro.id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function api_detran_pr($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $retValue = curl_exec($ch);
        curl_close($ch);
        return $retValue;
    }



}
