<?
class Certificacao
{
	private $id;
	private $id_departamento;
	private $certificacao;
	private $id_estado;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdDepartamento($arg)
	{
		$this->id_departamento = $arg;
	}
 	
	public function getIdDepartamento()
	{
		return $this->id_departamento;
	}


    public function setIdEstado($arg)
    {
        $this->id_estado = $arg;
    }

    public function getIdEstado()
    {
        return $this->id_estado;
    }

 	
	public function setCertificacao($arg)
	{
		$this->certificacao = $arg;
	}
 	
	public function getCertificacao()
	{
		return $this->certificacao;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO certificacao SET ';
		if ($this->getIdDepartamento() != "") $sql .= " id_departamento = ?";
		if ($this->getCertificacao() != "") $sql .= ",certificacao = ?";
        if ($this->getIdEstado() != "") $sql .= ",id_estado = ?";
        $stmt = $pdo->prepare($sql);
		if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		if ($this->getCertificacao() != "") $stmt->bindParam(++$x,$this->getCertificacao(),PDO::PARAM_STR);
        if ($this->getIdEstado() != "") $stmt->bindParam(++$x,$this->getIdEstado(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE certificacao SET ';
		if ($this->getIdDepartamento() != "") $sql .= "id_departamento = ?";
		if ($this->getCertificacao() != "") $sql .= ",certificacao = ?";
        if ($this->getIdEstado() != "") $sql .= ",id_estado = ?";
        $sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		if ($this->getCertificacao() != "") $stmt->bindParam(++$x,$this->getCertificacao(),PDO::PARAM_STR);
        if ($this->getIdEstado() != "") $stmt->bindParam(++$x,$this->getIdEstado(),PDO::PARAM_INT);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover()
	{
		$pdo = $this->getConexao();
		$sql = "DELETE FROM certificacao WHERE id = ?";
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
         return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE certificacao.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM certificacao
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				certificacao.*
			FROM certificacao
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY certificacao.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM certificacao WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}


    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                  certificacao.*, departamento.departamento, departamento.tipo_preco, estados.sigla AS estado_sigla
                FROM certificacao
                INNER JOIN departamento ON (certificacao.id_departamento = departamento.id)
                LEFT JOIN estados ON (certificacao.id_estado = estados.id)
                ORDER BY departamento.departamento, certificacao.certificacao ASC 
                 ";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function ListarDepartamento()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT certificacao.*, estados.sigla AS estado_sigla, departamento.tipo_preco 
                FROM certificacao 
                LEFT JOIN estados ON (certificacao.id_estado = estados.id)
                INNER JOIN departamento ON (certificacao.id_departamento = departamento.id)
                WHERE certificacao.id_departamento = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdDepartamento(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
