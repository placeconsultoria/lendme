
<?php
    define('URL_FILE',"../../../");
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/departamento/classe.departamento.php");


    $objDepartamento = new Departamento($pdo);
    $departamentos = $objDepartamento->Listar();

    if($_GET["id"] != ""){
       /* $objDepartamento = new Departamento($pdo);
        $objDepartamento->setId($_GET["id"]);
        $linha = $objDepartamento->Editar();*/
        $acao = "edit_certificacao";
    }else{
        $acao = "add_certificacao";
    }

?>

<form id="frm_certificacao">
	<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
    <input type="hidden" name="acao" value="<?= $acao ?>" />
	<div class="row form-group">
        <div class="col-md-12">
            <label for="id_departamento">SELECIONE O DEPARTAMENTO PARA A CERTIFICAÇÃO:</label>
            <select id="id_departamento" name="id_departamento" class="form-control">
               <?php
                    echo "<option value='0'>--SELECIONE--</option>";
                    foreach ($departamentos AS $departamento){
                        echo '<option value="'.$departamento["id"].'">'.$departamento["departamento"].'</option>';
                    }
                ?>
            </select>
        </div>
    </div>
    <div id="conteudo_form_certificacao"></div>

</form>


<script>
    $(document).ready(function () {
       $("#id_departamento").change(function () {
           var id_departamento = $(this).val();
           if(id_departamento == 0){
               $("#conteudo_form_certificacao").html("");
           }else{
               var url = "modulos/certificacao/template/tpl.frm.certificacao.includes.php?id_departamento="+id_departamento;
               $("#conteudo_form_certificacao").load(url);
           }

       });
    });
</script>

