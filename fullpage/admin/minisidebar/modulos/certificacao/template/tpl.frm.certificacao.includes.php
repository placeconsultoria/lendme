<?php

    define('URL_FILE',"../../../");
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/certificacao/classe.certificacao.php");
    include_once(URL_FILE . "modulos/departamento/classe.departamento.php");
    include_once(URL_FILE . "modulos/estados/classe.estados.php");

    if(isset($_GET["id_departamento"])){
        $id_departamento = $_GET["id_departamento"];
    }
    $objTipoDepartamento = NEW Departamento();
    $objTipoDepartamento->setId($id_departamento);
    $tipo_departamento = $objTipoDepartamento->GetTipoDepartamento();
    //TIPO 1 = FEDERAL
    //TIPO 2 = ESTADUAL

    if(isset($_GET["id_cerfificacao"])){
        include_once(URL_FILE . "modulos/certificacao/classe.certificacao.php");
        $objCertificacao = NEW Certificacao($pdo);
        $objCertificacao->setId($_GET["id_cerfificacao"]);
        $linha = $objCertificacao->Editar();
    }
?>


<?php
if($tipo_departamento == 1){
  echo '
    <div class="row form-group">
        <div class="col-md-12">
			<label for="departamento">CERTIFICAÇÃO:</label>
			<input type="text" name="certificacao"  id="certificacao" maxlength="255" class="form-control  " value="'.$linha["certificacao"].'"/>
        </div>
    </div>
  ';
}else{

    $objEstados = NEW Estados();
    $estados = $objEstados->GerarSelectEstados();
    foreach ($estados AS $estado){
        if($estado["id_estado"] == $linha["id_estado"]){$selected = "selected"; }else{$selected = "";}
        $option_estados .=  '<option value="'.$estado["id_estado"].'" '.$selected.'>'.$estado["nome"].'</option>';
    }
    echo '
    
     <div class="row form-group">
        <div class="col-md-4">
			<label for="id_estado">ESTADO:</label>
			<select name="id_estado" id="id_estado" class="form-control">'.$option_estados.'</select>
        </div>
        <div class="col-md-8">
			<label for="departamento">CERTIFICAÇÃO:</label>
			<input type="text" name="certificacao"  id="certificacao" maxlength="255" class="form-control  " value="'.$linha["certificacao"].'"/>
        </div>
    </div>
    
    ';
}
?>


