<?php

    define('URL_FILE',"../../../");
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/certificacao/classe.certificacao.php");
    include_once(URL_FILE . "modulos/departamento/classe.departamento.php");

    $objCertificacao = NEW Certificacao($pdo);
    $listar = $objCertificacao->Listar();
    $url_edit = "edit_certificacao.php?id=";



?>
<div class="col-md-12">
    <div class="table-responsive">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>DEPARTAMENTO:</th>
                <th>ESTADO:</th>
                <th>CERTIFICAÇÃO:</th>
                <th width="20%">AÇÕES</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $x = 0;
            if(count($listar)> 0){
                foreach($listar as $linha){

                    if($linha["id_estado"] == ""){
                        $estado = "FEDERAL";
                    }else{
                        $estado = $linha["estado_sigla"];
                    }

                    $objDepartamento = NEW Departamento();
                    $color = $objDepartamento->returnColor($linha["id_departamento"]);
                    echo '
                             <tr>
                                <td><h1 class="badge badge-'.$color.'">'.$linha["departamento"].'</h1></td>
                                <td>'.$estado.'</td>
                                <td>'.$linha["certificacao"].'</td>
                                <td>
                                   <a href="'.$url_edit.$linha["id"].'" class="btn btn-warning"> 
                                   GERENCIAR <i class="fa fa-pencil"></i>
                                   </a> 
                                </td>
                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>

<script src="../../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>
