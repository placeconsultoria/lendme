<?
define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/certificacao/classe.certificacao.php");
$app_comando = $_REQUEST["acao"];
switch($app_comando)
{

    case "list_ajax_tipo_departamento":
        include("template/ajax.certificacao.php");
        break;

	case "add_certificacao":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCertificacao = new Certificacao($pdo);
			$objCertificacao->setIdDepartamento($_REQUEST['id_departamento']);
			$objCertificacao->setCertificacao($_REQUEST['certificacao']);
            if($_REQUEST['id_estado'] > 0){$id_estado = $_REQUEST['id_estado'];}else{$id_estado = 0;}
            $objCertificacao->setIdEstado($id_estado);
			$novoId = $objCertificacao->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Sucesso ao cadastrar a certificação";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.certificacao.php";
		break;



	case "atualizar_certificacao":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCertificacao = new Certificacao($pdo);
			$objCertificacao->setId($_REQUEST['id']);
			$objCertificacao->setIdDepartamento($_REQUEST['id_departamento']);
			$objCertificacao->setCertificacao($_REQUEST['certificacao']);
            if($_REQUEST['id_estado'] > 0){$id_estado = $_REQUEST['id_estado'];}else{$id_estado = 0;}
            $objCertificacao->setIdEstado($id_estado);
			$objCertificacao->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Sucesso ao editar esta certificação";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.certificacao.php";
		break;



	case "deltar":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCertificacao = new Certificacao($pdo);
			$objCertificacao->setId($_REQUEST["id"]);
			$objCertificacao->Remover();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.certificacao.php";
		break;


}
