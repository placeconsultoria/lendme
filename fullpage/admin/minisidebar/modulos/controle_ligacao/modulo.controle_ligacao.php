<?

switch($app_comando)
{
	case "frm_adicionar_controle_ligacao":
		$template = "tpl.frm.controle_ligacao.php";
		break;

	case "adicionar_controle_ligacao":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objControleLigacao = new ControleLigacao($pdo);
			$objControleLigacao->setIdUsuario($_REQUEST['id_usuario']);
			$objControleLigacao->setStatus($_REQUEST['status']);
			$novoId = $objControleLigacao->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.controle_ligacao.php";
		break;

	case "frm_atualizar_controle_ligacao" :
		$controle_ligacao = new ControleLigacao();
		$controle_ligacao->setId($_REQUEST["app_codigo"]);
		$linha = $controle_ligacao->Editar();
		$template = "tpl.frm.controle_ligacao.php";
		break;

	case "atualizar_controle_ligacao":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objControleLigacao = new ControleLigacao($pdo);
			$objControleLigacao->setId($_REQUEST['id']);
			$objControleLigacao->setIdUsuario($_REQUEST['id_usuario']);
			$objControleLigacao->setStatus($_REQUEST['status']);
			$objControleLigacao->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.controle_ligacao.php";
		break;

	case "listar_controle_ligacao":
		$template = "tpl.geral.controle_ligacao.php";
		break;

	case "deletar_controle_ligacao":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objControleLigacao = new ControleLigacao($pdo);
			$objControleLigacao->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.controle_ligacao.php";
		break;

	case "ajax_listar_controle_ligacao":
		$template = "tpl.lis.controle_ligacao.php";
		break;

	case "controle_ligacao_pdf":
		$template = "tpl.lis.controle_ligacao.pdf.php";
		break;

	case "controle_ligacao_xlsx":
		$template = "tpl.lis.controle_ligacao.xlsx.php";
		break;

	case "controle_ligacao_print":
		$template = "tpl.lis.controle_ligacao.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["controle_ligacao"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("controle_ligacao");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.controle_ligacao.php";
		break;
}
