<?
class ControleLigacao
{
	private $id;
	private $id_usuario;
	private $status;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdUsuario($arg)
	{
		$this->id_usuario = $arg;
	}
 	
	public function getIdUsuario()
	{
		return $this->id_usuario;
	}
 	
	public function setStatus($arg)
	{
		$this->status = $arg;
	}
 	
	public function getStatus()
	{
		return $this->status;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO controle_ligacao SET ';
		if ($this->getIdUsuario() != "") $sql .= "id_usuario = ?";
		if ($this->getStatus() != "") $sql .= ",status = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdUsuario() != "") $stmt->bindParam(++$x,$this->getIdUsuario(),PDO::PARAM_INT);
		if ($this->getStatus() != "") $stmt->bindParam(++$x,$this->getStatus(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE controle_ligacao SET status = ?';

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getStatus(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover()
	{
		$pdo = $this->getConexao();
		$sql = "DELETE FROM controle_ligacao WHERE id = ?";
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}



	public function Listar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM controle_ligacao WHERE id_usuario = ? and status = '0'";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getIdUsuario(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function ListarOpenUser()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM controle_ligacao WHERE id_usuario = ? and status = '0'";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdUsuario(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
    }


}
