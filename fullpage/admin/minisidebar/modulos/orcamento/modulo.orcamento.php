<?

switch($app_comando)
{
	case "frm_adicionar_orcamento":
		$template = "tpl.frm.orcamento.php";
		break;

	case "adicionar_orcamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objOrcamento = new Orcamento($pdo);
			$objOrcamento->setToken($_REQUEST['token']);
			$objOrcamento->setDataHora($_REQUEST['data_hora']);
			$objOrcamento->setIdClienteOrcamento($_REQUEST['id_cliente_orcamento']);
			$objOrcamento->setAcesstoken($_REQUEST['acesstoken']);
			$objOrcamento->setStatus(0);
			$objOrcamento->setName($_REQUEST['name']);
			$novoId = $objOrcamento->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "OK";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.orcamento.php";
		break;


	case "atualizar_orcamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objOrcamento = new Orcamento($pdo);
			$objOrcamento->setId($_REQUEST['id']);
			$objOrcamento->setToken($_REQUEST['token']);
			$objOrcamento->setDataHora($_REQUEST['data_hora']);
			$objOrcamento->setIdClienteOrcamento($_REQUEST['id_cliente_orcamento']);
			$objOrcamento->setAcesstoken($_REQUEST['acesstoken']);
			$objOrcamento->setStatus($_REQUEST['status']);
			$objOrcamento->setName($_REQUEST['name']);
			$objOrcamento->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.orcamento.php";
		break;


	case "deletar_orcamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objOrcamento = new Orcamento($pdo);
			$objOrcamento->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.orcamento.php";
		break;




}
