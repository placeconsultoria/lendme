<?
class Usuario
{
	private $id;
	private $nome;
	private $email;
	private $ramal;
	private $celular;
	private $senha;
	private $token;
	private $nivel;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setEmail($arg)
	{
		$this->email = $arg;
	}
 	
	public function getEmail()
	{
		return $this->email;
	}
 	
	public function setRamal($arg)
	{
		$this->ramal = $arg;
	}
 	
	public function getRamal()
	{
		return $this->ramal;
	}
 	
	public function setCelular($arg)
	{
		$this->celular = $arg;
	}
 	
	public function getCelular()
	{
		return $this->celular;
	}
 	
	public function setSenha($arg)
	{
		$this->senha = $arg;
	}
 	
	public function getSenha()
	{
		return $this->senha;
	}
 	
	public function setToken($arg)
	{
		$this->token = $arg;
	}
 	
	public function getToken()
	{
		return $this->token;
	}

    public function setNivel($arg)
    {
        $this->nivel = $arg;
    }

    public function getNivel()
    {
        return $this->nivel;
    }
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO usuario SET ';
		if ($this->getNome() != "") $sql .= "nome = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getRamal() != "") $sql .= ",ramal = ?";
		if ($this->getCelular() != "") $sql .= ",celular = ?";
		if ($this->getSenha() != "") $sql .= ",senha = ?";
        if ($this->getNivel() != "") $sql .= ",nivel = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getRamal() != "") $stmt->bindParam(++$x,$this->getRamal(),PDO::PARAM_STR);
		if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
		if ($this->getSenha() != "") $stmt->bindParam(++$x,$this->getSenha(),PDO::PARAM_STR);
        if ($this->getNivel() != "") $stmt->bindParam(++$x,$this->getNivel(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}

    public function setTokenUser()
    {
        $pdo = $this->getConexao();
        $sql = 'UPDATE usuario SET token = ? WHERE id = ?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getToken(),PDO::PARAM_STR);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        return $pdo->lastInsertId() ;

    }
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE usuario SET ';
		if ($this->getNome() != "") $sql .= "nome = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getRamal() != "") $sql .= ",ramal = ?";
		if ($this->getCelular() != "") $sql .= ",celular = ?";
		if ($this->getSenha() != "") $sql .= ",senha = ?";
        if ($this->getNivel() != "") $sql .= ",nivel = ?";

		$sql .= ' WHERE token = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getRamal() != "") $stmt->bindParam(++$x,$this->getRamal(),PDO::PARAM_STR);
		if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
		if ($this->getSenha() != "") $stmt->bindParam(++$x,$this->getSenha(),PDO::PARAM_STR);
        if ($this->getNivel() != "") $stmt->bindParam(++$x,$this->getNivel(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getToken(),PDO::PARAM_STR);
		return $stmt->execute();
	}
	public function Deletar()
	{
		$pdo = $this->getConexao();
		$sql = "DELETE FROM usuario  WHERE token = ?";
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getToken(),PDO::PARAM_INT);

        return $stmt->execute();
	}



	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM usuario WHERE token = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getToken(),PDO::PARAM_STR);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM usuario ORDER BY nivel, nome asc";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function VerificaLogin(){
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM usuario WHERE email = ? AND senha = ? ";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
        $stmt->bindParam(++$x,$this->getSenha(),PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetch();
    }

    public function selectNivelSelected($nivel,$nivelBanco){
	    if($nivel == $nivelBanco) echo "selected ";

    }

    public function ReturnNivelUser(){
	    if($_SESSION["usuario"]["nivel"] == "1"){
	        $return = ' <span class="badge badge-info">@ADMIN MASTER</span>';
	    }else if ($_SESSION["usuario"]["nivel"] == "2") {
            $return = ' <span class="badge badge-inverse">OPERADOR(A)</span>';
	    }else if ($_SESSION["usuario"]["nivel"] == "3") {
            $return = ' <span class="badge badge-inverse">RECEPÇÃO</span>';
        }
        echo $return;
    }


    public function ReturnNivelUserList($nivel){
        if($nivel == "1"){
            $return = ' <span class="badge badge-info">@ADMIN MASTER</span>';
        }else if ($nivel == "2") {
            $return = ' <span class="badge badge-warning">OPERADOR(A)</span>';
        }else if ($nivel == "3") {
            $return = ' <span class="badge badge-inverse">RECEPÇÃO</span>';
        }
        return $return;
    }

    public function GetCombo()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM usuario ORDER BY nivel, nome asc";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
