<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/usuario/classe.usuario.php");

$app_comando = $_REQUEST["acao"];

switch($app_comando)
{
	case "frm_adicionar_usuario":
		$template = "tpl.frm.usuario.php";
		break;

	case "adicionar_usuario":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objUsuario = new Usuario($pdo);
			$objUsuario->setNome($_REQUEST['nome']);
			$objUsuario->setEmail($_REQUEST['email']);
			$objUsuario->setRamal($_REQUEST['ramal']);
			$objUsuario->setCelular($_REQUEST['celular']);
			$objUsuario->setSenha(sha1($_REQUEST['senha']));
			$objUsuario->setNivel($_REQUEST['nivel']);
			$id_usuario = $objUsuario->Adicionar();

			$objUsuario->setId($id_usuario);
			$objUsuario->setToken(sha1($id_usuario));
			$objUsuario->setTokenUser();

			$msg["codigo"] = 0;
			$msg["mensagem"] = "Usuario cadastrado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "erro";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.usuario.php";
		break;



	case "editar_usuario":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objUsuario = new Usuario($pdo);
			$objUsuario->setNome($_REQUEST['nome']);
			$objUsuario->setEmail($_REQUEST['email']);
			$objUsuario->setRamal($_REQUEST['ramal']);
            $objUsuario->setNivel($_REQUEST['nivel']);
			$objUsuario->setCelular($_REQUEST['celular']);
            $objUsuario->setToken($_REQUEST['token']);
			if($_REQUEST['senha'] != ""){ $objUsuario->setSenha(sha1($_REQUEST['senha']));}
            $objUsuario->setNivel($_REQUEST['nivel']);
            $objUsuario->Modificar();

			$msg["codigo"] = 0;
			$msg["mensagem"] = "Usuario editado com sucesso!";
			$pdo->commit();

		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.usuario.php";
		break;



	case "deletar_usuario":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objUsuario = new Usuario($pdo);
			$objUsuario->setToken($_REQUEST["token"]);
			$objUsuario->Deletar();

			$msg["codigo"] = 0;
			$msg["mensagem"] = "OK";
            $msg["url"] = "usuarios.php";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.usuario.php";
		break;


}
