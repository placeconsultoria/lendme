
<?php

    define('URL_FILE',"../../../");
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/usuario/classe.usuario.php");




    if($_GET["token"] != ""){
        $objUsuario = new Usuario($pdo);
        $objUsuario->setToken($_GET["token"]);
        $linha = $objUsuario->Editar();

        $acao = "editar_usuario";
    }else{
        $acao = "adicionar_usuario";
        $objUsuario = new Usuario($pdo);
        $linha['nivel'] = "0";
    }




?>

<form id="frm_usuario">
    <input type="hidden" name="token"  id="token"   value="<?=$linha['token'];?>"/>
    <input type="hidden" name="acao" value="<?php echo $acao ?>" />
	<div class="row form-group">
		<div class="col-md-4">
			<label for="nome">NOME</label>
			<input type="text" name="nome"  id="nome" maxlength="255" class="form-control validar-obrigatorio " value="<?=$linha['nome'];?>"/>
		</div>
        <div class="col-md-4">
            <label for="email">E-MAIL (LOGIN)</label>
            <input type="text" name="email"  id="email" maxlength="255" class="form-control validar-obrigatorio " value="<?=$linha['email'];?>"/>
        </div>
        <div class="col-md-4">
            <label for="nivel">NÍVEL</label>
            <select id="nivel" name="nivel" class="form-control">
                <option value="1" <?php $objUsuario->selectNivelSelected("1",$linha['nivel']); ?>>ADMINISTRADOR</option>
                <option value="2" <?php $objUsuario->selectNivelSelected("2",$linha['nivel']); ?>>OPERADOR(A)</option>
                <option value="3" <?php $objUsuario->selectNivelSelected("3",$linha['nivel']); ?>>RECEPÇÃO</option>
            </select>

        </div>
	</div>

	<div class="row form-group">
		<div class="col-md-4">
			<label for="ramal">RAMAL</label>
			<input type="text" name="ramal"  id="ramal" maxlength="20" class="form-control validar-obrigatorio " value="<?=$linha['ramal'];?>"/>
		</div>

		<div class="col-md-4">
			<label for="celular">CELULAR</label>
			<input type="text" name="celular"  id="celular"  class="form-control validar-obrigatorio " value="<?=$linha['celular'];?>" onKeyUp="mascaraTexto(event,'(99)99999-9999')" maxlength="14" onkeypress="return SomenteNumero(event);"/>
		</div>

		<div class="col-md-4">
			<label for="senha">SENHA (6 dígitos)</label>
			<input type="password" name="senha"  id="senha" maxlength="255" class="form-control validar-obrigatorio " />
		</div>
	</div>

</form>
