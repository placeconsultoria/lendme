<?php
    define('URL_FILE',"../../../");
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/usuario/classe.usuario.php");

    $objUsuario = new Usuario($pdo);
    $listar = $objUsuario->listar();

    $url_edit = "edit_usuario.php?token=";
?>


    <div class="col-md-12">
        <div class="table-responsive">
            <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>NÍVEL</th>
                    <th>NOME</th>
                    <th>E-MAIL</th>
                    <th>RAMAL</th>
                    <th>CELULAR</th>
                    <th>FUNÇÕES</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $x = 0;
                if(count($listar)> 0){
                    foreach($listar as $linha){
                        $token = $linha["token"];

                        $nivel = $objUsuario->ReturnNivelUserList($linha["nivel"]);


                        echo '
                             <tr>
                                <td>'.$nivel.'</td> 
                                <td>'.$linha["nome"].'</td>
                                <td>'.$linha["email"].'</td>
                                <td>'.$linha["ramal"].'</td>
                                <td>'.$linha["celular"].'</td>
                                <td><a href="'.$url_edit.$token.'" class="btn btn-warning"> GERENCIAR <i class="fa fa-pencil"></i></a> </td>
                            </tr>
                        ';
                    }
                }

                ?>
                </tbody>
            </table>

        </div>
    </div>



<script src="../../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>