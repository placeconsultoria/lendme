<form id="frm_contato_escritorio">
	<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="nome"> <?=RTL_NOME?></label>
			<input type="text" name="nome"  id="nome" maxlength="255" class="form-control  " value="<?=$linha['nome'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="telefone"> <?=RTL_TELEFONE?></label>
			<input type="text" name="telefone"  id="telefone" maxlength="20" class="form-control  " value="<?=$linha['telefone'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="celular"> <?=RTL_CELULAR?></label>
			<input type="text" name="celular"  id="celular" maxlength="20" class="form-control  " value="<?=$linha['celular'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="email"> <?=RTL_EMAIL?></label>
			<input type="text" name="email"  id="email" maxlength="255" class="form-control  " value="<?=$linha['email'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="data_nascimento"> <?=RTL_DATA_NASCIMENTO?></label>
			<input type="text" name="data_nascimento"  id="data_nascimento"  class="form-control  mask-date" value="<?=$linha['data_nascimento'];?>"/>
		</div>
	</div>
</form>

