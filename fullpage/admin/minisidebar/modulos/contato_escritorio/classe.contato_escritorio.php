<?
class ContatoEscritorio
{
	private $id;
	private $nome;
	private $telefone;
	private $celular;
	private $email;
	private $data_nascimento;
	private $id_endereco;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setTelefone($arg)
	{
		$this->telefone = $arg;
	}
 	
	public function getTelefone()
	{
		return $this->telefone;
	}
 	
	public function setCelular($arg)
	{
		$this->celular = $arg;
	}
 	
	public function getCelular()
	{
		return $this->celular;
	}
 	
	public function setEmail($arg)
	{
		$this->email = $arg;
	}
 	
	public function getEmail()
	{
		return $this->email;
	}
 	
	public function setDataNascimento($arg)
	{
		$this->data_nascimento = $arg;
	}
 	
	public function getDataNascimento()
	{
		return $this->data_nascimento;
	}
 	
	public function setIdEndereco($arg)
	{
		$this->id_endereco = $arg;
	}
 	
	public function getIdEndereco()
	{
		return $this->id_endereco;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = '
		INSERT INTO contato_escritorio SET 
			id = ?';
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getCelular() != "") $sql .= ",celular = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getDataNascimento() != "") $sql .= ",data_nascimento = ?";
		if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getDataNascimento() != "") $stmt->bindParam(++$x,$this->getDataNascimento(),PDO::PARAM_STR);
		if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = '
		UPDATE contato_escritorio SET 
			id = ?';
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getCelular() != "") $sql .= ",celular = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getDataNascimento() != "") $sql .= ",data_nascimento = ?";
		if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getDataNascimento() != "") $stmt->bindParam(++$x,$this->getDataNascimento(),PDO::PARAM_STR);
		if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM contato_escritorio WHERE id IN({$lista})";
		$sql = "UPDATE contato_escritorio SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE contato_escritorio.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM contato_escritorio
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				contato_escritorio.*
			FROM contato_escritorio
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY contato_escritorio.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM contato_escritorio WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}
}
