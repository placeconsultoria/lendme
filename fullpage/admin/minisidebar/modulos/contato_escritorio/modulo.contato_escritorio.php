<?

switch($app_comando)
{


	case "adicionar_contato_escritorio":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objContatoEscritorio = new ContatoEscritorio($pdo);
			$objContatoEscritorio->setNome($_REQUEST['nome']);
			$objContatoEscritorio->setTelefone($_REQUEST['telefone']);
			$objContatoEscritorio->setCelular($_REQUEST['celular']);
			$objContatoEscritorio->setEmail($_REQUEST['email']);
			$objContatoEscritorio->setDataNascimento($_REQUEST['data_nascimento']);
			$objContatoEscritorio->setIdEndereco($_REQUEST['id_endereco']);
			$novoId = $objContatoEscritorio->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.contato_escritorio.php";
		break;



	case "atualizar_contato_escritorio":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objContatoEscritorio = new ContatoEscritorio($pdo);
			$objContatoEscritorio->setId($_REQUEST['id']);
			$objContatoEscritorio->setNome($_REQUEST['nome']);
			$objContatoEscritorio->setTelefone($_REQUEST['telefone']);
			$objContatoEscritorio->setCelular($_REQUEST['celular']);
			$objContatoEscritorio->setEmail($_REQUEST['email']);
			$objContatoEscritorio->setDataNascimento($_REQUEST['data_nascimento']);
			$objContatoEscritorio->setIdEndereco($_REQUEST['id_endereco']);
			$objContatoEscritorio->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "CONTATO MODIFICADO";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.contato_escritorio.php";
		break;



	case "deletar_contato_escritorio":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objContatoEscritorio = new ContatoEscritorio($pdo);
			$objContatoEscritorio->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.contato_escritorio.php";
		break;



	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["contato_escritorio"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("contato_escritorio");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.contato_escritorio.php";
		break;
}
