<?
class Pesquisa
{
    private $id;
    private $pesquisa;
    private $data;
    private $ip;
    private $total;
    private $ids_produtos;
    private $conexao;

    public function setId($arg)
    {
        $this->id = $arg;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setPesquisa($arg)
    {
        $this->pesquisa = $arg;
    }

    public function getPesquisa()
    {
        return $this->pesquisa;
    }

    public function setData($arg)
    {
        $this->data = $arg;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setIp($arg)
    {
        $this->ip = $arg;
    }

    public function getIp()
    {
        return $this->ip;
    }


    public function setTotal($arg)
    {
        $this->total = $arg;
    }

    public function getTotal()
    {
        return $this->total;
    }


    public function setIdProdutos($arg)
    {
        $this->ids_produtos = $arg;
    }

    public function getIdProdutos()
    {
        return $this->ids_produtos;
    }

    public function setConexao($arg)
    {
        $this->conexao = $arg;
    }

    public function getConexao()
    {
        return $this->conexao;
    }

    public function __construct($conexao = "")
    {
        if ($conexao) {
            $this->conexao = $conexao;
        } else {
            $this->conexao = new Conexao();
        }
    }

    public function dateUnic($last)
    {
        $pdo = $this->getConexao();
        $sql = "select distinct(date(date_format(data, '%Y-%m-%d'))) as data from pesquisa order by data asc";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function countRegistrosDateUnic($data){
        $pdo = $this->getConexao();
        $where = "WHERE data BETWEEN '$data 00:00:00' AND '$data 23:59:59'";
        $sql = "SELECT count(id) AS total FROM pesquisa $where";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return  $stmt->fetch();
        
    }

    public function ListarUltimos30DiasDashBoard()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM pesquisa WHERE data < DATE_ADD(NOW(), INTERVAL +1 MONTH) ORDER BY ID DESC";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_ASSOC);
    }




}

?>