<?

switch($app_comando)
{
	case "frm_adicionar_pesquisa":
		$template = "tpl.frm.pesquisa.php";
		break;

	case "adicionar_pesquisa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPesquisa = new Pesquisa($pdo);
			$objPesquisa->setPesquisa($_REQUEST['pesquisa']);
			$objPesquisa->setData($_REQUEST['data']);
			$objPesquisa->setIp($_REQUEST['ip']);
			$novoId = $objPesquisa->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.pesquisa.php";
		break;

	case "frm_atualizar_pesquisa" :
		$pesquisa = new Pesquisa();
		$pesquisa->setId($_REQUEST["app_codigo"]);
		$linha = $pesquisa->Editar();
		$template = "tpl.frm.pesquisa.php";
		break;

	case "atualizar_pesquisa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPesquisa = new Pesquisa($pdo);
			$objPesquisa->setId($_REQUEST['id']);
			$objPesquisa->setPesquisa($_REQUEST['pesquisa']);
			$objPesquisa->setData($_REQUEST['data']);
			$objPesquisa->setIp($_REQUEST['ip']);
			$objPesquisa->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.pesquisa.php";
		break;

	case "listar_pesquisa":
		$template = "tpl.geral.pesquisa.php";
		break;

	case "deletar_pesquisa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPesquisa = new Pesquisa($pdo);
			$objPesquisa->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.pesquisa.php";
		break;

	case "ajax_listar_pesquisa":
		$template = "tpl.lis.pesquisa.php";
		break;

	case "pesquisa_pdf":
		$template = "tpl.lis.pesquisa.pdf.php";
		break;

	case "pesquisa_xlsx":
		$template = "tpl.lis.pesquisa.xlsx.php";
		break;

	case "pesquisa_print":
		$template = "tpl.lis.pesquisa.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["pesquisa"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("pesquisa");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.pesquisa.php";
		break;
}
