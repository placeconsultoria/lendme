<?
class CadastroCliente
{
	private $id;
	private $pasta;
	private $id_pasta_mae;
	private $exercito;
	private $policiafederal;
	private $policiacivil;
	private $exercito_atividades;
	private $policiafederal_atividades;
	private $policiacivil_atividades;
	private $policiacivil_delegacia;
	private $policiacivil_num_atividades;
	private $policiacivil_alvara_tipo;
	private $policiacivil_concessao_revalidacao;
	private $nome_empresa;
	private $cnpj;
	private $inscricao_estadual;
	private $CNAE;
	private $atividade_principal;
	private $conato_licenca;
	private $telefone_licenca;
	private $email_licenca;
	private $logradouro;
	private $bairro;
	private $cidade;
	private $id_cidade;
	private $estado;
	private $id_estado;
	private $cep;
	private $data_hora;
	private $grupo;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setPasta($arg)
	{
		$this->pasta = $arg;
	}
 	
	public function getPasta()
	{
		return $this->pasta;
	}
 	
	public function setIdPastaMae($arg)
	{
		$this->id_pasta_mae = $arg;
	}
 	
	public function getIdPastaMae()
	{
		return $this->id_pasta_mae;
	}
 	
	public function setExercito($arg)
	{
		$this->exercito = $arg;
	}
 	
	public function getExercito()
	{
		return $this->exercito;
	}
 	
	public function setPoliciafederal($arg)
	{
		$this->policiafederal = $arg;
	}
 	
	public function getPoliciafederal()
	{
		return $this->policiafederal;
	}
 	
	public function setPoliciacivil($arg)
	{
		$this->policiacivil = $arg;
	}
 	
	public function getPoliciacivil()
	{
		return $this->policiacivil;
	}
 	
	public function setExercitoAtividades($arg)
	{
		$this->exercito_atividades = $arg;
	}
 	
	public function getExercitoAtividades()
	{
		return $this->exercito_atividades;
	}
 	
	public function setPoliciafederalAtividades($arg)
	{
		$this->policiafederal_atividades = $arg;
	}
 	
	public function getPoliciafederalAtividades()
	{
		return $this->policiafederal_atividades;
	}
 	
	public function setPoliciacivilAtividades($arg)
	{
		$this->policiacivil_atividades = $arg;
	}
 	
	public function getPoliciacivilAtividades()
	{
		return $this->policiacivil_atividades;
	}
 	
	public function setPoliciacivilDelegacia($arg)
	{
		$this->policiacivil_delegacia = $arg;
	}
 	
	public function getPoliciacivilDelegacia()
	{
		return $this->policiacivil_delegacia;
	}
 	
	public function setPoliciacivilNumAtividades($arg)
	{
		$this->policiacivil_num_atividades = $arg;
	}
 	
	public function getPoliciacivilNumAtividades()
	{
		return $this->policiacivil_num_atividades;
	}
 	
	public function setPoliciacivilAlvaraTipo($arg)
	{
		$this->policiacivil_alvara_tipo = $arg;
	}
 	
	public function getPoliciacivilAlvaraTipo()
	{
		return $this->policiacivil_alvara_tipo;
	}
 	
	public function setPoliciacivilConcessaoRevalidacao($arg)
	{
		$this->policiacivil_concessao_revalidacao = $arg;
	}
 	
	public function getPoliciacivilConcessaoRevalidacao()
	{
		return $this->policiacivil_concessao_revalidacao;
	}
 	
	public function setNomeEmpresa($arg)
	{
		$this->nome_empresa = $arg;
	}
 	
	public function getNomeEmpresa()
	{
		return $this->nome_empresa;
	}
 	
	public function setCnpj($arg)
	{
		$this->cnpj = $arg;
	}
 	
	public function getCnpj()
	{
		return $this->cnpj;
	}
 	
	public function setInscricaoEstadual($arg)
	{
		$this->inscricao_estadual = $arg;
	}
 	
	public function getInscricaoEstadual()
	{
		return $this->inscricao_estadual;
	}
 	
	public function setCNAE($arg)
	{
		$this->CNAE = $arg;
	}
 	
	public function getCNAE()
	{
		return $this->CNAE;
	}
 	
	public function setAtividadePrincipal($arg)
	{
		$this->atividade_principal = $arg;
	}
 	
	public function getAtividadePrincipal()
	{
		return $this->atividade_principal;
	}
 	
	public function setConatoLicenca($arg)
	{
		$this->conato_licenca = $arg;
	}
 	
	public function getConatoLicenca()
	{
		return $this->conato_licenca;
	}
 	
	public function setTelefoneLicenca($arg)
	{
		$this->telefone_licenca = $arg;
	}
 	
	public function getTelefoneLicenca()
	{
		return $this->telefone_licenca;
	}
 	
	public function setEmailLicenca($arg)
	{
		$this->email_licenca = $arg;
	}
 	
	public function getEmailLicenca()
	{
		return $this->email_licenca;
	}
 	
	public function setLogradouro($arg)
	{
		$this->logradouro = $arg;
	}
 	
	public function getLogradouro()
	{
		return $this->logradouro;
	}
 	
	public function setBairro($arg)
	{
		$this->bairro = $arg;
	}
 	
	public function getBairro()
	{
		return $this->bairro;
	}
 	
	public function setCidade($arg)
	{
		$this->cidade = $arg;
	}
 	
	public function getCidade()
	{
		return $this->cidade;
	}
 	
	public function setIdCidade($arg)
	{
		$this->id_cidade = $arg;
	}
 	
	public function getIdCidade()
	{
		return $this->id_cidade;
	}
 	
	public function setEstado($arg)
	{
		$this->estado = $arg;
	}
 	
	public function getEstado()
	{
		return $this->estado;
	}
 	
	public function setIdEstado($arg)
	{
		$this->id_estado = $arg;
	}
 	
	public function getIdEstado()
	{
		return $this->id_estado;
	}
 	
	public function setCep($arg)
	{
		$this->cep = $arg;
	}
 	
	public function getCep()
	{
		return $this->cep;
	}


    public function setDataHora($arg)
    {
        $this->data_hora = $arg;
    }

    public function getDataHora()
    {
        return $this->data_hora;
    }



    public function setGrupo($arg)
    {
        $this->grupo = $arg;
    }

    public function getGrupo()
    {
        return $this->grupo;
    }


 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO cadastro_cliente SET data_hora = NOW() ';
		if ($this->getPasta() != "") $sql .= ",pasta = ?";
		if ($this->getIdPastaMae() != "") $sql .= ",id_pasta_mae = ?";
		if ($this->getExercito() != "") $sql .= ",exercito = ?";
		if ($this->getPoliciafederal() != "") $sql .= ",policiafederal = ?";
		if ($this->getPoliciacivil() != "") $sql .= ",policiacivil = ?";
		if ($this->getExercitoAtividades() != "") $sql .= ",exercito_atividades = ?";
		if ($this->getPoliciafederalAtividades() != "") $sql .= ",policiafederal_atividades = ?";
		if ($this->getPoliciacivilAtividades() != "") $sql .= ",policiacivil_atividades = ?";
		if ($this->getPoliciacivilDelegacia() != "") $sql .= ",policiacivil_delegacia = ?";
		if ($this->getPoliciacivilNumAtividades() != "") $sql .= ",policiacivil_num_atividades = ?";
		if ($this->getPoliciacivilAlvaraTipo() != "") $sql .= ",policiacivil_alvara_tipo = ?";
		if ($this->getPoliciacivilConcessaoRevalidacao() != "") $sql .= ",policiacivil_concessao_revalidacao = ?";
		if ($this->getNomeEmpresa() != "") $sql .= ",nome_empresa = ?";
		if ($this->getCnpj() != "") $sql .= ",cnpj = ?";
		if ($this->getInscricaoEstadual() != "") $sql .= ",inscricao_estadual = ?";
		if ($this->getCNAE() != "") $sql .= ",CNAE = ?";
		if ($this->getAtividadePrincipal() != "") $sql .= ",atividade_principal = ?";
		if ($this->getConatoLicenca() != "") $sql .= ",conato_licenca = ?";
		if ($this->getTelefoneLicenca() != "") $sql .= ",telefone_licenca = ?";
		if ($this->getEmailLicenca() != "") $sql .= ",email_licenca = ?";
		if ($this->getLogradouro() != "") $sql .= ",logradouro = ?";
		if ($this->getBairro() != "") $sql .= ",bairro = ?";
		if ($this->getCidade() != "") $sql .= ",cidade = ?";
		if ($this->getIdCidade() != "") $sql .= ",id_cidade = ?";
		if ($this->getEstado() != "") $sql .= ",estado = ?";
		if ($this->getIdEstado() != "") $sql .= ",id_estado = ?";
		if ($this->getCep() != "") $sql .= ",cep = ?";
        if ($this->getGrupo() != "") $sql .= ",grupo = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getPasta() != "") $stmt->bindParam(++$x,$this->getPasta(),PDO::PARAM_STR);
		if ($this->getIdPastaMae() != "") $stmt->bindParam(++$x,$this->getIdPastaMae(),PDO::PARAM_INT);
		if ($this->getExercito() != "") $stmt->bindParam(++$x,$this->getExercito(),PDO::PARAM_INT);
		if ($this->getPoliciafederal() != "") $stmt->bindParam(++$x,$this->getPoliciafederal(),PDO::PARAM_INT);
		if ($this->getPoliciacivil() != "") $stmt->bindParam(++$x,$this->getPoliciacivil(),PDO::PARAM_INT);
		if ($this->getExercitoAtividades() != "") $stmt->bindParam(++$x,$this->getExercitoAtividades(),PDO::PARAM_STR);
		if ($this->getPoliciafederalAtividades() != "") $stmt->bindParam(++$x,$this->getPoliciafederalAtividades(),PDO::PARAM_STR);
		if ($this->getPoliciacivilAtividades() != "") $stmt->bindParam(++$x,$this->getPoliciacivilAtividades(),PDO::PARAM_STR);
		if ($this->getPoliciacivilDelegacia() != "") $stmt->bindParam(++$x,$this->getPoliciacivilDelegacia(),PDO::PARAM_STR);
		if ($this->getPoliciacivilNumAtividades() != "") $stmt->bindParam(++$x,$this->getPoliciacivilNumAtividades(),PDO::PARAM_INT);
		if ($this->getPoliciacivilAlvaraTipo() != "") $stmt->bindParam(++$x,$this->getPoliciacivilAlvaraTipo(),PDO::PARAM_STR);
		if ($this->getPoliciacivilConcessaoRevalidacao() != "") $stmt->bindParam(++$x,$this->getPoliciacivilConcessaoRevalidacao(),PDO::PARAM_STR);
		if ($this->getNomeEmpresa() != "") $stmt->bindParam(++$x,$this->getNomeEmpresa(),PDO::PARAM_STR);
		if ($this->getCnpj() != "") $stmt->bindParam(++$x,$this->getCnpj(),PDO::PARAM_STR);
		if ($this->getInscricaoEstadual() != "") $stmt->bindParam(++$x,$this->getInscricaoEstadual(),PDO::PARAM_STR);
		if ($this->getCNAE() != "") $stmt->bindParam(++$x,$this->getCNAE(),PDO::PARAM_STR);
		if ($this->getAtividadePrincipal() != "") $stmt->bindParam(++$x,$this->getAtividadePrincipal(),PDO::PARAM_STR);
		if ($this->getConatoLicenca() != "") $stmt->bindParam(++$x,$this->getConatoLicenca(),PDO::PARAM_STR);
		if ($this->getTelefoneLicenca() != "") $stmt->bindParam(++$x,$this->getTelefoneLicenca(),PDO::PARAM_STR);
		if ($this->getEmailLicenca() != "") $stmt->bindParam(++$x,$this->getEmailLicenca(),PDO::PARAM_STR);
		if ($this->getLogradouro() != "") $stmt->bindParam(++$x,$this->getLogradouro(),PDO::PARAM_STR);
		if ($this->getBairro() != "") $stmt->bindParam(++$x,$this->getBairro(),PDO::PARAM_STR);
		if ($this->getCidade() != "") $stmt->bindParam(++$x,$this->getCidade(),PDO::PARAM_STR);
		if ($this->getIdCidade() != "") $stmt->bindParam(++$x,$this->getIdCidade(),PDO::PARAM_INT);
		if ($this->getEstado() != "") $stmt->bindParam(++$x,$this->getEstado(),PDO::PARAM_STR);
		if ($this->getIdEstado() != "") $stmt->bindParam(++$x,$this->getIdEstado(),PDO::PARAM_INT);
		if ($this->getCep() != "") $stmt->bindParam(++$x,$this->getCep(),PDO::PARAM_STR);
        if ($this->getGrupo() != "") $stmt->bindParam(++$x,$this->getGrupo(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE cadastro_cliente SET ';
		if ($this->getPasta() != "") $sql .= " pasta = ?";
		if ($this->getIdPastaMae() != "") $sql .= ",id_pasta_mae = ?";
		if ($this->getExercito() != "") $sql .= ",exercito = ?";
		if ($this->getPoliciafederal() != "") $sql .= ",policiafederal = ?";
		if ($this->getPoliciacivil() != "") $sql .= ",policiacivil = ?";
		if ($this->getExercitoAtividades() != "") $sql .= ",exercito_atividades = ?";
		if ($this->getPoliciafederalAtividades() != "") $sql .= ",policiafederal_atividades = ?";
		if ($this->getPoliciacivilAtividades() != "") $sql .= ",policiacivil_atividades = ?";
		if ($this->getPoliciacivilDelegacia() != "") $sql .= ",policiacivil_delegacia = ?";
		if ($this->getPoliciacivilNumAtividades() != "") $sql .= ",policiacivil_num_atividades = ?";
		if ($this->getPoliciacivilAlvaraTipo() != "") $sql .= ",policiacivil_alvara_tipo = ?";
		if ($this->getPoliciacivilConcessaoRevalidacao() != "") $sql .= ",policiacivil_concessao_revalidacao = ?";
		if ($this->getNomeEmpresa() != "") $sql .= ",nome_empresa = ?";
		if ($this->getCnpj() != "") $sql .= ",cnpj = ?";
		if ($this->getInscricaoEstadual() != "") $sql .= ",inscricao_estadual = ?";
		if ($this->getCNAE() != "") $sql .= ",CNAE = ?";
		if ($this->getAtividadePrincipal() != "") $sql .= ",atividade_principal = ?";
		if ($this->getConatoLicenca() != "") $sql .= ",conato_licenca = ?";
		if ($this->getTelefoneLicenca() != "") $sql .= ",telefone_licenca = ?";
		if ($this->getEmailLicenca() != "") $sql .= ",email_licenca = ?";
		if ($this->getLogradouro() != "") $sql .= ",logradouro = ?";
		if ($this->getBairro() != "") $sql .= ",bairro = ?";
		if ($this->getCidade() != "") $sql .= ",cidade = ?";
		if ($this->getIdCidade() != "") $sql .= ",id_cidade = ?";
		if ($this->getEstado() != "") $sql .= ",estado = ?";
		if ($this->getIdEstado() != "") $sql .= ",id_estado = ?";
		if ($this->getCep() != "") $sql .= ",cep = ?";
        if ($this->getGrupo() != "") $sql .= ",grupo = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getPasta() != "") $stmt->bindParam(++$x,$this->getPasta(),PDO::PARAM_STR);
		if ($this->getIdPastaMae() != "") $stmt->bindParam(++$x,$this->getIdPastaMae(),PDO::PARAM_INT);
		if ($this->getExercito() != "") $stmt->bindParam(++$x,$this->getExercito(),PDO::PARAM_INT);
		if ($this->getPoliciafederal() != "") $stmt->bindParam(++$x,$this->getPoliciafederal(),PDO::PARAM_INT);
		if ($this->getPoliciacivil() != "") $stmt->bindParam(++$x,$this->getPoliciacivil(),PDO::PARAM_INT);
		if ($this->getExercitoAtividades() != "") $stmt->bindParam(++$x,$this->getExercitoAtividades(),PDO::PARAM_STR);
		if ($this->getPoliciafederalAtividades() != "") $stmt->bindParam(++$x,$this->getPoliciafederalAtividades(),PDO::PARAM_STR);
		if ($this->getPoliciacivilAtividades() != "") $stmt->bindParam(++$x,$this->getPoliciacivilAtividades(),PDO::PARAM_STR);
		if ($this->getPoliciacivilDelegacia() != "") $stmt->bindParam(++$x,$this->getPoliciacivilDelegacia(),PDO::PARAM_STR);
		if ($this->getPoliciacivilNumAtividades() != "") $stmt->bindParam(++$x,$this->getPoliciacivilNumAtividades(),PDO::PARAM_INT);
		if ($this->getPoliciacivilAlvaraTipo() != "") $stmt->bindParam(++$x,$this->getPoliciacivilAlvaraTipo(),PDO::PARAM_STR);
		if ($this->getPoliciacivilConcessaoRevalidacao() != "") $stmt->bindParam(++$x,$this->getPoliciacivilConcessaoRevalidacao(),PDO::PARAM_STR);
		if ($this->getNomeEmpresa() != "") $stmt->bindParam(++$x,$this->getNomeEmpresa(),PDO::PARAM_STR);
		if ($this->getCnpj() != "") $stmt->bindParam(++$x,$this->getCnpj(),PDO::PARAM_STR);
		if ($this->getInscricaoEstadual() != "") $stmt->bindParam(++$x,$this->getInscricaoEstadual(),PDO::PARAM_STR);
		if ($this->getCNAE() != "") $stmt->bindParam(++$x,$this->getCNAE(),PDO::PARAM_STR);
		if ($this->getAtividadePrincipal() != "") $stmt->bindParam(++$x,$this->getAtividadePrincipal(),PDO::PARAM_STR);
		if ($this->getConatoLicenca() != "") $stmt->bindParam(++$x,$this->getConatoLicenca(),PDO::PARAM_STR);
		if ($this->getTelefoneLicenca() != "") $stmt->bindParam(++$x,$this->getTelefoneLicenca(),PDO::PARAM_STR);
		if ($this->getEmailLicenca() != "") $stmt->bindParam(++$x,$this->getEmailLicenca(),PDO::PARAM_STR);
		if ($this->getLogradouro() != "") $stmt->bindParam(++$x,$this->getLogradouro(),PDO::PARAM_STR);
		if ($this->getBairro() != "") $stmt->bindParam(++$x,$this->getBairro(),PDO::PARAM_STR);
		if ($this->getCidade() != "") $stmt->bindParam(++$x,$this->getCidade(),PDO::PARAM_STR);
		if ($this->getIdCidade() != "") $stmt->bindParam(++$x,$this->getIdCidade(),PDO::PARAM_INT);
		if ($this->getEstado() != "") $stmt->bindParam(++$x,$this->getEstado(),PDO::PARAM_STR);
		if ($this->getIdEstado() != "") $stmt->bindParam(++$x,$this->getIdEstado(),PDO::PARAM_INT);
		if ($this->getCep() != "") $stmt->bindParam(++$x,$this->getCep(),PDO::PARAM_STR);
        if ($this->getGrupo() != "") $stmt->bindParam(++$x,$this->getGrupo(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM cadastro_cliente WHERE id IN({$lista})";
		$sql = "UPDATE cadastro_cliente SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE cadastro_cliente.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM cadastro_cliente
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				cadastro_cliente.*
			FROM cadastro_cliente
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY cadastro_cliente.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM cadastro_cliente WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}
}
