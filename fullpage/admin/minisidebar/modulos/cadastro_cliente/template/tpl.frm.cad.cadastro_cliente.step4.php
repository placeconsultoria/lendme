<?php

    @session_start();
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    define('URL_FILE',"../../../");
    //require_once('inc/inc.sessao.php');
    //equire_once('inc/funcoes.php');
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
    include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
    include_once(URL_FILE . "modulos/estados/classe.estados.php");
    $objComboProdutos = NEW ProdutosControlados();
    $resulComboProdutos = $objComboProdutos->ComboProdutosControlados();

    $objGrupoEmpresa = NEW GrupoEmpresa();
    $resulComboGrupo = $objGrupoEmpresa->GetCombo();

    $objEstados = NEW Estados();
    $resulComboEstados = $objEstados->GerarSelectEstados();


?>


<form id="frm_cad_cadastro_cliente">
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">INFORMAÇÕES E DEPARTAMENTOS:</span> </h4>
        </div>
    </div>

    <div class="row form-group pb-0 mb-0">
        <div class="col-md-2 pb-0 mb-0">
            <div class="form-check has-success p-t-10 pb-0 mb-0">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="exercito" value="exercito"> EXÉRCITO
                </label>
            </div>
        </div>

        <div class="col-md-10 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="exercito_atividades" class="col-3 col-form-label">EXÉRCITO ATIVIDADES:</label>
                <div class="col-9">
                    <input id="exercito_atividades" class="form-control" name="exercito_atividades" type="text" value="" >
                </div>
            </div>
        </div>
    </div>

    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-2 pb-0 mb-0">
            <div class="form-check has-warning p-t-10 pb-0 mb-0">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="policiafederal" value="exercito"> POLÍCIA FEDERAL
                </label>
            </div>
        </div>

        <div class="col-md-10 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="policiafederal_atividades" class="col-3 col-form-label">DPF ATIVIDADES:</label>
                <div class="col-9">
                    <input name="policiafederal_atividades" class="form-control" type="text" value="" id="policiafederal_atividades">
                </div>
            </div>
        </div>
    </div>

    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-2 pb-0 mb-0">
            <div class="form-check has-inverse p-t-10 pb-0 mb-0">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="policiacivil" value="exercito"> POLÍCIA CIVIL
                </label>
            </div>
        </div>

        <div class="col-md-10 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="policiacivil_atividades" class="col-3 col-form-label">POLÍCIA CIVIL ATIVIDADES:</label>
                <div class="col-9">
                    <input name="policiacivil_atividades" class="form-control" type="text" value="" id="policiacivil_atividades">
                </div>
            </div>
        </div>
    </div>

    <div class="row form-group pb-0 mb-0 p-t-10">
        <div class="col-md-3 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="policiacivil_delegacia" class="col-4 col-form-label">DELEGACIA:</label>
                <div class="col-8">
                    <input name="policiacivil_delegacia" class="form-control" type="text" value="" id="policiacivil_delegacia">
                </div>
            </div>
        </div>

        <div class="col-md-3 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="policiacivil_num_atividades" class="col-9 col-form-label">ATIVIDADE CIVIL NÚMERO:</label>
                <div class="col-3">
                    <input name="policiacivil_num_atividades" class="form-control" type="text" value="" id="policiacivil_num_atividades">
                </div>
            </div>
        </div>

        <div class="col-md-3 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="policiacivil_alvara_tipo" class="col-5 col-form-label">ALVARÁ TIPO:</label>
                <div class="col-7">
                    <input name="policiacivil_alvara_tipo" class="form-control" type="text" value="" id="policiacivil_alvara_tipo">
                </div>
            </div>
        </div>

        <div class="col-md-3 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <div class="col-12">
                    <select name="policiacivil_concessao_revalidacao" class="form-control">
                        <option value="0">CONCESSÃO OU REVALIDAÇÃO</option>
                        <option value="1">CONCESSÃO</option>
                        <option value="2">REVALIDAÇÃO</option>
                    </select>
                </div>
            </div>
        </div>

    </div>



</form>


