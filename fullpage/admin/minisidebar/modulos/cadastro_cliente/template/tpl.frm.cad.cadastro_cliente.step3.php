<?php

    @session_start();
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    define('URL_FILE',"../../../");
    //require_once('inc/inc.sessao.php');
    //equire_once('inc/funcoes.php');
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
    include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
    include_once(URL_FILE . "modulos/estados/classe.estados.php");
    $objComboProdutos = NEW ProdutosControlados();
    $resulComboProdutos = $objComboProdutos->ComboProdutosControlados();

    $objGrupoEmpresa = NEW GrupoEmpresa();
    $resulComboGrupo = $objGrupoEmpresa->GetCombo();

    $objEstados = NEW Estados();
    $resulComboEstados = $objEstados->GerarSelectEstados();


?>


<form id="frm_cad_cadastro_cliente">
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">REPRESENTANTE:</span> </h4>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-3">
            <label for="representante_nome_1"> NOME:</label>
            <input type="text" name="representante_nome[]"  id="representante_nome_1" maxlength="255" class="form-control  " value="<?=$linha['nome'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_cargo_1"> CARGO:</label>
            <input type="text" name="representante_cargo[]"  id="representante_cargo_1" maxlength="100" class="form-control " value="<?=$linha['cargo'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_rg_1"> RG:</label>
            <input type="text" name="representante_rg[]"  id="representante_rg_1" maxlength="45" class="form-control  " value="<?=$linha['rg'];?>"/>
        </div>
        <div class="col-md-1">
            <label for="representante_orgao_rg_1"> ORGÃO:</label>
            <input type="text" name="representante_orgao_rg[]"  id="representante_orgao_rg_1" maxlength="100" class="form-control  " value="<?=$linha['orgao_rg'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_cpf_1"> CPF:</label>
            <input type="text" name="representante_cpf[]"  id="representante_cpf_1"  class="form-control  " value="<?=$linha['cpf'];?>" onkeyup="mascaraTexto(event,'999.999.999-99')" maxlength="14" />
        </div>

        <div class="col-md-2">
            <label for="representante_nascimento_1"> NASCIMENTO:</label>
            <input type="text" name="representante_nascimento[]"  id="representante_nascimento_1" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" class="form-control  " value="<?=$linha['cpf'];?>"/>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-3">
            <label for="representante_pai_1">PAI:</label>
            <input type="text" name="representante_pai[]"  id="representante_pai_1" maxlength="255" class="form-control  " value="<?=$linha['pai'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="representante_mae_1">MÃE:</label>
            <input type="text" name="representante_mae[]"  id="representante_mae_1" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_nacionalidade_1">NACIONALIDADE:</label>
            <input type="text" name="representante_nacionalidade[]"  id="representante_nacionalidade_1" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_naturalidade_1">NATURALIDADE:</label>
            <input type="text" name="representante_naturalidade[]"  id="representante_naturalidade_1" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_estado_nacimento_1">ESTADO NASCIMENTO:</label>
            <select name="representante_estado_nacimento[]" class="form-control">
                <option value="PARANÁ">PARANÁ</option>
            </select>

        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-2">
            <label for="representante_estado_civil_1">ESTADO CIVIL:</label>
            <select name="representante_estado_civil[]" id="representante_estado_civil_1" class="form-control">
                <option value="0">--SELECIONE--</option>
                <option value="1">SOLTEIRO(A)</option>
                <option value="2">CASADO(A)</option>
                <option value="3">SEPARADO(A)</option>
                <option value="4">DIVORCIADO(A)</option>
                <option value="5">VIÚVO(A)</option>
                <option value="6">AMASIADO(A)</option>
            </select>
        </div>
        <div class="col-md-2">
            <label for="representante_profissao_1"> PROFISSÃO:</label>
            <input type="text" name="representante_profissao[]"  id="representante_profissao_1" maxlength="100" class="form-control  " value="<?=$linha['profissao'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_grau_instrucao_1"> GRAU DE INSTRUÇÃO:</label>
            <input type="text" name="representante_grau_instrucao[]"  id="representante_grau_instrucao_1" maxlength="100" class="form-control  " value="<?=$linha['grau_instrucao'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_telefone_1"> TELEFONE:</label>
            <input type="text" name="representante_telefone[]"  id="representante_telefone_1" maxlength="100" class="form-control  " value="<?=$linha['grau_instrucao'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_celular_1"> CELULAR:</label>
            <input type="text" name="representante_celular[]"  id="representante_celular_1" maxlength="100" class="form-control mask-telefone "  value="<?=$linha['grau_instrucao'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_email_1"> E-MAIL:</label>
            <input type="text" name="representante_email[]"  id="representante_email_1" maxlength="100" class="form-control  " value="<?=$linha['grau_instrucao'];?>"/>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-2">
            <label for="cep"> CEP:</label>
            <input type="text" name="cep"  id="cep" onkeyup="mascaraTexto(event,'99999-999')" maxlength="9" class="form-control  " value="<?=$linha['cep'];?>"/>
        </div>
        <div class="col-md-4">
            <label for="logradouro"> LOGRADOURO:</label>
            <input type="text" name="logradouro"  id="logradouro" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="bairro"> BAIRRO:</label>
            <input type="text" name="bairro"  id="bairro" maxlength="255" class="form-control  " value="<?=$linha['bairro'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="bairro"> CIADADE:</label>
            <select name="id_cidade" class="form-control">
                <option>CURITIBA</option>
            </select>
        </div>
        <div class="col-md-1">
            <label for="bairro"> ESTADO:</label>
            <select name="id_estado" class="form-control">
                <option>PR</option>
            </select>
        </div>
    </div>
    <div class="row form-group ">
        <div class="col-md-12 text-center">
            <a href="#" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> ADICIONAR OUTRO REPRESENTANTE </a>
        </div>
    </div>
    <hr />



</form>


