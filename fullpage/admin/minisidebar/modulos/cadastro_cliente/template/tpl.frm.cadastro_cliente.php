<form id="frm_cadastro_cliente">
			<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="pasta"> <?=RTL_PASTA?></label>
			<input type="text" name="pasta"  id="pasta" maxlength="20" class="form-control  " value="<?=$linha['pasta'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="exercito"> <?=RTL_EXERCITO?></label>
			<input type="text" name="exercito"  id="exercito" maxlength="1" class="form-control  mask-numero" value="<?=$linha['exercito'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="policiafederal"> <?=RTL_POLICIAFEDERAL?></label>
			<input type="text" name="policiafederal"  id="policiafederal" maxlength="1" class="form-control  mask-numero" value="<?=$linha['policiafederal'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="policiacivil"> <?=RTL_POLICIACIVIL?></label>
			<input type="text" name="policiacivil"  id="policiacivil" maxlength="1" class="form-control  mask-numero" value="<?=$linha['policiacivil'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="policiacivil_delegacia"> <?=RTL_POLICIACIVIL_DELEGACIA?></label>
			<input type="text" name="policiacivil_delegacia"  id="policiacivil_delegacia" maxlength="255" class="form-control  " value="<?=$linha['policiacivil_delegacia'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="policiacivil_alvara_tipo"> <?=RTL_POLICIACIVIL_ALVARA_TIPO?></label>
			<input type="text" name="policiacivil_alvara_tipo"  id="policiacivil_alvara_tipo" maxlength="255" class="form-control  " value="<?=$linha['policiacivil_alvara_tipo'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="nome_empresa"> <?=RTL_NOME_EMPRESA?></label>
			<input type="text" name="nome_empresa"  id="nome_empresa" maxlength="255" class="form-control  " value="<?=$linha['nome_empresa'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="cnpj"> <?=RTL_CNPJ?></label>
			<input type="text" name="cnpj"  id="cnpj" maxlength="50" class="form-control  " value="<?=$linha['cnpj'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="inscricao_estadual"> <?=RTL_INSCRICAO_ESTADUAL?></label>
			<input type="text" name="inscricao_estadual"  id="inscricao_estadual" maxlength="100" class="form-control  " value="<?=$linha['inscricao_estadual'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="CNAE"> <?=RTL_CNAE?></label>
			<input type="text" name="CNAE"  id="CNAE" maxlength="45" class="form-control  " value="<?=$linha['CNAE'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="conato_licenca"> <?=RTL_CONATO_LICENCA?></label>
			<input type="text" name="conato_licenca"  id="conato_licenca" maxlength="255" class="form-control  " value="<?=$linha['conato_licenca'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="telefone_licenca"> <?=RTL_TELEFONE_LICENCA?></label>
			<input type="text" name="telefone_licenca"  id="telefone_licenca" maxlength="45" class="form-control  " value="<?=$linha['telefone_licenca'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="email_licenca"> <?=RTL_EMAIL_LICENCA?></label>
			<input type="text" name="email_licenca"  id="email_licenca" maxlength="255" class="form-control  " value="<?=$linha['email_licenca'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="logradouro"> <?=RTL_LOGRADOURO?></label>
			<input type="text" name="logradouro"  id="logradouro" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="bairro"> <?=RTL_BAIRRO?></label>
			<input type="text" name="bairro"  id="bairro" maxlength="255" class="form-control  " value="<?=$linha['bairro'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="estado"> <?=RTL_ESTADO?></label>
			<input type="text" name="estado"  id="estado" maxlength="2" class="form-control  " value="<?=$linha['estado'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="cep"> <?=RTL_CEP?></label>
			<input type="text" name="cep"  id="cep" maxlength="15" class="form-control  " value="<?=$linha['cep'];?>"/>
		</div>
	</div>
</form>
<?
include_once("modulos/cadastro_cliente/template/js.frm.cadastro_cliente.php");
?>
