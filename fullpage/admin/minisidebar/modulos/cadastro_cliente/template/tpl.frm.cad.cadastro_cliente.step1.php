<?php

    @session_start();
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    define('URL_FILE',"../../../");
    //require_once('inc/inc.sessao.php');
    //equire_once('inc/funcoes.php');
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
    include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
    include_once(URL_FILE . "modulos/estados/classe.estados.php");
    $objComboProdutos = NEW ProdutosControlados();
    $resulComboProdutos = $objComboProdutos->ComboProdutosControlados();

    $objGrupoEmpresa = NEW GrupoEmpresa();
    $resulComboGrupo = $objGrupoEmpresa->GetCombo();

    $objEstados = NEW Estados();
    $resulComboEstados = $objEstados->GerarSelectEstados();


?>


<form id="frm_cad_cadastro_cliente">
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h5><span class="badge badge-inverse">INFORMAÇÕES INICIAIS PARA CONTROLE INTERNO:</span></h5>
        </div>
    </div>


	<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
    <div class="row form-group">
        <div class="col-md-4">
            <div class="form-group pb-0 mb-0 row">
                <label for="exercito_atividades" class="col-6 col-form-label">PASTA PARA CONTROLE:</label>
                <div class="col-4 text-left">
                    <input id="pasta" class="form-control" name="pasta" type="text" value="" >
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="form-group pb-0 mb-0 row">
                <label for="grupo" class="col-5 col-form-label">GRUPO DA EMPRESA:</label>
                <div class="col-7 text-left">
                    <select name="grupo" id="grupo" class="form-control">
                        <option value="0">--SELECIONE--</option>
                        <?php foreach ($resulComboGrupo as $linha){ echo '<option value="'.$linha["id"].'">'.$linha["grupo"].'</option>'; } ?>

                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <button name="btn_add_novo_grupo" type="button" class="btn btn-info">ADICIONAR UM NOVO GRUPO</button>
        </div>

    </div>


    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">INFORMAÇÕES DA EMPRESA:</span> </h4>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-5">
			<label for="nome_empresa">EMPRESA:</label>
			<input type="text" name="nome_empresa"  id="nome_empresa" maxlength="255" class="form-control  " value="<?=$linha['nome_empresa'];?>"/>
		</div>
        <div class="col-md-4">
            <label for="cnpj"> CNPJ:</label>
            <input type="text" name="cnpj"  id="cnpj" class="form-control  " value="<?=$linha['cnpj'];?>"  onkeyup="mascaraTexto(event,'99.999.999/9999-99')" maxlength="18"/>
        </div>
        <div class="col-md-3">
            <label for="inscricao_estadual"> INSCRIÇÃO ESTADUAL:</label>
            <input type="text" name="inscricao_estadual"  id="inscricao_estadual" maxlength="100" class="form-control  " value="<?=$linha['inscricao_estadual'];?>"/>
        </div>

	</div>
	<div class="row form-group">
        <div class="col-md-9">
            <label for="atividade_principal"> ATIVIDADE PRINCIPAL:</label>
            <input type="text" name="atividade_principal"  id="atividade_principal" maxlength="255" class="form-control  " value="<?=$linha['atividade_principal'];?>"/>
        </div>
		<div class="col-md-3">
			<label for="CNAE"> CNAE</label>
			<input type="text" name="CNAE"  id="CNAE" maxlength="45" class="form-control" value="<?=$linha['CNAE'];?>"/>
		</div>
	</div>
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">ENDEREÇO LICENÇA:</span> </h4>
        </div>
    </div>
	<div class="row form-group">
        <div class="col-md-2">
            <label for="cep"> CEP:</label>
            <input type="text" name="cep"  id="cep" onkeyup="mascaraTexto(event,'99999-999')" maxlength="9" class="form-control  " value="<?=$linha['cep'];?>"/>
        </div>
		<div class="col-md-4">
			<label for="logradouro"> LOGRADOURO:</label>
			<input type="text" name="logradouro"  id="logradouro" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
		</div>
        <div class="col-md-2">
            <label for="bairro"> BAIRRO:</label>
            <input type="text" name="bairro"  id="bairro" maxlength="255" class="form-control  " value="<?=$linha['bairro'];?>"/>
        </div>
        <div class="col-md-1">
            <label for="id_estado"> ESTADO:</label>
            <select id="id_estado" name="id_estado" class="form-control">
                <option value="0">--</option>
                <?php foreach ($resulComboEstados as $linha){ echo '<option value="'.$linha["id_estado"].'">'.$linha["sigla"].'</option>'; } ?>
            </select>
        </div>
        <div class="col-md-3">
            <label for="id_cidade"> CIDADE:</label>
            <select id="id_cidade" name="id_cidade" class="form-control">
                <option>--SELECIONE UM ESTADO--</option>
            </select>
        </div>

	</div>
    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">CONTATO LICENÇA:</span> </h4>
        </div>
    </div>
	<div class="row form-group">
        <div class="col-md-4">
            <label for="conato_licenca"> CONTATO LICENÇA:</label>
            <input type="text" name="conato_licenca"  id="conato_licenca" maxlength="255" class="form-control  " value="<?=$linha['conato_licenca'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="telefone_licenca"> TELEFONE LICENÇA:</label>
            <input type="text" name="telefone_licenca"  id="telefone_licenca" maxlength="45" class="form-control  " value="<?=$linha['telefone_licenca'];?>"/>
        </div>
        <div class="col-md-5">
            <label for="email_licenca"> E-MAIL LICENÇA:</label>
            <input type="text" name="email_licenca"  id="email_licenca" maxlength="255" class="form-control  " value="<?=$linha['email_licenca'];?>"/>
        </div>
	</div>
    <hr />



</form>


