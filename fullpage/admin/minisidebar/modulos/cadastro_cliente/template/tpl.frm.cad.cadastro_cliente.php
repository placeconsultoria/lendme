<?php

    @session_start();
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    define('URL_FILE',"../../../");
    //require_once('inc/inc.sessao.php');
    //equire_once('inc/funcoes.php');
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
    include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
    include_once(URL_FILE . "modulos/estados/classe.estados.php");
    $objComboProdutos = NEW ProdutosControlados();
    $resulComboProdutos = $objComboProdutos->ComboProdutosControlados();

    $objGrupoEmpresa = NEW GrupoEmpresa();
    $resulComboGrupo = $objGrupoEmpresa->GetCombo();

    $objEstados = NEW Estados();
    $resulComboEstados = $objEstados->GerarSelectEstados();


?>


<form id="frm_cad_cadastro_cliente">
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h5><span class="badge badge-inverse">INFORMAÇÕES INICIAIS PARA CONTROLE INTERNO:</span></h5>
        </div>
    </div>


	<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
    <div class="row form-group">
        <div class="col-md-4">
            <div class="form-group pb-0 mb-0 row">
                <label for="exercito_atividades" class="col-6 col-form-label">PASTA PARA CONTROLE:</label>
                <div class="col-4 text-left">
                    <input id="pasta" class="form-control" name="pasta" type="text" value="" >
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="form-group pb-0 mb-0 row">
                <label for="grupo" class="col-5 col-form-label">GRUPO DA EMPRESA:</label>
                <div class="col-7 text-left">
                    <select name="grupo" id="grupo" class="form-control">
                        <option value="0">--SELECIONE--</option>
                        <?php foreach ($resulComboGrupo as $linha){ echo '<option value="'.$linha["id"].'">'.$linha["grupo"].'</option>'; } ?>

                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <button name="btn_add_novo_grupo" type="button" class="btn btn-info">ADICIONAR UM NOVO GRUPO</button>
        </div>

    </div>


    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
           <h4><span class="badge badge-inverse">INFORMAÇÕES DE DEPARTAMENTOS:</span> </h4>
        </div>
    </div>

	<div class="row form-group pb-0 mb-0">
		<div class="col-md-2 pb-0 mb-0">
            <div class="form-check has-success p-t-10 pb-0 mb-0">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="exercito" value="exercito"> EXÉRCITO
                </label>
            </div>
        </div>

        <div class="col-md-10 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="exercito_atividades" class="col-3 col-form-label">EXÉRCITO ATIVIDADES:</label>
                <div class="col-9">
                    <input id="exercito_atividades" class="form-control" name="exercito_atividades" type="text" value="" >
                </div>
            </div>
        </div>
    </div>

    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-2 pb-0 mb-0">
            <div class="form-check has-warning p-t-10 pb-0 mb-0">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="policiafederal" value="exercito"> POLÍCIA FEDERAL
                </label>
            </div>
        </div>

        <div class="col-md-10 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="policiafederal_atividades" class="col-3 col-form-label">DPF ATIVIDADES:</label>
                <div class="col-9">
                    <input name="policiafederal_atividades" class="form-control" type="text" value="" id="policiafederal_atividades">
                </div>
            </div>
        </div>
    </div>

    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-2 pb-0 mb-0">
            <div class="form-check has-inverse p-t-10 pb-0 mb-0">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="policiacivil" value="exercito"> POLÍCIA CIVIL
                </label>
            </div>
        </div>

        <div class="col-md-10 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="policiacivil_atividades" class="col-3 col-form-label">POLÍCIA CIVIL ATIVIDADES:</label>
                <div class="col-9">
                    <input name="policiacivil_atividades" class="form-control" type="text" value="" id="policiacivil_atividades">
                </div>
            </div>
        </div>
    </div>

    <div class="row form-group pb-0 mb-0 p-t-10">
        <div class="col-md-3 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="policiacivil_delegacia" class="col-4 col-form-label">DELEGACIA:</label>
                <div class="col-8">
                    <input name="policiacivil_delegacia" class="form-control" type="text" value="" id="policiacivil_delegacia">
                </div>
            </div>
        </div>

        <div class="col-md-3 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="policiacivil_num_atividades" class="col-9 col-form-label">ATIVIDADE CIVIL NÚMERO:</label>
                <div class="col-3">
                    <input name="policiacivil_num_atividades" class="form-control" type="text" value="" id="policiacivil_num_atividades">
                </div>
            </div>
        </div>

        <div class="col-md-3 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <label for="policiacivil_alvara_tipo" class="col-5 col-form-label">ALVARÁ TIPO:</label>
                <div class="col-7">
                    <input name="policiacivil_alvara_tipo" class="form-control" type="text" value="" id="policiacivil_alvara_tipo">
                </div>
            </div>
        </div>

        <div class="col-md-3 pb-0 mb-0">
            <div class="form-group pb-0 mb-0 row">
                <div class="col-12">
                    <select name="policiacivil_concessao_revalidacao" class="form-control">
                        <option value="0">CONCESSÃO OU REVALIDAÇÃO</option>
                        <option value="1">CONCESSÃO</option>
                        <option value="2">REVALIDAÇÃO</option>
                    </select>
                </div>
            </div>
        </div>

    </div>
    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">INFORMAÇÕES DA EMPRESA:</span> </h4>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-5">
			<label for="nome_empresa">EMPRESA:</label>
			<input type="text" name="nome_empresa"  id="nome_empresa" maxlength="255" class="form-control  " value="<?=$linha['nome_empresa'];?>"/>
		</div>
        <div class="col-md-4">
            <label for="cnpj"> CNPJ:</label>
            <input type="text" name="cnpj"  id="cnpj" class="form-control  " value="<?=$linha['cnpj'];?>"  onkeyup="mascaraTexto(event,'99.999.999/9999-99')" maxlength="18"/>
        </div>
        <div class="col-md-3">
            <label for="inscricao_estadual"> INSCRIÇÃO ESTADUAL:</label>
            <input type="text" name="inscricao_estadual"  id="inscricao_estadual" maxlength="100" class="form-control  " value="<?=$linha['inscricao_estadual'];?>"/>
        </div>

	</div>
	<div class="row form-group">
        <div class="col-md-9">
            <label for="atividade_principal"> ATIVIDADE PRINCIPAL:</label>
            <input type="text" name="atividade_principal"  id="atividade_principal" maxlength="255" class="form-control  " value="<?=$linha['atividade_principal'];?>"/>
        </div>
		<div class="col-md-3">
			<label for="CNAE"> CNAE</label>
			<input type="text" name="CNAE"  id="CNAE" maxlength="45" class="form-control" value="<?=$linha['CNAE'];?>"/>
		</div>
	</div>
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">ENDEREÇO LICENÇA:</span> </h4>
        </div>
    </div>
	<div class="row form-group">
        <div class="col-md-2">
            <label for="cep"> CEP:</label>
            <input type="text" name="cep"  id="cep" onkeyup="mascaraTexto(event,'99999-999')" maxlength="9" class="form-control  " value="<?=$linha['cep'];?>"/>
        </div>
		<div class="col-md-4">
			<label for="logradouro"> LOGRADOURO:</label>
			<input type="text" name="logradouro"  id="logradouro" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
		</div>
        <div class="col-md-2">
            <label for="bairro"> BAIRRO:</label>
            <input type="text" name="bairro"  id="bairro" maxlength="255" class="form-control  " value="<?=$linha['bairro'];?>"/>
        </div>
        <div class="col-md-1">
            <label for="id_estado"> ESTADO:</label>
            <select id="id_estado" name="id_estado" class="form-control">
                <option value="0">--</option>
                <?php foreach ($resulComboEstados as $linha){ echo '<option value="'.$linha["id_estado"].'">'.$linha["sigla"].'</option>'; } ?>
            </select>
        </div>
        <div class="col-md-3">
            <label for="id_cidade"> CIDADE:</label>
            <select id="id_cidade" name="id_cidade" class="form-control">
                <option>--SELECIONE UM ESTADO--</option>
            </select>
        </div>

	</div>
    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">CONTATO LICENÇA:</span> </h4>
        </div>
    </div>
	<div class="row form-group">
        <div class="col-md-4">
            <label for="conato_licenca"> CONTATO LICENÇA:</label>
            <input type="text" name="conato_licenca"  id="conato_licenca" maxlength="255" class="form-control  " value="<?=$linha['conato_licenca'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="telefone_licenca"> TELEFONE LICENÇA:</label>
            <input type="text" name="telefone_licenca"  id="telefone_licenca" maxlength="45" class="form-control  " value="<?=$linha['telefone_licenca'];?>"/>
        </div>
        <div class="col-md-5">
            <label for="email_licenca"> E-MAIL LICENÇA:</label>
            <input type="text" name="email_licenca"  id="email_licenca" maxlength="255" class="form-control  " value="<?=$linha['email_licenca'];?>"/>
        </div>
	</div>
    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">CONTATO ESCRITÓRIO:</span> </h4>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-3">
            <label for="contato_nome_1"> NOME DO CONTATO:</label>
            <input type="text" name="contato_nome[]"  id="contato_nome_1" maxlength="255" class="form-control  " value="<?=$linha['nome'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="contato_telefone_1">TELEFONE:</label>
            <input type="text" name="contato_telefone[]"  id="contato_telefone_1" maxlength="20" class="form-control  " value="<?=$linha['telefone'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="contato_celular_1"> CELULAR:</label>
            <input type="text" name="contato_celular[]"  id="contato_celular_1" maxlength="20" class="form-control  " value="<?=$linha['celular'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="contato_email_1"> CONTATO E-MAIL:</label>
            <input type="text" name="contato_email[]"  id="contato_email_1" maxlength="255" class="form-control  " value="<?=$linha['email'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="contato_data_nascimento_1"> NASCIMENTO:</label>
            <input type="text" name="contato_data_nascimento[]"  id="contato_data_nascimento_1"  class="form-control  mask-date" value="<?=$linha['data_nascimento'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>

    </div>
    <div class="row form-group">
            <div class="col-md-2">
                <label for="cep"> CEP:</label>
                <input type="text" name="cep"  id="cep" onkeyup="mascaraTexto(event,'99999-999')" maxlength="9" class="form-control  " value="<?=$linha['cep'];?>"/>
            </div>
            <div class="col-md-4">
                <label for="logradouro"> LOGRADOURO:</label>
                <input type="text" name="logradouro"  id="logradouro" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
            </div>
            <div class="col-md-2">
                <label for="bairro"> BAIRRO:</label>
                <input type="text" name="bairro"  id="bairro" maxlength="255" class="form-control  " value="<?=$linha['bairro'];?>"/>
            </div>
            <div class="col-md-3">
                <label for="bairro"> CIADADE:</label>
                <select name="id_cidade" class="form-control">
                    <option>CURITIBA</option>
                </select>
            </div>
            <div class="col-md-1">
                <label for="bairro"> ESTADO:</label>
                <select name="id_estado" class="form-control">
                    <option>PR</option>
                </select>
            </div>
     </div>
    <div class="row form-group ">
        <div class="col-md-12 text-center">
            <a href="#" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> ADICIONAR OUTRO CONTATO </a>
        </div>
    </div>
    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">REPRESENTANTE:</span> </h4>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-3">
            <label for="representante_nome_1"> NOME:</label>
            <input type="text" name="representante_nome[]"  id="representante_nome_1" maxlength="255" class="form-control  " value="<?=$linha['nome'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_cargo_1"> CARGO:</label>
            <input type="text" name="representante_cargo[]"  id="representante_cargo_1" maxlength="100" class="form-control " value="<?=$linha['cargo'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_rg_1"> RG:</label>
            <input type="text" name="representante_rg[]"  id="representante_rg_1" maxlength="45" class="form-control  " value="<?=$linha['rg'];?>"/>
        </div>
        <div class="col-md-1">
            <label for="representante_orgao_rg_1"> ORGÃO:</label>
            <input type="text" name="representante_orgao_rg[]"  id="representante_orgao_rg_1" maxlength="100" class="form-control  " value="<?=$linha['orgao_rg'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_cpf_1"> CPF:</label>
            <input type="text" name="representante_cpf[]"  id="representante_cpf_1"  class="form-control  " value="<?=$linha['cpf'];?>" onkeyup="mascaraTexto(event,'999.999.999-99')" maxlength="14" />
        </div>

        <div class="col-md-2">
            <label for="representante_nascimento_1"> NASCIMENTO:</label>
            <input type="text" name="representante_nascimento[]"  id="representante_nascimento_1" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" class="form-control  " value="<?=$linha['cpf'];?>"/>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-3">
            <label for="representante_pai_1">PAI:</label>
            <input type="text" name="representante_pai[]"  id="representante_pai_1" maxlength="255" class="form-control  " value="<?=$linha['pai'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="representante_mae_1">MÃE:</label>
            <input type="text" name="representante_mae[]"  id="representante_mae_1" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_nacionalidade_1">NACIONALIDADE:</label>
            <input type="text" name="representante_nacionalidade[]"  id="representante_nacionalidade_1" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_naturalidade_1">NATURALIDADE:</label>
            <input type="text" name="representante_naturalidade[]"  id="representante_naturalidade_1" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_estado_nacimento_1">ESTADO NASCIMENTO:</label>
            <select name="representante_estado_nacimento[]" class="form-control">
                <option value="PARANÁ">PARANÁ</option>
            </select>

        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-2">
            <label for="representante_estado_civil_1">ESTADO CIVIL:</label>
            <select name="representante_estado_civil[]" id="representante_estado_civil_1" class="form-control">
                <option value="0">--SELECIONE--</option>
                <option value="1">SOLTEIRO(A)</option>
                <option value="2">CASADO(A)</option>
                <option value="3">SEPARADO(A)</option>
                <option value="4">DIVORCIADO(A)</option>
                <option value="5">VIÚVO(A)</option>
                <option value="6">AMASIADO(A)</option>
            </select>
        </div>
        <div class="col-md-2">
            <label for="representante_profissao_1"> PROFISSÃO:</label>
            <input type="text" name="representante_profissao[]"  id="representante_profissao_1" maxlength="100" class="form-control  " value="<?=$linha['profissao'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_grau_instrucao_1"> GRAU DE INSTRUÇÃO:</label>
            <input type="text" name="representante_grau_instrucao[]"  id="representante_grau_instrucao_1" maxlength="100" class="form-control  " value="<?=$linha['grau_instrucao'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_telefone_1"> TELEFONE:</label>
            <input type="text" name="representante_telefone[]"  id="representante_telefone_1" maxlength="100" class="form-control  " value="<?=$linha['grau_instrucao'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_celular_1"> CELULAR:</label>
            <input type="text" name="representante_celular[]"  id="representante_celular_1" maxlength="100" class="form-control mask-telefone "  value="<?=$linha['grau_instrucao'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="representante_email_1"> E-MAIL:</label>
            <input type="text" name="representante_email[]"  id="representante_email_1" maxlength="100" class="form-control  " value="<?=$linha['grau_instrucao'];?>"/>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-2">
            <label for="cep"> CEP:</label>
            <input type="text" name="cep"  id="cep" onkeyup="mascaraTexto(event,'99999-999')" maxlength="9" class="form-control  " value="<?=$linha['cep'];?>"/>
        </div>
        <div class="col-md-4">
            <label for="logradouro"> LOGRADOURO:</label>
            <input type="text" name="logradouro"  id="logradouro" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="bairro"> BAIRRO:</label>
            <input type="text" name="bairro"  id="bairro" maxlength="255" class="form-control  " value="<?=$linha['bairro'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="bairro"> CIADADE:</label>
            <select name="id_cidade" class="form-control">
                <option>CURITIBA</option>
            </select>
        </div>
        <div class="col-md-1">
            <label for="bairro"> ESTADO:</label>
            <select name="id_estado" class="form-control">
                <option>PR</option>
            </select>
        </div>
    </div>
    <div class="row form-group ">
        <div class="col-md-12 text-center">
            <a href="#" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> ADICIONAR OUTRO REPRESENTANTE </a>
        </div>
    </div>
    <hr />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">LISTA DE PRODUTOS CONTROLADOS:</span> </h4>
        </div>
    </div>

    <div class="row form-group pb-0 mb-0">
        <div class="col-md-6 pb-0 mb-0">
            <select id="lista_produto_id_1" name="lista_produto_id[]" class="select2 pb-0 mb-0" style="width: 100%; height: 60px;">
                <option>--SELECIONE O PRODUTO CONTROLADO--</option>
                <?php foreach ($resulComboProdutos as $linha){ echo '<option value="'.$linha["id"].'">'.$linha["nome"].'</option>'; } ?>
            </select>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <input placeholder="CAT. CONTROLE" type="text" name="lista_produto_cat_controle[]"  id="lista_produto_cat_controle_1" maxlength="255" class="form-control ajusta-input " value="<?=$linha['cat_controle'];?>"/>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <input placeholder="QUANTIDADE" type="text" name="lista_produto_quantidade[]"  id="ista_produto_quantidade_1" maxlength="255" class="form-control ajusta-input  " value="<?=$linha['cat_controle'];?>"/>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <select name="lista_produto_unidade[]" id="lista_produto_unidade_1" class="form-control ajusta-input">
                <option>--UNIDADE--</option>
                <option>KG</option>
                <option>KG</option>
            </select>
        </div>
    </div>
    <hr class="ajusta-input" />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-6 pb-0 mb-0">
            <select id="lista_produto_id_1" name="lista_produto_id[]" class="select2 pb-0 mb-0" style="width: 100%; height: 60px;">
                <option>--SELECIONE O PRODUTO CONTROLADO--</option>
                <?php foreach ($resulComboProdutos as $linha){ echo '<option value="'.$linha["id"].'">'.$linha["nome"].'</option>'; } ?>
            </select>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <input placeholder="CAT. CONTROLE" type="text" name="lista_produto_cat_controle[]"  id="lista_produto_cat_controle_1" maxlength="255" class="form-control ajusta-input " value="<?=$linha['cat_controle'];?>"/>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <input placeholder="QUANTIDADE" type="text" name="lista_produto_quantidade[]"  id="ista_produto_quantidade_1" maxlength="255" class="form-control ajusta-input  " value="<?=$linha['cat_controle'];?>"/>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <select name="lista_produto_unidade[]" id="lista_produto_unidade_1" class="form-control ajusta-input">
                <option>--UNIDADE--</option>
                <option>KG</option>
                <option>KG</option>
            </select>
        </div>
    </div>
    <hr class="ajusta-input" />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-6 pb-0 mb-0">
            <select id="lista_produto_id_1" name="lista_produto_id[]" class="select2 pb-0 mb-0" style="width: 100%; height: 60px;">
                <option>--SELECIONE O PRODUTO CONTROLADO--</option>
                <?php foreach ($resulComboProdutos as $linha){ echo '<option value="'.$linha["id"].'">'.$linha["nome"].'</option>'; } ?>
            </select>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <input placeholder="CAT. CONTROLE" type="text" name="lista_produto_cat_controle[]"  id="lista_produto_cat_controle_1" maxlength="255" class="form-control ajusta-input " value="<?=$linha['cat_controle'];?>"/>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <input placeholder="QUANTIDADE" type="text" name="lista_produto_quantidade[]"  id="ista_produto_quantidade_1" maxlength="255" class="form-control ajusta-input  " value="<?=$linha['cat_controle'];?>"/>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <select name="lista_produto_unidade[]" id="lista_produto_unidade_1" class="form-control ajusta-input">
                <option>--UNIDADE--</option>
                <option>KG</option>
                <option>KG</option>
            </select>
        </div>
    </div>
    <hr class="ajusta-input" />
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-6 pb-0 mb-0">
            <select id="lista_produto_id_1" name="lista_produto_id[]" class="select2 pb-0 mb-0" style="width: 100%; height: 60px;">
                <option>--SELECIONE O PRODUTO CONTROLADO--</option>
                <?php foreach ($resulComboProdutos as $linha){ echo '<option value="'.$linha["id"].'">'.$linha["nome"].'</option>'; } ?>
            </select>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <input placeholder="CAT. CONTROLE" type="text" name="lista_produto_cat_controle[]"  id="lista_produto_cat_controle_1" maxlength="255" class="form-control ajusta-input " value="<?=$linha['cat_controle'];?>"/>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <input placeholder="QUANTIDADE" type="text" name="lista_produto_quantidade[]"  id="ista_produto_quantidade_1" maxlength="255" class="form-control ajusta-input  " value="<?=$linha['cat_controle'];?>"/>
        </div>
        <div class="col-md-2 pb-0 mb-0">
            <select name="lista_produto_unidade[]" id="lista_produto_unidade_1" class="form-control ajusta-input">
                <option>--UNIDADE--</option>
                <option>KG</option>
                <option>KG</option>
            </select>
        </div>
    </div>
    <div id="load_linha_produto"><!-- LOAD MAIS LINHAS --></div>
    <div class="row form-group p-t-20 ">
        <div class="col-md-12 text-center">
            <a href="#" name="add_linha_produto_controlado" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> ADICIONAR OUTRA LINHA PARA PRODUTO CONTROLADO </a>
        </div>
    </div>
    <hr />


</form>


