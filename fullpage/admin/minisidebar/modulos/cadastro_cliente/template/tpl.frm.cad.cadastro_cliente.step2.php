<?php

    @session_start();
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    define('URL_FILE',"../../../");
    //require_once('inc/inc.sessao.php');
    //equire_once('inc/funcoes.php');
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
    include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
    include_once(URL_FILE . "modulos/estados/classe.estados.php");
    $objComboProdutos = NEW ProdutosControlados();
    $resulComboProdutos = $objComboProdutos->ComboProdutosControlados();

    $objGrupoEmpresa = NEW GrupoEmpresa();
    $resulComboGrupo = $objGrupoEmpresa->GetCombo();

    $objEstados = NEW Estados();
    $resulComboEstados = $objEstados->GerarSelectEstados();


?>


<form id="frm_cad_cadastro_cliente">
    <div class="row form-group pb-0 mb-0">
        <div class="col-md-12">
            <h4><span class="badge badge-inverse">CONTATO ESCRITÓRIO:</span> </h4>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-3">
            <label for="contato_nome_1"> NOME DO CONTATO:</label>
            <input type="text" name="contato_nome[]"  id="contato_nome_1" maxlength="255" class="form-control  " value="<?=$linha['nome'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="contato_telefone_1">TELEFONE:</label>
            <input type="text" name="contato_telefone[]"  id="contato_telefone_1" maxlength="20" class="form-control  " value="<?=$linha['telefone'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="contato_celular_1"> CELULAR:</label>
            <input type="text" name="contato_celular[]"  id="contato_celular_1" maxlength="20" class="form-control  " value="<?=$linha['celular'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="contato_email_1"> CONTATO E-MAIL:</label>
            <input type="text" name="contato_email[]"  id="contato_email_1" maxlength="255" class="form-control  " value="<?=$linha['email'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="contato_data_nascimento_1"> NASCIMENTO:</label>
            <input type="text" name="contato_data_nascimento[]"  id="contato_data_nascimento_1"  class="form-control  mask-date" value="<?=$linha['data_nascimento'];?>" onkeyup="mascaraTexto(event,'99/99/9999')" maxlength="10" />
        </div>

    </div>
    <div class="row form-group">
        <div class="col-md-1">
            <label for="cep"> CEP:</label>
            <input type="text" name="cep"  id="cep" onkeyup="mascaraTexto(event,'99999-999')" maxlength="9" class="form-control  " value="<?=$linha['cep'];?>"/>
        </div>
        <div class="col-md-3">
            <label for="logradouro"> LOGRADOURO:</label>
            <input type="text" name="logradouro"  id="logradouro" maxlength="255" class="form-control  " value="<?=$linha['logradouro'];?>"/>
        </div>
        <div class="col-md-2">
            <label for="bairro"> BAIRRO:</label>
            <input type="text" name="bairro"  id="bairro" maxlength="255" class="form-control  " value="<?=$linha['bairro'];?>"/>
        </div>

        <div class="col-md-1">
            <label for="bairro"> ESTADO:</label>
            <select name="id_estado" class="form-control">
                <option>PR</option>
            </select>
        </div>

        <div class="col-md-3">
            <label for="bairro"> CIDADE:</label>
            <select name="id_cidade" class="form-control">
                <option>CURITIBA</option>
            </select>
        </div>
        <div class="col-md-2">
            <label for="bairro"> CAIXA POSTAL:</label>
            <input type="text" name="caixa_postal"  id="caixa_postal" maxlength="255" class="form-control  " value="<?=$linha['caixa_postal'];?>"/>
        </div>
    </div>
    <div class="row form-group ">
        <div class="col-md-12 text-center">
            <a href="#" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> ADICIONAR OUTRO CONTATO DO ESCRITÓRIO </a>
        </div>
    </div>
    <hr />


</form>


