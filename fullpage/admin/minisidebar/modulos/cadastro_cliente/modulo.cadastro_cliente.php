<?
define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/cadastro_cliente/classe.cadastro_cliente.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{


	case "adicionar_cadastro_cliente":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCadastroCliente = new CadastroCliente($pdo);
			$objCadastroCliente->setPasta($_REQUEST['pasta']);
			$objCadastroCliente->setIdPastaMae($_REQUEST['id_pasta_mae']);
			$objCadastroCliente->setExercito($_REQUEST['exercito']);
			$objCadastroCliente->setPoliciafederal($_REQUEST['policiafederal']);
			$objCadastroCliente->setPoliciacivil($_REQUEST['policiacivil']);
			$objCadastroCliente->setExercitoAtividades($_REQUEST['exercito_atividades']);
			$objCadastroCliente->setPoliciafederalAtividades($_REQUEST['policiafederal_atividades']);
			$objCadastroCliente->setPoliciacivilAtividades($_REQUEST['policiacivil_atividades']);
			$objCadastroCliente->setPoliciacivilDelegacia($_REQUEST['policiacivil_delegacia']);
			$objCadastroCliente->setPoliciacivilNumAtividades($_REQUEST['policiacivil_num_atividades']);
			$objCadastroCliente->setPoliciacivilAlvaraTipo($_REQUEST['policiacivil_alvara_tipo']);
			$objCadastroCliente->setPoliciacivilConcessaoRevalidacao($_REQUEST['policiacivil_concessao_revalidacao']);
			$objCadastroCliente->setNomeEmpresa($_REQUEST['nome_empresa']);
			$objCadastroCliente->setCnpj($_REQUEST['cnpj']);
			$objCadastroCliente->setInscricaoEstadual($_REQUEST['inscricao_estadual']);
			$objCadastroCliente->setCNAE($_REQUEST['CNAE']);
			$objCadastroCliente->setAtividadePrincipal($_REQUEST['atividade_principal']);
			$objCadastroCliente->setConatoLicenca($_REQUEST['conato_licenca']);
			$objCadastroCliente->setTelefoneLicenca($_REQUEST['telefone_licenca']);
			$objCadastroCliente->setEmailLicenca($_REQUEST['email_licenca']);
			$objCadastroCliente->setLogradouro($_REQUEST['logradouro']);
			$objCadastroCliente->setBairro($_REQUEST['bairro']);
			$objCadastroCliente->setCidade($_REQUEST['cidade']);
			$objCadastroCliente->setIdCidade($_REQUEST['id_cidade']);
			$objCadastroCliente->setEstado($_REQUEST['estado']);
			$objCadastroCliente->setIdEstado($_REQUEST['id_estado']);
			$objCadastroCliente->setCep($_REQUEST['cep']);
            $objCadastroCliente->setGrupo($_REQUEST['grupo']);
			$novoId = $objCadastroCliente->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.cadastro_cliente.php";
		break;



	case "atualizar_cadastro_cliente":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCadastroCliente = new CadastroCliente($pdo);
			$objCadastroCliente->setId($_REQUEST['id']);
			$objCadastroCliente->setPasta($_REQUEST['pasta']);
			$objCadastroCliente->setIdPastaMae($_REQUEST['id_pasta_mae']);
			$objCadastroCliente->setExercito($_REQUEST['exercito']);
			$objCadastroCliente->setPoliciafederal($_REQUEST['policiafederal']);
			$objCadastroCliente->setPoliciacivil($_REQUEST['policiacivil']);
			$objCadastroCliente->setExercitoAtividades($_REQUEST['exercito_atividades']);
			$objCadastroCliente->setPoliciafederalAtividades($_REQUEST['policiafederal_atividades']);
			$objCadastroCliente->setPoliciacivilAtividades($_REQUEST['policiacivil_atividades']);
			$objCadastroCliente->setPoliciacivilDelegacia($_REQUEST['policiacivil_delegacia']);
			$objCadastroCliente->setPoliciacivilNumAtividades($_REQUEST['policiacivil_num_atividades']);
			$objCadastroCliente->setPoliciacivilAlvaraTipo($_REQUEST['policiacivil_alvara_tipo']);
			$objCadastroCliente->setPoliciacivilConcessaoRevalidacao($_REQUEST['policiacivil_concessao_revalidacao']);
			$objCadastroCliente->setNomeEmpresa($_REQUEST['nome_empresa']);
			$objCadastroCliente->setCnpj($_REQUEST['cnpj']);
			$objCadastroCliente->setInscricaoEstadual($_REQUEST['inscricao_estadual']);
			$objCadastroCliente->setCNAE($_REQUEST['CNAE']);
			$objCadastroCliente->setAtividadePrincipal($_REQUEST['atividade_principal']);
			$objCadastroCliente->setConatoLicenca($_REQUEST['conato_licenca']);
			$objCadastroCliente->setTelefoneLicenca($_REQUEST['telefone_licenca']);
			$objCadastroCliente->setEmailLicenca($_REQUEST['email_licenca']);
			$objCadastroCliente->setLogradouro($_REQUEST['logradouro']);
			$objCadastroCliente->setBairro($_REQUEST['bairro']);
			$objCadastroCliente->setCidade($_REQUEST['cidade']);
			$objCadastroCliente->setIdCidade($_REQUEST['id_cidade']);
			$objCadastroCliente->setEstado($_REQUEST['estado']);
			$objCadastroCliente->setIdEstado($_REQUEST['id_estado']);
			$objCadastroCliente->setCep($_REQUEST['cep']);
			$objCadastroCliente->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.cadastro_cliente.php";
		break;

	case "deletar_cadastro_cliente":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCadastroCliente = new CadastroCliente($pdo);
			$objCadastroCliente->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.cadastro_cliente.php";
		break;



	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["cadastro_cliente"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("cadastro_cliente");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.cadastro_cliente.php";
		break;
}
