<?php

define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");

$objProdutos = new ProdutosControlados();
$lista = $objProdutos->Listar();
$url_edit = "edit_produto.php?id=";

//print_r($lista);

?>

<div class="col-md-12">
    <div class="table-responsive">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th><small>NCM.</small></th>
                <th><small>CONTROLE</small></th>
                <th><small>LISTA / N. ORDEM</small></th>
                <th><small>GRUPOS</small></th>
                <th><small>PRODUTO</small></th>
                <th><small>NOMENCLATURA</small></th>
                <th><small>CONCENTRAÇÃO</small></th>
                <th><small>DENSIDADE</small></th>
                <th><small>AÇÕES</small></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(count($lista) > 0){
                foreach($lista as $linha){
                    $id = $linha["id"];

                    echo '
                             <tr>
                                <td>'.$linha["cod_ncm"].'</td>
                                <td>'.$linha["cat_controle"].'</td>
                                <td>'.$linha["num_ordem"].'</td>
                                <td>'.$linha["grupos"].'</td>
                                <td>'.$linha["nome"].'</td>
                                <td>'.$linha["nomenclatura"].'</td>
                                <td>'.$linha["concentracao"].'</td>
                                <td>'.$linha["densidade"].'</td>
                                <td><a href="'.$url_edit.$id.'" class="btn btn-warning"> GERENCIAR <i class="fa fa-pencil"></i></a> </td>

                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>



<script src="../../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>
