<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
$app_comando = $_REQUEST["acao"];



switch($app_comando)
{
	case "frm_adicionar_produtos_controlados":
		$template = "tpl.frm.produtos_controlados.php";
		break;

    case "editar_voce_sabia_intra":
        $pdo = new Conexao();
        $pdo->beginTransaction();
        try {
            $objProdutosControlados = new ProdutosControlados($pdo);
            $objProdutosControlados->setVoceSaibaIntra($_REQUEST['texto']);
            $objProdutosControlados->setId($_REQUEST['id_produto']);
            $res = $objProdutosControlados->EditVoceSabiaIntra();
            $msg["codigo"] = 0;
            $msg["mensagem"] = "SUCESSO EM EDITAR VOCÊ SABIA PARA ESTE PRODUTO CONTROLADO";
            $pdo->commit();
        } catch (Exception $e) {
            $msg["codigo"] = 1;
            $msg["mensagem"] = "ERRO: ".$e->getMessage();
            $msg["debug"] = $e->getMessage();
            $pdo->rollBack();
        }
        echo json_encode($msg);
        $template = "ajax.produtos_controlados.php";
        break;


	case "adicionar_produtos_controlados":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objProdutosControlados = new ProdutosControlados($pdo);
			$objProdutosControlados->setCodNcm($_REQUEST['cod_ncm']);
			$objProdutosControlados->setCatControle($_REQUEST['cat_controle']);
			$objProdutosControlados->setGrupos($_REQUEST['grupos']);
			$objProdutosControlados->setNome($_REQUEST['nome']);
			$objProdutosControlados->setNomenclatura($_REQUEST['nomenclatura']);
			$objProdutosControlados->setConcentracao($_REQUEST['concentracao']);
			$objProdutosControlados->setDensidade($_REQUEST['densidade']);
			$objProdutosControlados->setDescricao($_REQUEST['descricao']);
            $objProdutosControlados->setNumOrdem($_REQUEST['num_ordem']);
			$objProdutosControlados->setDataCadastro($_REQUEST['data_cadastro']);
			$novoId = $objProdutosControlados->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "SUCESSO EM ADICIONAR ESTE PRODUTO CONTROLADO";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO: ".$e->getMessage();
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.produtos_controlados.php";
		break;



	case "edit_produto_controlado":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objProdutosControlados = new ProdutosControlados($pdo);
			$objProdutosControlados->setId($_REQUEST['id']);
			$objProdutosControlados->setCodNcm($_REQUEST['cod_ncm']);
			$objProdutosControlados->setCatControle($_REQUEST['cat_controle']);
			$objProdutosControlados->setGrupos($_REQUEST['grupos']);
			$objProdutosControlados->setNome($_REQUEST['nome']);
			$objProdutosControlados->setNomenclatura($_REQUEST['nomenclatura']);
			$objProdutosControlados->setConcentracao($_REQUEST['concentracao']);
			$objProdutosControlados->setDensidade($_REQUEST['densidade']);
			$objProdutosControlados->setDescricao($_REQUEST['descricao']);
			$objProdutosControlados->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Produto alterado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro em alterar o produto";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.produtos_controlados.php";
		break;

	case "listar_produtos_controlados":
		$template = "tpl.geral.produtos_controlados.php";
		break;

	case "deletar_produtos_controlados":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objProdutosControlados = new ProdutosControlados($pdo);
			$objProdutosControlados->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.produtos_controlados.php";
		break;

	case "ajax_listar_produtos_controlados":
		$template = "tpl.lis.produtos_controlados.php";
		break;

	case "produtos_controlados_pdf":
		$template = "tpl.lis.produtos_controlados.pdf.php";
		break;

	case "produtos_controlados_xlsx":
		$template = "tpl.lis.produtos_controlados.xlsx.php";
		break;

	case "produtos_controlados_print":
		$template = "tpl.lis.produtos_controlados.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["produtos_controlados"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("produtos_controlados");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.produtos_controlados.php";
		break;
}
