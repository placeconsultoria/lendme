<?
class ProdutosControlados
{
	private $id;
	private $cod_ncm;
	private $cat_controle;
	private $num_ordem;
	private $grupos;
	private $nome;
	private $nomenclatura;
	private $concentracao;
	private $densidade;
	private $descricao;
	private $data_cadastro;
	private $voce_sabia_intra;
	private $codigo_dpf;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setCodNcm($arg)
	{
		$this->cod_ncm = $arg;
	}
 	
	public function getCodNcm()
	{
		return $this->cod_ncm;
	}
 	
	public function setCatControle($arg)
	{
		$this->cat_controle = $arg;
	}
 	
	public function getCatControle()
	{
		return $this->cat_controle;
	}
 	
	public function setGrupos($arg)
	{
		$this->grupos = $arg;
	}
 	
	public function getGrupos()
	{
		return $this->grupos;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setNomenclatura($arg)
	{
		$this->nomenclatura = $arg;
	}
 	
	public function getNomenclatura()
	{
		return $this->nomenclatura;
	}
 	
	public function setConcentracao($arg)
	{
		$this->concentracao = $arg;
	}
 	
	public function getConcentracao()
	{
		return $this->concentracao;
	}
 	
	public function setDensidade($arg)
	{
		$this->densidade = $arg;
	}
 	
	public function getDensidade()
	{
		return $this->densidade;
	}
 	
	public function setDescricao($arg)
	{
		$this->descricao = $arg;
	}
 	
	public function getDescricao()
	{
		return $this->descricao;
	}
 	
	public function setDataCadastro($arg)
	{
		$this->data_cadastro = $arg;
	}

    public function getVoceSabiaIntra()
    {
        return $this->voce_sabia_intra;
    }

    public function setVoceSaibaIntra($arg)
    {
        $this->voce_sabia_intra = $arg;
    }


    public function getCodigoDpf()
    {
        return $this->codigo_dpf;
    }

    public function setCodigoDpf($arg)
    {
        $this->codigo_dpf = $arg;
    }
 	
	public function getDataCadastro()
	{
		return $this->data_cadastro;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}


	public function setNumOrdem($arg)
    {
        $this->num_ordem = $arg;
    }

	public function getNumOrdem()
    {
        return $this->num_ordem;
    }

 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO produtos_controlados SET data_cadastro = NOW()';
		if ($this->getCodNcm() != "") $sql .= ",cod_ncm = ?";
		if ($this->getCatControle() != "") $sql .= ",cat_controle = ?";
		if ($this->getGrupos() != "") $sql .= ",grupos = ?";
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getNomenclatura() != "") $sql .= ",nomenclatura = ?";
		if ($this->getConcentracao() != "") $sql .= ",concentracao = ?";
		if ($this->getDensidade() != "") $sql .= ",densidade = ?";
		if ($this->getDescricao() != "") $sql .= ",descricao = ?";
        if ($this->getNumOrdem() != "") $sql .= ",num_ordem = ?";
        if ($this->getCodigoDpf() != "") $sql .= ",codigo_dpf = ?";
        $stmt = $pdo->prepare($sql);
		if ($this->getCodNcm() != "") $stmt->bindParam(++$x,$this->getCodNcm(),PDO::PARAM_STR);
		if ($this->getCatControle() != "") $stmt->bindParam(++$x,$this->getCatControle(),PDO::PARAM_INT);
		if ($this->getGrupos() != "") $stmt->bindParam(++$x,$this->getGrupos(),PDO::PARAM_STR);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getNomenclatura() != "") $stmt->bindParam(++$x,$this->getNomenclatura(),PDO::PARAM_STR);
		if ($this->getConcentracao() != "") $stmt->bindParam(++$x,$this->getConcentracao(),PDO::PARAM_STR);
		if ($this->getDensidade() != "") $stmt->bindParam(++$x,$this->getDensidade(),PDO::PARAM_STR);
		if ($this->getDescricao() != "") $stmt->bindParam(++$x,$this->getDescricao(),PDO::PARAM_STR);
        if ($this->getNumOrdem() != "") $stmt->bindParam(++$x,$this->getNumOrdem(),PDO::PARAM_STR);
        if ($this->getCodigoDpf() != "") $stmt->bindParam(++$x,$this->getCodigoDpf(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE produtos_controlados SET nome = ?, nomenclatura =?  ';
		if ($this->getCodNcm() != "") $sql .= " ,cod_ncm = ?";
		if ($this->getCatControle() != "") $sql .= ",cat_controle = ?";
		if ($this->getGrupos() != "") $sql .= ",grupos = ?";
		if ($this->getConcentracao() != "") $sql .= ",concentracao = ?";
		if ($this->getDensidade() != "") $sql .= ",densidade = ?";
		if ($this->getDescricao() != "") $sql .= ",descricao = ?";
		if ($this->getDataCadastro() != "") $sql .= ",data_cadastro = ?";
        if ($this->getNumOrdem() != "") $sql .= ",num_ordem = ?";


        $sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
        $stmt->bindParam(++$x,$this->getNomenclatura(),PDO::PARAM_STR);

        if ($this->getCodNcm() != "") $stmt->bindParam(++$x,$this->getCodNcm(),PDO::PARAM_STR);
		if ($this->getCatControle() != "") $stmt->bindParam(++$x,$this->getCatControle(),PDO::PARAM_INT);
		if ($this->getGrupos() != "") $stmt->bindParam(++$x,$this->getGrupos(),PDO::PARAM_STR);
		if ($this->getConcentracao() != "") $stmt->bindParam(++$x,$this->getConcentracao(),PDO::PARAM_STR);
		if ($this->getDensidade() != "") $stmt->bindParam(++$x,$this->getDensidade(),PDO::PARAM_STR);
		if ($this->getDescricao() != "") $stmt->bindParam(++$x,$this->getDescricao(),PDO::PARAM_STR);
		if ($this->getDataCadastro() != "") $stmt->bindParam(++$x,$this->getDataCadastro(),PDO::PARAM_STR);
        if ($this->getNumOrdem() != "") $stmt->bindParam(++$x,$this->getNumOrdem(),PDO::PARAM_STR);

        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover()
	{
		$pdo = $this->getConexao();
		$sql = "DELETE FROM produtos_controlados WHERE id = ?";
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "WHERE produtos_controlados.id > 0";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM produtos_controlados
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				produtos_controlados.*
			FROM produtos_controlados
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY produtos_controlados.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM produtos_controlados WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}


    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM produtos_controlados";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function ComboProdutosControlados()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id, nome FROM produtos_controlados ORDER BY cat_controle, nome ";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function EditVoceSabiaIntra()
    {
        $pdo = $this->getConexao();
        $sql = 'UPDATE produtos_controlados SET ';
        if ($this->getVoceSabiaIntra() != "") $sql .= " voce_sabia_intra = ?";
        $sql .= ' WHERE id = ?';
        $stmt = $pdo->prepare($sql);
        if ($this->getVoceSabiaIntra() != "") $stmt->bindParam(++$x,$this->getVoceSabiaIntra(),PDO::PARAM_STR);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
        return $stmt->execute();
    }

    public function SelectedSelect($valor, $banco){
	    if($valor == $banco) echo "selected";
    }


}
