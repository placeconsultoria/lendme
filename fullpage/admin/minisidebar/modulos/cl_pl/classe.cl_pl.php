<?
class ClPl
{
	private $id;
	private $id_controle;
	private $id_usuario;
	private $tipo;
	private $palavra;
	private $ocorrencia;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdControle($arg)
	{
		$this->id_controle = $arg;
	}
 	
	public function getIdControle()
	{
		return $this->id_controle;
	}
 	
	public function setIdUsuario($arg)
	{
		$this->id_usuario = $arg;
	}
 	
	public function getIdUsuario()
	{
		return $this->id_usuario;
	}
 	
	public function setTipo($arg)
	{
		$this->tipo = $arg;
	}
 	
	public function getTipo()
	{
		return $this->tipo;
	}
 	
	public function setPalavra($arg)
	{
		$this->palavra = $arg;
	}
 	
	public function getPalavra()
	{
		return $this->palavra;
	}
 	
	public function setOcorrencia($arg)
	{
		$this->ocorrencia = $arg;
	}
 	
	public function getOcorrencia()
	{
		return $this->ocorrencia;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO cl_pl SET ';
		if ($this->getIdControle() != "") $sql .= "id_controle = ?";
		if ($this->getIdUsuario() != "") $sql .= ",id_usuario = ?";
		if ($this->getTipo() != "") $sql .= ",tipo = ?";
		if ($this->getPalavra() != "") $sql .= ",palavra = ?";
		if ($this->getOcorrencia() != "") $sql .= ",ocorrencia = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdControle() != "") $stmt->bindParam(++$x,$this->getIdControle(),PDO::PARAM_INT);
		if ($this->getIdUsuario() != "") $stmt->bindParam(++$x,$this->getIdUsuario(),PDO::PARAM_INT);
		if ($this->getTipo() != "") $stmt->bindParam(++$x,$this->getTipo(),PDO::PARAM_INT);
		if ($this->getPalavra() != "") $stmt->bindParam(++$x,$this->getPalavra(),PDO::PARAM_INT);
		if ($this->getOcorrencia() != "") $stmt->bindParam(++$x,$this->getOcorrencia(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE cl_pl SET id_usuario = ?';
		if ($this->getTipo() != "") $sql .= ",tipo = ?";
		if ($this->getPalavra() != "") $sql .= ",palavra = ?";
		if ($this->getOcorrencia() != "") $sql .= ",ocorrencia = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdControle() != "") $stmt->bindParam(++$x,$this->getIdControle(),PDO::PARAM_INT);
		if ($this->getIdUsuario() != "") $stmt->bindParam(++$x,$this->getIdUsuario(),PDO::PARAM_INT);
		if ($this->getTipo() != "") $stmt->bindParam(++$x,$this->getTipo(),PDO::PARAM_INT);
		if ($this->getPalavra() != "") $stmt->bindParam(++$x,$this->getPalavra(),PDO::PARAM_INT);
		if ($this->getOcorrencia() != "") $stmt->bindParam(++$x,$this->getOcorrencia(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM cl_pl WHERE id IN({$lista})";
		$sql = "UPDATE cl_pl SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}


	public function verificaSeExiste(){
        $pdo = $this->getConexao();
        $sql = "SELECT count(id) as total FROM cl_pl WHERE palavra = ? AND id_controle = ? AND id_usuario = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getPalavra(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getIdControle(),PDO::PARAM_INT);
        $stmt->bindParam(3,$this->getIdUsuario(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function verificaControleArray(){
        $pdo = $this->getConexao();
        $sql = "SELECT palavra FROM cl_pl WHERE id_controle = ? AND id_usuario = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdControle(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getIdUsuario(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function controleFront(){
        $pdo = $this->getConexao();
        $sql = "SELECT 
                cl_pl.palavra as id_palavra_chave, 
                palavras_chaves.palavra_chave, 
                palavras.titulo, palavras.tipo, palavras.tipo2, palavras.url,
                categoria_palavaras.cor, categoria_palavaras.icone, categoria_palavaras.categoria,
                palavras.id as id_palavra_geral
                FROM cl_pl
                INNER JOIN palavras_chaves ON palavras_chaves.id = cl_pl.palavra
                INNER JOIN palavras ON  palavras.id = palavras_chaves.id_palavra
                INNER JOIN categoria_palavaras ON categoria_palavaras.id = palavras.id_categoria
                WHERE cl_pl.id_controle = ? AND cl_pl.id_usuario = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdControle(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getIdUsuario(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }






	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM cl_pl WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}
}
