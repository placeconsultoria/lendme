<?

switch($app_comando)
{
	case "frm_adicionar_cl_pl":
		$template = "tpl.frm.cl_pl.php";
		break;

	case "adicionar_cl_pl":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objClPl = new ClPl($pdo);
			$objClPl->setIdControle($_REQUEST['id_controle']);
			$objClPl->setIdUsuario($_REQUEST['id_usuario']);
			$objClPl->setTipo($_REQUEST['tipo']);
			$objClPl->setPalavra($_REQUEST['palavra']);
			$objClPl->setOcorrencia($_REQUEST['ocorrencia']);
			$novoId = $objClPl->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.cl_pl.php";
		break;

	case "frm_atualizar_cl_pl" :
		$cl_pl = new ClPl();
		$cl_pl->setId($_REQUEST["app_codigo"]);
		$linha = $cl_pl->Editar();
		$template = "tpl.frm.cl_pl.php";
		break;

	case "atualizar_cl_pl":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objClPl = new ClPl($pdo);
			$objClPl->setId($_REQUEST['id']);
			$objClPl->setIdControle($_REQUEST['id_controle']);
			$objClPl->setIdUsuario($_REQUEST['id_usuario']);
			$objClPl->setTipo($_REQUEST['tipo']);
			$objClPl->setPalavra($_REQUEST['palavra']);
			$objClPl->setOcorrencia($_REQUEST['ocorrencia']);
			$objClPl->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.cl_pl.php";
		break;

	case "listar_cl_pl":
		$template = "tpl.geral.cl_pl.php";
		break;

	case "deletar_cl_pl":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objClPl = new ClPl($pdo);
			$objClPl->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.cl_pl.php";
		break;

	case "ajax_listar_cl_pl":
		$template = "tpl.lis.cl_pl.php";
		break;

	case "cl_pl_pdf":
		$template = "tpl.lis.cl_pl.pdf.php";
		break;

	case "cl_pl_xlsx":
		$template = "tpl.lis.cl_pl.xlsx.php";
		break;

	case "cl_pl_print":
		$template = "tpl.lis.cl_pl.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["cl_pl"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("cl_pl");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.cl_pl.php";
		break;
}
