<script type="text/javascript">
$(function()
{
});

/*
 * Executa o post do formulário
 * */
function ExecutarSisBlindadosVeiculo(dialog, url)
{
	if (ValidateForm($("#frm_sis_blindados_veiculo"))) {
		// ao clicar em salvar enviando dados por post via AJAX
		$.post(url,
			$("#frm_sis_blindados_veiculo").serialize(),
			// pegando resposta do retorno do post
			function (response)
			{
				if (response["codigo"] == 0) {
					dialog.close();
					toastr.success(response["mensagem"], "<?=ROTULO_MENSAGEM?>");
					AtualizarGridSisBlindadosVeiculo(0, "");
				} else {
					toastr.warning(response["mensagem"], "<?=ROTULO_MENSAGEM?>");
				}
			}
			, "json" // definindo retorno para o formato json
		);
	}
}
</script>
