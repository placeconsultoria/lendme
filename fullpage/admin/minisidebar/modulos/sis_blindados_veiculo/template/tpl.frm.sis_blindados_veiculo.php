<form id="frm_sis_blindados_veiculo">
			<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="placa"> <?=RTL_PLACA?></label>
			<input type="text" name="placa"  id="placa" maxlength="10" class="form-control  " value="<?=$linha['placa'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="renavam"> <?=RTL_RENAVAM?></label>
			<input type="text" name="renavam"  id="renavam" maxlength="100" class="form-control  " value="<?=$linha['renavam'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="tipo_especie"> <?=RTL_TIPO_ESPECIE?></label>
			<input type="text" name="tipo_especie"  id="tipo_especie" maxlength="100" class="form-control  " value="<?=$linha['tipo_especie'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="ano"> <?=RTL_ANO?></label>
			<input type="text" name="ano"  id="ano" maxlength="10" class="form-control  " value="<?=$linha['ano'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="combustivel"> <?=RTL_COMBUSTIVEL?></label>
			<input type="text" name="combustivel"  id="combustivel" maxlength="20" class="form-control  " value="<?=$linha['combustivel'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="cor"> <?=RTL_COR?></label>
			<input type="text" name="cor"  id="cor" maxlength="50" class="form-control  " value="<?=$linha['cor'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="blindagem"> <?=RTL_BLINDAGEM?></label>
			<input type="text" name="blindagem"  id="blindagem" maxlength="10" class="form-control  mask-numero" value="<?=$linha['blindagem'];?>"/>
		</div>
	</div>
</form>
<?
include_once("modulos/sis_blindados_veiculo/template/js.frm.sis_blindados_veiculo.php");
?>
