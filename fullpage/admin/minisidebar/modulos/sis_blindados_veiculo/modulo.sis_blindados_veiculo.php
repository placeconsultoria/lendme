<?


define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/sis_blindados_veiculo/classe.sis_blindados_veiculo.php");
$app_comando = $_REQUEST["acao"];



switch($app_comando)
{

	case "gestao_veiculos":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {


		    $objVerifica = new SisBlindadosVeiculo();
		    $objVerifica->setIdCadastro($_REQUEST['id_cadastro']);
		    $verifica = $objVerifica->VerificaSeTemCadastro();
		    $verifica = $verifica["total"];


			$objSisBlindadosVeiculo = new SisBlindadosVeiculo($pdo);
			$objSisBlindadosVeiculo->setPlaca($_REQUEST['placa']);
            $objSisBlindadosVeiculo->setModelo($_REQUEST['modelo']);
			$objSisBlindadosVeiculo->setRenavam($_REQUEST['renavam']);
			$objSisBlindadosVeiculo->setTipoEspecie($_REQUEST['tipo_especie']);
			$objSisBlindadosVeiculo->setAno($_REQUEST['ano']);
			$objSisBlindadosVeiculo->setCombustivel($_REQUEST['combustivel']);
			$objSisBlindadosVeiculo->setCor($_REQUEST['cor']);
			$objSisBlindadosVeiculo->setBlindagem($_REQUEST['blindagem']);
			$objSisBlindadosVeiculo->setIdCadastro($_REQUEST['id_cadastro']);
            $objSisBlindadosVeiculo->setChassi($_REQUEST['chassi']);

			if($verifica == 0)
			{
                $novoId = $objSisBlindadosVeiculo->Adicionar();
            }else{
                $objSisBlindadosVeiculo->Modificar();
            }





			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_veiculo.php";
		break;



	case "atualizar_sis_blindados_veiculo":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objSisBlindadosVeiculo = new SisBlindadosVeiculo($pdo);
			$objSisBlindadosVeiculo->setId($_REQUEST['id']);
			$objSisBlindadosVeiculo->setPlaca($_REQUEST['placa']);
			$objSisBlindadosVeiculo->setRenavam($_REQUEST['renavam']);
			$objSisBlindadosVeiculo->setTipoEspecie($_REQUEST['tipo_especie']);
			$objSisBlindadosVeiculo->setAno($_REQUEST['ano']);
			$objSisBlindadosVeiculo->setCombustivel($_REQUEST['combustivel']);
			$objSisBlindadosVeiculo->setCor($_REQUEST['cor']);
			$objSisBlindadosVeiculo->setBlindagem($_REQUEST['blindagem']);
			$objSisBlindadosVeiculo->setIdCadastro($_REQUEST['id_cadastro']);
			$objSisBlindadosVeiculo->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_veiculo.php";
		break;


	case "deletar_sis_blindados_veiculo":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objSisBlindadosVeiculo = new SisBlindadosVeiculo($pdo);
			$objSisBlindadosVeiculo->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_veiculo.php";
		break;



}
