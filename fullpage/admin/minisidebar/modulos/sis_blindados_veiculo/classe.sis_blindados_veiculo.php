<?
class SisBlindadosVeiculo
{
	private $id;
	private $placa;
	private $renavam;
	private $tipo_especie;
	private $ano;
	private $combustivel;
	private $cor;
	private $blindagem;
	private $id_cadastro;
	private $modelo;
	private $chassi;
	private $conexao;

    public function setModelo($arg)
    {
        $this->modelo = $arg;
    }

    public function getModelo()
    {
        return $this->modelo;
    }

    public function setChassi($arg)
    {
        $this->chassi = $arg;
    }

    public function getChassi()
    {
        return $this->chassi;
    }

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setPlaca($arg)
	{
		$this->placa = $arg;
	}
 	
	public function getPlaca()
	{
		return $this->placa;
	}
 	
	public function setRenavam($arg)
	{
		$this->renavam = $arg;
	}
 	
	public function getRenavam()
	{
		return $this->renavam;
	}
 	
	public function setTipoEspecie($arg)
	{
		$this->tipo_especie = $arg;
	}
 	
	public function getTipoEspecie()
	{
		return $this->tipo_especie;
	}
 	
	public function setAno($arg)
	{
		$this->ano = $arg;
	}
 	
	public function getAno()
	{
		return $this->ano;
	}
 	
	public function setCombustivel($arg)
	{
		$this->combustivel = $arg;
	}
 	
	public function getCombustivel()
	{
		return $this->combustivel;
	}
 	
	public function setCor($arg)
	{
		$this->cor = $arg;
	}
 	
	public function getCor()
	{
		return $this->cor;
	}
 	
	public function setBlindagem($arg)
	{
		$this->blindagem = $arg;
	}
 	
	public function getBlindagem()
	{
		return $this->blindagem;
	}
 	
	public function setIdCadastro($arg)
	{
		$this->id_cadastro = $arg;
	}
 	
	public function getIdCadastro()
	{
		return $this->id_cadastro;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO sis_blindados_veiculo SET ';
		if ($this->getPlaca() != "") $sql .= "placa = ?";
		if ($this->getRenavam() != "") $sql .= ",renavam = ?";
		if ($this->getTipoEspecie() != "") $sql .= ",tipo_especie = ?";
		if ($this->getAno() != "") $sql .= ",ano = ?";
		if ($this->getCombustivel() != "") $sql .= ",combustivel = ?";
		if ($this->getCor() != "") $sql .= ",cor = ?";
		if ($this->getBlindagem() != "") $sql .= ",blindagem = ?";
		if ($this->getIdCadastro() != "") $sql .= ",id_cadastro = ?";
        if ($this->getModelo() != "") $sql .= ",modelo = ?";
        if ($this->getChassi() != "") $sql .= ",chassi = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getPlaca() != "") $stmt->bindParam(++$x,$this->getPlaca(),PDO::PARAM_STR);
		if ($this->getRenavam() != "") $stmt->bindParam(++$x,$this->getRenavam(),PDO::PARAM_STR);
		if ($this->getTipoEspecie() != "") $stmt->bindParam(++$x,$this->getTipoEspecie(),PDO::PARAM_STR);
		if ($this->getAno() != "") $stmt->bindParam(++$x,$this->getAno(),PDO::PARAM_STR);
		if ($this->getCombustivel() != "") $stmt->bindParam(++$x,$this->getCombustivel(),PDO::PARAM_STR);
		if ($this->getCor() != "") $stmt->bindParam(++$x,$this->getCor(),PDO::PARAM_STR);
		if ($this->getBlindagem() != "") $stmt->bindParam(++$x,$this->getBlindagem(),PDO::PARAM_INT);
		if ($this->getIdCadastro() != "") $stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_INT);
        if ($this->getModelo() != "") $stmt->bindParam(++$x,$this->getModelo(),PDO::PARAM_STR);
        if ($this->getChassi() != "") $stmt->bindParam(++$x,$this->getChassi(),PDO::PARAM_STR);

		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE sis_blindados_veiculo SET ';
		if ($this->getPlaca() != "") $sql .= "placa = ?";
		if ($this->getRenavam() != "") $sql .= ",renavam = ?";
		if ($this->getTipoEspecie() != "") $sql .= ",tipo_especie = ?";
		if ($this->getAno() != "") $sql .= ",ano = ?";
		if ($this->getCombustivel() != "") $sql .= ",combustivel = ?";
		if ($this->getCor() != "") $sql .= ",cor = ?";
		if ($this->getBlindagem() != "") $sql .= ",blindagem = ?";
		if ($this->getIdCadastro() != "") $sql .= ",id_cadastro = ?";
        if ($this->getModelo() != "") $sql .= ",modelo = ?";
        if ($this->getChassi() != "") $sql .= ",chassi = ?";

		$sql .= ' WHERE id_cadastro = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getPlaca() != "") $stmt->bindParam(++$x,$this->getPlaca(),PDO::PARAM_STR);
		if ($this->getRenavam() != "") $stmt->bindParam(++$x,$this->getRenavam(),PDO::PARAM_STR);
		if ($this->getTipoEspecie() != "") $stmt->bindParam(++$x,$this->getTipoEspecie(),PDO::PARAM_STR);
		if ($this->getAno() != "") $stmt->bindParam(++$x,$this->getAno(),PDO::PARAM_STR);
		if ($this->getCombustivel() != "") $stmt->bindParam(++$x,$this->getCombustivel(),PDO::PARAM_STR);
		if ($this->getCor() != "") $stmt->bindParam(++$x,$this->getCor(),PDO::PARAM_STR);
		if ($this->getBlindagem() != "") $stmt->bindParam(++$x,$this->getBlindagem(),PDO::PARAM_INT);
		if ($this->getIdCadastro() != "") $stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_INT);
        if ($this->getModelo() != "") $stmt->bindParam(++$x,$this->getModelo(),PDO::PARAM_STR);
        if ($this->getChassi() != "") $stmt->bindParam(++$x,$this->getChassi(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM sis_blindados_veiculo WHERE id IN({$lista})";
		$sql = "UPDATE sis_blindados_veiculo SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE sis_blindados_veiculo.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM sis_blindados_veiculo
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				sis_blindados_veiculo.*
			FROM sis_blindados_veiculo
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY sis_blindados_veiculo.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function GetInfo()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM sis_blindados_veiculo WHERE id_cadastro = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getIdCadastro(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}


    public function VerificaSeTemCadastro()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT count(id) as total FROM sis_blindados_veiculo WHERE id_cadastro = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdCadastro(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
    }

}
