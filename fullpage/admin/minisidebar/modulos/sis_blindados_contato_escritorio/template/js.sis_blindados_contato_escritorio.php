<?php
/**
* @author Fernando Carmo
* @copyright 2016
*/
?>
<script type="text/javascript">
	$(function()
	{
		LoadDiv("#conteudo_sis_blindados_contato_escritorio");;
		AtualizarGridSisBlindadosContatoEscritorio(0,"");
	});

	function AtualizarGridSisBlindadosContatoEscritorio(pagina,busca,filtro,ordem)
	{
		if(filtro == "" || filtro === undefined)  filtro = ""; 
		if(ordem == "" || ordem  === undefined)  ordem = "";

		var toPost = { 
			pagina: pagina,
			busca: busca,
			filtro: filtro,
			ordem: ordem
		};

		$("#conteudo_sis_blindados_contato_escritorio").load("index_xml.php?app_modulo=sis_blindados_contato_escritorio&app_comando=ajax_listar_sis_blindados_contato_escritorio", toPost);
	}

	function ImprimirRelatorio(form)
	{
		if (ValidarFormulario()) {
			form.action = "index_print.php?app_modulo=sis_blindados_contato_escritorio&app_comando=sis_blindados_contato_escritorio_print";
			form.target = "_blank";
			form.submit();
		}
	}

	function GerarPdf(form)
	{
		if (ValidarFormulario()) {
			form.action = "index_file.php?app_modulo=sis_blindados_contato_escritorio&app_comando=sis_blindados_contato_escritorio_pdf";
			form.target = "_blank";
			form.submit();
		}
	}

	function GerarXml(form)
	{
		if (ValidarFormulario()) {
			form.action = "index_file.php?app_modulo=sis_blindados_contato_escritorio&app_comando=sis_blindados_contato_escritorio_xlsx";
			form.target = "_blank";
			form.submit();
		}
	}

	function AbrirConfig()
	{
		BootstrapDialog.show({
			size:      BootstrapDialog.SIZE_SMALL,
			type:      BootstrapDialog.TYPE_DEFAULT,
			title:     "<div class='titulo_modal'><?=ROTULO_CONFIGURACOES?></div>",
			message:   $("<div></div>").load("index_xml.php?app_modulo=sis_blindados_contato_escritorio&app_comando=frm_configurar_listagem"),
			draggable: true,
			buttons:   [{
				label:    "<?=ROTULO_SALVAR?>",
				cssClass: "btn-lg btn-confirm",
				action:   function (dialogRef)
						{
							SalvarConfiguracoes(dialogRef, "index_xml.php?app_modulo=sis_blindados_contato_escritorio&app_comando=configurar_listagem", AtualizarGridSisBlindadosContatoEscritorio)
						}
			}]
		});
	}
</script>
