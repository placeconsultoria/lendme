<?php

define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/sis_blindados_contato_escritorio/classe.sis_blindados_contato_escritorio.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");
include_once(URL_FILE . "modulos/cidades/classe.cidades.php");


$id_cadastro = $_GET["id"];
$objContatoEscritorio = new SisBlindadosContatoEscritorio();
$objContatoEscritorio->setIdCadastro($id_cadastro);
$resulContatoEscritorio = $objContatoEscritorio->GetContatosCadastro();

$objEstados = NEW Estados();
$resulComboEstados = $objEstados->GerarSelectEstados();


if(@count($resulContatoEscritorio) > 0){
    foreach ($resulContatoEscritorio as $linha){
        $objCidades = new Cidades();
        $objCidades->setIdEstado($linha["id_estado"]);
        $resulComboCidades = $objCidades->ComboCidade();
        $estados = "";
        $cidades = "";

        foreach ($resulComboEstados as $linha2){
            if($linha2["id_estado"] == $linha["id_estado"]){
                $selected = "selected";
            }else{
                $selected = "";
            }
            $estados .= '<option '.$selected.' value="'.$linha2["id_estado"].'">'.$linha2["sigla"].'</option>';
        }

        foreach ($resulComboCidades as $linha3){
            if($linha3["id"] == $linha["id_cidade"]){
                $selected = "selected";
            }else{
                $selected = "";
            }
            $cidades .= '<option '.$selected.' value="'.$linha3["id"].'">'.$linha3["nome"].'</option>';
        }



        echo '
                <hr />
                <div class="row form-group pb-0 mb-0">
                    <div class="col-md-12">
                        <h4><span class="badge badge-inverse">CONTATO ESCRITÓRIO:</span> </h4>
                    </div>
                </div>
            
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="nome_contato_escritorio"> NOME DO CONTATO:</label>
                        <input type="text" name="nome"  id="nome_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["nome"].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="telefone_contato_escritorio">TELEFONE:</label>
                        <input type="text" name="telefone"  id="telefone_contato_escritorio" maxlength="20" class="form-control mask_telefone "  value="'.$linha["telefone"].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="celular_contato_escritorio"> CELULAR:</label>
                        <input type="text" name="celular"  id="celular_contato_escritorio" maxlength="20" class="form-control mask_telefone  "  value="'.$linha["celular"].'"/>
                    </div>
                    <div class="col-md-3">
                        <label for="email_contato_escritorio"> CONTATO E-MAIL:</label>
                        <input type="text" name="email"  id="email_contato_escritorio" maxlength="255" class="form-control  "  value="'.$linha["email"].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="nascimento"> NASCIMENTO:</label>
                        <input type="text" name="nascimento"  id="nascimento"  class="form-control  mask-date"  value="'.Conexao::PrepararDataPHP($linha["nascimento"]).'" onkeyup="mascaraTexto(event,\'99/99/9999\')" maxlength="10" />
                    </div>
            
            
                </div>
                
                <div class="row form-group">
                    <div class="col-md-1">
                        <label for="cep_contato_escritorio"> CEP:</label>
                        <input type="text" name="cep"  id="cep_contato_escritorio" onkeyup="mascaraTexto(event,\'99999-999\')" maxlength="9" class="form-control  " value="'.$linha["cep"].'"/>
                    </div>
                    <div class="col-md-3">
                        <label for="logradouro_contato_escritorio"> LOGRADOURO:</label>
                        <input type="text" name="logradouro"  id="logradouro_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["logradouro"].'"/>
                    </div>
            
                    <div class="col-md-1">
                        <label for="numero_contato_escritorio"> NÚMERO:</label>
                        <input type="text" name="numero"  id="numero_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["numero"].'"/>
                    </div>
            
                    <div class="col-md-1">
                        <label for="complemento_contato_escritorio"> COMPLEMENTO:</label>
                        <input type="text" name="complemento"  id="complemento_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["complemento"].'"/>
                    </div>
            
            
                    <div class="col-md-2">
                        <label for="bairro_contato_escritorio"> BAIRRO:</label>
                        <input type="text" name="bairro"  id="bairro_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["bairro"].'"/>
                    </div>
            
                    <div class="col-md-1">
                        <label for="id_estado_contato_escritorio"> ESTADO:</label>
                        <select id="id_estado_contato_escritorio" name="id_estado" class="form-control">
                        '.$estados.'
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="id_cidade_contato_escritorio"> CIDADE:</label>
                        <select id="id_cidade_contato_escritorio" name="id_cidade" class="form-control">
                        '.$cidades.'
                        </select>
                    </div>
            
                    <div class="col-md-1">
                        <label for="caixa_postal_contato_escritorio">CAIXA POSTAL:</label>
                        <input type="text" name="caixa_postal"  id="caixa_postal_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["caixa_postal"].'"/>
                    </div>
                 </div>   
               
        
        ';

    }
}


?>
