<?
define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/sis_blindados_contato_escritorio/classe.sis_blindados_contato_escritorio.php");
include_once(URL_FILE . "modulos/enderecos/classe.enderecos.php");
$app_comando = $_REQUEST["acao"];


switch($app_comando)
{


	case "adicionar_sis_blindados_contato_escritorio":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {

            $objEnderecos = new Enderecos();
            $objEnderecos->setLogradouro($_REQUEST['logradouro']);
            $objEnderecos->setNumero($_REQUEST['numero']);
            $objEnderecos->setComplemento($_REQUEST['complemento']);
            $objEnderecos->setCep($_REQUEST['cep']);
            $objEnderecos->setBairro($_REQUEST['bairro']);
            $objEnderecos->setIdCidade($_REQUEST['id_cidade']);
            $objEnderecos->setIdEstado($_REQUEST['id_estado']);
            $objEnderecos->setCaixaPostal($_REQUEST['caixa_postal']);
            $novoIdEndereco = $objEnderecos->Adicionar();

			$objSisBlindadosContatoEscritorio = new SisBlindadosContatoEscritorio($pdo);
			$objSisBlindadosContatoEscritorio->setIdCadastro($_REQUEST['id_cadastro']);
			$objSisBlindadosContatoEscritorio->setIdEndereco($novoIdEndereco);
			$objSisBlindadosContatoEscritorio->setNome($_REQUEST['nome']);
			$objSisBlindadosContatoEscritorio->setTelefone($_REQUEST['telefone']);
			$objSisBlindadosContatoEscritorio->setCelular($_REQUEST['celular']);
			$objSisBlindadosContatoEscritorio->setEmail($_REQUEST['email']);
			$objSisBlindadosContatoEscritorio->setNascimento(Conexao::PrepararDataBD($_REQUEST['nascimento']));
			$novoId = $objSisBlindadosContatoEscritorio->Adicionar();

			$msg["codigo"] = 0;
            $msg["id_cadastro"] = $_REQUEST['id_cadastro'];
			$msg["mensagem"] = "SUCESSO EM ADICIONAR";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_contato_escritorio.php";
		break;

	case "frm_atualizar_sis_blindados_contato_escritorio" :
		$sis_blindados_contato_escritorio = new SisBlindadosContatoEscritorio();
		$sis_blindados_contato_escritorio->setId($_REQUEST["app_codigo"]);
		$linha = $sis_blindados_contato_escritorio->Editar();
		$template = "tpl.frm.sis_blindados_contato_escritorio.php";
		break;

	case "atualizar_sis_blindados_contato_escritorio":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objSisBlindadosContatoEscritorio = new SisBlindadosContatoEscritorio($pdo);
			$objSisBlindadosContatoEscritorio->setId($_REQUEST['id']);
			$objSisBlindadosContatoEscritorio->setIdCadastro($_REQUEST['id_cadastro']);
			$objSisBlindadosContatoEscritorio->setIdEndereco($_REQUEST['id_endereco']);
			$objSisBlindadosContatoEscritorio->setNome($_REQUEST['nome']);
			$objSisBlindadosContatoEscritorio->setTelefone($_REQUEST['telefone']);
			$objSisBlindadosContatoEscritorio->setCelular($_REQUEST['celular']);
			$objSisBlindadosContatoEscritorio->setEmail($_REQUEST['email']);
			$objSisBlindadosContatoEscritorio->setNascimento($_REQUEST['nascimento']);
			$objSisBlindadosContatoEscritorio->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_contato_escritorio.php";
		break;



	case "deletar_sis_blindados_contato_escritorio":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objSisBlindadosContatoEscritorio = new SisBlindadosContatoEscritorio($pdo);
			$objSisBlindadosContatoEscritorio->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_contato_escritorio.php";
		break;


}
