<?
class SisBlindadosContatoEscritorio
{
	private $id;
	private $id_cadastro;
	private $id_endereco;
	private $nome;
	private $telefone;
	private $celular;
	private $email;
	private $nascimento;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdCadastro($arg)
	{
		$this->id_cadastro = $arg;
	}
 	
	public function getIdCadastro()
	{
		return $this->id_cadastro;
	}
 	
	public function setIdEndereco($arg)
	{
		$this->id_endereco = $arg;
	}
 	
	public function getIdEndereco()
	{
		return $this->id_endereco;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setTelefone($arg)
	{
		$this->telefone = $arg;
	}
 	
	public function getTelefone()
	{
		return $this->telefone;
	}
 	
	public function setCelular($arg)
	{
		$this->celular = $arg;
	}
 	
	public function getCelular()
	{
		return $this->celular;
	}
 	
	public function setEmail($arg)
	{
		$this->email = $arg;
	}
 	
	public function getEmail()
	{
		return $this->email;
	}
 	
	public function setNascimento($arg)
	{
		$this->nascimento = $arg;
	}
 	
	public function getNascimento()
	{
		return $this->nascimento;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO sis_blindados_contato_escritorio SET ';
		if ($this->getIdCadastro() != "") $sql .= "id_cadastro = ?";
		if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getCelular() != "") $sql .= ",celular = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getNascimento() != "") $sql .= ",nascimento = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdCadastro() != "") $stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_INT);
		if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getNascimento() != "") $stmt->bindParam(++$x,$this->getNascimento(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE sis_blindados_contato_escritorio SET ';
		if ($this->getIdCadastro() != "") $sql .= "id_cadastro = ?";
		if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getCelular() != "") $sql .= ",celular = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getNascimento() != "") $sql .= ",nascimento = ?";
		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getIdCadastro() != "") $stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_INT);
		if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getNascimento() != "") $stmt->bindParam(++$x,$this->getNascimento(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM sis_blindados_contato_escritorio WHERE id IN({$lista})";
		$sql = "UPDATE sis_blindados_contato_escritorio SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE sis_blindados_contato_escritorio.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM sis_blindados_contato_escritorio
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				sis_blindados_contato_escritorio.*
			FROM sis_blindados_contato_escritorio
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY sis_blindados_contato_escritorio.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function GetContatosCadastro()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT sis_blindados_contato_escritorio.*,
                       enderecos.caixa_postal, enderecos.id as id_endereco, enderecos.logradouro, enderecos.numero, enderecos.complemento, enderecos.cep, enderecos.bairro, enderecos.id_cidade, enderecos.id_estado 
 
                FROM sis_blindados_contato_escritorio 
                 INNER JOIN enderecos ON (sis_blindados_contato_escritorio.id_endereco = enderecos.id)
                WHERE sis_blindados_contato_escritorio.id_cadastro = ?
                ORDER BY sis_blindados_contato_escritorio.nome ";

		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getIdCadastro(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
}
