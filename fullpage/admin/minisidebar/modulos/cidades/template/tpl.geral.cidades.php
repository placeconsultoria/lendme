<?php

include("modulos/cidades/template/js.cidades.php");
?>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">        <li><b>Cadastro</b></li>        <li class="active"><?=RTL_CIDADES?></li>    </ol>    <h1 class="page-header"><?=RTL_CIDADES?> <small> Descrição do módulo</small></h1>    <div class="panel panel-inverse" data-sortable-id="ui-widget-1" style="">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a class="btn btn-xs btn-icon btn-circle btn-default" href="javascript:;" data-click="panel-expand">
            	       <i class="fa fa-expand"></i>
            	</a>
            	<a class="btn btn-xs btn-icon btn-circle btn-success" href="javascript:;" data-click="panel-reload">
            	    <i class="fa fa-repeat"></i>
            	</a>
            	<a class="btn btn-xs btn-icon btn-circle btn-warning" href="javascript:;" onclick="AtualizarGridAcao(0,'')" data-click="panel-collapse">
            	    <i class="fa fa-minus"></i>
            	</a>
            </div>
            <h4 class="panel-title">Listagem</h4>
        </div>
        <div class="panel-body" id="conteudo_cidades"></div>
    </div>
</div>
