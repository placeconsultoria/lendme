<?
class GrupoEmpresa
{
	private $id;
	private $grupo;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setGrupo($arg)
	{
		$this->grupo = $arg;
	}
 	
	public function getGrupo()
	{
		return $this->grupo;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO grupo_empresa SET ';
		if ($this->getGrupo() != "") $sql .= " grupo = ?";
        $stmt = $pdo->prepare($sql);
		if ($this->getGrupo() != "") $stmt->bindParam(++$x,$this->getGrupo(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE grupo_empresa SET ';
		if ($this->getGrupo() != "") $sql .= " grupo = ?";
        $sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getGrupo() != "") $stmt->bindParam(++$x,$this->getGrupo(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM grupo_empresa WHERE id IN({$lista})";
		$sql = "UPDATE grupo_empresa SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}



	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM grupo_empresa WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

	public  function GetCombo()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM grupo_empresa order by grupo";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }
}
