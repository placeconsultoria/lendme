<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/grupo_empresa/classe.grupo_empresa.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{

    case "get_combo_ajax":

        $objGrupoEmpresa = NEW GrupoEmpresa();
        $resul = $objGrupoEmpresa->GetCombo();
        echo json_encode($resul);
        break;

	case "adicionar_grupo_empresa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objGrupoEmpresa = new GrupoEmpresa($pdo);
			$objGrupoEmpresa->setGrupo($_REQUEST['grupo']);
			$novoId = $objGrupoEmpresa->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Grupo cadastrado com sucesso, e o mesmo já está no select do grupo.";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.grupo_empresa.php";
		break;



	case "atualizar_grupo_empresa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objGrupoEmpresa = new GrupoEmpresa($pdo);
			$objGrupoEmpresa->setId($_REQUEST['id']);
			$objGrupoEmpresa->setGrupo($_REQUEST['grupo']);
			$objGrupoEmpresa->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.grupo_empresa.php";
		break;



	case "deletar_grupo_empresa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objGrupoEmpresa = new GrupoEmpresa($pdo);
			$objGrupoEmpresa->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.grupo_empresa.php";
		break;


}
