<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/categoria_palavaras/classe.categoria_palavaras.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{
	case "frm_adicionar_categoria_palavaras":
		$template = "tpl.frm.categoria_palavaras.php";
		break;

	case "adicionar_categoria_palavras":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCategoriaPalavaras = new CategoriaPalavaras($pdo);
			$objCategoriaPalavaras->setCategoria($_REQUEST['categoria']);
			$objCategoriaPalavaras->setCor($_REQUEST['cor']);
			$objCategoriaPalavaras->setIcone($_REQUEST['icone']);
			$novoId = $objCategoriaPalavaras->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "CATEGORIA CADASTRADAS";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.categoria_palavaras.php";
		break;

	case "frm_atualizar_categoria_palavaras" :
		$categoria_palavaras = new CategoriaPalavaras();
		$categoria_palavaras->setId($_REQUEST["app_codigo"]);
		$linha = $categoria_palavaras->Editar();
		$template = "tpl.frm.categoria_palavaras.php";
		break;

	case "atualizar_categoria_palavras":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCategoriaPalavaras = new CategoriaPalavaras($pdo);
			$objCategoriaPalavaras->setId($_REQUEST['id']);
			$objCategoriaPalavaras->setCategoria($_REQUEST['categoria']);
			$objCategoriaPalavaras->setCor($_REQUEST['cor']);
			$objCategoriaPalavaras->setIcone($_REQUEST['icone']);
			$objCategoriaPalavaras->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.categoria_palavaras.php";
		break;

	case "listar_categoria_palavaras":
		$template = "tpl.geral.categoria_palavaras.php";
		break;

	case "deleter_categoria":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objCategoriaPalavaras = new CategoriaPalavaras($pdo);
			$objCategoriaPalavaras->setId($_GET["token"]);
			$objCategoriaPalavaras->Remover();
            $msg["codigo"] = 0;
            $msg["mensagem"] = "OK";
            $msg["url"] = "categoria_palavras.php";

            $pdo->commit();
        } catch (Exception $e) {
            $msg["codigo"] = 1;
            $msg["mensagem"] = "ERRO";
            $msg["debug"] = $e->getMessage();
            $pdo->rollBack();
        }
		echo json_encode($msg);
		$template = "ajax.categoria_palavaras.php";
		break;

	case "ajax_listar_categoria_palavaras":
		$template = "tpl.lis.categoria_palavaras.php";
		break;

	case "categoria_palavaras_pdf":
		$template = "tpl.lis.categoria_palavaras.pdf.php";
		break;

	case "categoria_palavaras_xlsx":
		$template = "tpl.lis.categoria_palavaras.xlsx.php";
		break;

	case "categoria_palavaras_print":
		$template = "tpl.lis.categoria_palavaras.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["categoria_palavaras"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("categoria_palavaras");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.categoria_palavaras.php";
		break;
}
