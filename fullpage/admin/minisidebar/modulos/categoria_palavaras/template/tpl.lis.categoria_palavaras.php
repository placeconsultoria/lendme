<?php
define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/categoria_palavaras/classe.categoria_palavaras.php");
include_once(URL_FILE . "modulos/palavras_chaves/classe.palavras_chaves.php");




$objCategoriasPalavras = new CategoriaPalavaras($pdo);
$listar = $objCategoriasPalavras->Listar();

$url_edit = "edit_categoria_palavara.php?id=";


?>


<div class="col-md-12">
    <div class="table-responsive">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>CATEGORIA</th>
                <th>PALAVRAS</th>
                <th>GERENCIAR</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $x = 0;
            if(count($listar)> 0){
                foreach($listar as $linha){
                    $id = $linha["id"];

                    $objPalavrasChaves = new PalavrasChaves();
                    $objPalavrasChaves->setIdPalavra($linha["id"]);
                    $total = $objPalavrasChaves->totalPalavra();
                    $total = $total["total"];

                    echo '
                             <tr>
                                <td><span class="text-uppercase btn btn-'.$linha["cor"].'"><i class="'.$linha["icone"].'"></i> '.$linha["categoria"].' </span> </td>
                                <td>'.$total.'</td>
                                <td><a href="'.$url_edit.$id.'" class="btn btn-warning"> GERENCIAR <i class="fa fa-pencil"></i></a> </td>
                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>



<script src="../../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>