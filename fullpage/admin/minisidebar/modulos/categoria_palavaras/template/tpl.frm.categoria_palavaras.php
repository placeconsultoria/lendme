<?php


define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/categoria_palavaras/classe.categoria_palavaras.php");


if($_GET["token"] != ""){
    $objCategoriaPalavras = new CategoriaPalavaras($pdo);
    $objCategoriaPalavras->setId($_GET["token"]);
    $linha = $objCategoriaPalavras->Editar();
    $acao = "atualizar_categoria_palavras";

    $label = '<span class="text-'.$linha["cor"].'">COR DE ESTILO</span>';
    $icone = 'ÍCONE: <i class="text-'.$linha["cor"].' '.$linha["icone"].'"></i>';

}else{
    $objCategoriaPalavras = new CategoriaPalavaras($pdo);
    $acao = "adicionar_categoria_palavras";
    $linha["cor"] = "nenhuma";
    $label = 'COR DE ESTILO';
    $icone = 'ÍCONE';
}


?>

<form id="frm_categoria_palavaras">
    <input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
    <input type="hidden" name="acao" id="acao" value="<?= $acao ?>" />
	<div class="row form-group">
		<div class="col-md-4">
			<label for="categoria">TÍTULO DA CATEGORIA </label>
			<input type="text" name="categoria"  id="categoria" maxlength="100" class="form-control validar-obrigatorio " value="<?=$linha['categoria'];?>"/>
		</div>
        <div class="col-md-4">
            <label for="cor"><?= $label ?></label>
            <select name="cor" id="cor" class="form-control">
              <? if($linha["id"] == "") echo '<option>--SELECIONE---</option>'; ?>
                <option value="inverse" class="btn btn-inverse" <? $objCategoriaPalavras->returnSelectColor("inverse",$linha["cor"]); ?>>FUNDO PRETO</option>
                <option value="warning" class="btn btn-warning" <? $objCategoriaPalavras->returnSelectColor("warning",$linha["cor"]); ?> >FUNDO AMARELO</option>
                <option value="danger" class="btn btn-danger" <? $objCategoriaPalavras->returnSelectColor("danger",$linha["cor"]); ?>>FUNDO VERMELHO</option>
                <option value="info" class="btn btn-info" <? $objCategoriaPalavras->returnSelectColor("info",$linha["cor"]); ?>>FUNDO AZUL</option>
                <option value="success" class="btn btn-success" <? $objCategoriaPalavras->returnSelectColor("success",$linha["cor"]); ?> >FUNDO VERDE</option>

            </select>
        </div>
        <div class="col-md-4">
            <label for="icone"><?= $icone ?> </label>
            <input type="text" name="icone"  id="icone" maxlength="100" class="form-control validar-obrigatorio " value="<?=$linha['icone'];?>"/>
        </div>
	</div>
    <div class="row form-group">
        <div class="col-md-12">
            <p><strong>Atenção:</strong> Cadastre as palavras com letras minúsculas.</p>
        </div>
    </div>

</form>

