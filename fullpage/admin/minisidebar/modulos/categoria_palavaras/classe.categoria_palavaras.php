<?


class CategoriaPalavaras
{
	private $id;
	private $categoria;
	private $cor;
	private $icone;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setCategoria($arg)
	{
		$this->categoria = $arg;
	}
 	
	public function getCategoria()
	{
		return $this->categoria;
	}
 	
	public function setCor($arg)
	{
		$this->cor = $arg;
	}
 	
	public function getCor()
	{
		return $this->cor;
	}
 	
	public function setIcone($arg)
	{
		$this->icone = $arg;
	}
 	
	public function getIcone()
	{
		return $this->icone;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO categoria_palavaras SET ';
		if ($this->getCategoria() != "") $sql .= " categoria = ?";
		if ($this->getCor() != "") $sql .= ",cor = ?";
		if ($this->getIcone() != "") $sql .= ",icone = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getCategoria() != "") $stmt->bindParam(++$x,$this->getCategoria(),PDO::PARAM_STR);
		if ($this->getCor() != "") $stmt->bindParam(++$x,$this->getCor(),PDO::PARAM_STR);
		if ($this->getIcone() != "") $stmt->bindParam(++$x,$this->getIcone(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE categoria_palavaras SET ';
		if ($this->getCategoria() != "") $sql .= "categoria = ?";
		if ($this->getCor() != "") $sql .= ",cor = ?";
		if ($this->getIcone() != "") $sql .= ",icone = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getCategoria() != "") $stmt->bindParam(++$x,$this->getCategoria(),PDO::PARAM_STR);
		if ($this->getCor() != "") $stmt->bindParam(++$x,$this->getCor(),PDO::PARAM_STR);
		if ($this->getIcone() != "") $stmt->bindParam(++$x,$this->getIcone(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover()
	{
		$pdo = $this->getConexao();
		$sql = "DELETE FROM categoria_palavaras WHERE id =?";
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);

        return $stmt->execute();
	}



	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM categoria_palavaras WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT categoria_palavaras.* FROM categoria_palavaras ORDER BY categoria_palavaras.categoria asc";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function GetCombo()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT categoria_palavaras.* FROM categoria_palavaras ORDER BY categoria_palavaras.categoria asc";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function returnSelectColor($cor, $cor_banco){
	    if($cor == $cor_banco) echo "selected";
    }


}
