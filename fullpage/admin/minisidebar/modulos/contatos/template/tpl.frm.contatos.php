<?php
define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/contatos/classe.contatos.php");
include_once(URL_FILE . "modulos/clientes/classe.clientes.php");

$objClientes = new Clientes($pdo);
$comboClientes = $objClientes->Listar();

if($_GET["id"] != ""){
    $objCliente = new Clientes($pdo);
    $objCliente->setId($_GET["id"]);
    $linha = $objCliente->Listar();

    $objContatos = new Contatos($pdo);
    $objContatos->setId($_GET["id"]);
    $linha = $objContatos->Editar();

    $acao = "atualizar_contatos";
}else{
    $acao = "adicionar_contatos";
}
?>
<form id="frm_contatos">
<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
<input type="hidden" name="acao" value="<?php echo $acao ?>" />

    <div class="row form-group">
        <div class="col-md-12">
            <label for="id_empresa">EMPRESA </label>
            <select id="id_empresa" name="id_empresa" class="form-control" >
                <?php
                if(!isset($linha["id"])){
                    echo '<option value="">--SELECIONE--</option>';
                }
                if(count($comboClientes)> 0){
                    foreach($comboClientes as $cliente){
                        if($cliente["id"] == $linha["id_empresa"]){
                            $sel = "selected";
                        }else{
                            $sel = "";
                        }

                        echo '<option value="'.$cliente["id"].'" '.$sel.' class="text-uppercase uppercase">'.$cliente['pasta'].' - '.$cliente["nome"].' ('.$cliente['cidade'].'/'.$cliente['estado'].')</option>';
                    }
                }

                ?>
            </select>
        </div>
    </div>
    <div class="row form-group">
		<div class="col-md-4">
			<label for="nome">NOME</label>
			<input type="text" name="nome"  id="nome" maxlength="255" class="form-control validar-obrigatorio " value="<?=$linha['nome'];?>"/>
		</div>
        <div class="col-md-5">
            <label for="email">E-MAIL</label>
            <input type="text" name="email"  id="email" maxlength="255" class="form-control validar-obrigatorio " value="<?=$linha['email'];?>"/>
        </div>

        <div class="col-md-3">
            <label for="cargo">CARGO </label>
            <input type="text" name="cargo"  id="cargo" maxlength="255" class="form-control validar-obrigatorio " value="<?=$linha['cargo'];?>"/>
        </div>

	</div>

	<div class="row form-group">
		<div class="col-md-4">
			<label for="telefone">FIXO </label>
			<input type="text" name="telefone"  id="telefone" maxlength="20" class="form-control validar-obrigatorio " value="<?=$linha['telefone'];?>" onKeyUp="mascaraTexto(event,'(99)9999-9999')" maxlength="13" onkeypress="return SomenteNumero(event);"/>
		</div>
		<div class="col-md-4">
			<label for="telefone2">CELULAR </label>
			<input type="text" name="telefone2"  id="telefone2" maxlength="20" class="form-control validar-obrigatorio " value="<?=$linha['telefone2'];?>" onKeyUp="mascaraTexto(event,'(99)99999-9999')" maxlength="14" onkeypress="return SomenteNumero(event);"/>
		</div>
	    <div class="col-md-4">
			<label for="telefone3">OUTRO</label>
			<input type="text" name="telefone3"  id="telefone3" maxlength="20" class="form-control validar-obrigatorio " value="<?=$linha['telefone3'];?>" onKeyUp="mascaraTexto(event,'(99)99999-9999')" maxlength="14" onkeypress="return SomenteNumero(event);"/>
		</div>
	</div>

</form>
