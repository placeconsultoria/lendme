<?php
define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/contatos/classe.contatos.php");

$objContato = new Contatos($pdo);
$listar = $objContato->Listar();
$url_edit = "edit_contato.php?id=";
?>

<div class="col-md-12">
    <div class="table-responsive">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>NOME</th>
                <th>EMPRESA</th>
                <th>EMAIL</th>
                <th>TELEFONES</th>
                <th>FUNÇÕES</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $x = 0;
            if(count($listar)> 0){
                foreach($listar as $linha){
                    $token = $linha["id"];





                    echo '
                             <tr>
                                <td class="text-uppercase">'.$linha["nome"].'</td>
                                <td class="text-uppercase">'.$linha['pasta'].' - '.$linha["empresa"].' ('.$linha["cidade"].'/'.$linha["estado"].')</td>
                                <td>'.$linha["email"].'</td>
                                <td><small>'.$linha["telefone"].' '.$linha["telefone2"].'</small></td>
                                <td><a href="'.$url_edit.$token.'" class="btn btn-warning"> GERENCIAR <i class="fa fa-pencil"></i></a> </td>
                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>



<script src="../../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>