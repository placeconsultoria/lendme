<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/contatos/classe.contatos.php");

$app_comando = $_REQUEST["acao"];

switch($app_comando)
{
	case "filtra_contatos":

	    $objContatos = new Contatos();
	    $objContatos->setIdEmpresa($_REQUEST["app_codigo"]);
	    $resultado = $objContatos->GetComboAjax();
        echo json_encode($resultado);



		break;

	case "adicionar_contatos":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objContatos = new Contatos($pdo);
			$objContatos->setNome($_REQUEST['nome']);
			$objContatos->setIdEmpresa($_REQUEST['id_empresa']);
			$objContatos->setTelefone($_REQUEST['telefone']);
			$objContatos->setTelefone2($_REQUEST['telefone2']);
			$objContatos->setTelefone3($_REQUEST['telefone3']);
			$objContatos->setCargo($_REQUEST['cargo']);
			$objContatos->setEmail($_REQUEST['email']);
			$novoId = $objContatos->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Contato adicionado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.contatos.php";
		break;

	case "frm_atualizar_contatos" :
		$contatos = new Contatos();
		$contatos->setId($_REQUEST["app_codigo"]);
		$linha = $contatos->Editar();
		$template = "tpl.frm.contatos.php";
		break;

	case "atualizar_contatos":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objContatos = new Contatos($pdo);
			$objContatos->setId($_REQUEST['id']);
			$objContatos->setNome($_REQUEST['nome']);
			$objContatos->setIdEmpresa($_REQUEST['id_empresa']);
			$objContatos->setTelefone($_REQUEST['telefone']);
			$objContatos->setTelefone2($_REQUEST['telefone2']);
			$objContatos->setTelefone3($_REQUEST['telefone3']);
			$objContatos->setCargo($_REQUEST['cargo']);
			$objContatos->setEmail($_REQUEST['email']);
			$objContatos->Modificar();
			$msg["codigo"] = 0;
            $msg["mensagem"] = "Contato alterado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.contatos.php";
		break;

	case "listar_contatos":
		$template = "tpl.geral.contatos.php";
		break;

    case "deletar_contato":
        $pdo = new Conexao();
        $pdo->beginTransaction();
        try {
            $objContatos = new Contatos($pdo);
            $objContatos ->setId($_REQUEST["id"]);
            $objContatos->Remover();
            $msg["codigo"] = 0;
            $msg["mensagem"] = "OK";
            $msg["url"] = "contatos.php";

            $pdo->commit();
        } catch (Exception $e) {
            $msg["codigo"] = 1;
            $msg["mensagem"] = "ERRO";
            $msg["debug"] = $e->getMessage();
            $pdo->rollBack();
        }
        echo json_encode($msg);
        $template = "ajax.clientes.php";
        break;

	case "ajax_listar_contatos":
		$template = "tpl.lis.contatos.php";
		break;

	case "contatos_pdf":
		$template = "tpl.lis.contatos.pdf.php";
		break;

	case "contatos_xlsx":
		$template = "tpl.lis.contatos.xlsx.php";
		break;

	case "contatos_print":
		$template = "tpl.lis.contatos.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["contatos"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("contatos");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.contatos.php";
		break;
}
