<?
class Contatos
{
	private $id;
	private $nome;
	private $id_empresa;
	private $telefone;
	private $telefone2;
	private $telefone3;
	private $cargo;
	private $email;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setIdEmpresa($arg)
	{
		$this->id_empresa = $arg;
	}
 	
	public function getIdEmpresa()
	{
		return $this->id_empresa;
	}
 	
	public function setTelefone($arg)
	{
		$this->telefone = $arg;
	}
 	
	public function getTelefone()
	{
		return $this->telefone;
	}
 	
	public function setTelefone2($arg)
	{
		$this->telefone2 = $arg;
	}
 	
	public function getTelefone2()
	{
		return $this->telefone2;
	}
 	
	public function setTelefone3($arg)
	{
		$this->telefone3 = $arg;
	}
 	
	public function getTelefone3()
	{
		return $this->telefone3;
	}
 	
	public function setCargo($arg)
	{
		$this->cargo = $arg;
	}
 	
	public function getCargo()
	{
		return $this->cargo;
	}
 	
	public function setEmail($arg)
	{
		$this->email = $arg;
	}
 	
	public function getEmail()
	{
		return $this->email;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO contatos SET ';
		if ($this->getNome() != "") $sql .= "nome = ?";
		if ($this->getIdEmpresa() != "") $sql .= ",id_empresa = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getTelefone2() != "") $sql .= ",telefone2 = ?";
		if ($this->getTelefone3() != "") $sql .= ",telefone3 = ?";
		if ($this->getCargo() != "") $sql .= ",cargo = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getIdEmpresa() != "") $stmt->bindParam(++$x,$this->getIdEmpresa(),PDO::PARAM_INT);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getTelefone2() != "") $stmt->bindParam(++$x,$this->getTelefone2(),PDO::PARAM_STR);
		if ($this->getTelefone3() != "") $stmt->bindParam(++$x,$this->getTelefone3(),PDO::PARAM_STR);
		if ($this->getCargo() != "") $stmt->bindParam(++$x,$this->getCargo(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE contatos SET id = ?';
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getIdEmpresa() != "") $sql .= ",id_empresa = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getTelefone2() != "") $sql .= ",telefone2 = ?";
		if ($this->getTelefone3() != "") $sql .= ",telefone3 = ?";
		if ($this->getCargo() != "") $sql .= ",cargo = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getIdEmpresa() != "") $stmt->bindParam(++$x,$this->getIdEmpresa(),PDO::PARAM_INT);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getTelefone2() != "") $stmt->bindParam(++$x,$this->getTelefone2(),PDO::PARAM_STR);
		if ($this->getTelefone3() != "") $stmt->bindParam(++$x,$this->getTelefone3(),PDO::PARAM_STR);
		if ($this->getCargo() != "") $stmt->bindParam(++$x,$this->getCargo(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}


    public function Remover()
    {
        $pdo = $this->getConexao();

        $sql = "DELETE FROM contatos WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
        return $stmt->execute();
    }



	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM contatos WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                    clientes.nome as empresa, clientes.pasta,
                    contatos.*,
                    cidades.nome as cidade,
                    estados.sigla as estado 
                    FROM contatos
                    INNER JOIN clientes ON clientes.id = contatos.id_empresa
                    INNER JOIN cidades ON cidades.id = clientes.id_cidade
                    INNER JOIN estados ON estados.id = clientes.id_estado
                    ORDER BY contatos.nome asc";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }




    public function GetCombo()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                  id AS id_conato, nome as contato
                FROM contatos 
               
                ORDER BY nome ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function GetComboAjax()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                  id AS id_conato, nome as contato, cargo
                FROM contatos 
                WHERE id_empresa = ? 
               
                ORDER BY nome ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdEmpresa(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}
