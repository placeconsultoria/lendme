<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/sis_blindados_representante/classe.sis_blindados_representante.php");
include_once(URL_FILE . "modulos/enderecos/classe.enderecos.php");
$app_comando = $_REQUEST["acao"];


switch($app_comando)
{

    case "get_info_representante_copy":
        $id_cadastro = $_GET["app_codigo"];
        $objRepresentantes = new SisBlindadosRepresentante();
        $objRepresentantes->setId($id_cadastro);
        $resulContatoEscritorio = $objRepresentantes->GetRepresentantesCadastroInfo();

        echo json_encode($resulContatoEscritorio);



        break;

	case "adicionar_sis_blindados_representante":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {


            $objEnderecos = new Enderecos();
            $objEnderecos->setLogradouro($_REQUEST['logradouro']);
            $objEnderecos->setNumero($_REQUEST['numero']);
            $objEnderecos->setComplemento($_REQUEST['complemento']);
            $objEnderecos->setCep($_REQUEST['cep']);
            $objEnderecos->setBairro($_REQUEST['bairro']);
            $objEnderecos->setIdCidade($_REQUEST['id_cidade']);
            $objEnderecos->setIdEstado($_REQUEST['id_estado']);
            $novoIdEndereco = $objEnderecos->Adicionar();

			$objSisBlindadosRepresentante = new SisBlindadosRepresentante($pdo);
			$objSisBlindadosRepresentante->setIdCadastro($_REQUEST['id_cadastro']);
			$objSisBlindadosRepresentante->setNome($_REQUEST['nome']);
			$objSisBlindadosRepresentante->setCpf($_REQUEST['cpf']);
			$objSisBlindadosRepresentante->setRg($_REQUEST['rg']);
			$objSisBlindadosRepresentante->setEmissor($_REQUEST['emissor']);
			$objSisBlindadosRepresentante->setDataEmissao(Conexao::PrepararDataBD($_REQUEST['data_emissao']));
			$objSisBlindadosRepresentante->setNascimento(Conexao::PrepararDataBD($_REQUEST['nascimento']));
			$objSisBlindadosRepresentante->setPai($_REQUEST['pai']);
			$objSisBlindadosRepresentante->setMae($_REQUEST['mae']);
			$objSisBlindadosRepresentante->setNacionalidade($_REQUEST['nacionalidade']);
			$objSisBlindadosRepresentante->setNaturalidade($_REQUEST['naturalidade']);
			$objSisBlindadosRepresentante->setEstadoNascimento($_REQUEST['estado_nascimento']);
			$objSisBlindadosRepresentante->setEstadoCivil($_REQUEST['estado_civil']);
			$objSisBlindadosRepresentante->setGrauInstrucao($_REQUEST['grau_instrucao']);
			$objSisBlindadosRepresentante->setProfissao($_REQUEST['profissao']);
			$objSisBlindadosRepresentante->setCargo($_REQUEST['cargo']);
			$objSisBlindadosRepresentante->setTelefone($_REQUEST['telefone']);
			$objSisBlindadosRepresentante->setCelular($_REQUEST['celular']);
			$objSisBlindadosRepresentante->setEmail($_REQUEST['email']);
			$objSisBlindadosRepresentante->setIdEndereco($novoIdEndereco);
			$novoId = $objSisBlindadosRepresentante->Adicionar();
			$msg["codigo"] = 0;
            $msg["id_cadastro"] = $_REQUEST['id_cadastro'];
			$msg["mensagem"] = "ADICIONADO COM SUCESSO!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_representante.php";
		break;

	case "frm_atualizar_sis_blindados_representante" :
		$sis_blindados_representante = new SisBlindadosRepresentante();
		$sis_blindados_representante->setId($_REQUEST["app_codigo"]);
		$linha = $sis_blindados_representante->Editar();
		$template = "tpl.frm.sis_blindados_representante.php";
		break;

	case "atualizar_sis_blindados_representante":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objSisBlindadosRepresentante = new SisBlindadosRepresentante($pdo);
			$objSisBlindadosRepresentante->setId($_REQUEST['id']);
			$objSisBlindadosRepresentante->setIdCadastro($_REQUEST['id_cadastro']);
			$objSisBlindadosRepresentante->setNome($_REQUEST['nome']);
			$objSisBlindadosRepresentante->setCpf($_REQUEST['cpf']);
			$objSisBlindadosRepresentante->setRg($_REQUEST['rg']);
			$objSisBlindadosRepresentante->setEmissor($_REQUEST['emissor']);
			$objSisBlindadosRepresentante->setDataEmissao($_REQUEST['data_emissao']);
			$objSisBlindadosRepresentante->setNascimento($_REQUEST['nascimento']);
			$objSisBlindadosRepresentante->setPai($_REQUEST['pai']);
			$objSisBlindadosRepresentante->setMae($_REQUEST['mae']);
			$objSisBlindadosRepresentante->setNacionalidade($_REQUEST['nacionalidade']);
			$objSisBlindadosRepresentante->setNaturalidade($_REQUEST['naturalidade']);
			$objSisBlindadosRepresentante->setEstadoNascimento($_REQUEST['estado_nascimento']);
			$objSisBlindadosRepresentante->setEstadoCivil($_REQUEST['estado_civil']);
			$objSisBlindadosRepresentante->setGrauInstrucao($_REQUEST['grau_instrucao']);
			$objSisBlindadosRepresentante->setProfissao($_REQUEST['profissao']);
			$objSisBlindadosRepresentante->setCargo($_REQUEST['cargo']);
			$objSisBlindadosRepresentante->setTelefone($_REQUEST['telefone']);
			$objSisBlindadosRepresentante->setCelular($_REQUEST['celular']);
			$objSisBlindadosRepresentante->setEmail($_REQUEST['email']);
			$objSisBlindadosRepresentante->setIdEndereco($_REQUEST['id_endereco']);
			$objSisBlindadosRepresentante->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_representante.php";
		break;

	case "listar_sis_blindados_representante":
		$template = "tpl.geral.sis_blindados_representante.php";
		break;

	case "deletar_sis_blindados_representante":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objSisBlindadosRepresentante = new SisBlindadosRepresentante($pdo);
			$objSisBlindadosRepresentante->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.sis_blindados_representante.php";
		break;


}
