<?
class SisBlindadosRepresentante
{
	private $id;
	private $id_cadastro;
	private $nome;
	private $cpf;
	private $rg;
	private $emissor;
	private $data_emissao;
	private $nascimento;
	private $pai;
	private $mae;
	private $nacionalidade;
	private $naturalidade;
	private $estado_nascimento;
	private $estado_civil;
	private $grau_instrucao;
	private $profissao;
	private $cargo;
	private $telefone;
	private $celular;
	private $email;
	private $id_endereco;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdCadastro($arg)
	{
		$this->id_cadastro = $arg;
	}
 	
	public function getIdCadastro()
	{
		return $this->id_cadastro;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setCpf($arg)
	{
		$this->cpf = $arg;
	}
 	
	public function getCpf()
	{
		return $this->cpf;
	}
 	
	public function setRg($arg)
	{
		$this->rg = $arg;
	}
 	
	public function getRg()
	{
		return $this->rg;
	}
 	
	public function setEmissor($arg)
	{
		$this->emissor = $arg;
	}
 	
	public function getEmissor()
	{
		return $this->emissor;
	}
 	
	public function setDataEmissao($arg)
	{
		$this->data_emissao = $arg;
	}
 	
	public function getDataEmissao()
	{
		return $this->data_emissao;
	}
 	
	public function setNascimento($arg)
	{
		$this->nascimento = $arg;
	}
 	
	public function getNascimento()
	{
		return $this->nascimento;
	}
 	
	public function setPai($arg)
	{
		$this->pai = $arg;
	}
 	
	public function getPai()
	{
		return $this->pai;
	}
 	
	public function setMae($arg)
	{
		$this->mae = $arg;
	}
 	
	public function getMae()
	{
		return $this->mae;
	}
 	
	public function setNacionalidade($arg)
	{
		$this->nacionalidade = $arg;
	}
 	
	public function getNacionalidade()
	{
		return $this->nacionalidade;
	}
 	
	public function setNaturalidade($arg)
	{
		$this->naturalidade = $arg;
	}
 	
	public function getNaturalidade()
	{
		return $this->naturalidade;
	}
 	
	public function setEstadoNascimento($arg)
	{
		$this->estado_nascimento = $arg;
	}
 	
	public function getEstadoNascimento()
	{
		return $this->estado_nascimento;
	}
 	
	public function setEstadoCivil($arg)
	{
		$this->estado_civil = $arg;
	}
 	
	public function getEstadoCivil()
	{
		return $this->estado_civil;
	}
 	
	public function setGrauInstrucao($arg)
	{
		$this->grau_instrucao = $arg;
	}
 	
	public function getGrauInstrucao()
	{
		return $this->grau_instrucao;
	}
 	
	public function setProfissao($arg)
	{
		$this->profissao = $arg;
	}
 	
	public function getProfissao()
	{
		return $this->profissao;
	}
 	
	public function setCargo($arg)
	{
		$this->cargo = $arg;
	}
 	
	public function getCargo()
	{
		return $this->cargo;
	}
 	
	public function setTelefone($arg)
	{
		$this->telefone = $arg;
	}
 	
	public function getTelefone()
	{
		return $this->telefone;
	}
 	
	public function setCelular($arg)
	{
		$this->celular = $arg;
	}
 	
	public function getCelular()
	{
		return $this->celular;
	}
 	
	public function setEmail($arg)
	{
		$this->email = $arg;
	}
 	
	public function getEmail()
	{
		return $this->email;
	}
 	
	public function setIdEndereco($arg)
	{
		$this->id_endereco = $arg;
	}
 	
	public function getIdEndereco()
	{
		return $this->id_endereco;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO sis_blindados_representante SET ';
		if ($this->getIdCadastro() != "") $sql .= " id_cadastro = ?";
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getCpf() != "") $sql .= ",cpf = ?";
		if ($this->getRg() != "") $sql .= ",rg = ?";
		if ($this->getEmissor() != "") $sql .= ",emissor = ?";
		if ($this->getDataEmissao() != "") $sql .= ",data_emissao = ?";
		if ($this->getNascimento() != "") $sql .= ",nascimento = ?";
		if ($this->getPai() != "") $sql .= ",pai = ?";
		if ($this->getMae() != "") $sql .= ",mae = ?";
		if ($this->getNacionalidade() != "") $sql .= ",nacionalidade = ?";
		if ($this->getNaturalidade() != "") $sql .= ",naturalidade = ?";
		if ($this->getEstadoNascimento() != "") $sql .= ",estado_nascimento = ?";
		if ($this->getEstadoCivil() != "") $sql .= ",estado_civil = ?";
		if ($this->getGrauInstrucao() != "") $sql .= ",grau_instrucao = ?";
		if ($this->getProfissao() != "") $sql .= ",profissao = ?";
		if ($this->getCargo() != "") $sql .= ",cargo = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getCelular() != "") $sql .= ",celular = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdCadastro() != "") $stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_STR);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getCpf() != "") $stmt->bindParam(++$x,$this->getCpf(),PDO::PARAM_STR);
		if ($this->getRg() != "") $stmt->bindParam(++$x,$this->getRg(),PDO::PARAM_STR);
		if ($this->getEmissor() != "") $stmt->bindParam(++$x,$this->getEmissor(),PDO::PARAM_STR);
		if ($this->getDataEmissao() != "") $stmt->bindParam(++$x,$this->getDataEmissao(),PDO::PARAM_STR);
		if ($this->getNascimento() != "") $stmt->bindParam(++$x,$this->getNascimento(),PDO::PARAM_STR);
		if ($this->getPai() != "") $stmt->bindParam(++$x,$this->getPai(),PDO::PARAM_STR);
		if ($this->getMae() != "") $stmt->bindParam(++$x,$this->getMae(),PDO::PARAM_STR);
		if ($this->getNacionalidade() != "") $stmt->bindParam(++$x,$this->getNacionalidade(),PDO::PARAM_STR);
		if ($this->getNaturalidade() != "") $stmt->bindParam(++$x,$this->getNaturalidade(),PDO::PARAM_STR);
		if ($this->getEstadoNascimento() != "") $stmt->bindParam(++$x,$this->getEstadoNascimento(),PDO::PARAM_INT);
		if ($this->getEstadoCivil() != "") $stmt->bindParam(++$x,$this->getEstadoCivil(),PDO::PARAM_INT);
		if ($this->getGrauInstrucao() != "") $stmt->bindParam(++$x,$this->getGrauInstrucao(),PDO::PARAM_INT);
		if ($this->getProfissao() != "") $stmt->bindParam(++$x,$this->getProfissao(),PDO::PARAM_STR);
		if ($this->getCargo() != "") $stmt->bindParam(++$x,$this->getCargo(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = '
		UPDATE sis_blindados_representante SET 
			id = ?';
		if ($this->getIdCadastro() != "") $sql .= ",id_cadastro = ?";
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getCpf() != "") $sql .= ",cpf = ?";
		if ($this->getRg() != "") $sql .= ",rg = ?";
		if ($this->getEmissor() != "") $sql .= ",emissor = ?";
		if ($this->getDataEmissao() != "") $sql .= ",data_emissao = ?";
		if ($this->getNascimento() != "") $sql .= ",nascimento = ?";
		if ($this->getPai() != "") $sql .= ",pai = ?";
		if ($this->getMae() != "") $sql .= ",mae = ?";
		if ($this->getNacionalidade() != "") $sql .= ",nacionalidade = ?";
		if ($this->getNaturalidade() != "") $sql .= ",naturalidade = ?";
		if ($this->getEstadoNascimento() != "") $sql .= ",estado_nascimento = ?";
		if ($this->getEstadoCivil() != "") $sql .= ",estado_civil = ?";
		if ($this->getGrauInstrucao() != "") $sql .= ",grau_instrucao = ?";
		if ($this->getProfissao() != "") $sql .= ",profissao = ?";
		if ($this->getCargo() != "") $sql .= ",cargo = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getCelular() != "") $sql .= ",celular = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getIdEndereco() != "") $sql .= ",id_endereco = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdCadastro() != "") $stmt->bindParam(++$x,$this->getIdCadastro(),PDO::PARAM_STR);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getCpf() != "") $stmt->bindParam(++$x,$this->getCpf(),PDO::PARAM_STR);
		if ($this->getRg() != "") $stmt->bindParam(++$x,$this->getRg(),PDO::PARAM_STR);
		if ($this->getEmissor() != "") $stmt->bindParam(++$x,$this->getEmissor(),PDO::PARAM_STR);
		if ($this->getDataEmissao() != "") $stmt->bindParam(++$x,$this->getDataEmissao(),PDO::PARAM_STR);
		if ($this->getNascimento() != "") $stmt->bindParam(++$x,$this->getNascimento(),PDO::PARAM_STR);
		if ($this->getPai() != "") $stmt->bindParam(++$x,$this->getPai(),PDO::PARAM_STR);
		if ($this->getMae() != "") $stmt->bindParam(++$x,$this->getMae(),PDO::PARAM_STR);
		if ($this->getNacionalidade() != "") $stmt->bindParam(++$x,$this->getNacionalidade(),PDO::PARAM_STR);
		if ($this->getNaturalidade() != "") $stmt->bindParam(++$x,$this->getNaturalidade(),PDO::PARAM_STR);
		if ($this->getEstadoNascimento() != "") $stmt->bindParam(++$x,$this->getEstadoNascimento(),PDO::PARAM_INT);
		if ($this->getEstadoCivil() != "") $stmt->bindParam(++$x,$this->getEstadoCivil(),PDO::PARAM_INT);
		if ($this->getGrauInstrucao() != "") $stmt->bindParam(++$x,$this->getGrauInstrucao(),PDO::PARAM_INT);
		if ($this->getProfissao() != "") $stmt->bindParam(++$x,$this->getProfissao(),PDO::PARAM_STR);
		if ($this->getCargo() != "") $stmt->bindParam(++$x,$this->getCargo(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getCelular() != "") $stmt->bindParam(++$x,$this->getCelular(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getIdEndereco() != "") $stmt->bindParam(++$x,$this->getIdEndereco(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM sis_blindados_representante WHERE id IN({$lista})";
		$sql = "UPDATE sis_blindados_representante SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE sis_blindados_representante.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM sis_blindados_representante
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				sis_blindados_representante.*
			FROM sis_blindados_representante
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY sis_blindados_representante.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM sis_blindados_representante WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function GetRepresentantesCadastro()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                      sis_blindados_representante.*,
                      enderecos.id as id_endereco, enderecos.logradouro, enderecos.numero, enderecos.complemento, enderecos.cep, enderecos.bairro, enderecos.id_cidade, enderecos.id_estado 
 
                FROM  sis_blindados_representante 
                INNER JOIN enderecos ON (sis_blindados_representante.id_endereco = enderecos.id)
                WHERE sis_blindados_representante.id_cadastro = ?
                ORDER BY sis_blindados_representante.nome ";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdCadastro(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function GetRepresentantesCadastroInfo()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                      sis_blindados_representante.*,
                      enderecos.id as id_endereco, enderecos.logradouro, enderecos.numero, enderecos.complemento, enderecos.cep, enderecos.bairro, enderecos.id_cidade, enderecos.id_estado 
 
                FROM  sis_blindados_representante 
                INNER JOIN enderecos ON (sis_blindados_representante.id_endereco = enderecos.id)
                WHERE sis_blindados_representante.id = ?
                ORDER BY sis_blindados_representante.nome ";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
    }
}
