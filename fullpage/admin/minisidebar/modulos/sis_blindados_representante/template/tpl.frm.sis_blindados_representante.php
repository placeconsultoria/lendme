<form id="frm_sis_blindados_representante">
			<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="nome"> <?=RTL_NOME?></label>
			<input type="text" name="nome"  id="nome" maxlength="255" class="form-control  " value="<?=$linha['nome'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="cpf"> <?=RTL_CPF?></label>
			<input type="text" name="cpf"  id="cpf" maxlength="20" class="form-control  " value="<?=$linha['cpf'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="rg"> <?=RTL_RG?></label>
			<input type="text" name="rg"  id="rg" maxlength="20" class="form-control  " value="<?=$linha['rg'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="emissor"> <?=RTL_EMISSOR?></label>
			<input type="text" name="emissor"  id="emissor" maxlength="100" class="form-control  " value="<?=$linha['emissor'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="data_emissao"> <?=RTL_DATA_EMISSAO?></label>
			<input type="text" name="data_emissao"  id="data_emissao"  class="form-control  mask-date" value="<?=$linha['data_emissao'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="nascimento"> <?=RTL_NASCIMENTO?></label>
			<input type="text" name="nascimento"  id="nascimento"  class="form-control  mask-date" value="<?=$linha['nascimento'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="pai"> <?=RTL_PAI?></label>
			<input type="text" name="pai"  id="pai" maxlength="255" class="form-control  " value="<?=$linha['pai'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="mae"> <?=RTL_MAE?></label>
			<input type="text" name="mae"  id="mae" maxlength="255" class="form-control  " value="<?=$linha['mae'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="estado_nascimento"> <?=RTL_ESTADO_NASCIMENTO?></label>
			<input type="text" name="estado_nascimento"  id="estado_nascimento" maxlength="10" class="form-control  mask-numero" value="<?=$linha['estado_nascimento'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="estado_civil"> <?=RTL_ESTADO_CIVIL?></label>
			<input type="text" name="estado_civil"  id="estado_civil" maxlength="10" class="form-control  mask-numero" value="<?=$linha['estado_civil'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="grau_instrucao"> <?=RTL_GRAU_INSTRUCAO?></label>
			<input type="text" name="grau_instrucao"  id="grau_instrucao" maxlength="10" class="form-control  mask-numero" value="<?=$linha['grau_instrucao'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="profissao"> <?=RTL_PROFISSAO?></label>
			<input type="text" name="profissao"  id="profissao" maxlength="100" class="form-control  " value="<?=$linha['profissao'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="cargo"> <?=RTL_CARGO?></label>
			<input type="text" name="cargo"  id="cargo" maxlength="100" class="form-control  " value="<?=$linha['cargo'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="telefone"> <?=RTL_TELEFONE?></label>
			<input type="text" name="telefone"  id="telefone" maxlength="20" class="form-control  " value="<?=$linha['telefone'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="celular"> <?=RTL_CELULAR?></label>
			<input type="text" name="celular"  id="celular" maxlength="20" class="form-control  " value="<?=$linha['celular'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="email"> <?=RTL_EMAIL?></label>
			<input type="text" name="email"  id="email" maxlength="150" class="form-control  " value="<?=$linha['email'];?>"/>
		</div>
	</div>
</form>
<?
include_once("modulos/sis_blindados_representante/template/js.frm.sis_blindados_representante.php");
?>
