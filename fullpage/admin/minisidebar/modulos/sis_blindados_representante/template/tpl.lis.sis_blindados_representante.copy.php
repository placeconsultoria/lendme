<?php

define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/sis_blindados_representante/classe.sis_blindados_representante.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");
include_once(URL_FILE . "modulos/cidades/classe.cidades.php");


$id_cadastro = $_GET["app_codigo"];
$objRepresentantes = new SisBlindadosRepresentante();
$objRepresentantes->setIdCadastro($id_cadastro);
$resulContatoEscritorio = $objRepresentantes->GetRepresentantesCadastro();



?>


<div class="col-md-12">
    <div class="table-responsive">
        <table  class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>NOME COMPLETO:</th>
                <th>CPF:</th>
                <th width="20%">AÇÕES</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $x = 0;
            if(count($resulContatoEscritorio)> 0){
                foreach($resulContatoEscritorio as $linha){
                    echo '
                             <tr>
                                <td class="text-uppercase">'.$linha["nome"].'</td>
                                <td>'.$linha["cpf"].'</td>
                                <td>
                                   <a href="'.$linha["id"].'" class="btn btn-warning btn_copy_select_representante"> 
                                       COPIAR <i class="fa fa-copy"></i>
                                   </a> 
                                </td>
                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>


