<?php

define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/sis_blindados_representante/classe.sis_blindados_representante.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");
include_once(URL_FILE . "modulos/cidades/classe.cidades.php");


$id_cadastro = $_GET["id"];
$objRepresentantes = new SisBlindadosRepresentante();
$objRepresentantes->setIdCadastro($id_cadastro);
$resulContatoEscritorio = $objRepresentantes->GetRepresentantesCadastro();

$objEstados = NEW Estados();
$resulComboEstados = $objEstados->GerarSelectEstados();




if(@count($resulContatoEscritorio) > 0){
    foreach ($resulContatoEscritorio as $linha){
        $objCidades = new Cidades();
        $objCidades->setIdEstado($linha["id_estado"]);
        $resulComboCidades = $objCidades->ComboCidade();
        $estados = "";
        $cidades = "";

        foreach ($resulComboEstados as $linha2){
            if($linha2["id_estado"] == $linha["id_estado"]){
                $selected = "selected";
            }else{
                $selected = "";
            }
            $estados .= '<option '.$selected.' value="'.$linha2["id_estado"].'">'.$linha2["sigla"].'</option>';
        }


        foreach ($resulComboEstados as $linha4){
            if($linha4["id_estado"] == $linha["estado_nascimento"]){
                $selected = "selected";
            }else{
                $selected = "";
            }
            $estados2 .= '<option '.$selected.' value="'.$linha4["id_estado"].'">'.$linha4["sigla"].'</option>';
        }


        foreach ($resulComboCidades as $linha3){
            if($linha3["id"] == $linha["id_cidade"]){
                $selected = "selected";
            }else{
                $selected = "";
            }
            $cidades .= '<option '.$selected.' value="'.$linha3["id"].'">'.$linha3["nome"].'</option>';
        }

        if($linha["estado_civil"] == 1 ){
            $estado_civil1 = "selected";
        }else if($linha["estado_civil"] == 2 ) {
            $estado_civil2 = "selected";
        }else if($linha["estado_civil"] == 3 ) {
            $estado_civil3 = "selected";
        }else if($linha["estado_civil"] == 4 ) {
            $estado_civil4 = "selected";
        }else if($linha["estado_civil"] == 5 ) {
            $estado_civil5 = "selected";
        }

        if($linha["grau_instrucao"] == 1 ){
            $grau_instrucao1 = "selected";
        }else if($linha["grau_instrucao"] == 2 ) {
            $grau_instrucao2 = "selected";
        }else if($linha["grau_instrucao"] == 3 ) {
            $grau_instrucao3 = "selected";
        }else if($linha["grau_instrucao"] == 4 ) {
            $grau_instrucao4 = "selected";
        }else if($linha["grau_instrucao"] == 5 ) {
            $grau_instrucao5 = "selected";
        }else if($linha["grau_instrucao"] == 6) {
            $grau_instrucao6 = "selected";
        }



        echo '<div class="row form-group pb-0 mb-0">
                    <div class="col-md-12">
                        <h4><span class="badge badge-inverse">REPRESENTANTE:</span> </h4>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="representante_nome"> NOME:</label>
                        <input required type="text" name="nome"  id="representante_nome" maxlength="255" class="form-control  " value="'.$linha["nome"].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="representante_cpf"> CPF:</label>
                        <input required type="text" name="cpf"  id="representante_cpf" maxlength="100" class="form-control " value="'.$linha["cpf"].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="representante_rg"> RG/RNE:</label>
                        <input required type="text" name="rg"  id="representante_rg" maxlength="45" class="form-control  " value="'.$linha["rg"].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="representante_emissor"> ORGÃO EMISSOR:</label>
                        <input required type="text" name="emissor"  id="representante_emissor" maxlength="100" class="form-control  " value="'.$linha["emissor"].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="representante_data_emissao"> DATA DE EMISSÃO:</label>
                        <input required type="text" name="data_emissao"  id="representante_data_emissao"  class="form-control  " value="'.Conexao::PrepararDataPHP($linha["data_emissao"]).'"  onkeyup="mascaraTexto(event,\'99/99/9999\')" maxlength="10"   />
                    </div>
            
                    <div class="col-md-1">
                        <label for="representante_nascimento"> NASCIMENTO:</label>
                        <input required type="text" name="nascimento"  id="representante_nascimento" onkeyup="mascaraTexto(event,\'99/99/9999\')" maxlength="10" class="form-control  " value="'.Conexao::PrepararDataPHP($linha["nascimento"]).'"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="representante_pai">PAI:</label>
                        <input required type="text" name="pai"  id="representante_pai" maxlength="255" class="form-control  " value="'.$linha['pai'].'"/>
                    </div>
                    <div class="col-md-3">
                        <label for="representante_mae">MÃE:</label>
                        <input required type="text" name="mae"  id="representante_mae" maxlength="255" class="form-control  " value="'.$linha['mae'].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="representante_nacionalidade">NACIONALIDADE:</label>
                        <input required type="text" name="nacionalidade"  id="representante_nacionalidade" maxlength="255" class="form-control  "  value="'.$linha['nacionalidade'].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="representante_naturalidade">NATURALIDADE:</label>
                        <input required type="text" name="naturalidade"  id="representante_naturalidade" maxlength="255" class="form-control  " value="'.$linha['naturalidade'].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="representante_estado_nascimento">ESTADO NASCIMENTO:</label>
                        <select id="representante_estado_nascimento" name="estado_nascimento" class="form-control">
                        '.$estados2.'
            
                        </select>
            
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="representante_estado_civil">ESTADO CIVIL:</label>
                        <select required name="estado_civil" id="representante_estado_civil" class="form-control">
                            <option value="0">--SELECIONE--</option>
                            <option value="1" '.$estado_civil1.'>CASADO(A)</option>
                            <option value="2" '.$estado_civil2.'>DIVORCIADO(A)</option>
                            <option value="3  '.$estado_civil3.'>UNIÃO ESTÁVEL</option>
                            <option value="4" '.$estado_civil4.'>SOLTEIRO(A)</option>
                            <option value="5" '.$estado_civil5.'>VIÚVO(A)</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label  for="representante_grau_instrucao"> GRAU DE INSTRUÇÃO:</label>
                        <select name="grau_instrucao" id="representante_grau_instrucao" class="form-control">
                            <option value="0">--SELECIONE--</option>
                            <option value="1" '.$grau_instrucao1.'>ENSINO FUNDAMENTAL COMPLETO</option>
                            <option value="2" '.$grau_instrucao2.'>ENSINO FUNDAMENTAL INCOMPLETO</option>
                            <option value="3" '.$grau_instrucao3.'>ENSIMO MÉDIO COMPLETO</option>
                            <option value="4" '.$grau_instrucao4.'>ENSIMO MÉDIO INCOMPLETO</option>
                            <option value="5" '.$grau_instrucao5.'>ENSINO SUPERIOR COMPLETO</option>
                            <option value="6" '.$grau_instrucao6.'>ENSINO SUPERIOR INCOMPLETO</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <label for="representante_profissao"> PROFISSÃO:</label>
                        <input required type="text" name="profissao"  id="representante_profissao" maxlength="100" class="form-control  " value="'.$linha['profissao'].'"/>
                    </div>
                    <div class="col-md-1">
                        <label for="representante_cargo"> CARGO:</label>
                        <input required type="text" name="cargo"  id="representante_cargo" maxlength="100" class="form-control  " value="'.$linha['cargo'].'"/>
                    </div>
            
                    <div class="col-md-2">
                        <label for="representante_telefone"> TELEFONE:</label>
                        <input required type="text" name="telefone"  id="representante_telefone" maxlength="100" class="form-control  " value="'.$linha['telefone'].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="representante_celular"> CELULAR:</label>
                        <input required type="text" name="celular"  id="representante_celular" maxlength="100" class="form-control mask-telefone "  value="'.$linha['celular'].'"/>
                    </div>
                    <div class="col-md-2">
                        <label for="representante_email"> E-MAIL:</label>
                        <input  required type="text" name="email"  id="representante_email" maxlength="100" class="form-control  " value="'.$linha['email'].'"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-1">
                        <label for="cep_contato_escritorio"> CEP:</label>
                        <input type="text" name="cep"  id="cep_contato_escritorio" onkeyup="mascaraTexto(event,\'99999-999\')" maxlength="9" class="form-control  " value="'.$linha["cep"].'"/>
                    </div>
                    <div class="col-md-3">
                        <label for="logradouro_contato_escritorio"> LOGRADOURO:</label>
                        <input type="text" name="logradouro"  id="logradouro_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["logradouro"].'"/>
                    </div>
            
                    <div class="col-md-1">
                        <label for="numero_contato_escritorio"> NÚMERO:</label>
                        <input type="text" name="numero"  id="numero_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["numero"].'"/>
                    </div>
            
                    <div class="col-md-1">
                        <label for="complemento_contato_escritorio"> COMPLEMENTO:</label>
                        <input type="text" name="complemento"  id="complemento_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["complemento"].'"/>
                    </div>
            
            
                    <div class="col-md-2">
                        <label for="bairro_contato_escritorio"> BAIRRO:</label>
                        <input type="text" name="bairro"  id="bairro_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["bairro"].'"/>
                    </div>
            
                    <div class="col-md-1">
                        <label for="id_estado_contato_escritorio"> ESTADO:</label>
                        <select id="id_estado_contato_escritorio" name="id_estado" class="form-control">
                        '.$estados.'
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="id_cidade_contato_escritorio"> CIDADE:</label>
                        <select id="id_cidade_contato_escritorio" name="id_cidade" class="form-control">
                        '.$cidades.'
                        </select>
                    </div>
            
                    <div class="col-md-1">
                        <label for="caixa_postal_contato_escritorio">CAIXA POSTAL:</label>
                        <input type="text" name="caixa_postal"  id="caixa_postal_contato_escritorio" maxlength="255" class="form-control  " value="'.$linha["caixa_postal"].'"/>
                    </div>
                 </div>   
               
           
                
         
                <hr /> 
               
        
        ';

    }
}

?>

