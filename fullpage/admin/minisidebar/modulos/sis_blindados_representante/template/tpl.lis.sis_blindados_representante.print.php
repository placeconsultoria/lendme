<?php
$numeroRegistros      = 20000;
$numeroInicioRegistro = 0;
$busca                = $_REQUEST["busca"];

$objSisBlindadosRepresentante = new SisBlindadosRepresentante();
$listar = $objSisBlindadosRepresentante->ListarPaginacao($_SESSION['usuario']['id_grupo'],$numeroRegistros,$numeroInicioRegistro,$busca,$filtro,$ordem);

$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_ID];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_NOME];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CPF];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_RG];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_EMISSOR];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_DATA_EMISSAO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_NASCIMENTO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_PAI];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_MAE];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_NACIONALIDADE];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_NATURALIDADE];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_ESTADO_NASCIMENTO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_ESTADO_CIVIL];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_GRAU_INSTRUCAO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_PROFISSAO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CARGO];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_TELEFONE];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_CELULAR];
$dados_coluna["dados_th"][] = ["configuracao" => "", "nome" => RTL_EMAIL];

$x = 0;
if (count($listar[0]) > 0) {
	foreach ($listar[0] as $linha) {
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["id"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["nome"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["cpf"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["rg"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["emissor"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["data_emissao"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["nascimento"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["pai"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["mae"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["nacionalidade"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["naturalidade"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["estado_nascimento"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["estado_civil"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["grau_instrucao"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["profissao"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["cargo"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["telefone"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["celular"],"class"=> "uppercase"];
		$dados_linha[$x]["dados_td"][] = ["valor" => $linha["email"],"class"=> "uppercase"];
		$x++;
	}
}
//Componente::FiltrarRelatorioConfiguracao($dados_coluna, $dados_linha, $_SESSION["configuracao_usuario"]["sis_blindados_representante"]);

$filtro[ROTULO_LISTAGEM] = RTL_SIS_BLINDADOS_REPRESENTANTE;
//$filtro[ROTULO_RELATORIO] = RTL_SIS_BLINDADOS_REPRESENTANTE;
if ($_REQUEST["id_grupo"] > 0) {
	$filtro[ROTULO_GRUPO] = $_REQUEST["nome_grupo"];
}
if ($_REQUEST["id_veiculo"] > 0) {
	$filtro[ROTULO_VEICULO] = $_REQUEST["nome_veiculo"];
}
$filtro[ROTULO_DATA_INICIAL] = $_REQUEST["data_hora_inicio"];
$filtro[ROTULO_DATA_FINAL]   = $_REQUEST["data_hora_fim"];

$tabela                 = new GerarTabelaPrint();
$tabela->buscaAtiva     = false;
$tabela->nome           = "";
$tabela->totalRegistros = $listar[1]->total;
$tabela->dados          = $dados_linha;
$tabela->center         = $center;
$tabela->colunas        = $dados_coluna;
$tabela->botaoAdicionar = false;
$tabela->botao          = false;
$tabela->paginacao      = false;
$tabela->filtro         = $filtro;
echo $tabela->CriarTabela();
