<?
class Legislacoes
{
	private $id;
	private $texto;
	private $data;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setTexto($arg)
	{
		$this->texto = $arg;
	}
 	
	public function getTexto()
	{
		return $this->texto;
	}
 	
	public function setData($arg)
	{
		$this->data = $arg;
	}
 	
	public function getData()
	{
		return $this->data;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}


	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE legislacoes SET data = NOW() ';
		if ($this->getTexto() != "") $sql .= ", texto = ?";
        $sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getTexto() != "") $stmt->bindParam(++$x,$this->getTexto(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}


	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM legislacoes WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

	public function getTitleLegislacao($id){
	    if($id == "1"){
	        $title = "EXÉRCITO";
        }else if($id == "2"){
	        $title = "POLÍCIA FEDERAL";
        }else if($id == "3"){
	        $title = "POLÍCIA CIVIL";
        }
        return $title;
    }
}
