<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/legislacoes/classe.legislacoes.php");
$app_comando = $_REQUEST["acao"];


switch($app_comando)
{


	case "alt_legislacao":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objLegislacoes = new Legislacoes($pdo);
			$objLegislacoes->setId($_REQUEST['id']);
			$objLegislacoes->setTexto($_REQUEST['texto']);
			$objLegislacoes->setData($_REQUEST['data']);
			$objLegislacoes->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Esta legislação foi alterada com sucesso!";
            $msg["data"] = date("d/m/Y - H:i:s");
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.legislacoes.php";
		break;


}
