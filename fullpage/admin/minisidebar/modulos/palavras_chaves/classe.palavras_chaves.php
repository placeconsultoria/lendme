<?
class PalavrasChaves
{
	private $id;
	private $id_palavra;
	private $palavra_chave;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdPalavra($arg)
	{
		$this->id_palavra = $arg;
	}
 	
	public function getIdPalavra()
	{
		return $this->id_palavra;
	}
 	
	public function setPalavraChave($arg)
	{
		$this->palavra_chave = $arg;
	}
 	
	public function getPalavraChave()
	{
		return $this->palavra_chave;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO palavras_chaves SET ';
		if ($this->getIdPalavra() != "") $sql .= " id_palavra = ? ";
		if ($this->getPalavraChave() != "") $sql .= " ,palavra_chave = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdPalavra() != "") $stmt->bindParam(++$x,$this->getIdPalavra(),PDO::PARAM_INT);
		if ($this->getPalavraChave() != "") $stmt->bindParam(++$x,$this->getPalavraChave(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = '
		UPDATE palavras_chaves SET 
			id = ?';
		if ($this->getIdPalavra() != "") $sql .= ",id_palavra = ?";
		if ($this->getPalavraChave() != "") $sql .= ",palavra_chave = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdPalavra() != "") $stmt->bindParam(++$x,$this->getIdPalavra(),PDO::PARAM_INT);
		if ($this->getPalavraChave() != "") $stmt->bindParam(++$x,$this->getPalavraChave(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
    public function Remover()
    {
        $pdo = $this->getConexao();

        $sql = "DELETE FROM palavras_chaves WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
        return $stmt->execute();
    }



	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM palavras_chaves WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT *
                FROM palavras_chaves
                WHERE id_palavra = ?
                ORDER BY palavra_chave";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdPalavra(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function totalPalavra()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT count(id) as total
                FROM palavras_chaves
                WHERE id_palavra = ?
                ORDER BY palavra_chave";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdPalavra(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
    }


    public function ListagemParaComparacao()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id, palavra_chave
                FROM palavras_chaves
               ";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


}
