<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/palavras_chaves/classe.palavras_chaves.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{

	case "add_palavra_chave":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPalavrasChaves = new PalavrasChaves($pdo);
			$objPalavrasChaves->setIdPalavra($_REQUEST['id_palavra']);
			$objPalavrasChaves->setPalavraChave($_REQUEST['palavra_chave']);
			$novoId = $objPalavrasChaves->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Palavra chave adicionada com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.palavras_chaves.php";
		break;



	case "atualizar_palavras_chaves":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPalavrasChaves = new PalavrasChaves($pdo);
			$objPalavrasChaves->setId($_REQUEST['id']);
			$objPalavrasChaves->setIdPalavra($_REQUEST['id_palavra']);
			$objPalavrasChaves->setPalavraChave($_REQUEST['palavra_chave']);
			$objPalavrasChaves->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.palavras_chaves.php";
		break;

	case "listar_palavras_chaves":
		$template = "tpl.geral.palavras_chaves.php";
		break;

	case "deletar":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPalavrasChaves = new PalavrasChaves($pdo);
			$objPalavrasChaves->setId($_REQUEST["id"]);
			$objPalavrasChaves->Remover();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Item removido com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.palavras_chaves.php";
		break;

	case "ajax_listar_palavras_chaves":
		$template = "tpl.lis.palavras_chaves.php";
		break;

	case "palavras_chaves_pdf":
		$template = "tpl.lis.palavras_chaves.pdf.php";
		break;

	case "palavras_chaves_xlsx":
		$template = "tpl.lis.palavras_chaves.xlsx.php";
		break;

	case "palavras_chaves_print":
		$template = "tpl.lis.palavras_chaves.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["palavras_chaves"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("palavras_chaves");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.palavras_chaves.php";
		break;
}
