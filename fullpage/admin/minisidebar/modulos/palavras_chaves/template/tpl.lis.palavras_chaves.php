<?php
define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/categoria_palavaras/classe.categoria_palavaras.php");
include_once(URL_FILE . "modulos/palavras_chaves/classe.palavras_chaves.php");


$objPalavrasChaves = new PalavrasChaves();
$objPalavrasChaves->setIdPalavra($_GET["idPalavra"]);
$listar = $objPalavrasChaves->Listar();




?>


<div class="col-md-12">
    <div class="table-responsive">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>PALAVRA CHAVE</th>

                <th>GERENCIAR</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(count($listar) > 0){
                foreach($listar as $linha){
                    $id = $linha["id"];


                    echo '
                             <tr>
                                <td>'.$linha["palavra_chave"].'</td>
                                <td><a href="'.$id.'" class="btn btn-danger" name="btn_apagar_palavra_chave"> APAGAR <i class="fa fa-trash"></i></a> </td>

                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>



<script src="../../../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();

        $('a[name="btn_apagar_palavra_chave"]').click(function (event) {
            event.preventDefault();
            var token = $(this).attr("href");
            var url = "modulos/palavras_chaves/modulo.palavras_chaves.php?acao=deletar&id="+token;


            $.post(url,
                // pegando resposta do retorno do post
                function (response)
                {

                    if (response["codigo"] == 0) {

                        var token2 =  $("#gettoken").val();
                        $("#load_palavras_chaves").load("modulos/palavras_chaves/template/tpl.lis.palavras_chaves.php?idPalavra="+token2);

                        swal("SUCESSO!","Item excluido com sucesso!");

                    } else {
                        swal("ERRO!","Detalhe: "+response["debug"],"error");
                    }
                }
                , "json" // definindo retorno para o formato json
            );

        });


    });
</script>