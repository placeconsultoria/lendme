<?
class Import
{
	private $campo1;
	private $campo2;
	private $campo3;
	private $campo4;
	private $campo5;
	private $campo6;
	private $campo7;
	private $campo8;
	private $campo9;
	private $campo10;
	private $campo11;
	private $conexao;

	public function setCampo1($arg)
	{
		$this->campo1 = $arg;
	}
 	
	public function getCampo1()
	{
		return $this->campo1;
	}
 	
	public function setCampo2($arg)
	{
		$this->campo2 = $arg;
	}
 	
	public function getCampo2()
	{
		return $this->campo2;
	}
 	
	public function setCampo3($arg)
	{
		$this->campo3 = $arg;
	}
 	
	public function getCampo3()
	{
		return $this->campo3;
	}
 	
	public function setCampo4($arg)
	{
		$this->campo4 = $arg;
	}
 	
	public function getCampo4()
	{
		return $this->campo4;
	}
 	
	public function setCampo5($arg)
	{
		$this->campo5 = $arg;
	}
 	
	public function getCampo5()
	{
		return $this->campo5;
	}
 	
	public function setCampo6($arg)
	{
		$this->campo6 = $arg;
	}
 	
	public function getCampo6()
	{
		return $this->campo6;
	}
 	
	public function setCampo7($arg)
	{
		$this->campo7 = $arg;
	}
 	
	public function getCampo7()
	{
		return $this->campo7;
	}
 	
	public function setCampo8($arg)
	{
		$this->campo8 = $arg;
	}
 	
	public function getCampo8()
	{
		return $this->campo8;
	}
 	
	public function setCampo9($arg)
	{
		$this->campo9 = $arg;
	}
 	
	public function getCampo9()
	{
		return $this->campo9;
	}
 	
	public function setCampo10($arg)
	{
		$this->campo10 = $arg;
	}
 	
	public function getCampo10()
	{
		return $this->campo10;
	}
 	
	public function setCampo11($arg)
	{
		$this->campo11 = $arg;
	}
 	
	public function getCampo11()
	{
		return $this->campo11;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Listar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM import10";
		$stmt = $pdo->prepare($sql);
		$stmt->execute();
        $linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $linhas;
	}

}
