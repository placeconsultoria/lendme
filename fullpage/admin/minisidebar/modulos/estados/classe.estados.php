<?
class Estados
{
	private $id;
	private $sigla;
	private $nome;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setSigla($arg)
	{
		$this->sigla = $arg;
	}
 	
	public function getSigla()
	{
		return $this->sigla;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = '
		INSERT INTO estados SET 
			id = ?';
		if ($this->getSigla() != "") $sql .= ",sigla = ?";
		if ($this->getNome() != "") $sql .= ",nome = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getSigla() != "") $stmt->bindParam(++$x,$this->getSigla(),PDO::PARAM_STR);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = '
		UPDATE estados SET 
			id = ?';
		if ($this->getSigla() != "") $sql .= ",sigla = ?";
		if ($this->getNome() != "") $sql .= ",nome = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getSigla() != "") $stmt->bindParam(++$x,$this->getSigla(),PDO::PARAM_STR);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM estados WHERE id IN({$lista})";
		$sql = "UPDATE estados SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}



	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM estados WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}
    public function GerarSelectEstados()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id as id_estado, nome, sigla FROM estados ORDER BY nome ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function GerarSelectEstadosServico()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id as id_estado, nome, sigla FROM estados ORDER BY nome ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
