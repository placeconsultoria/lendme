<?
class PreOrcamento
{
	private $id;
	private $data_criacao;
	private $data_interacao_cliente;
	private $cliente;
	private $status;
	private $id_cliente_cadastro;
	private $tipo_cadastro;
	private $url_name;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setDataCriacao($arg)
	{
		$this->data_criacao = $arg;
	}
 	
	public function getDataCriacao()
	{
		return $this->data_criacao;
	}
 	
	public function setDataInteracaoCliente($arg)
	{
		$this->data_interacao_cliente = $arg;
	}
 	
	public function getDataInteracaoCliente()
	{
		return $this->data_interacao_cliente;
	}
 	
	public function setCliente($arg)
	{
		$this->cliente = $arg;
	}
 	
	public function getCliente()
	{
		return $this->cliente;
	}
 	
	public function setStatus($arg)
	{
		$this->status = $arg;
	}
 	
	public function getStatus()
	{
		return $this->status;
	}
 	
	public function setIdClienteCadastro($arg)
	{
		$this->id_cliente_cadastro = $arg;
	}
 	
	public function getIdClienteCadastro()
	{
		return $this->id_cliente_cadastro;
	}
 	
	public function setTipoCadastro($arg)
	{
		$this->tipo_cadastro = $arg;
	}
 	
	public function getTipoCadastro()
	{
		return $this->tipo_cadastro;
	}

	//

    public function setUrlName($arg)
    {
        $this->url_name = $arg;
    }

    public function getUrlName()
    {
        return $this->url_name;
    }
    //
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO pre_orcamento SET data_criacao = NOW() ';
		if ($this->getDataInteracaoCliente() != "") $sql .= ",data_interacao_cliente = ?";
		if ($this->getCliente() != "") $sql .= ",cliente = ?";
		if ($this->getStatus() != "") $sql .= ",status = ?";
		if ($this->getIdClienteCadastro() != "") $sql .= ",id_cliente_cadastro = ?";
		if ($this->getTipoCadastro() != "") $sql .= ",tipo_cadastro = ?";
        if ($this->getUrlName() != "") $sql .= ",url_name = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getDataInteracaoCliente() != "") $stmt->bindParam(++$x,$this->getDataInteracaoCliente(),PDO::PARAM_STR);
		if ($this->getCliente() != "") $stmt->bindParam(++$x,$this->getCliente(),PDO::PARAM_STR);
		if ($this->getStatus() != "") $stmt->bindParam(++$x,$this->getStatus(),PDO::PARAM_INT);
		if ($this->getIdClienteCadastro() != "") $stmt->bindParam(++$x,$this->getIdClienteCadastro(),PDO::PARAM_INT);
		if ($this->getTipoCadastro() != "") $stmt->bindParam(++$x,$this->getTipoCadastro(),PDO::PARAM_INT);
        if ($this->getUrlName() != "") $stmt->bindParam(++$x,$this->getUrlName(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE pre_orcamento SET id = ?';
		if ($this->getDataInteracaoCliente() != "") $sql .= ",data_interacao_cliente = ?";
		if ($this->getCliente() != "") $sql .= ",cliente = ?";
		if ($this->getStatus() != "") $sql .= ",status = ?";
		if ($this->getIdClienteCadastro() != "") $sql .= ",id_cliente_cadastro = ?";
		if ($this->getTipoCadastro() != "") $sql .= ",tipo_cadastro = ?";
        if ($this->getUrlName() != "") $sql .= ",url_name = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getDataInteracaoCliente() != "") $stmt->bindParam(++$x,$this->getDataInteracaoCliente(),PDO::PARAM_STR);
		if ($this->getCliente() != "") $stmt->bindParam(++$x,$this->getCliente(),PDO::PARAM_STR);
		if ($this->getStatus() != "") $stmt->bindParam(++$x,$this->getStatus(),PDO::PARAM_INT);
		if ($this->getIdClienteCadastro() != "") $stmt->bindParam(++$x,$this->getIdClienteCadastro(),PDO::PARAM_INT);
		if ($this->getTipoCadastro() != "") $stmt->bindParam(++$x,$this->getTipoCadastro(),PDO::PARAM_INT);
        if ($this->getUrlName() != "") $stmt->bindParam(++$x,$this->getUrlName(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);

		return $stmt->execute();
	}
	public function Remover()
	{
		$pdo = $this->getConexao();
		$sql = "DELETE FROM pre_orcamento WHERE id = ?";
		//$sql = "UPDATE pre_orcamento SET excluido = UTC_TIMESTAMP() WHERE id = ?";
		$stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
        return $stmt->execute();
	}



	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM pre_orcamento WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function Listar()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM pre_orcamento ORDER BY ID ASC";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function translate_similar_chars($buffer) {
        $single_double = array(
            'Æ' => 'AE',
            'æ' => 'ae'
        );
        $buffer = strtr(
            $buffer,
            $single_double
        );
        $buffer = strtr(
            utf8_decode($buffer),
            utf8_decode('áàâäãåÁÀÂÄÃÅÞþßćčçĆČÇđĐÐéèêëÉÈÊËíìîïÍÌÎÏñÑóòôöõðøÓÒÔÖÕŕŔšŠ$úùûüÚÙÛÜýÿÝžŽØªº¹²³'),
            utf8_decode('aaaaaaAAAAAAbbBcccCCCdDDeeeeEEEEiiiiIIIInNoooooooOOOOOrRsSSuuuuUUUUyyYzZ0ao123')
        );
        $buffer = utf8_encode($buffer);
        return $buffer;
    }

    public function generate_friendly_url($buffer) {
        $buffer = html_entity_decode($buffer);                       // Converte todas as entidades HTML para os seus caracteres
        $buffer = $this->translate_similar_chars($buffer);                  // Converte caracteres que não estão no padrão para representações deles
        $buffer = strtolower($buffer);                               // Converte uma string para minúscula
        $buffer = preg_replace("/[\s]+/", " ", $buffer);             // Comprime múltiplas ocorrências de espaços
        $buffer = preg_replace("/[_]+/", "_", $buffer);              // Comprime múltiplas ocorrências de underscores
        $buffer = preg_replace("/[-]+/", "-", $buffer);              // Comprime múltiplas ocorrências de hífens
        $buffer = preg_replace("/[\/]+/", "-", $buffer);             // Comprime múltiplas ocorrências de barras
        $buffer = preg_replace("/[\\\]+/", "-", $buffer);            // Comprime múltiplas ocorrências de barras invertidas
        $buffer = preg_replace("/[[\s]+]?-[[\s]+]?/", "-", $buffer); // Remove espaços antes e após hífens
        $buffer = preg_replace("/[\s]/", "-", $buffer);              // Converte espaços em hífens
        $buffer = preg_replace("/[_]/", "-", $buffer);               // Converte underscores em hífens
        $buffer = preg_replace("/[\/]/", "-", $buffer);              // Converte barras em hífens
        $buffer = preg_replace("/[\\\]/", "-", $buffer);             // Converte barras invertidas em hífens
        $buffer = preg_replace("/[^a-z0-9_-]/", "", $buffer);      // Remove caracteres que não estejam no padrão

        return $buffer;
    }

    public function getStatusPreCadastro($param){
        if($param == 0){
            $status = '<h3><span class="badge badge-default">AGUARDANDO</span> </h3>';
        }else if($param == 1){
            $status = '<h3><span class="badge badge-warning">ACESSOU</span> </h3>';
        }else if($param == 2){
            $status = '<h3><span class="badge badge-info">FAZENDO O ORÇAMENTO</span> </h3>';
        }else if($param == 3){
            $status = '<h3><span class="badge badge-info">ORÇAMENTO PRONTO</span> </h3>';
        }

        return $status;

    }
}
