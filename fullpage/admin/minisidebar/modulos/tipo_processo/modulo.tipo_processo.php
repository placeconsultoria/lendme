<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/tipo_processo/classe.tipo_processo.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{


	case "adicionar_tipo_processo":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objTipoProcesso = new TipoProcesso($pdo);
			$objTipoProcesso->setIdDepartamento($_REQUEST['id_departamento']);
			$objTipoProcesso->setIdCertificacao($_REQUEST['id_certificacao']);
			$objTipoProcesso->setProcesso($_REQUEST['processo']);
			$objTipoProcesso->setTaxa($_REQUEST['taxa']);
			$objTipoProcesso->setHonorario($_REQUEST['honorario']);
			$novoId = $objTipoProcesso->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.tipo_processo.php";
		break;



	case "atualizar_tipo_processo":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objTipoProcesso = new TipoProcesso($pdo);
			$objTipoProcesso->setId($_REQUEST['id']);
			$objTipoProcesso->setIdDepartamento($_REQUEST['id_departamento']);
			$objTipoProcesso->setIdCertificacao($_REQUEST['id_certificacao']);
			$objTipoProcesso->setProcesso($_REQUEST['processo']);
			$objTipoProcesso->setTaxa($_REQUEST['taxa']);
			$objTipoProcesso->setHonorario($_REQUEST['honorario']);
			$objTipoProcesso->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.tipo_processo.php";
		break;



	case "deletar_tipo_processo":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objTipoProcesso = new TipoProcesso($pdo);
			$objTipoProcesso->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.tipo_processo.php";
		break;



}
