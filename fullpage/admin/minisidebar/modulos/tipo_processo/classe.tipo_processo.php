<?
class TipoProcesso
{
	private $id;
	private $id_departamento;
	private $id_certificacao;
	private $processo;
	private $taxa;
	private $honorario;
	private $validade;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdDepartamento($arg)
	{
		$this->id_departamento = $arg;
	}
 	
	public function getIdDepartamento()
	{
		return $this->id_departamento;
	}

	public function setIdCertificacao($arg)
	{
		$this->id_certificacao = $arg;
	}
 	
	public function getIdCertificacao()
	{
		return $this->id_certificacao;
	}
 	
	public function setProcesso($arg)
	{
		$this->processo = $arg;
	}
 	
	public function getProcesso()
	{
		return $this->processo;
	}
 	
	public function setTaxa($arg)
	{
		//$this->taxa = $arg;
        $this->taxa = str_replace(",",".",str_replace(".","",$arg));
	}
 	
	public function getTaxa()
	{
		return $this->taxa;
	}
 	
	public function setHonorario($arg)
	{
		//$this->honorario = $arg;
        $this->honorario = str_replace(",",".",str_replace(".","",$arg));
	}
 	
	public function getHonorario()
	{
		return $this->honorario;
	}


    public function setValidade($arg)
    {
        $this->validade = $arg;
    }

    public function getValidade()
    {
        return $this->validade;
    }

 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO valor_tipo_processo SET ';
		if ($this->getIdDepartamento() != "") $sql .= "id_departamento = ?";
		if ($this->getIdCertificacao() != "") $sql .= ",id_certificacao = ?";
		if ($this->getProcesso() != "") $sql .= ",processo = ?";
		if ($this->getTaxa() != "") $sql .= ",taxa = ?";
		if ($this->getHonorario() != "") $sql .= ",honorario = ?";
        if ($this->getValidade() != "") $sql .= ",validade = ?";
        $stmt = $pdo->prepare($sql);
		if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		if ($this->getIdCertificacao() != "") $stmt->bindParam(++$x,$this->getIdCertificacao(),PDO::PARAM_INT);
		if ($this->getProcesso() != "") $stmt->bindParam(++$x,$this->getProcesso(),PDO::PARAM_STR);
		if ($this->getTaxa() != "") $stmt->bindParam(++$x,$this->getTaxa(),PDO::PARAM_STR);
		if ($this->getHonorario() != "") $stmt->bindParam(++$x,$this->getHonorario(),PDO::PARAM_STR);
        if ($this->getValidade() != "") $stmt->bindParam(++$x,$this->getValidade(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE valor_tipo_processo SET ';
		if ($this->getIdDepartamento() != "") $sql .= "id_departamento = ?";
		if ($this->getIdCertificacao() != "") $sql .= ",id_certificacao = ?";
		if ($this->getProcesso() != "") $sql .= ",processo = ?";
		if ($this->getTaxa() != "") $sql .= ",taxa = ?";
		if ($this->getHonorario() != "") $sql .= ",honorario = ?";
        if ($this->getValidade() != "") $sql .= ",validade = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		if ($this->getIdCertificacao() != "") $stmt->bindParam(++$x,$this->getIdCertificacao(),PDO::PARAM_INT);
		if ($this->getProcesso() != "") $stmt->bindParam(++$x,$this->getProcesso(),PDO::PARAM_STR);
		if ($this->getTaxa() != "") $stmt->bindParam(++$x,$this->getTaxa(),PDO::PARAM_STR);
		if ($this->getHonorario() != "") $stmt->bindParam(++$x,$this->getHonorario(),PDO::PARAM_STR);
        if ($this->getValidade() != "") $stmt->bindParam(++$x,$this->getValidade(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover()
	{
		$pdo = $this->getConexao();
		$sql = "DELETE FROM valor_tipo_processo WHERE id = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE tipo_processo.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM tipo_processo
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				tipo_processo.*
			FROM tipo_processo
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY tipo_processo.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM valor_tipo_processo WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}


}
