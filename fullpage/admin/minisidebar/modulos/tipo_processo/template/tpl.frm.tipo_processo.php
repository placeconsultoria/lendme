<?php
    define('URL_FILE',"../../../");
    ini_set("display_errors", true);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    include_once(URL_FILE . "classes/Conexao.php");
    include_once(URL_FILE . "modulos/departamento/classe.departamento.php");


    $objDepartamento = new Departamento($pdo);
    $departamentos = $objDepartamento->Listar();

    if($_GET["id"] != ""){
        /* $objDepartamento = new Departamento($pdo);
         $objDepartamento->setId($_GET["id"]);
         $linha = $objDepartamento->Editar();*/
        $acao = "edit_certificacao";
    }else{
        $acao = "add_certificacao";
    }

?>

<form id="frm_tipo_processo">
	<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
    <div class="row form-group">
        <div class="col-md-4">
            <label for="id_departamento">DEPARTAMENTO:</label>
            <select id="id_departamento" name="id_departamento" class="form-control">
                <?php
                echo "<option value='0'>--SELECIONE--</option>";
                foreach ($departamentos AS $departamento){
                    echo '<option value="'.$departamento["id"].'">'.$departamento["departamento"].'</option>';
                }
                ?>
            </select>
        </div>
        <div class="col-md-8">
            <label for="id_certificacao">SELECIONE A CERTIFICAÇÃO:</label>
            <select id="id_certificacao" name="id_certificacao" class="form-control">
                <?php echo "<option value='0'>--AGUARDE--</option>"; ?>
            </select>
        </div>


    </div>
    <div id="load_frm_include">
    </div>
</form>

<script type="text/javascript">
    $(function()
    {
        $('.mask-valor').mask('#.##0,00', { reverse: true });
        $("#id_departamento").change(function () {
            var id_departamento = $(this).val();
            $("#id_certificacao").html('<option>CARREGANDO...</option>');
            $("#load_frm_include").html('<div class="alert alert-info" role="alert"><i class="fa fa-spin fa-spinner"></i> Aguarde, estamos processando...</div>');
            var options = [];
            $.getJSON("modulos/certificacao/modulo.certificacao.php?acao=list_ajax_tipo_departamento&app_codigo="+id_departamento, function(result) {
                options.push('<option value="">-- SELECIONE --</option>');
                for (var i = 0; i < result.length; i++)
                {
                    certificacao = result[i].certificacao;
                    estado = result[i].estado_sigla;
                    tipo_preco  = result[i].tipo_preco;
                    if(estado != null){
                        option = "("+estado+") - "+certificacao;
                    }else{
                        option = certificacao;
                    }
                    options.push('<option value="'+result[i].id+'">'+option+'</option>');
                }

                $("#id_certificacao").html(options.join(''));
                if(tipo_preco == 1){
                   $("#load_frm_include").load("modulos/tipo_processo/template/tpl.frm.tipo_processo.include.1.php");
                }else if(tipo_preco == 2){
                    $("#load_frm_include").load("modulos/tipo_processo/template/tpl.frm.tipo_processo.include.2.php");
                }
            });
        });
    });


</script>
