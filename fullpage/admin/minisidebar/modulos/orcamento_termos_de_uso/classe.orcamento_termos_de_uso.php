<?
class OrcamentoTermosDeUso
{
	private $id;
	private $texto;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setTexto($arg)
	{
		$this->texto = $arg;
	}
 	
	public function getTexto()
	{
		return $this->texto;
	}
 	

 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}


	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE orcamento_termos_de_uso SET ';
		if ($this->getTexto() != "") $sql .= " texto = ?";
        $sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		if ($this->getTexto() != "") $stmt->bindParam(++$x,$this->getTexto(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}


	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM orcamento_termos_de_uso WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}


}
