<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/orcamento_termos_de_uso/classe.orcamento_termos_de_uso.php");
$app_comando = $_REQUEST["acao"];


switch($app_comando)
{


	case "alt_termos_uso":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objTermosDeUso = new OrcamentoTermosDeUso($pdo);
            $objTermosDeUso->setId(1);
            $objTermosDeUso->setTexto($_REQUEST['texto']);
            $objTermosDeUso->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Termos de uso alterado!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		//$template = "ajax.legislacoes.php";
		break;


}
