<?php

/**
 * @author Squall Robert
 * @copyright 2011
 */
define('TITLE',"GESTÃO CALL CENTER - LICENSER");

class Conexao extends PDO
{


    private $servidore_banco = "186.233.148.44";
    private $base_dados = "produtoscontrolados";
    private $usuario_banco = "wvkgctbr";
    private $senha_banco = "9MVNdMDghAaL";



    private $porta_banco = "3306";

    public  $erro;
    public $stmt;
    public function __construct()
    {
        try
        {
            $pdo = parent::__construct("mysql:host={$this->servidore_banco};port={$this->porta_banco};dbname={$this->base_dados}", "$this->usuario_banco", "$this->senha_banco");
            parent::exec("SET CHARACTER SET utf8");
            // mostra erros
            parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            parent::setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            parent::setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
            parent::setAttribute(PDO::ATTR_ORACLE_NULLS,PDO::NULL_EMPTY_STRING);
            parent::setAttribute(PDO::ATTR_TIMEOUT, 10 );

            // esconde erros
            //parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            $this->stmt = $pdo;

            $this->erro = parent::errorInfo();
            $pdo = null;
            return $this->stmt;
        }
        catch(PDOException $e)
        {
            echo 'Error: '.$e->getMessage();
        }
    }
    function __destruct() {
        $this->stmt = NULL;
    }


    public function BuscaGeo($sql,$server = '200.195.203.167',$user = 'linkmon',$pass = 'linkonda123',$base = 'geobase')
    {
        $conexao2 = mysqli_connect($server,$user,$pass);
        $db2 = mysqli_select_db($conexao2,$base);

        @mysqli_query("SET CHARACTER SET utf8");
        $sql = mysqli_query($conexao2,$sql);
        $row = mysqli_fetch_assoc($sql);
        mysqli_close($conexao2);

        $endereco = $row['endereco'];
        if($row['bairro'] != "")    $endereco .= " - ". $row['bairro'] ;
        if($row['cep'] != "")       $endereco .= " - ". $row['cep'] ;
        if($row['cidade'] != "")    $endereco .= " - ". $row['cidade'] ;
        if($row['estado'] != "")    $endereco .= " - ". $row['estado'] ;
        if($row['pais'] != "")      $endereco .= " - ". $row['pais'] ;

        $endereco = strtr($endereco, "áàãâéêíóôõúüç??ÀÃÂÉÊ??ÓÔÕÚÜÇ- ", "aaaaeeiooouucaaaaeeiooouuc- ");

        if($row['endereco']!= "")
            $resultado['logradouro'] = htmlspecialchars($endereco,ENT_NOQUOTES, 'UTF-8');
        else
        {
            $resultado['logradouro'] = "";

        }

        $resultado['distancia'] = $row['distancia'];

        //print_r($resultado);
        return $resultado;
    }
    public function ExecProcedures($sql)
    {

        $conexao2 = mysqli_connect($this->servidor,$this->usuario,$this->senha);
        $db2 = mysqli_select_db($conexao2,$this->base);

        @mysqli_query("SET CHARACTER SET utf8");
        $sql = mysqli_query($conexao2,$sql) or die (mysqli_error($conexao2));
        $row = mysqli_fetch_assoc($sql);
        mysqli_close($conexao2);

        $retorno =serialize($row);

        return $retorno;
    }

    public static function PrepararDataBD( $data, $fuso_horario = null, $formatoSaida = "Y-m-d H:i:s" )
    {
        list($data, $hora) = explode(" ", $data);
        $data = explode("/", $data);

//        if(isset($fuso_horario))
//        {
//            if( is_numeric($fuso_horario) )
//                $fuso_horario = FusoHorario::Editar($fuso_horario);
//
//            $hora = explode(":", $hora);
//            $data = date($formatoSaida, mktime($hora[0] - $fuso_horario['multiplicador'] * $fuso_horario['gmt_hora'], $hora[1] - $fuso_horario['multiplicador'] * $fuso_horario['gmt_minuto'], $hora[2], $data[1], $data[0], $data[2]));
//        }
//        else
//        {
            $data = $data[2]."-".$data[1]."-".$data[0]." ".$hora;
        //}

        return $data;
    }

    public static function PrepararDataPHP( $data, $fuso_horario = null, $formatoSaida = "d/m/Y H:i:s" )
    {
        if( !$data )
            return $data;

        list($data, $hora) = explode(" ", $data);
        $data = explode("-", $data);

//        if(isset($fuso_horario))
//        {
//            if( is_numeric($fuso_horario) )
//                $fuso_horario = FusoHorario::Editar($fuso_horario);
//
//            $hora = explode(":", $hora);
//            $data = date($formatoSaida, mktime($hora[0] + $fuso_horario['multiplicador'] * $fuso_horario['gmt_hora'], $hora[1] + $fuso_horario['multiplicador'] * $fuso_horario['gmt_minuto'], $hora[2], $data[1], $data[2], $data[0]));
//        }
//        else
//        {
            $data = $data[2]."/".$data[1]."/".$data[0]." ".$hora;
        //}

        return $data;
    }

    public function getURLBase()
    {
        return new Erro(Erro::ERRO_NENHUM, $this->urlBase);
    }

    public function getDiretorioBase()
    {
        return new Erro(Erro::ERRO_NENHUM, $this->diretorioBase);
    }

    ///////////////////////////////////////////////////////////////////////
    //	$parametrosDestino
    //		[servidor]
    //		[usuario]
    //		[senha]
    //		[base]
    //		[like]
    ///////////////////////////////////////////////////////////////////////
    public function ListarTabelas( $parametros = array() )
    {
        $consulta = "SHOW TABLES";

        if( $parametros['like'] != "" )
        {
            $consulta .= " LIKE '{$parametros['like']}'";
            $parametros['like'] = " ({$parametros['like']})";
        }

        if( !$parametros['servidor'] )	$parametros['servidor'] = $this->servidor;
        if( !$parametros['usuario' ] )	$parametros['usuario' ] = $this->usuario;
        if( !$parametros['senha'   ] )	$parametros['senha'   ] = $this->senha;
        if( !$parametros['base'    ] )	$parametros['base'    ] = $this->base;


        // Executa a consulta montada
        if( @mysql_connect( $parametros['servidor'], $parametros['usuario'], $parametros['senha'] ) )
        {
            if( @mysql_select_db( $parametros['base'] ) )
            {
                @mysql_query("SET CHARACTER SET utf8");
                $resultado = @mysql_query( $consulta );
            }
        }

        if( $resultado )
        {
            $retorno = array();

            while($linha = mysql_fetch_assoc($resultado))
                array_push($retorno,  array("nome_tabela"=>$linha["Tables_in_{$parametros['base']}{$parametros['like']}"]));
        }

        return new Erro(mysql_errno(), $retorno, $consulta);
    }

    public function MostrarScriptCriacaoTabela( $nomeTabela = "" )
    {
        // Executa a consulta montada
        if( @mysql_connect( $this->servidor, $this->usuario, $this->senha ) )
        {
            if( @mysql_select_db( $this->base ) )
            {
                @mysql_query("SET CHARACTER SET utf8");

                if( !$nomeTabela )
                    $nomeTabela = $this->nomeTabela;

                $createTable = @mysql_query("SHOW CREATE TABLE `$nomeTabela`");

                if( $createTable )
                {
                    $createTable = @mysql_fetch_assoc( $createTable );
                    return $createTable['Create Table'];
                }
            }
        }

        return false;
    }

    public function CriarTabela( $consulta, $parServidor = "", $parUsuario = "", $parSenha = "", $parBase = "" )
    {
        if( !$parSevidor )	$parSevidor = $this->servidor;
        if( !$parUsuario )	$parUsuario = $this->usuario;
        if( !$parSenha   )	$parSenha   = $this->senha;
        if( !$parBase    )	$parBase    = $this->base;


        // Executa a consulta montada
        if( @mysql_connect( $this->servidor, $this->usuario, $this->senha ) )
        {
            if( @mysql_select_db( $this->base ) )
            {
                @mysql_query("SET CHARACTER SET utf8");
                @mysql_query( $consulta );
            }
        }

        return new Erro(mysql_errno(), null, $consulta);
    }

    ///////////////////////////////////////////////////////////////////////
    //	$nomeTabela
    //
    //	$parametrosDestino
    //		[servidor]
    //		[usuario]
    //		[senha]
    //		[base]
    //		[nomeTabela]
    ///////////////////////////////////////////////////////////////////////
    public function MoverTabela( $nomeTabela = "", $parametrosDestino = array() )
    {
        set_time_limit(0);

        if( !$nomeTabela )	$nomeTabela = $this->nomeTabela;

        if( !$parametrosDestino['servidor'] ) $parametrosDestino['servidor'] = $this->servidorQuarentena;
        if( !$parametrosDestino['usuario' ] ) $parametrosDestino['usuario' ] = $this->usuarioQuarentena;
        if( !$parametrosDestino['senha'   ] ) $parametrosDestino['senha'   ] = $this->senhaQuarentena;
        if( !$parametrosDestino['base'    ] ) $parametrosDestino['base'    ] = $this->baseQuarentena;

        if( $this->base == $parametrosDestino['base'] )
            return new Erro(mysql_errno(), $nomeTabela);


        // Verifica se a tabela existe na base de origem
        $tabela = $this->ListarTabelas(
            array(
                "like" => $nomeTabela
            )
        );

        // Se sim
        if( count($tabela->resultado) )
        {
            // Verifica se a tabela existe na base de destino
            $tabela = $this->ListarTabelas(
                array(
                    "servidor" => $parametrosDestino['servidor'],
                    "usuario" => $parametrosDestino['usuario'],
                    "senha" => $parametrosDestino['senha'],
                    "base" => $parametrosDestino['base'],
                    "like" => $nomeTabela
                )
            );

            $conexaoDestino = @mysql_connect( $parametrosDestino['servidor'], $parametrosDestino['usuario'], $parametrosDestino['senha'] );

            if( mysql_errno() )
                return new Erro(mysql_errno(), $nomeTabela, $script);

            // Se a tabela nao existe na base destino
            if( count($tabela->resultado) == 0 )
            {
                // Copia o script de criacao
                $script = $this->MostrarScriptCriacaoTabela( $nomeTabela );

                // Cria a tabela na base de destino
                if( @mysql_select_db( $parametrosDestino['base'], $conexaoDestino ) )
                {
                    @mysql_query("SET CHARACTER SET utf8", $conexaoDestino );
                    @mysql_query($script, $conexaoDestino );
                }

                if( mysql_errno() )
                    return new Erro(mysql_errno(), $nomeTabela, $script);
            }

            $conexaoOrigem = mysql_connect( $this->servidor, $this->usuario, $this->senha, true );

            if( mysql_errno() )
                return new Erro(mysql_errno(), $nomeTabela, $script);

            @mysql_select_db( $this->base, $conexaoOrigem );

            if( mysql_errno() )
                return new Erro(mysql_errno(), $nomeTabela, $script);

            @mysql_query("SET CHARACTER SET utf8", $conexaoOrigem );

            // Se as bases origem e destino estão no mesmo servidor
            if( $this->servidor == $parametrosDestino['servidor'] )
            {
                // Copia os dados da tabela de origem para a de destino
                $resultadoOrigem = @mysql_query( "INSERT INTO `{$parametrosDestino['base']}`.`$nomeTabela` SELECT * FROM `{$this->base}`.`$nomeTabela`" );

                if( mysql_errno() )
                    return new Erro(mysql_errno(), $nomeTabela, $script);
            }
            else
            {
                // Copia os dados da tabela de origem para a de destino
                $resultadoOrigem = @mysql_query( "SELECT * FROM `$nomeTabela`", $conexaoOrigem );

                if( mysql_errno() )
                    return new Erro(mysql_errno(), $nomeTabela, $script);

                while( $linha = @mysql_fetch_assoc($resultadoOrigem) )
                {
                    foreach( $linha as $campo => $valor )
                        $linha[$campo] = "'".str_replace("'", "\\'", $valor)."'";

                    $valores = implode(",", $linha);

                    $resultadoDestino = @mysql_query( "INSERT INTO `$nomeTabela` VALUES($valores)", $conexaoDestino );

                    if( mysql_errno() )
                        return new Erro(mysql_errno(), $nomeTabela, $script);
                }
            }

            // Apaga a tabela de origem
            @mysql_query("DROP TABLE `$nomeTabela`", $conexaoOrigem);
        }
        else
            return new Erro(mysql_errno(), $nomeTabela);
    }

    public function FormatarTextos($texto)
    {
        $textoFinal = strtr($texto, "áàãâéêíóôõúüç�?ÀÃÂÉÊ�?ÓÔÕÚÜÇ. ", "aaaaeeiooouucaaaaeeiooouuc._");
        return $textoFinal;
    }

    public static function ConverterMaiusculo($str)
    {
        $maiusculas = "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ";
        $minusculas = "àáâãäåæçèéêëìíîïðñòóôõöøùúûüý";

        $str = strtoupper(strtr($str, $minusculas, $maiusculas));
        return strtr($str, array("ß" => "SS"));
    }

    /*private function Traduzir( &$registros )
    {
        if( !$this->traducoes )
        {
            $objIdioma = new Idioma();

            /*if(!is_numeric($this->_idIdioma))
            {
                $this->_idIdioma = 1;
            }* /

            $this->traducoes = $objIdioma->CarregarArquivo($this->_siglaIdioma, null, null, "Tabela.php");
        }

        // Como pode ser que o campo funcionalidade.nome pode ter um apelido
        // Traduz o que for poss�vel em todos os textos de todas as colunas
        $totalRegistros = count($registros);
        for( $i = 0; $i < $totalRegistros; $i++ )
        {
            foreach( $registros[$i] as $campo => $valor )
            {
                if( $this->traducoes[$valor] )
                {
                    if($campo != "identificador")$registros[$i][$campo] = $this->traducoes[$valor];
                }

                //if( @constant($valor) )
                //	$registros[$i][$campo] = constant($valor);
            }
        }
    }*/

    public function TratarErro($erroPDO)
    {
        return new Erro(mysql_errno(),  mysql_affected_rows(), $consulta, mysql_error());
    }


    //Data e hora padrao brasileiro dia/mes/ano
    public function DeterminarTabelas($dataHoraInicio, $dataHoraFim, $idFusoHorario, $parametros = array(),$antigo = 1)
    {

        $fuso_horario = FusoHorario::Editar($idFusoHorario);

        $dataHoraInicio = Tabela::PrepararDataBD($dataHoraInicio, $fuso_horario );

        $dataHoraFim = Tabela::PrepararDataBD($dataHoraFim, $fuso_horario );

        list($data, $hora) = explode(" ", $dataHoraInicio);
        list($ano, $mes, $dia) = explode("-", $data);
        list($hora, $minuto, $segundo) = explode(":", $hora);
        $timestampInicial = @mktime($hora, $minuto, $segundo, $mes, $dia, $ano);

        list($data, $hora) = explode(" ", $dataHoraFim);
        list($ano, $mes, $dia) = explode("-", $data);
        list($hora, $minuto, $segundo) = explode(":", $hora);
        $timestampFinal = @mktime($hora, $minuto, $segundo, $mes, $dia, $ano);


        //if($parametros != null)
        $parametros['like'] = 'posicao_%';
        //else
        //$parametros = array('like' => 'posicao_%');
        if($antigo == 2)
        {
            $parametros['servidor'] = "192.168.0.2";
            $parametros['usuario' ] = "devel";
            $parametros['senha'   ] = "devel";
            $parametros['base'    ] = "monitoramento_bkp";
        }
        $tabelasExistentes = $this->ListarTabelas( $parametros );

        //print_R($tabelasExistentes);
        //die;
        $qtdTabelasExistentes = count($tabelasExistentes->resultado);
        for( $i = 0; $i < $qtdTabelasExistentes; $i++ )
            $tabelasExistentes->resultado[$i] = $tabelasExistentes->resultado[$i]['nome_tabela'];

        $i = $timestampInicial;
        //echo $i;
        $datas = array();
        while($i <= $timestampFinal)
        {
            $tabelaPosicao = "posicao_".date("Ymd", $i);
            //echo $tabelaPosicao;

            if( array_search($tabelaPosicao, $tabelasExistentes->resultado) !== false )
                $datas[$tabelaPosicao] = $tabelaPosicao;

            if($i >= $timestampFinal)
                break;

            if(($timestampFinal - $i) > (60*60*24))
                $i+=(60*60*24);
            else
                $i+=($timestampFinal-$i);

        }

        return $datas;
    }

    public function FusoHorario($idFusoHorario = null)
    {

        if($idFusoHorario == null)
            $idFusoHorario = 23;

        $pdo = new Conexao();

        $sql = "SELECT * FROM fuso_horario WHERE id = $idFusoHorario";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $linha = $stmt->fetchAll(PDO::FETCH_OBJ);
        //print_r($linha);
        // Se ha um fuso-horario cadastrado com este ID
        if(count($linha[0]) > 0)
        {

            $editar['id'			        ] = $linha[0]->id;
            $editar['multiplicador'	        ] = $linha[0]->multiplicador;
            $editar['gmt_hora'		        ] = str_pad( $linha[0]->gmt_hora, 2, "0", STR_PAD_LEFT );
            $editar['gmt_minuto'	        ] = str_pad( $linha[0]->gmt_minuto, 2, "0", STR_PAD_LEFT );
            $editar['verao_multiplicador'	] = $linha[0]->verao_multiplicador;
            $editar['verao_hora'	        ] = str_pad( $linha[0]->verao_hora, 2, "0", STR_PAD_LEFT );
            $editar['verao_minuto'	        ] = str_pad( $linha[0]->verao_minuto, 2, "0", STR_PAD_LEFT );
        }
        else
        {
            $editar['id'					] = 0;
            $editar['multiplicador'			] = 0;
            $editar['gmt_hora'				] = 0;
            $editar['gmt_minuto'			] = 0;
            $editar['verao_multiplicador'	] = 0;
            $editar['verao_hora'			] = 0;
            $editar['verao_minuto'			] = 0;
        }

        return $editar;
    }

    public function ListarTabelasConexao( $parametros = array() )
    {
        $pdo = new Conexao();

        $sql = "SHOW TABLES LIKE 'posicao_%'";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //print_r($rs);

        if(count($rs) > 0)
        {

            $retorno = array();
            foreach($rs as $row)
                array_push($retorno,array("nome_tabela" => $row['tables_in_monitoramento (posicao_%)']));
        }

        return $retorno;
    }

    //Data e hora padrao brasileiro dia/mes/ano
    public function DeterminarTabelasConexao($dataHoraInicio, $dataHoraFim, $idFusoHorario, $parametros = array(),$antigo = 1)
    {
        $pdo = new Conexao();
        $fuso_horario = $this->FusoHorario($idFusoHorario);
        $dataHoraInicio = $pdo->PrepararDataBD($dataHoraInicio, $fuso_horario );
        $dataHoraFim = $pdo->PrepararDataBD($dataHoraFim, $fuso_horario );

        list($data, $hora) = explode(" ", $dataHoraInicio);
        list($ano, $mes, $dia) = explode("-", $data);
        list($hora, $minuto, $segundo) = explode(":", $hora);
        $timestampInicial = @mktime($hora, $minuto, $segundo, $mes, $dia, $ano);

        list($data, $hora) = explode(" ", $dataHoraFim);
        list($ano, $mes, $dia) = explode("-", $data);
        list($hora, $minuto, $segundo) = explode(":", $hora);
        $timestampFinal = @mktime($hora, $minuto, $segundo, $mes, $dia, $ano);


        $tabelasExistentes = $this->ListarTabelasConexao( $parametros );

        $qtdTabelasExistentes = count($tabelasExistentes);
        for( $i = 0; $i < $qtdTabelasExistentes; $i++ )
            $tabelasExistentes[$i] = $tabelasExistentes[$i]['nome_tabela'];


        //print_r($tabelasExistentes);
        $i = $timestampInicial;
        //echo $i;
        $datas = array();
        while($i <= $timestampFinal)
        {
            $tabelaPosicao = "posicao_".date("Ymd", $i);

            if( @array_search($tabelaPosicao, $tabelasExistentes) !== false )
                $datas[$tabelaPosicao] = $tabelaPosicao;

            if($i >= $timestampFinal)
                break;

            if(($timestampFinal - $i) > (60*60*24))
                $i+=(60*60*24);
            else
                $i+=($timestampFinal-$i);

        }

        return $datas;
    }
    public static function pr($dados)
    {
        echo "<pre>";
        print_r($dados);
        echo "</pre>";
    }

}
?>