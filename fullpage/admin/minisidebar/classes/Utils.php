<?php

/**
 * Objeto com funções utilitárias utilizadas em várias áreas do sistema.
 *
 */
abstract class Utils
{
	/**
	 * Trata um valor monetário para ser inserido num campo do tipo float do mysql
	 *
	 * @param string $valor Exemplo 1.123,45
	 */
	static function TrataFloat($valor)
	{
		return str_replace(",", ".", str_replace(".", "", $valor));
	}

	/**
	 * Formata um float com duas decimais, separador de decimal com vírgula e milhar com ponto.
	 *
	 * @param mixed $valor    (String ou float)
	 * @param int   $decimais Número de decimais desejados
	 */
	static function MaskFloat($valor, $decimais = 2)
	{
		if (isset($decimais) == false || $decimais == NULL || trim($decimais) == "" || $decimais < 0) {
			$decimais = 2;
		}

		return number_format($valor, $decimais, ',', '.');
	}

	/**
	 * Função para calcular o próximo dia útil de uma data - Se a data for um dia útil retorna a data informada.
	 *
	 * @param string $data  Data no formato Y-m-d
	 * @param string $saida Formato de saída - padrão "Y-m-d"
	 *
	 * @return string
	 */
	static function ProximoDiaUtil($data, $saida = 'Y-m-d')
	{
		// Converte $data em um UNIX TIMESTAMP
		$timestamp = strtotime($data);

		// Calcula qual o dia da semana de $data
		// O resultado será um valor numérico:
		// 1 -> Segunda ... 7 -> Domingo
		$dia = date('N', $timestamp);

		// Se for sábado (6) ou domingo (7), calcula a próxima segunda-feira
		if ($dia >= 6) {
			$timestamp_final = $timestamp + ((8 - $dia) * 3600 * 24);
		} else {
			// Não é sábado nem domingo, mantém a data de entrada
			$timestamp_final = $timestamp;
		}

		return date($saida, $timestamp_final);
	}

	/**
	 * Retorna o útimo dia do mês
	 *
	 * @param int $mes
	 * @param int $ano
	 */
	static function UltimoDiaMes($mes, $ano)
	{
		$dias   = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
		$ultimo = mktime(0, 0, 0, $mes, $dias, $ano);

		return date("j", $ultimo);
	}

	/**
	 *
	 * Retorna um objeto Datetime da data formatada
	 *
	 * @param string $data
	 *
	 * @return DateTime
	 */
	static public function createDate($data)
	{
		if ($data == '') {
			return NULL;
		}

		list($data, $hora) = explode(" ", $data);
		$data = explode("/", $data);
		$data = $data[2] . "-" . $data[1] . "-" . $data[0] . " " . $hora;

		return new DateTime($data);
	}

	/**
	 * Trata todos os requests feitos ao servidor
	 *
	 * @autor Squall Rober (Francisco José)
	 * */
	static public function TratarRequest()
	{
		$toEscape = Array("SELECT ", "INSERT ", "DELETE ", "UPDATE ", "DROP ", " FROM ", "UNION", " DATABASE ", " TABLE ", " LIKE");

		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST AS $itemPost => $valorPost) {
				if (!is_array($valorPost)) {
					$_POST[$itemPost] = trim(addslashes(str_ireplace($toEscape, chr(92), $valorPost)));
				} else {
					$_POST[$itemPost] = Utils::TratarArray($valorPost);
				}
			}
		}

		if (is_array($_GET) && count($_GET) > 0) {
			foreach ($_GET AS $itemGet => $valorGet) {
				if (!is_array($valorGet)) {
					$_GET[$itemGet] = trim(addslashes(str_ireplace($toEscape, chr(92), $valorGet)));
				} else {
					$_GET[$itemGet] = Utils::TratarArray($valorGet);
				}
			}
		}

		if (is_array($_REQUEST) && count($_REQUEST) > 0) {
			foreach ($_REQUEST AS $itemRequest => $valorRequest) {
				if (!is_array($valorRequest)) {
					$_REQUEST[$itemRequest] = trim(addslashes(str_ireplace($toEscape, chr(92), $valorRequest)));
				} else {
					$_REQUEST[$itemRequest] = Utils::TratarArray($valorRequest);
				}
			}
		}
	}

	/**
	 * Trata arrays nos requests, recursivamente
	 *
	 * @autor Squall Rober (Francisco José)
	 * @param array $array Array a ser tratado
	 *
	 * @return array $array
	 * */
	static public function TratarArray($array)
	{
		$toEscape = Array("SELECT ", "INSERT ", "DELETE ", "UPDATE ", "DROP ", " FROM ", "UNION", " DATABASE ", " TABLE ", " LIKE");

		if (is_array($array)) {
			foreach ($array AS $chave => $valor) {
				if (!is_array($valor)) {
					$array[$chave] = trim(addslashes(str_ireplace($toEscape, chr(92), $valor)));
				} else {
					$array[$chave] = self::TratarArray($valor);
				}
			}
		}
		return $array;
	}

	/**
	 * Envio de email padrão
	 *
	 * @parametros['conteudo'] -> conteúdo html a ser enviado
	 * @parametros['assunto'] -> Assunto do email
	 * @parametros['email'][] -> emails
	 * */
	static public function EnviarEmail($parametros)
	{
		$html =  "<html>";
		$html .= "\t<head>";
		$html .= "\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
		$html .= "\t\t<title>Link Monitoramento</title>";
		$html .= "\t</head>";
		$html .= "\t<body>";
		$html .= "\t\t<table width=\"560\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-top:-8px;\">";
		$html .= "\t\t\t<tr>";
		$html .= "\t\t\t\t<td height=\"150\" valign=\"top\"><img src=\"http://seguro.linkmonitoramento.com.br/app/news/img/topo.png\" width=\"560\" height=\"117\" /></td>";
		$html .= "\t\t\t</tr>";
		$html .= "\t\t\t<tr>";
		$html .= "\t\t\t\t<td>";
		$html .= "\t\t\t\t\t<p style=\"color:#555555;font:20px 'Arial',sans-serif;line-height:26px;\">{$parametros['conteudo']}</p>";
		$html .= "\t\t\t\t</td>";
		$html .= "\t\t\t</tr>";
		$html .= "\t\t\t<tr>";
		$html .= "\t\t\t\t<td align=\"center\" height=\"80\" style=\"color:#555555;font:12px 'Arial',sans-serif;border-top:1px dotted #9c9c9c;\">Copyright 2015. Link Monitoramento. Todos os direitos reservados.</td>";
		$html .= "\t\t\t</tr>";
		$html .= "\t\t</table>";
		$html .= "\t</body>";
		$html .= "</html>";

		//$dados['email']['gerencia'] = "francisco@linkmonitoramento.com.br";
		$enviar = new Email();
		$enviar->EnviarEmail($parametros['assunto'],$html,$parametros['email']);

	}
    static public function getAge($start_ts)
    {
        $end_ts = time();
        $anniversary = date('z', $start_ts);
        $days_in_year = date('L', $start_ts) ? 366 : 365;
        $sec_per_day = 86400; // 60 * 60 * 24
        $diff = $end_ts - $start_ts;
        $diff -= $diff % $sec_per_day; // discard misc seconds
        $days = $diff / $sec_per_day; // number of days to iterate through;

        if( $diff < 0 )
            return false; // $start_ts is in the future!

        if( $days < $days_in_year ) // less than 1 year old; don't need to loop
            return 0;

        $years = 0;

        do {
            // add the remaining days left in the current year first.
            $left_in_year = $days_in_year - $anniversary;
            if( $days < $left_in_year )
                $left_in_year = $days;

            $start_ts += $sec_per_day * $left_in_year;
            $days -= $left_in_year;
            // are there enough days remaining to make it to the next birthday?
            if( $days > $anniversary )
            {
                $years++; // increment years;
                $to_anniv = $anniversary;
            } else {
                $to_anniv = $days;
            }

            // add the days remaining to either the next birthday, or today's date, whichever
            // is closer.
            $start_ts += $sec_per_day * $to_anniv;
            $days -= $to_anniv;
            // reevaluate $days_in_year so that leap year is taken into account.
            $days_in_year = date('L', $start_ts) ? 366 : 365;
        } while( $days > 0 );

        // return the result.
        return $years;
    }

    static public function AcrescetarData($tipo,$data,$quant)
    {
        $d = explode("-",$data);
        if($tipo == "mes")
            return date('Y-m-d',mktime(0,0,0,$d[1]+$quant,$d[2],$d[0]));
        elseif($tipo == "dia")
            return date('Y-m-d',mktime(0,0,0,$d[1],$d[2]+$quant,$d[0]));
        elseif($tipo == "ano")
            return date('Y-m-d',mktime(0,0,0,$d[1],$d[2],$d[0]+$quant));
        else
            return $data;
    }
    static public function AcrescetarDataPHP($tipo,$data,$quant)
    {
        $d = explode("/",$data);
        if($tipo == "mes")
            return date('d/m/Y',mktime(0,0,0,$d[1]+$quant,$d[0],$d[2]));
        elseif($tipo == "dia")
            return date('d/m/Y',mktime(0,0,0,$d[1],$d[0]+$quant,$d[2]));
        elseif($tipo == "ano")
            return date('d/m/Y',mktime(0,0,0,$d[1],$d[0],$d[2]+$quant));
        else
            return $data;
    }
    static public function DiminuirDataPHP($tipo,$data,$quant)
    {
        $d = explode("/",$data);
        if($tipo == "mes")
            return date('d/m/Y',mktime(0,0,0,($d[1]-$quant),$d[0],$d[2]));
        elseif($tipo == "dia")
            return date('d/m/Y',mktime(0,0,0,$d[1],($d[0]-$quant),$d[2]));
        elseif($tipo == "ano")
            return date('d/m/Y',mktime(0,0,0,$d[1],$d[0],($d[2]-$quant)));
        else
            return $data;
    }
    static public function DiminuirDataDB($tipo,$data,$quant)
    {
        $d = explode("/",$data);
        if($tipo == "mes")
            return date('Y-m-d',mktime(0,0,0,($d[1]-$quant),$d[2],$d[0]));
        elseif($tipo == "dia")
            return date('Y-m-d',mktime(0,0,0,$d[1],($d[2]-$quant),$d[0]));
        elseif($tipo == "ano")
            return date('Y-m-d',mktime(0,0,0,$d[1],$d[2],($d[0]-$quant)));
        else
            return $data;
    }
    static public function TipoExames($arg)
    {
        $tiposEvento = array(
            "A" => "ADMISSIONAL",
            "D" => "DEMISSIONAL",
            "P" => "PERIÓDICO",
            "R" => "RETORNO TRAB.",
            "M" => "MUDANÇA FUNC."
        );
        return $tiposEvento[$arg];
    }
    static public  function PegarIdade($data)
    {
        if($data == "") return;

        $date = new DateTime( $data ); // data e hora de nascimento
        $interval = $date->diff( new DateTime( ) ); // data e hora atual

        return $interval->format( '%Y ' );

    }
    /* TRANFORMA <BR> EM \N
    */
    function br2nl($string)
    {
        return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
    }
    static public function DiaSemanaNome($data)
    {
        // data deve esta no padrão pt-br
        // Traz o dia da semana para qualquer data informada
        $dia =  substr($data,0,2);
        $mes =  substr($data,3,2);
        $ano =  substr($data,6,9);
        $diasemana = date("w", mktime(0,0,0,$mes,$dia,$ano) );
        switch($diasemana)
        {
            case"0":
                $diasemana = "Domingo";
                break;
            case"1":
                $diasemana = "Segunda-Feira";
                break;
            case"2":
                $diasemana = "Terça-Feira";
                break;
            case"3":
                $diasemana = "Quarta-Feira";
                break;
            case"4":
                $diasemana = "Quinta-Feira";
                break;
            case"5":
                $diasemana = "Sexta-Feira";
                break;
            case"6":
                $diasemana = "Sábado";
                break;
        }
        return $diasemana;
    }

}