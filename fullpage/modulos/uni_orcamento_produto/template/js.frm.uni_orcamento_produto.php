<script type="text/javascript">
$(function()
{
});

/*
 * Executa o post do formulário
 * */
function ExecutarUniOrcamentoProduto(dialog, url)
{
	if (ValidateForm($("#frm_uni_orcamento_produto"))) {
		// ao clicar em salvar enviando dados por post via AJAX
		$.post(url,
			$("#frm_uni_orcamento_produto").serialize(),
			// pegando resposta do retorno do post
			function (response)
			{
				if (response["codigo"] == 0) {
					dialog.close();
					toastr.success(response["mensagem"], "<?=ROTULO_MENSAGEM?>");
					AtualizarGridUniOrcamentoProduto(0, "");
				} else {
					toastr.warning(response["mensagem"], "<?=ROTULO_MENSAGEM?>");
				}
			}
			, "json" // definindo retorno para o formato json
		);
	}
}
</script>
