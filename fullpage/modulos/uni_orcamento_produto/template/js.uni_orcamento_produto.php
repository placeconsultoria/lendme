<?php
/**
* @author Fernando Carmo
* @copyright 2016
*/
?>
<script type="text/javascript">
	$(function()
	{
		LoadDiv("#conteudo_uni_orcamento_produto");;
		AtualizarGridUniOrcamentoProduto(0,"");
	});

	function AtualizarGridUniOrcamentoProduto(pagina,busca,filtro,ordem)
	{
		if(filtro == "" || filtro === undefined)  filtro = ""; 
		if(ordem == "" || ordem  === undefined)  ordem = "";

		var toPost = { 
			pagina: pagina,
			busca: busca,
			filtro: filtro,
			ordem: ordem
		};

		$("#conteudo_uni_orcamento_produto").load("index_xml.php?app_modulo=uni_orcamento_produto&app_comando=ajax_listar_uni_orcamento_produto", toPost);
	}

	function ImprimirRelatorio(form)
	{
		if (ValidarFormulario()) {
			form.action = "index_print.php?app_modulo=uni_orcamento_produto&app_comando=uni_orcamento_produto_print";
			form.target = "_blank";
			form.submit();
		}
	}

	function GerarPdf(form)
	{
		if (ValidarFormulario()) {
			form.action = "index_file.php?app_modulo=uni_orcamento_produto&app_comando=uni_orcamento_produto_pdf";
			form.target = "_blank";
			form.submit();
		}
	}

	function GerarXml(form)
	{
		if (ValidarFormulario()) {
			form.action = "index_file.php?app_modulo=uni_orcamento_produto&app_comando=uni_orcamento_produto_xlsx";
			form.target = "_blank";
			form.submit();
		}
	}

	function AbrirConfig()
	{
		BootstrapDialog.show({
			size:      BootstrapDialog.SIZE_SMALL,
			type:      BootstrapDialog.TYPE_DEFAULT,
			title:     "<div class='titulo_modal'><?=ROTULO_CONFIGURACOES?></div>",
			message:   $("<div></div>").load("index_xml.php?app_modulo=uni_orcamento_produto&app_comando=frm_configurar_listagem"),
			draggable: true,
			buttons:   [{
				label:    "<?=ROTULO_SALVAR?>",
				cssClass: "btn-lg btn-confirm",
				action:   function (dialogRef)
						{
							SalvarConfiguracoes(dialogRef, "index_xml.php?app_modulo=uni_orcamento_produto&app_comando=configurar_listagem", AtualizarGridUniOrcamentoProduto)
						}
			}]
		});
	}
</script>
