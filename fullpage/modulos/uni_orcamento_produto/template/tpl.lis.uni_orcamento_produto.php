<?php

define('URL_FILE',"../../admin/minisidebar/");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once("../../../../admin/minisidebar/classes/Conexao.php");
include_once("../../../modulos/uni_orcamento_produto/classe.uni_orcamento_produto.php");

$objOrcamentoProdutos = new UniOrcamentoProduto();
$objOrcamentoProdutos->setIdOrcamento(1);
$resulProdutosAdd = $objOrcamentoProdutos->ListarFront();

?>

<div class="pi-col-xs-12">
<div class="pi-responsive-table-sm">
    <table class="pi-table pi-table-complex pi-table-zebra pi-table-hovered pi-round pi-table-shadow pi-table-all-borders">

        <!-- Table head -->
        <thead>

        <!-- Table row -->
        <tr>
            <th class="pi-text-left">
                PRODUTO CONTROLADO:
            </th>
            <th class="pi-text-left">
                CÓDIGO NCM:
            </th>
            <th class="pi-text-left">
                CATEGORIA CONTROLE:
            </th>
            <th width="10%"  class="pi-text-left">
                QTD:
            </th>
            <th width="5%"  class="pi-text-left">
                UN:
            </th>
        </tr>

        </thead>

        <tbody>

        <?php
            if(count($resulProdutosAdd) > 0){
                foreach ($resulProdutosAdd as $linha){

                    if($linha["unidade"] == 1){
                        $unidade = "UN";
                    }else  if($linha["unidade"] == 2){
                        $unidade = "L";
                    }else  if($linha["unidade"] == 3){
                        $unidade = "KG";
                    }

                    echo '
                         <tr>
                            <td>
                            '.$linha["nome"].'
                            </td>
                            <td>
                             '.$linha["cod_ncm"].'
                            </td>
                            <td>
                             '.$linha["cat_controle"].'
                            </td>
                            <td>
                             '.$linha["quantidade"].'
                            </td>
                            <td>
                                '.$unidade.'
                            </td>
                        </tr>
                    ';

                }
            }
        ?>





        </tbody>
        <!-- End table body -->

    </table>
</div>
</div>

