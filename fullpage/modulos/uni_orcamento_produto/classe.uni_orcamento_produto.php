<?
class UniOrcamentoProduto
{
	private $id;
	private $id_orcamento;
	private $id_produto;
	private $quantidade;
	private $unidade;
	private $data_hora;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdOrcamento($arg)
	{
		$this->id_orcamento = $arg;
	}
 	
	public function getIdOrcamento()
	{
		return $this->id_orcamento;
	}
 	
	public function setIdProduto($arg)
	{
		$this->id_produto = $arg;
	}
 	
	public function getIdProduto()
	{
		return $this->id_produto;
	}
 	
	public function setQuantidade($arg)
	{
		$this->quantidade = $arg;
	}
 	
	public function getQuantidade()
	{
		return $this->quantidade;
	}
 	
	public function setUnidade($arg)
	{
		$this->unidade = $arg;
	}
 	
	public function getUnidade()
	{
		return $this->unidade;
	}
 	
	public function setDataHora($arg)
	{
		$this->data_hora = $arg;
	}
 	
	public function getDataHora()
	{
		return $this->data_hora;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO uni_orcamento_produto SET data_hora = now() ';
		if ($this->getIdOrcamento() != "") $sql .= ",id_orcamento = ?";
		if ($this->getIdProduto() != "") $sql .= ",id_produto = ?";
		if ($this->getQuantidade() != "") $sql .= ",quantidade = ?";
		if ($this->getUnidade() != "") $sql .= ",unidade = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdOrcamento() != "") $stmt->bindParam(++$x,$this->getIdOrcamento(),PDO::PARAM_INT);
		if ($this->getIdProduto() != "") $stmt->bindParam(++$x,$this->getIdProduto(),PDO::PARAM_INT);
		if ($this->getQuantidade() != "") $stmt->bindParam(++$x,$this->getQuantidade(),PDO::PARAM_STR);
		if ($this->getUnidade() != "") $stmt->bindParam(++$x,$this->getUnidade(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = '
		UPDATE uni_orcamento_produto SET 
			id = ?';
		if ($this->getIdOrcamento() != "") $sql .= ",id_orcamento = ?";
		if ($this->getIdProduto() != "") $sql .= ",id_produto = ?";
		if ($this->getQuantidade() != "") $sql .= ",quantidade = ?";
		if ($this->getUnidade() != "") $sql .= ",unidade = ?";
		if ($this->getDataHora() != "") $sql .= ",data_hora = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdOrcamento() != "") $stmt->bindParam(++$x,$this->getIdOrcamento(),PDO::PARAM_INT);
		if ($this->getIdProduto() != "") $stmt->bindParam(++$x,$this->getIdProduto(),PDO::PARAM_INT);
		if ($this->getQuantidade() != "") $stmt->bindParam(++$x,$this->getQuantidade(),PDO::PARAM_STR);
		if ($this->getUnidade() != "") $stmt->bindParam(++$x,$this->getUnidade(),PDO::PARAM_INT);
		if ($this->getDataHora() != "") $stmt->bindParam(++$x,$this->getDataHora(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM uni_orcamento_produto WHERE id IN({$lista})";
		$sql = "UPDATE uni_orcamento_produto SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE uni_orcamento_produto.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM uni_orcamento_produto
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				uni_orcamento_produto.*
			FROM uni_orcamento_produto
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY uni_orcamento_produto.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM uni_orcamento_produto WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}


    public function ListarFront()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT 
                  uni_orcamento_produto.*,
                  produtos_controlados.cod_ncm, produtos_controlados.nome, produtos_controlados.cat_controle
                FROM uni_orcamento_produto 
                INNER JOIN produtos_controlados ON (produtos_controlados.id = uni_orcamento_produto.id_produto )
                WHERE uni_orcamento_produto.id_orcamento = ? ORDER BY produtos_controlados.nome ";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdOrcamento(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
