<?

define('URL_FILE',"../../admin/minisidebar/");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once("../../../admin/minisidebar/classes/Conexao.php");
include_once("../../modulos/uni_orcamento_produto/classe.uni_orcamento_produto.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{

    case "adicionar_uni_orcamento_produto":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objUniOrcamentoProduto = new UniOrcamentoProduto($pdo);
			$objUniOrcamentoProduto->setIdOrcamento($_REQUEST['id_orcamento']);
			$objUniOrcamentoProduto->setIdProduto($_REQUEST['id_produto']);
			$objUniOrcamentoProduto->setQuantidade($_REQUEST['quantidade']);
			$objUniOrcamentoProduto->setUnidade($_REQUEST['unidade']);
			$novoId = $objUniOrcamentoProduto->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "ok";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "erro";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.uni_orcamento_produto.php";
		break;


}
