<?

define('URL_FILE',"../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/pre_orcamento/classe.pre_orcamento.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{

	case "add_pre_cadastro":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPreOrcamento = new PreOrcamento($pdo);
			$objPreOrcamento->setCliente($_REQUEST['cliente']);
			$objPreOrcamento->setTipoCadastro(1);
			$objPreOrcamento->setUrlName($objPreOrcamento->generate_friendly_url($_REQUEST["cliente"]));
			$novoId = $objPreOrcamento->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Pré orçamento cadastrado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.pre_orcamento.php";
		break;


	case "edit_pre_cadastro":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPreOrcamento = new PreOrcamento($pdo);
			$objPreOrcamento->setId($_REQUEST['id']);
			$objPreOrcamento->setCliente($_REQUEST['cliente']);
            $objPreOrcamento->setUrlName($objPreOrcamento->generate_friendly_url($_REQUEST["cliente"]));

            $objPreOrcamento->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "Pré orçamento atualizado com sucesso!";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro!";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.pre_orcamento.php";
		break;



	case "deltar_pre_orcamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPreOrcamento = new PreOrcamento($pdo);
			$objPreOrcamento->setId($_REQUEST["id"]);
			$objPreOrcamento->Remover();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.pre_orcamento.php";
		break;

}
