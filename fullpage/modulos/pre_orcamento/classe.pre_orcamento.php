<?
class PreOrcamento
{
	private $id;
	private $data_criacao;
	private $data_interacao_cliente;
	private $cliente;
	private $status;
	private $id_cliente_cadastro;
	private $tipo_cadastro;
	private $url_name;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setDataCriacao($arg)
	{
		$this->data_criacao = $arg;
	}
 	
	public function getDataCriacao()
	{
		return $this->data_criacao;
	}
 	
	public function setDataInteracaoCliente($arg)
	{
		$this->data_interacao_cliente = $arg;
	}
 	
	public function getDataInteracaoCliente()
	{
		return $this->data_interacao_cliente;
	}
 	
	public function setCliente($arg)
	{
		$this->cliente = $arg;
	}
 	
	public function getCliente()
	{
		return $this->cliente;
	}
 	
	public function setStatus($arg)
	{
		$this->status = $arg;
	}
 	
	public function getStatus()
	{
		return $this->status;
	}
 	
	public function setIdClienteCadastro($arg)
	{
		$this->id_cliente_cadastro = $arg;
	}
 	
	public function getIdClienteCadastro()
	{
		return $this->id_cliente_cadastro;
	}
 	
	public function setTipoCadastro($arg)
	{
		$this->tipo_cadastro = $arg;
	}
 	
	public function getTipoCadastro()
	{
		return $this->tipo_cadastro;
	}

	//

    public function setUrlName($arg)
    {
        $this->url_name = $arg;
    }

    public function getUrlName()
    {
        return $this->url_name;
    }
    //
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}


	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE pre_orcamento SET id = ?';
		if ($this->getDataInteracaoCliente() != "") $sql .= ",data_interacao_cliente = ?";
		if ($this->getCliente() != "") $sql .= ",cliente = ?";
		if ($this->getStatus() != "") $sql .= ",status = ?";
		if ($this->getIdClienteCadastro() != "") $sql .= ",id_cliente_cadastro = ?";
		if ($this->getTipoCadastro() != "") $sql .= ",tipo_cadastro = ?";
        if ($this->getUrlName() != "") $sql .= ",url_name = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getDataInteracaoCliente() != "") $stmt->bindParam(++$x,$this->getDataInteracaoCliente(),PDO::PARAM_STR);
		if ($this->getCliente() != "") $stmt->bindParam(++$x,$this->getCliente(),PDO::PARAM_STR);
		if ($this->getStatus() != "") $stmt->bindParam(++$x,$this->getStatus(),PDO::PARAM_INT);
		if ($this->getIdClienteCadastro() != "") $stmt->bindParam(++$x,$this->getIdClienteCadastro(),PDO::PARAM_INT);
		if ($this->getTipoCadastro() != "") $stmt->bindParam(++$x,$this->getTipoCadastro(),PDO::PARAM_INT);
        if ($this->getUrlName() != "") $stmt->bindParam(++$x,$this->getUrlName(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);

		return $stmt->execute();
	}




	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM pre_orcamento WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function getIdUrLName()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT * FROM pre_orcamento WHERE url_name = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getUrlName(),PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function AlteraVerificao()
    {
        $pdo = $this->getConexao();
        $sql = 'UPDATE pre_orcamento SET data_interacao_cliente = NOW(), status = "1" ';
        $sql .= ' WHERE id = ?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);

        return $stmt->execute();
    }


}
