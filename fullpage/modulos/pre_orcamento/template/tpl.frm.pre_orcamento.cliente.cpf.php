
<?php

@session_start();
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
define('URL_FILE',"../../../../admin/minisidebar/");
//require_once('inc/inc.sessao.php');
//equire_once('inc/funcoes.php');
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");


$objEstados = NEW Estados();
$resulComboEstados = $objEstados->GerarSelectEstados();


?>

<input type="hidden" name="tipo" value="2" />

    <div class="pi-row pi-grid-small-margins">

        <div class="pi-col-sm-3">
            <div class="form-group">
                <label for="nome">NOME COMPLETO:</label>
                <input name="nome" required type="text" class="form-control" id="nome" placeholder="">
            </div>
        </div>

        <div class="pi-col-sm-3">
            <div class="form-group">
                <label for="email">E-MAIL:</label>
                <input name="email" required type="text" class="form-control" id="email" placeholder="">
            </div>
        </div>
        <div class="pi-col-sm-2">
            <div class="form-group">
                <label for="telefone">TELEFONE:</label>
                <input name="telefone"  required type="text" class="form-control" id="telefone2" placeholder="">
            </div>
        </div>


        <div class="pi-col-sm-2">
            <div class="form-group">
                <label for="cpfcnpj">CPF:</label>
                <input name="cpfcnpj"  required type="text" class="form-control" id="cpfcnpj" onkeyup="mascaraTexto(event,'999.999.999-99')" maxlength="14"  >

            </div>
        </div>

        <div class="pi-col-sm-2">
            <div class="form-group">
                <label for="id_estado">ESTADO DA LICENÇA:</label>
                <select name="id_estado" id="id_estado" class="form-control">
                    <?php foreach ($resulComboEstados as $linha){ echo '<option value="'.$linha["id_estado"].'">'.$linha["sigla"].'</option>'; } ?>
                </select>
            </div>
        </div>

    </div>





<script>
    function mascaraTexto(evento, mascara){
        var campo, valor, i, tam, caracter;

        if (document.all) // Internet Explorer
            campo = evento.srcElement;
        else
            campo= evento.target;
        valor = campo.value;
        tam = valor.length;
        for(i=0;i<mascara.length;i++){
            caracter = mascara.charAt(i);
            if(caracter!="9")
                if(i<tam & caracter!=valor.charAt(i))
                    campo.value = valor.substring(0,i) + caracter + valor.substring(i,tam);

        }
    }

    $("#telefone, #telefone2").keydown(function () {
        //Recebe o elemento ativo
        var focus = $(document.activeElement);
        //Timeout para pegar o valor do campo depois do evento, sem ele, o valor é testado antes do evento ser finalizado
        setTimeout(function () {
            //Se o campo focado é algum dos 3 campos de telefone, aplica a máscara de acordo
            if (focus.attr('id') == "telefone" || focus.attr('id') == "telefone2") {
                if (focus.val().length <= 14) {
                    focus.unmask();
                    focus.mask("(00) 0000-00009");
                }
                else {
                    focus.unmask();
                    focus.mask("(00) 00000-0000");
                }
            }
        }, 10);
    });
</script>


