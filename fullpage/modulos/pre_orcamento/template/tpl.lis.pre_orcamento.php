<?php
define('URL_FILE',"../../../");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once(URL_FILE . "classes/Conexao.php");
include_once(URL_FILE . "modulos/pre_orcamento/classe.pre_orcamento.php");

$objPreCadastro = new PreOrcamento($pdo);
$listar = $objPreCadastro->Listar();
$url_edit = "edit_pre_cadastro_orcamento.php?id=";

?>


<div class="col-md-12">
    <div class="table-responsive">
        <table  class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>DATA CADASTRO:</th>
                <th>CLIENTE:</th>
                <th>STATUS:</th>
                <th>CÓDIGO ACESSO:</th>
                <th width="20%">AÇÕES</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $x = 0;
            if(count($listar)> 0){
                foreach($listar as $linha){
                    echo '
                             <tr>
                                <td>'.Conexao::PrepararDataPHP($linha["data_criacao"]).'</td>
                                 <td>'.$linha["cliente"].'</td>
                                 <td><h3><span class="badge badge-warning">AGUARDANDO</span> </h3></td>
                                 <td>'.$linha["url_name"].'</td>
                                <td>
                                   <a href="'.$url_edit.$linha["id"].'" class="btn btn-warning"> 
                                   GERENCIAR <i class="fa fa-pencil"></i>
                                   </a> 
                                </td>
                            </tr>
                        ';
                }
            }

            ?>
            </tbody>
        </table>

    </div>
</div>


