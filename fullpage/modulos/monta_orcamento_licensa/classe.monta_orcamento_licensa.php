<?
class MontaOrcamentoLicensa
{
	private $id;
	private $id_monta_orcamento;
	private $id_departamento;
	private $id_certificacao;
	private $id_processo;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdMontaOrcamento($arg)
	{
		$this->id_monta_orcamento = $arg;
	}
 	
	public function getIdMontaOrcamento()
	{
		return $this->id_monta_orcamento;
	}
 	
	public function setIdDepartamento($arg)
	{
		$this->id_departamento = $arg;
	}
 	
	public function getIdDepartamento()
	{
		return $this->id_departamento;
	}
 	
	public function setIdCertificacao($arg)
	{
		$this->id_certificacao = $arg;
	}
 	
	public function getIdCertificacao()
	{
		return $this->id_certificacao;
	}
 	
	public function setIdProcesso($arg)
	{
		$this->id_processo = $arg;
	}
 	
	public function getIdProcesso()
	{
		return $this->id_processo;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO monta_orcamento_licensa SET ';
		if ($this->getIdMontaOrcamento() != "") $sql .= "id_monta_orcamento = ?";
		if ($this->getIdDepartamento() != "") $sql .= ",id_departamento = ?";
		if ($this->getIdCertificacao() != "") $sql .= ",id_certificacao = ?";
		if ($this->getIdProcesso() != "") $sql .= ",id_processo = ? ";
        $stmt = $pdo->prepare($sql);
		if ($this->getIdMontaOrcamento() != "") $stmt->bindParam(++$x,$this->getIdMontaOrcamento(),PDO::PARAM_INT);
		if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		if ($this->getIdCertificacao() != "") $stmt->bindParam(++$x,$this->getIdCertificacao(),PDO::PARAM_INT);
		if ($this->getIdProcesso() != "") $stmt->bindParam(++$x,$this->getIdProcesso(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = '
		UPDATE monta_orcamento_licensa SET 
			id = ?';
		if ($this->getIdMontaOrcamento() != "") $sql .= ",id_monta_orcamento = ?";
		if ($this->getIdDepartamento() != "") $sql .= ",id_departamento = ?";
		if ($this->getIdCertificacao() != "") $sql .= ",id_certificacao = ?";
		if ($this->getIdProcesso() != "") $sql .= ",id_processo = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdMontaOrcamento() != "") $stmt->bindParam(++$x,$this->getIdMontaOrcamento(),PDO::PARAM_INT);
		if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		if ($this->getIdCertificacao() != "") $stmt->bindParam(++$x,$this->getIdCertificacao(),PDO::PARAM_INT);
		if ($this->getIdProcesso() != "") $stmt->bindParam(++$x,$this->getIdProcesso(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM monta_orcamento_licensa WHERE id IN({$lista})";
		$sql = "UPDATE monta_orcamento_licensa SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE monta_orcamento_licensa.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM monta_orcamento_licensa
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				monta_orcamento_licensa.*
			FROM monta_orcamento_licensa
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY monta_orcamento_licensa.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM monta_orcamento_licensa WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}


    public function GetLcensasOrcamento()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id_processo FROM monta_orcamento_licensa WHERE id_monta_orcamento = ? and id_departamento = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdMontaOrcamento(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getIdDepartamento(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function GetLcensasOrcamentoPDF($id_monta_orcamento, $id_departamento)
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id_processo FROM monta_orcamento_licensa WHERE id_monta_orcamento = '$id_monta_orcamento' and id_departamento = '$id_departamento'";
        $stmt = $pdo->prepare($sql);

        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
