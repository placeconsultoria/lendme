<?

switch($app_comando)
{
	case "frm_adicionar_monta_orcamento_licensa":
		$template = "tpl.frm.monta_orcamento_licensa.php";
		break;

	case "adicionar_monta_orcamento_licensa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamentoLicensa = new MontaOrcamentoLicensa($pdo);
			$objMontaOrcamentoLicensa->setIdMontaOrcamento($_REQUEST['id_monta_orcamento']);
			$objMontaOrcamentoLicensa->setIdDepartamento($_REQUEST['id_departamento']);
			$objMontaOrcamentoLicensa->setIdCertificacao($_REQUEST['id_certificacao']);
			$objMontaOrcamentoLicensa->setIdProcesso($_REQUEST['id_processo']);
			$novoId = $objMontaOrcamentoLicensa->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento_licensa.php";
		break;

	case "frm_atualizar_monta_orcamento_licensa" :
		$monta_orcamento_licensa = new MontaOrcamentoLicensa();
		$monta_orcamento_licensa->setId($_REQUEST["app_codigo"]);
		$linha = $monta_orcamento_licensa->Editar();
		$template = "tpl.frm.monta_orcamento_licensa.php";
		break;

	case "atualizar_monta_orcamento_licensa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamentoLicensa = new MontaOrcamentoLicensa($pdo);
			$objMontaOrcamentoLicensa->setId($_REQUEST['id']);
			$objMontaOrcamentoLicensa->setIdMontaOrcamento($_REQUEST['id_monta_orcamento']);
			$objMontaOrcamentoLicensa->setIdDepartamento($_REQUEST['id_departamento']);
			$objMontaOrcamentoLicensa->setIdCertificacao($_REQUEST['id_certificacao']);
			$objMontaOrcamentoLicensa->setIdProcesso($_REQUEST['id_processo']);
			$objMontaOrcamentoLicensa->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento_licensa.php";
		break;

	case "listar_monta_orcamento_licensa":
		$template = "tpl.geral.monta_orcamento_licensa.php";
		break;

	case "deletar_monta_orcamento_licensa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamentoLicensa = new MontaOrcamentoLicensa($pdo);
			$objMontaOrcamentoLicensa->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento_licensa.php";
		break;

	case "ajax_listar_monta_orcamento_licensa":
		$template = "tpl.lis.monta_orcamento_licensa.php";
		break;

	case "monta_orcamento_licensa_pdf":
		$template = "tpl.lis.monta_orcamento_licensa.pdf.php";
		break;

	case "monta_orcamento_licensa_xlsx":
		$template = "tpl.lis.monta_orcamento_licensa.xlsx.php";
		break;

	case "monta_orcamento_licensa_print":
		$template = "tpl.lis.monta_orcamento_licensa.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["monta_orcamento_licensa"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("monta_orcamento_licensa");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento_licensa.php";
		break;
}
