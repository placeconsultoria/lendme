<?
class MontaOrcamentoAtividades
{
	private $id;
	private $id_monta_orcamento;
	private $id_departamento;
	private $id_atividade;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdMontaOrcamento($arg)
	{
		$this->id_monta_orcamento = $arg;
	}
 	
	public function getIdMontaOrcamento()
	{
		return $this->id_monta_orcamento;
	}
 	
	public function setIdDepartamento($arg)
	{
		$this->id_departamento = $arg;
	}
 	
	public function getIdDepartamento()
	{
		return $this->id_departamento;
	}
 	
	public function setIdAtividade($arg)
	{
		$this->id_atividade = $arg;
	}
 	
	public function getIdAtividade()
	{
		return $this->id_atividade;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO monta_orcamento_atividades SET ';
		if ($this->getIdMontaOrcamento() != "") $sql .= "id_monta_orcamento = ?";
		if ($this->getIdDepartamento() != "") $sql .= ",id_departamento = ?";
		if ($this->getIdAtividade() != "") $sql .= ",id_atividade = ?";
        $stmt = $pdo->prepare($sql);
		if ($this->getIdMontaOrcamento() != "") $stmt->bindParam(++$x,$this->getIdMontaOrcamento(),PDO::PARAM_INT);
		if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		if ($this->getIdAtividade() != "") $stmt->bindParam(++$x,$this->getIdAtividade(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE monta_orcamento_atividades SET id = ?';
		if ($this->getIdMontaOrcamento() != "") $sql .= ",id_monta_orcamento = ?";
		if ($this->getIdDepartamento() != "") $sql .= ",id_departamento = ?";
		if ($this->getIdAtividade() != "") $sql .= ",id_atividade = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdMontaOrcamento() != "") $stmt->bindParam(++$x,$this->getIdMontaOrcamento(),PDO::PARAM_INT);
		if ($this->getIdDepartamento() != "") $stmt->bindParam(++$x,$this->getIdDepartamento(),PDO::PARAM_INT);
		if ($this->getIdAtividade() != "") $stmt->bindParam(++$x,$this->getIdAtividade(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM monta_orcamento_atividades WHERE id IN({$lista})";
		$sql = "UPDATE monta_orcamento_atividades SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE monta_orcamento_atividades.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM monta_orcamento_atividades
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				monta_orcamento_atividades.*
			FROM monta_orcamento_atividades
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY monta_orcamento_atividades.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM monta_orcamento_atividades WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function GetLicenasDepartamentosOrcamento()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id_atividade FROM monta_orcamento_atividades WHERE id_departamento = ? AND id_monta_orcamento = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdDepartamento(),PDO::PARAM_INT);
        $stmt->bindParam(2,$this->getIdMontaOrcamento(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }



    public function GetLicenasDepartamentosOrcamentoPDF($idDepartamento, $idMontaOrcamento)
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id_atividade FROM monta_orcamento_atividades WHERE id_departamento = '$idDepartamento' AND id_monta_orcamento = '$idMontaOrcamento'";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
