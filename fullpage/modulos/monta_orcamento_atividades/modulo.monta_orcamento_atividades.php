<?

define('URL_FILE',"../../admin/minisidebar/");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once("../../../admin/minisidebar/classes/Conexao.php");
include_once("../../modulos/monta_orcamento_atividades/classe.monta_orcamento_atividades.php");



switch($app_comando)
{


	case "adicionar_monta_orcamento_atividades":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamentoAtividades = new MontaOrcamentoAtividades($pdo);
			$objMontaOrcamentoAtividades->setIdMontaOrcamento($_REQUEST['id_monta_orcamento']);
			$objMontaOrcamentoAtividades->setIdDepartamento($_REQUEST['id_departamento']);
			$objMontaOrcamentoAtividades->setIdAtividade($_REQUEST['id_atividade']);
			$novoId = $objMontaOrcamentoAtividades->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento_atividades.php";
		break;

	case "frm_atualizar_monta_orcamento_atividades" :
		$monta_orcamento_atividades = new MontaOrcamentoAtividades();
		$monta_orcamento_atividades->setId($_REQUEST["app_codigo"]);
		$linha = $monta_orcamento_atividades->Editar();
		$template = "tpl.frm.monta_orcamento_atividades.php";
		break;

	case "atualizar_monta_orcamento_atividades":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamentoAtividades = new MontaOrcamentoAtividades($pdo);
			$objMontaOrcamentoAtividades->setId($_REQUEST['id']);
			$objMontaOrcamentoAtividades->setIdMontaOrcamento($_REQUEST['id_monta_orcamento']);
			$objMontaOrcamentoAtividades->setIdDepartamento($_REQUEST['id_departamento']);
			$objMontaOrcamentoAtividades->setIdAtividade($_REQUEST['id_atividade']);
			$objMontaOrcamentoAtividades->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento_atividades.php";
		break;

	case "listar_monta_orcamento_atividades":
		$template = "tpl.geral.monta_orcamento_atividades.php";
		break;

	case "deletar_monta_orcamento_atividades":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamentoAtividades = new MontaOrcamentoAtividades($pdo);
			$objMontaOrcamentoAtividades->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento_atividades.php";
		break;


	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["monta_orcamento_atividades"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("monta_orcamento_atividades");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento_atividades.php";
		break;
}
