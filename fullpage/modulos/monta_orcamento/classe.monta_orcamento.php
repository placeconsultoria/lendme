<?
class MontaOrcamento
{
	private $id;
	private $id_pre_cadastro;
	private $data_hora;
	private $status;
	private $valor_eb;
	private $valor_pf;
	private $valor_civil;
	private $desconto;
	private $token;
	private $conexao;

    public function setToken($arg)
    {
        $this->token = $arg;
    }

    public function getToken()
    {
        return $this->token;
    }

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdPreCadastro($arg)
	{
		$this->id_pre_cadastro = $arg;
	}
 	
	public function getIdPreCadastro()
	{
		return $this->id_pre_cadastro;
	}
 	
	public function setDataHora($arg)
	{
		$this->data_hora = $arg;
	}
 	
	public function getDataHora()
	{
		return $this->data_hora;
	}
 	
	public function setStatus($arg)
	{
		$this->status = $arg;
	}
 	
	public function getStatus()
	{
		return $this->status;
	}
 	
	public function setValorEb($arg)
	{
		$this->valor_eb = $arg;
	}
 	
	public function getValorEb()
	{
		return $this->valor_eb;
	}
 	
	public function setValorPf($arg)
	{
		$this->valor_pf = $arg;
	}
 	
	public function getValorPf()
	{
		return $this->valor_pf;
	}
 	
	public function setValorCivil($arg)
	{
		$this->valor_civil = $arg;
	}
 	
	public function getValorCivil()
	{
		return $this->valor_civil;
	}
 	
	public function setDesconto($arg)
	{
		$this->desconto = $arg;
	}
 	
	public function getDesconto()
	{
		return $this->desconto;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO monta_orcamento SET data_hora = NOW() ';
		if ($this->getIdPreCadastro() != "") $sql .= ",id_pre_cadastro = ?";
		if ($this->getStatus() != "") $sql .= ",status = ?";
		if ($this->getValorEb() != "") $sql .= ",valor_eb = ?";
		if ($this->getValorPf() != "") $sql .= ",valor_pf = ?";
		if ($this->getValorCivil() != "") $sql .= ",valor_civil = ?";
		if ($this->getDesconto() != "") $sql .= ",desconto = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getIdPreCadastro() != "") $stmt->bindParam(++$x,$this->getIdPreCadastro(),PDO::PARAM_INT);
		if ($this->getStatus() != "") $stmt->bindParam(++$x,$this->getStatus(),PDO::PARAM_INT);
		if ($this->getValorEb() != "") $stmt->bindParam(++$x,$this->getValorEb(),PDO::PARAM_STR);
		if ($this->getValorPf() != "") $stmt->bindParam(++$x,$this->getValorPf(),PDO::PARAM_STR);
		if ($this->getValorCivil() != "") $stmt->bindParam(++$x,$this->getValorCivil(),PDO::PARAM_STR);
		if ($this->getDesconto() != "") $stmt->bindParam(++$x,$this->getDesconto(),PDO::PARAM_STR);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}

    public function ModificarToken()
    {
        $pdo = $this->getConexao();
        $sql = 'UPDATE monta_orcamento SET token = ?';
        $sql .= ' WHERE id = ?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(++$x,$this->getToken(),PDO::PARAM_STR);
        $stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
        return $stmt->execute();
    }

	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE monta_orcamento SET id = ?';
		if ($this->getIdPreCadastro() != "") $sql .= ",id_pre_cadastro = ?";
		if ($this->getDataHora() != "") $sql .= ",data_hora = ?";
		if ($this->getStatus() != "") $sql .= ",status = ?";
		if ($this->getValorEb() != "") $sql .= ",valor_eb = ?";
		if ($this->getValorPf() != "") $sql .= ",valor_pf = ?";
		if ($this->getValorCivil() != "") $sql .= ",valor_civil = ?";
		if ($this->getDesconto() != "") $sql .= ",desconto = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdPreCadastro() != "") $stmt->bindParam(++$x,$this->getIdPreCadastro(),PDO::PARAM_INT);
		if ($this->getDataHora() != "") $stmt->bindParam(++$x,$this->getDataHora(),PDO::PARAM_STR);
		if ($this->getStatus() != "") $stmt->bindParam(++$x,$this->getStatus(),PDO::PARAM_INT);
		if ($this->getValorEb() != "") $stmt->bindParam(++$x,$this->getValorEb(),PDO::PARAM_STR);
		if ($this->getValorPf() != "") $stmt->bindParam(++$x,$this->getValorPf(),PDO::PARAM_STR);
		if ($this->getValorCivil() != "") $stmt->bindParam(++$x,$this->getValorCivil(),PDO::PARAM_STR);
		if ($this->getDesconto() != "") $stmt->bindParam(++$x,$this->getDesconto(),PDO::PARAM_STR);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}



	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM monta_orcamento WHERE id IN({$lista})";
		$sql = "UPDATE monta_orcamento SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}


    public function GetInfoTokenEmpresa()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT pre_orcamento_empresa.*, monta_orcamento.*, estados.nome as estado
                      FROM monta_orcamento 
                LEFT JOIN pre_orcamento_empresa ON (pre_orcamento_empresa.id = monta_orcamento.id_pre_cadastro) 
                LEFT JOIN estados ON (estados.id = pre_orcamento_empresa.id_estado)
                WHERE monta_orcamento.token = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getToken(),PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function getIdComToken()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id
                      FROM monta_orcamento 
                where token = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getToken(),PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch();
    }

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM monta_orcamento WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

	public function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
    {
// Caracteres de cada tipo
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        $retorno = '';
        $caracteres = '';
        $caracteres .= $lmin;
        if ($maiusculas) $caracteres .= $lmai;
        if ($numeros) $caracteres .= $num;
        if ($simbolos) $caracteres .= $simb;
        $len = strlen($caracteres);
        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand-1];
        }
        return $retorno;
    }


}
