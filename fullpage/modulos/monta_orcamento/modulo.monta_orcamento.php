<?

define('URL_FILE',"../../admin/minisidebar/");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once("../../admin/minisidebar/classes/Conexao.php");
include_once("../../modulos/monta_orcamento/classe.monta_orcamento.php");
include_once("../../modulos/monta_orcamento_atividades/classe.monta_orcamento_atividades.php");
include_once("../../modulos/monta_orcamento_licensa/classe.monta_orcamento_licensa.php");
include_once("../../modulos/monta_orcamentos_processo_civil/classe.monta_orcamentos_processo_civil.php");



//CLASSES PARA LISTAR E MONTAR OS FORMS//
include_once(URL_FILE . "modulos/produtos_controlados/classe.produtos_controlados.php");
include_once(URL_FILE . "modulos/estados/classe.estados.php");
include_once(URL_FILE . "modulos/atividades_pr_pf/classe.atividades_pr_pf.php");
include_once(URL_FILE . "modulos/config_valor/classe.config_valor.php");
include_once(URL_FILE . "modulos/processos_civil/classe.processos_civil.php");
include_once("../../modulos/pre_orcamento/classe.pre_orcamento.php");
include_once("../../modulos/pre_orcamento_empresa/classe.pre_orcamento_empresa.php");


$app_comando = $_REQUEST["acao"];

switch($app_comando)
{

    case "monta_orcamento":

        $pdo = new Conexao();
        $pdo->beginTransaction();
        try {

            //CRIANDO O ORÇAMENTO
            $objMontaOrcamento = new MontaOrcamento($pdo);
            $objMontaOrcamento->setIdPreCadastro($_REQUEST['id_pre_orcamento']);
            $objMontaOrcamento->setStatus(1);
            $id_novo_orcamento = $objMontaOrcamento->Adicionar();

            //ALTERANDO TOKEN
            $objMontaOrcamento->setId($id_novo_orcamento);
            $token = $objMontaOrcamento->geraSenha(8,true,true,false).$id_novo_orcamento;
            $objMontaOrcamento->setToken($token);
            $objMontaOrcamento->ModificarToken();


            //ATIVICADES EB
            $objAtividades = NEW AtividadesPrPf();
            $objAtividades->setIdDepartamento(1);
            $atividadesEB = $objAtividades->ListarFrontDepartamento();

            if(count($atividadesEB) > 0){
                foreach ($atividadesEB as $linha){
                    $checkbox = "atividade_eb_".$linha["id"];
                    if(isset($_REQUEST[$checkbox])){
                        $objOrcamentoAtividade = NEW MontaOrcamentoAtividades($pdo);
                        $objOrcamentoAtividade->setIdDepartamento(1);
                        $objOrcamentoAtividade->setIdAtividade($linha["id"]);
                        $objOrcamentoAtividade->setIdMontaOrcamento($id_novo_orcamento);
                        $objOrcamentoAtividade->Adicionar();
                    }
                }
            }


            //LICENÇAS EB - CR
            $objConfiValores = NEW ConfigValor();
            $objConfiValores->setIdDepartamento(1);
            $objConfiValores->setIdCertificacao(12);
            $licenca_EB_CR = $objConfiValores->ListarPorDepartamentoFront();

            if(count($licenca_EB_CR) > 0){
                foreach ($licenca_EB_CR as $linha){
                    $checkbox = "licenca_eb_cr_".$linha["id"];
                    if(isset($_REQUEST[$checkbox])){
                        $objLicensa = NEW MontaOrcamentoLicensa($pdo);
                        $objLicensa->setIdMontaOrcamento($id_novo_orcamento);
                        $objLicensa->setIdDepartamento(1);
                        $objLicensa->setIdCertificacao(12);
                        $objLicensa->setIdProcesso($linha["id"]);
                        $objLicensa->Adicionar();
                    }
                }
            }


            //LICENÇAS EB - TR
            $objConfiValores = NEW ConfigValor();
            $objConfiValores->setIdDepartamento(1);
            $objConfiValores->setIdCertificacao(13);
            $licenca_EB_TR = $objConfiValores->ListarPorDepartamentoFront();

            if(count($licenca_EB_TR) > 0){
                foreach ($licenca_EB_TR as $linha){
                    $checkbox = "licenca_eb_tr_".$linha["id"];
                    if(isset($_REQUEST[$checkbox])){
                        $objLicensa = NEW MontaOrcamentoLicensa($pdo);
                        $objLicensa->setIdMontaOrcamento($id_novo_orcamento);
                        $objLicensa->setIdDepartamento(1);
                        $objLicensa->setIdCertificacao(13);
                        $objLicensa->setIdProcesso($linha["id"]);
                        $objLicensa->Adicionar();
                    }
                }
            }



            //POLICIA FEDERAL CRC
            $objConfiValores = NEW ConfigValor();
            $objConfiValores->setIdDepartamento(2);
            $objConfiValores->setIdCertificacao(15);
            $reulConfigValoresPF1 = $objConfiValores->ListarPorDepartamentoFront();

            if(count($reulConfigValoresPF1) > 0){
                foreach ($reulConfigValoresPF1 as $linha){
                    $checkbox = "licenca_pf_crc_".$linha["id"];
                    if(isset($_REQUEST[$checkbox])){
                        $objLicensa = NEW MontaOrcamentoLicensa($pdo);
                        $objLicensa->setIdMontaOrcamento($id_novo_orcamento);
                        $objLicensa->setIdDepartamento(2);
                        $objLicensa->setIdCertificacao(15);
                        $objLicensa->setIdProcesso($linha["id"]);
                        $objLicensa->Adicionar();
                    }
                }
            }

            //POLICIA FEDERAL CLF
            $objConfiValores = NEW ConfigValor();
            $objConfiValores->setIdDepartamento(2);
            $objConfiValores->setIdCertificacao(23);
            $reulConfigValoresPF2 = $objConfiValores->ListarPorDepartamentoFront();
                if(count($reulConfigValoresPF2) > 0){
                foreach ($reulConfigValoresPF2 as $linha){
                    $checkbox = "licenca_pf_clf_".$linha["id"];
                    if(isset($_REQUEST[$checkbox])){
                        $objLicensa = NEW MontaOrcamentoLicensa($pdo);
                        $objLicensa->setIdMontaOrcamento($id_novo_orcamento);
                        $objLicensa->setIdDepartamento(2);
                        $objLicensa->setIdCertificacao(23);
                        $objLicensa->setIdProcesso($linha["id"]);
                        $objLicensa->Adicionar();
                    }
                }
            }

            //POLICIA FEDERAL ATIVIDADES
            $objAtividades = NEW AtividadesPrPf();
            $objAtividades->setIdDepartamento(2);
            $atividadesPF = $objAtividades->ListarFrontDepartamento();
            if(count($atividadesPF) > 0){
                foreach ($atividadesPF as $linha){
                    $checkbox = "atividade_pf_".$linha["id"];
                    if(isset($_REQUEST[$checkbox])){
                        $objOrcamentoAtividade = NEW MontaOrcamentoAtividades($pdo);
                        $objOrcamentoAtividade->setIdDepartamento(2);
                        $objOrcamentoAtividade->setIdAtividade($linha["id"]);
                        $objOrcamentoAtividade->setIdMontaOrcamento($id_novo_orcamento);
                        $objOrcamentoAtividade->Adicionar();
                    }
                }
            }


            //POLICIA CIVIL LICENÇAS/ALVARAS
            $objConfigValor = new ConfigValor();
            $objConfigValor->setIdDepartamento(3);
            $objConfigValor->setidEstado($_REQUEST["id_estado"]);
            $listarPC = $objConfigValor->ListarPorDepartamentoPoliciaCivil();
            if(count($listarPC) > 0){
                foreach ($listarPC as $linha){
                    $checkbox = "licenca_civil_".$linha["id"];
                    if(isset($_REQUEST[$checkbox])){
                        $objLicensa = NEW MontaOrcamentoLicensa($pdo);
                        $objLicensa->setIdMontaOrcamento($id_novo_orcamento);
                        $objLicensa->setIdDepartamento(3);
                        $objLicensa->setIdCertificacao($linha["id_certificacao"]);
                        $objLicensa->setIdProcesso($linha["id"]);
                        $objLicensa->Adicionar();
                    }
                }
            }

            //POLICIA CIVIL AITIVIDADES
            $objProcessosCivil = new ProcessosCivil();
            $resul_processos_civil = $objProcessosCivil->Listar();
            if(count($resul_processos_civil) > 0){
                foreach ($resul_processos_civil as $linha){
                    $checkbox = "tipo_processo_civil".$linha["id"];
                    if(isset($_REQUEST[$checkbox])){
                        $objProcessoCivilInput = NEW MontaOrcamentosProcessoCivil();
                        $objProcessoCivilInput->setIdMontaOrcamento($id_novo_orcamento);
                        $objProcessoCivilInput->setIdProcessoCivil($linha["id"]);
                        $objProcessoCivilInput->Adicionar();

                    }
                }
            }

            $msg["codigo"] = 0;
            $msg["token"] = $token;
            $pdo->commit();
        } catch (Exception $e) {
            $msg["codigo"] = 1;
            $msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
            $msg["debug"] = $e->getMessage();
            $pdo->rollBack();
        }
        echo json_encode($msg);


        break;

	case "adicionar_monta_orcamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamento = new MontaOrcamento($pdo);
			$objMontaOrcamento->setIdPreCadastro($_REQUEST['id_pre_cadastro']);
			$objMontaOrcamento->setDataHora($_REQUEST['data_hora']);
			$objMontaOrcamento->setStatus($_REQUEST['status']);
			$objMontaOrcamento->setValorEb($_REQUEST['valor_eb']);
			$objMontaOrcamento->setValorPf($_REQUEST['valor_pf']);
			$objMontaOrcamento->setValorCivil($_REQUEST['valor_civil']);
			$objMontaOrcamento->setDesconto($_REQUEST['desconto']);
			$novoId = $objMontaOrcamento->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento.php";
		break;



	case "atualizar_monta_orcamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamento = new MontaOrcamento($pdo);
			$objMontaOrcamento->setId($_REQUEST['id']);
			$objMontaOrcamento->setIdPreCadastro($_REQUEST['id_pre_cadastro']);
			$objMontaOrcamento->setDataHora($_REQUEST['data_hora']);
			$objMontaOrcamento->setStatus($_REQUEST['status']);
			$objMontaOrcamento->setValorEb($_REQUEST['valor_eb']);
			$objMontaOrcamento->setValorPf($_REQUEST['valor_pf']);
			$objMontaOrcamento->setValorCivil($_REQUEST['valor_civil']);
			$objMontaOrcamento->setDesconto($_REQUEST['desconto']);
			$objMontaOrcamento->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento.php";
		break;



	case "deletar_monta_orcamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamento = new MontaOrcamento($pdo);
			$objMontaOrcamento->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento.php";
		break;



	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["monta_orcamento"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("monta_orcamento");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamento.php";
		break;
}
