<?

define('URL_FILE',"../../admin/minisidebar/");
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once("../../../admin/minisidebar/classes/Conexao.php");
include_once("../../modulos/orcamento/classe.orcamento.php");
$app_comando = $_REQUEST["acao"];

switch($app_comando)
{


	case "adicionar_orcamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objOrcamento = new Orcamento($pdo);
			$objOrcamento->setToken($_REQUEST['token']);
			$objOrcamento->setDataHora($_REQUEST['data_hora']);
			$objOrcamento->setIdClienteOrcamento($_REQUEST['id_cliente_orcamento']);
			$novoId = $objOrcamento->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = "OK";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "ERRO";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.orcamento.php";
		break;

	case "frm_atualizar_orcamento" :
		$orcamento = new Orcamento();
		$orcamento->setId($_REQUEST["app_codigo"]);
		$linha = $orcamento->Editar();
		$template = "tpl.frm.orcamento.php";
		break;

	case "atualizar_orcamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objOrcamento = new Orcamento($pdo);
			$objOrcamento->setId($_REQUEST['id']);
			$objOrcamento->setToken($_REQUEST['token']);
			$objOrcamento->setDataHora($_REQUEST['data_hora']);
			$objOrcamento->setIdClienteOrcamento($_REQUEST['id_cliente_orcamento']);
			$objOrcamento->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.orcamento.php";
		break;

	case "listar_orcamento":
		$template = "tpl.geral.orcamento.php";
		break;

	case "deletar_orcamento":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objOrcamento = new Orcamento($pdo);
			$objOrcamento->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.orcamento.php";
		break;


}
