<?
class Orcamento
{
	private $id;
	private $token;
	private $data_hora;
	private $id_cliente_orcamento;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setToken($arg)
	{
		$this->token = $arg;
	}
 	
	public function getToken()
	{
		return $this->token;
	}
 	
	public function setDataHora($arg)
	{
		$this->data_hora = $arg;
	}
 	
	public function getDataHora()
	{
		return $this->data_hora;
	}
 	
	public function setIdClienteOrcamento($arg)
	{
		$this->id_cliente_orcamento = $arg;
	}
 	
	public function getIdClienteOrcamento()
	{
		return $this->id_cliente_orcamento;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO orcamento SET data_hora = NOW() ';
		if ($this->getToken() != "") $sql .= ",token = ?";
		if ($this->getIdClienteOrcamento() != "") $sql .= ",id_cliente_orcamento = ?";

		$stmt = $pdo->prepare($sql);
		if ($this->getToken() != "") $stmt->bindParam(++$x,$this->getToken(),PDO::PARAM_STR);
		if ($this->getIdClienteOrcamento() != "") $stmt->bindParam(++$x,$this->getIdClienteOrcamento(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = 'UPDATE orcamento SET id = ?';
		if ($this->getToken() != "") $sql .= ",token = ?";
		if ($this->getDataHora() != "") $sql .= ",data_hora = ?";
		if ($this->getIdClienteOrcamento() != "") $sql .= ",id_cliente_orcamento = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getToken() != "") $stmt->bindParam(++$x,$this->getToken(),PDO::PARAM_STR);
		if ($this->getDataHora() != "") $stmt->bindParam(++$x,$this->getDataHora(),PDO::PARAM_STR);
		if ($this->getIdClienteOrcamento() != "") $stmt->bindParam(++$x,$this->getIdClienteOrcamento(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM orcamento WHERE id IN({$lista})";
		$sql = "UPDATE orcamento SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE orcamento.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM orcamento
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				orcamento.*
			FROM orcamento
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY orcamento.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM orcamento WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}
}
