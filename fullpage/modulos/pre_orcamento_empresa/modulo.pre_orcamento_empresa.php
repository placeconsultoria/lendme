<?

ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
include_once("../../admin/minisidebar/classes/Conexao.php");
include_once("classe.pre_orcamento_empresa.php");

$app_comando = $_REQUEST["acao"];


switch($app_comando)
{


	case "cad_empresa_pre_cadastro":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPreOrcamentoEmpresa = new PreOrcamentoEmpresa($pdo);
			$objPreOrcamentoEmpresa->setNome($_REQUEST['nome']);
			$objPreOrcamentoEmpresa->setEmail($_REQUEST['email']);
			$objPreOrcamentoEmpresa->setTelefone($_REQUEST['telefone']);
			$objPreOrcamentoEmpresa->setCpfcnpj($_REQUEST['cpfcnpj']);
			$objPreOrcamentoEmpresa->setTipo(1);
			$objPreOrcamentoEmpresa->setIdPreOrcamento(0);
			$objPreOrcamentoEmpresa->setRazaoSocial($_REQUEST['razao_social']);
            $objPreOrcamentoEmpresa->setIdEstado($_REQUEST['id_estado']);
            $objPreOrcamentoEmpresa->setPorteEmpresa($_REQUEST['porte_empresa']);
			$novoId = $objPreOrcamentoEmpresa->Adicionar();


			$msg["codigo"] = 0;
            $msg["id"] = $novoId;
			$msg["mensagem"] = "Sucesso";
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = "Erro";
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.pre_orcamento_empresa.php";
		break;



	case "atualizar_pre_orcamento_empresa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPreOrcamentoEmpresa = new PreOrcamentoEmpresa($pdo);
			$objPreOrcamentoEmpresa->setId($_REQUEST['id']);
			$objPreOrcamentoEmpresa->setNome($_REQUEST['nome']);
			$objPreOrcamentoEmpresa->setEmail($_REQUEST['email']);
			$objPreOrcamentoEmpresa->setTelefone($_REQUEST['telefone']);
			$objPreOrcamentoEmpresa->setCpfcnpj($_REQUEST['cpfcnpj']);
			$objPreOrcamentoEmpresa->setTipo($_REQUEST['tipo']);
			$objPreOrcamentoEmpresa->setIdPreOrcamento($_REQUEST['id_pre_orcamento']);
			$objPreOrcamentoEmpresa->setRazaoSocial($_REQUEST['razao_social']);
			$objPreOrcamentoEmpresa->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.pre_orcamento_empresa.php";
		break;



	case "deletar_pre_orcamento_empresa":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objPreOrcamentoEmpresa = new PreOrcamentoEmpresa($pdo);
			$objPreOrcamentoEmpresa->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.pre_orcamento_empresa.php";
		break;

}
