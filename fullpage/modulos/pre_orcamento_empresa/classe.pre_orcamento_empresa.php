<?
class PreOrcamentoEmpresa
{
	private $id;
	private $nome;
	private $email;
	private $telefone;
	private $cpfcnpj;
	private $tipo;
	private $id_pre_orcamento;
	private $razao_social;
    private $id_estado;
    private $porte_empresa;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setNome($arg)
	{
		$this->nome = $arg;
	}
 	
	public function getNome()
	{
		return $this->nome;
	}
 	
	public function setEmail($arg)
	{
		$this->email = $arg;
	}
 	
	public function getEmail()
	{
		return $this->email;
	}
 	
	public function setTelefone($arg)
	{
		$this->telefone = $arg;
	}
 	
	public function getTelefone()
	{
		return $this->telefone;
	}
 	
	public function setCpfcnpj($arg)
	{
		$this->cpfcnpj = $arg;
	}
 	
	public function getCpfcnpj()
	{
		return $this->cpfcnpj;
	}
 	
	public function setTipo($arg)
	{
		$this->tipo = $arg;
	}
 	
	public function getTipo()
	{
		return $this->tipo;
	}
 	
	public function setIdPreOrcamento($arg)
	{
		$this->id_pre_orcamento = $arg;
	}
 	
	public function getIdPreOrcamento()
	{
		return $this->id_pre_orcamento;
	}
 	
	public function setRazaoSocial($arg)
	{
		$this->razao_social = $arg;
	}
 	
	public function getRazaoSocial()
	{
		return $this->razao_social;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}



    public function getIdEstado()
    {
        return $this->id_estado;
    }

    public function setIdEstado($arg)
    {
        $this->id_estado = $arg;
    }

    public function getPorteEmpresa()
    {
        return $this->porte_empresa;
    }

    public function setPorteEmpresa($arg)
    {
        $this->porte_empresa = $arg;
    }

 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO pre_orcamento_empresa SET ';
		if ($this->getNome() != "") $sql .= " nome = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getCpfcnpj() != "") $sql .= ",cpfcnpj = ?";
		if ($this->getTipo() != "") $sql .= ",tipo = ?";
		if ($this->getIdPreOrcamento() != "") $sql .= ",id_pre_orcamento = ?";
		if ($this->getRazaoSocial() != "") $sql .= ",razao_social = ?";
        if ($this->getIdEstado() != "") $sql .= ",id_estado = ?";
        if ($this->getPorteEmpresa() != "") $sql .= ",porte_empresa = ?";
        $stmt = $pdo->prepare($sql);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getCpfcnpj() != "") $stmt->bindParam(++$x,$this->getCpfcnpj(),PDO::PARAM_STR);
		if ($this->getTipo() != "") $stmt->bindParam(++$x,$this->getTipo(),PDO::PARAM_INT);
		if ($this->getIdPreOrcamento() != "") $stmt->bindParam(++$x,$this->getIdPreOrcamento(),PDO::PARAM_INT);
		if ($this->getRazaoSocial() != "") $stmt->bindParam(++$x,$this->getRazaoSocial(),PDO::PARAM_STR);
        if ($this->getIdEstado() != "") $stmt->bindParam(++$x,$this->getIdEstado(),PDO::PARAM_INT);
        if ($this->getPorteEmpresa() != "") $stmt->bindParam(++$x,$this->getPorteEmpresa(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = '
		UPDATE pre_orcamento_empresa SET 
			id = ?';
		if ($this->getNome() != "") $sql .= ",nome = ?";
		if ($this->getEmail() != "") $sql .= ",email = ?";
		if ($this->getTelefone() != "") $sql .= ",telefone = ?";
		if ($this->getCpfcnpj() != "") $sql .= ",cpfcnpj = ?";
		if ($this->getTipo() != "") $sql .= ",tipo = ?";
		if ($this->getIdPreOrcamento() != "") $sql .= ",id_pre_orcamento = ?";
		if ($this->getRazaoSocial() != "") $sql .= ",razao_social = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getNome() != "") $stmt->bindParam(++$x,$this->getNome(),PDO::PARAM_STR);
		if ($this->getEmail() != "") $stmt->bindParam(++$x,$this->getEmail(),PDO::PARAM_STR);
		if ($this->getTelefone() != "") $stmt->bindParam(++$x,$this->getTelefone(),PDO::PARAM_STR);
		if ($this->getCpfcnpj() != "") $stmt->bindParam(++$x,$this->getCpfcnpj(),PDO::PARAM_STR);
		if ($this->getTipo() != "") $stmt->bindParam(++$x,$this->getTipo(),PDO::PARAM_INT);
		if ($this->getIdPreOrcamento() != "") $stmt->bindParam(++$x,$this->getIdPreOrcamento(),PDO::PARAM_INT);
		if ($this->getRazaoSocial() != "") $stmt->bindParam(++$x,$this->getRazaoSocial(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM pre_orcamento_empresa WHERE id IN({$lista})";
		$sql = "UPDATE pre_orcamento_empresa SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}


	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT 
                    pre_orcamento_empresa.*,
                    estados.nome as estado 
                    FROM pre_orcamento_empresa 
                    INNER JOIN estados ON pre_orcamento_empresa.id_estado = estados.id 
                    WHERE pre_orcamento_empresa.id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}
}
