<form id="frm_pre_orcamento_empresa">
			<input type="hidden" name="id"  id="id"   value="<?=$linha['id'];?>"/>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="nome"> <?=RTL_NOME?></label>
			<input type="text" name="nome"  id="nome" maxlength="255" class="form-control  " value="<?=$linha['nome'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="email"> <?=RTL_EMAIL?></label>
			<input type="text" name="email"  id="email" maxlength="255" class="form-control  " value="<?=$linha['email'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="telefone"> <?=RTL_TELEFONE?></label>
			<input type="text" name="telefone"  id="telefone" maxlength="20" class="form-control  " value="<?=$linha['telefone'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="cpfcnpj"> <?=RTL_CPFCNPJ?></label>
			<input type="text" name="cpfcnpj"  id="cpfcnpj" maxlength="30" class="form-control  " value="<?=$linha['cpfcnpj'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="tipo"> <?=RTL_TIPO?></label>
			<input type="text" name="tipo"  id="tipo" maxlength="1" class="form-control  mask-numero" value="<?=$linha['tipo'];?>"/>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-12">
			<label for="razao_social"> <?=RTL_RAZAO_SOCIAL?></label>
			<input type="text" name="razao_social"  id="razao_social" maxlength="11" class="form-control  mask-numero" value="<?=$linha['razao_social'];?>"/>
		</div>
	</div>
</form>
<?
include_once("modulos/pre_orcamento_empresa/template/js.frm.pre_orcamento_empresa.php");
?>
