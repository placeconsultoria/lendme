<script type="text/javascript">
$(function()
{
});

/*
 * Executa o post do formulário
 * */
function ExecutarMontaOrcamentosProcessoCivil(dialog, url)
{
	if (ValidateForm($("#frm_monta_orcamentos_processo_civil"))) {
		// ao clicar em salvar enviando dados por post via AJAX
		$.post(url,
			$("#frm_monta_orcamentos_processo_civil").serialize(),
			// pegando resposta do retorno do post
			function (response)
			{
				if (response["codigo"] == 0) {
					dialog.close();
					toastr.success(response["mensagem"], "<?=ROTULO_MENSAGEM?>");
					AtualizarGridMontaOrcamentosProcessoCivil(0, "");
				} else {
					toastr.warning(response["mensagem"], "<?=ROTULO_MENSAGEM?>");
				}
			}
			, "json" // definindo retorno para o formato json
		);
	}
}
</script>
