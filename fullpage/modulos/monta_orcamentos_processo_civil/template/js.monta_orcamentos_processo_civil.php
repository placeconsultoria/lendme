<?php
/**
* @author Fernando Carmo
* @copyright 2016
*/
?>
<script type="text/javascript">
	$(function()
	{
		LoadDiv("#conteudo_monta_orcamentos_processo_civil");;
		AtualizarGridMontaOrcamentosProcessoCivil(0,"");
	});

	function AtualizarGridMontaOrcamentosProcessoCivil(pagina,busca,filtro,ordem)
	{
		if(filtro == "" || filtro === undefined)  filtro = ""; 
		if(ordem == "" || ordem  === undefined)  ordem = "";

		var toPost = { 
			pagina: pagina,
			busca: busca,
			filtro: filtro,
			ordem: ordem
		};

		$("#conteudo_monta_orcamentos_processo_civil").load("index_xml.php?app_modulo=monta_orcamentos_processo_civil&app_comando=ajax_listar_monta_orcamentos_processo_civil", toPost);
	}

	function ImprimirRelatorio(form)
	{
		if (ValidarFormulario()) {
			form.action = "index_print.php?app_modulo=monta_orcamentos_processo_civil&app_comando=monta_orcamentos_processo_civil_print";
			form.target = "_blank";
			form.submit();
		}
	}

	function GerarPdf(form)
	{
		if (ValidarFormulario()) {
			form.action = "index_file.php?app_modulo=monta_orcamentos_processo_civil&app_comando=monta_orcamentos_processo_civil_pdf";
			form.target = "_blank";
			form.submit();
		}
	}

	function GerarXml(form)
	{
		if (ValidarFormulario()) {
			form.action = "index_file.php?app_modulo=monta_orcamentos_processo_civil&app_comando=monta_orcamentos_processo_civil_xlsx";
			form.target = "_blank";
			form.submit();
		}
	}

	function AbrirConfig()
	{
		BootstrapDialog.show({
			size:      BootstrapDialog.SIZE_SMALL,
			type:      BootstrapDialog.TYPE_DEFAULT,
			title:     "<div class='titulo_modal'><?=ROTULO_CONFIGURACOES?></div>",
			message:   $("<div></div>").load("index_xml.php?app_modulo=monta_orcamentos_processo_civil&app_comando=frm_configurar_listagem"),
			draggable: true,
			buttons:   [{
				label:    "<?=ROTULO_SALVAR?>",
				cssClass: "btn-lg btn-confirm",
				action:   function (dialogRef)
						{
							SalvarConfiguracoes(dialogRef, "index_xml.php?app_modulo=monta_orcamentos_processo_civil&app_comando=configurar_listagem", AtualizarGridMontaOrcamentosProcessoCivil)
						}
			}]
		});
	}
</script>
