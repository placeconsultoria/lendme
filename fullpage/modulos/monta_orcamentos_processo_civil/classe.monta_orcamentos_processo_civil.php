<?
class MontaOrcamentosProcessoCivil
{
	private $id;
	private $id_monta_orcamento;
	private $id_processo_civil;
	private $conexao;

	public function setId($arg)
	{
		$this->id = $arg;
	}
 	
	public function getId()
	{
		return $this->id;
	}
 	
	public function setIdMontaOrcamento($arg)
	{
		$this->id_monta_orcamento = $arg;
	}
 	
	public function getIdMontaOrcamento()
	{
		return $this->id_monta_orcamento;
	}
 	
	public function setIdProcessoCivil($arg)
	{
		$this->id_processo_civil = $arg;
	}
 	
	public function getIdProcessoCivil()
	{
		return $this->id_processo_civil;
	}
 	
	public function setConexao($arg)
	{
		$this->conexao = $arg;
	}
 	
	public function getConexao()
	{
		return $this->conexao;
	}
 	
	public function __construct($conexao = "")
	{
		if ($conexao) {
			$this->conexao = $conexao;
		} else {
			$this->conexao = new Conexao();
		}
	}

	public function Adicionar()
	{
		$pdo = $this->getConexao();
		$sql = 'INSERT INTO monta_orcamentos_processo_civil SET ';
		if ($this->getIdMontaOrcamento() != "") $sql .= "id_monta_orcamento = ?";
		if ($this->getIdProcessoCivil() != "") $sql .= ",id_processo_civil = ?";
        $stmt = $pdo->prepare($sql);
		if ($this->getIdMontaOrcamento() != "") $stmt->bindParam(++$x,$this->getIdMontaOrcamento(),PDO::PARAM_INT);
		if ($this->getIdProcessoCivil() != "") $stmt->bindParam(++$x,$this->getIdProcessoCivil(),PDO::PARAM_INT);
		$stmt->execute();
		return $pdo->lastInsertId() ;
	}
	public function Modificar()
	{
		$pdo = $this->getConexao();
		$sql = '
		UPDATE monta_orcamentos_processo_civil SET 
			id = ?';
		if ($this->getIdMontaOrcamento() != "") $sql .= ",id_monta_orcamento = ?";
		if ($this->getIdProcessoCivil() != "") $sql .= ",id_processo_civil = ?";

		$sql .= ' WHERE id = ?';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		if ($this->getIdMontaOrcamento() != "") $stmt->bindParam(++$x,$this->getIdMontaOrcamento(),PDO::PARAM_INT);
		if ($this->getIdProcessoCivil() != "") $stmt->bindParam(++$x,$this->getIdProcessoCivil(),PDO::PARAM_INT);
		$stmt->bindParam(++$x,$this->getId(),PDO::PARAM_INT);
		return $stmt->execute();
	}
	public function Remover($lista)
	{
		$pdo = $this->getConexao();
		$lista = implode(",",$lista);
		//$sql = "DELETE FROM monta_orcamentos_processo_civil WHERE id IN({$lista})";
		$sql = "UPDATE monta_orcamentos_processo_civil SET excluido = UTC_TIMESTAMP() WHERE id IN({$lista})";
		$stmt = $pdo->prepare($sql);
		return $stmt->execute();
	}

	public function ListarPaginacao($idGrupo,$numeroRegistros,$numeroInicioRegistro,$busca = "",$filtro = "",$ordem = "")
	{
		$pdo = $this->getConexao();
		
		$joins = "
		
		";
		
		$where = "
			WHERE monta_orcamentos_processo_civil.id > 0
		";
		
		if($busca != "") $where .= " AND (titulo LIKE :busca)";
		
		$sql = "
			SELECT COUNT(*) AS total
			FROM monta_orcamentos_processo_civil
			$joins
			$where
		";

		$stmt = $pdo->prepare($sql);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$totalRegistros = $stmt->fetch(PDO::FETCH_OBJ)->total;

		$sql = "
			SELECT 
				monta_orcamentos_processo_civil.*
			FROM monta_orcamentos_processo_civil
			$joins
			$where
		";

		if($filtro != "") $sql .=" ORDER BY $filtro $ordem"; else $sql .=" ORDER BY monta_orcamentos_processo_civil.id DESC";
		$sql .= " LIMIT :offset,:limit";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":offset",$numeroInicioRegistro,PDO::PARAM_INT);
		$stmt->bindParam(":limit",$numeroRegistros,PDO::PARAM_INT);

		if($busca != "") {
			$busca = "%".$busca."%";
			$stmt->bindParam(":busca",$busca,PDO::PARAM_STR);
		}

		$stmt->execute();
		$linhas = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return [$linhas,$totalRegistros];
	}

	public function Editar()
	{
		$pdo = $this->getConexao();
		$sql = "SELECT * FROM monta_orcamentos_processo_civil WHERE id = ?";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(1,$this->getId(),PDO::PARAM_INT);
		$stmt->execute();
		return $stmt->fetch();
	}

    public function ListarProcessosOrcamento()
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id_processo_civil FROM monta_orcamentos_processo_civil WHERE id_monta_orcamento = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1,$this->getIdMontaOrcamento(),PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function ListarProcessosOrcamentoPDF($idMontaOrcamento)
    {
        $pdo = $this->getConexao();
        $sql = "SELECT id_processo_civil FROM monta_orcamentos_processo_civil WHERE id_monta_orcamento = '$idMontaOrcamento'";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
