<?

switch($app_comando)
{
	case "frm_adicionar_monta_orcamentos_processo_civil":
		$template = "tpl.frm.monta_orcamentos_processo_civil.php";
		break;

	case "adicionar_monta_orcamentos_processo_civil":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamentosProcessoCivil = new MontaOrcamentosProcessoCivil($pdo);
			$objMontaOrcamentosProcessoCivil->setIdMontaOrcamento($_REQUEST['id_monta_orcamento']);
			$objMontaOrcamentosProcessoCivil->setIdProcessoCivil($_REQUEST['id_processo_civil']);
			$novoId = $objMontaOrcamentosProcessoCivil->Adicionar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_ADICIONAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamentos_processo_civil.php";
		break;

	case "frm_atualizar_monta_orcamentos_processo_civil" :
		$monta_orcamentos_processo_civil = new MontaOrcamentosProcessoCivil();
		$monta_orcamentos_processo_civil->setId($_REQUEST["app_codigo"]);
		$linha = $monta_orcamentos_processo_civil->Editar();
		$template = "tpl.frm.monta_orcamentos_processo_civil.php";
		break;

	case "atualizar_monta_orcamentos_processo_civil":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamentosProcessoCivil = new MontaOrcamentosProcessoCivil($pdo);
			$objMontaOrcamentosProcessoCivil->setId($_REQUEST['id']);
			$objMontaOrcamentosProcessoCivil->setIdMontaOrcamento($_REQUEST['id_monta_orcamento']);
			$objMontaOrcamentosProcessoCivil->setIdProcessoCivil($_REQUEST['id_processo_civil']);
			$objMontaOrcamentosProcessoCivil->Modificar();
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_MODIFICAR;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamentos_processo_civil.php";
		break;

	case "listar_monta_orcamentos_processo_civil":
		$template = "tpl.geral.monta_orcamentos_processo_civil.php";
		break;

	case "deletar_monta_orcamentos_processo_civil":
		$pdo = new Conexao();
		$pdo->beginTransaction();
		try {
			$objMontaOrcamentosProcessoCivil = new MontaOrcamentosProcessoCivil($pdo);
			$objMontaOrcamentosProcessoCivil->Remover($_REQUEST['registros']);
			$msg["codigo"] = 0;
			$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			$pdo->commit();
		} catch (Exception $e) {
			$msg["codigo"] = 1;
			$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			$msg["debug"] = $e->getMessage();
			$pdo->rollBack();
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamentos_processo_civil.php";
		break;

	case "ajax_listar_monta_orcamentos_processo_civil":
		$template = "tpl.lis.monta_orcamentos_processo_civil.php";
		break;

	case "monta_orcamentos_processo_civil_pdf":
		$template = "tpl.lis.monta_orcamentos_processo_civil.pdf.php";
		break;

	case "monta_orcamentos_processo_civil_xlsx":
		$template = "tpl.lis.monta_orcamentos_processo_civil.xlsx.php";
		break;

	case "monta_orcamentos_processo_civil_print":
		$template = "tpl.lis.monta_orcamentos_processo_civil.print.php";
		break;

	case "frm_configurar_listagem":
		$template = "configuracao_listagem.php";
		break;

	case "configurar_listagem":
		$usuarioConfiguracao = new UsuarioConfiguracao();
		foreach ($_POST as $checkbox => $idCampo) {
			if ($checkbox == "limite_colunas") {
				continue;
			}
			if (is_array($idCampo)) {
				if ($idCampo["valor"] == "") {
					continue;
				} else {
					$colunasSelecionadas[$checkbox] = ["id_campo" => $idCampo["id"], "valor_campo" => $idCampo["valor"]];
				}
			} else {
				$colunasSelecionadas[$checkbox] = $idCampo;
			}
		}
		$countColunasSelecionadas = count($colunasSelecionadas);
		if ($countColunasSelecionadas > 0) {
			if (($countColunasSelecionadas > $_REQUEST["limite_colunas"]) && $_REQUEST["limite_colunas"] != "0" && $_REQUEST["limite_colunas"] != "") {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_LIMITE_COLUNAS_EXCEDIDO_RESOLUCAO;
				echo json_encode($msg);
				die();
			}
			$_SESSION["configuracao_usuario"]["monta_orcamentos_processo_civil"] = $colunasSelecionadas;
			$usuarioConfiguracao->setIdUsuario($_SESSION["usuario"]["id"]);
			$usuarioConfiguracao->setDirModulo("monta_orcamentos_processo_civil");
			$usuarioConfiguracao->LimparConfiguracoes();
			foreach ($colunasSelecionadas AS $nomeCampo => $idCampo) {
				if (is_array($idCampo)) {
					$usuarioConfiguracao->setIdCampoModulo($idCampo["id_campo"]);
					$usuarioConfiguracao->setValorCampo($idCampo["valor_campo"]);
				} else {
					$usuarioConfiguracao->setIdCampoModulo($idCampo);
				}
				$resultado = $usuarioConfiguracao->AdicionarUsuarioConfiguracao();
				if (!$resultado) {
					break;
				}
			}
			if ($resultado) {
				$msg["codigo"]   = 0;
				$msg["mensagem"] = TXT_ALERT_SUCESSO_OPERACAO;
			} else {
				$msg["codigo"]   = 1;
				$msg["mensagem"] = TXT_ALERT_ERRO_OPERACAO;
			}
		} else {
			$msg["codigo"]   = 1;
			$msg["mensagem"] = TXT_ALERT_SELECIONAR_COLUNAS;
		}
		echo json_encode($msg);
		$template = "ajax.monta_orcamentos_processo_civil.php";
		break;
}
