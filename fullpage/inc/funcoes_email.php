<?php

function get_topo_email(){

$topo_email = '					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<title>PRODUTOS CONTROLADOS</title>
					</head>
					
					<style>
					
					body{
						padding:0;
						margin:0;
						background:#f8fafa;
						padding-top:20px;
						padding-bottom:40px;
						width: 100%;
						text-align: center;
					}
					
					p{
						font-family:Verdana, Geneva, sans-serif;
						font-size:12px;
						color:#666;
						letter-spacing:0.05em;
						line-height:120%;
					}
					
					 <style> .table, th, td {  border: 1px solid black; } </style>
                     
					
					</style>
					<body style="background:#f8fafa; padding-top:20px; padding-bottom:40px; width: 100%; text-align: center;">
					
					<table width="800px" border="0" cellpadding="0" cellspacing="0" align="center" style="margin:auto;">
					  <tr>
						<td><p style="text-align:center; padding-bottom:20px; font-size:14px; color:#666; display:block; width:100%; letter-spacing:0;" ><strong>Aviso:</strong> por favor, <strong>habilite</strong> ou <strong>aprove</strong> para exibir as imagens deste email para melhor visualização do mesmo.</p></td>
					  </tr>
					  <tr style="background:#FFF;">
						<td width="800px" height="307px" style="background:url(http://produtoscontrolados.com.br/img_email/img_top.jpg) center no-repeat;">
							<img src="http://produtoscontrolados.com.br/img_email/img_top.jpg" alt="Por favor, habilite ou aprove para exibir as imagens deste email para melhor visualização do mesmo" />
						</td>
					  </tr>
					  <tr style="background:#FFF;">
						<td style="text-align:center; padding-bottom:20px; padding-top:20px;">';
	return $topo_email;
}
						
	
function get_footer_email(){						

$footer_email = '		 </td>
					  </tr>
					 <tr style="background:#FFF;">
						<td width="800px" height="125px" style="background:url(http://produtoscontrolados.com.br/img_email/img_footer.jpg) center no-repeat;">
							<img src="http://produtoscontrolados.com.br/img_email/img_footer.jpg" alt="Por favor, habilite ou aprove para exibir as imagens deste email para melhor visualização do mesmo" />
						</td>
					  </tr>
					</table>
					</body>
					</html>';

return $footer_email;					
					
}



//

function envia_email_diagnostico($pergunta1,$pergunta2,$pergunta3,$pergunta4,$pergunta5,$nome,$email,$cnpj,$telefone,$empresa,$texto){
    require_once("PHPMailer/class.phpmailer.php");
    // Inicia a classe PHPMailer
    $mail = new PHPMailer(true);

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP

    try {
        $mail->Host = 'smtp.licenser.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
        $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
        $mail->Port       = 587; //  Usar 587 porta SMTP
        $mail->Username = 'site@licenser.com.br'; // Usuário do servidor SMTP (endereço de email)
        $mail->Password = 'MAGx0880'; // Senha do servidor SMTP (senha do email usado)
        $mail->SetFrom('site@licenser.com.br', 'LICENSER');
        $mail->AddReplyTo($email, $nome);
        $mail->Subject = utf8_decode('[LICENSER SITE] DIAGNÓSTICO');
        $mail->AddAddress("leonardo@qeasy.com.br","Leonardo");
       // $mail->AddAddress("marketing@licenser.com.br","Marketing Licenser");
        // $mail->AddCC('comercial@licenser.com.br', 'Comercial Licenser');

        $corpo_email = '
		 	
						    <p style="display:block; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:12px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
								DIAGNÓSTICO:
							</p>
                            
						    <p style="display:block; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:12px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
						           Você conhece as determinações legais relativas a produtos controlados?
						            <br /> '.$pergunta1.'
						   </p>
						   
						    <p style="display:block; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:12px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
                                Atualmente você possui licença para atuar com produtos controlados?
						        <br /> '.$pergunta2.'
						   </p>
						   
						    <p style="display:block; width:80%; height:auto;  margin: auto;  font-weight:normal; font-size:12px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
                                Você conhece os procedimentos após obter o licenciamento para produtos controlados, tais como, instalações físicas e mapas de movimentação, dentre outros?
						        <br /> '.$pergunta3.'
						   </p>
						   
						    <p style="display:block;  width:80%; height:auto;   margin: auto; font-weight:normal; font-size:12px; font-family:Arial, Helvetica, sans-serif;  text-align: left" class="h1">
                                Você se considera apto a exercer atividades junto aos Órgãos Fiscalizadores de Produtos Controlados (Polícia Federal, Polícia Civil e Exército Brasileiro)?
						        <br /> '.$pergunta4.'
						   </p>
						  
                            
                            <div style="width: 80%; margin: auto; display: block; text-align: left">
                                <p><strong>NOME:</strong> '.$nome.' </p>
                                <p><strong>RAZÃO SOCIAL:</strong> '.$empresa.' </p>
                                <p><strong>CNPJ:</strong> '.$cnpj.' </p>
                                <p><strong>TELEFONE:</strong> '.$telefone.' </p>
                                <p><strong>E-MAIL:</strong> '.$email.' </p>
                                <p><strong>MENSAGEM:</strong> <br /> '.$texto.' </p>
                            </div>
            				
		 ';

        $topo_email = get_topo_email();
        $footer_email = get_footer_email();
        $email = utf8_decode($topo_email.$corpo_email.$footer_email);

        $mail->IsHTML(true); //enviar em HTML
        //Define o corpo do email
        $mail->MsgHTML($email);
        $mail->Send();

        //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
        $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
    }

    return $erro;
}

	

function envia_email_contato_1($nome, $razao_social, $cpf_cnpj, $email, $telefone, $mensagem){
	require_once("PHPMailer/class.phpmailer.php");
	// Inicia a classe PHPMailer
	$mail = new PHPMailer(true);
	 
	// Define os dados do servidor e tipo de conexão
	// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	$mail->IsSMTP(); // Define que a mensagem será SMTP
	 
	try {
		 $mail->Host = 'smtp.licenser.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
		 $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
		 $mail->Port       = 587; //  Usar 587 porta SMTP
		 $mail->Username = 'site@licenser.com.br'; // Usuário do servidor SMTP (endereço de email)
		 $mail->Password = 'MAGx0880'; // Senha do servidor SMTP (senha do email usado)
	 	 $mail->SetFrom('site@licenser.com.br', 'PRODUTOS CONTROLADOS');
		 $mail->AddReplyTo($email, $nome);
		 $mail->Subject = utf8_decode('[CONTATO] [PJ] - PRODUTOS CONTROLADOS BY LICENSER');
         //$mail->AddAddress("leonardo@qeasy.com.br","Leonardo");
		 $mail->AddAddress("marketing@licenser.com.br","Marketing Licenser");
        // $mail->AddCC('comercial@licenser.com.br', 'Comercial Licenser');



			 
		 $corpo_email = '
		 	
							<p style="display:block; width:100%; height:auto;  font-weight:normal; font-size:18px; font-family:Arial, Helvetica, sans-serif; color:#24313c;" class="h1">
								CHEGOU UM NOVO CONTATO PELO SITE!
							</p>
                            
                           <p style="display:block; width:100%; height:auto; padding-bottom:10px; font-weight:normal; font-size:18px; font-family:Arial, Helvetica, sans-serif; color:#24313c;" class="h1">
								INFORMAÇÕES DO CONTATO (PESSOA JURÍDICA)
							</p>
                            
                            <div style="width: 80%; margin: auto; display: block; text-align: left">
                                <p><strong>NOME:</strong> '.$nome.' </p>
                                <p><strong>RAZÃO SOCIAL:</strong> '.$razao_social.' </p>
                                <p><strong>CNPJ:</strong> '.$cpf_cnpj.' </p>
                                <p><strong>TELEFONE:</strong> '.$telefone.' </p>
                                <p><strong>E-MAIL:</strong> '.$email.' </p>
                                <p><strong>MENSAGEM:</strong> <br /> '.$mensagem.' </p>
                            </div>
                            
                            
                            
                            
                            
							
		 ';
		 
		$topo_email = get_topo_email();
		$footer_email = get_footer_email();
		$email = utf8_decode($topo_email.$corpo_email.$footer_email);
	 
		 $mail->IsHTML(true); //enviar em HTML
		 //Define o corpo do email
		 $mail->MsgHTML($email); 
		 $mail->Send();
		
		//caso apresente algum erro é apresentado abaixo com essa exceção.
		}catch (phpmailerException $e) {
		 $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
		}
		
		return $erro;
}


function envia_email_contato_2($nome, $cpf_cnpj, $email, $telefone, $mensagem){
    require_once("PHPMailer/class.phpmailer.php");
    // Inicia a classe PHPMailer
    $mail = new PHPMailer(true);

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP

    try {
        $mail->Host = 'smtp.licenser.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
        $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
        $mail->Port       = 587; //  Usar 587 porta SMTP
        $mail->Username = 'site@licenser.com.br'; // Usuário do servidor SMTP (endereço de email)
        $mail->Password = 'MAGx0880'; // Senha do servidor SMTP (senha do email usado)
        $mail->SetFrom('site@licenser.com.br', 'PRODUTOS CONTROLADOS');
        $mail->AddReplyTo($email, $nome);
        $mail->Subject = utf8_decode('[CONTATO] [PF] - PRODUTOS CONTROLADOS BY LICENSER');
        $mail->AddAddress("marketing@licenser.com.br","Marketing Licenser");
        //$mail->AddAddress("leonardo@qeasy.com.br","Leonardo");


        $corpo_email = '
		 	
							<p style="display:block; width:100%; height:auto;  font-weight:normal; font-size:18px; font-family:Arial, Helvetica, sans-serif; color:#24313c;" class="h1">
								CHEGOU UM NOVO CONTATO PELO SITE!
							</p>
                            
                           <p style="display:block; width:100%; height:auto; padding-bottom:10px; font-weight:normal; font-size:18px; font-family:Arial, Helvetica, sans-serif; color:#24313c;" class="h1">
								INFORMAÇÕES DO CONTATO (PESSOA FÍSICA)
							</p>
                            
                            <div style="width: 80%; margin: auto; display: block; text-align: left">
                                <p><strong>NOME:</strong> '.$nome.' </p>
                                <p><strong>CPF:</strong> '.$cpf_cnpj.' </p>
                                <p><strong>TELEFONE:</strong> '.$telefone.' </p>
                                <p><strong>E-MAIL:</strong> '.$email.' </p>
                                <p><strong>MENSAGEM:</strong> <br /> '.$mensagem.' </p>
                            </div>
                            
                            
                            
                            
                            
							
		 ';

        $topo_email = get_topo_email();
        $footer_email = get_footer_email();
        $email = utf8_decode($topo_email.$corpo_email.$footer_email);

        $mail->IsHTML(true); //enviar em HTML
        //Define o corpo do email
        $mail->MsgHTML($email);
        $mail->Send();

        //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
        $erro =   "erro".$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
    }

    return $erro;
}






	
	
?>