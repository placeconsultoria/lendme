<?php


include_once("includes/config.inc.php");
include_once("classes/Url.php");
include_once("classes/TopoHtml.php");

$URL_SITE = Url::getBase();
$app_modulo  = Url::getURL(0);
$app_comando = Url::getURL(1);
$app_codigo  = Url::getURL(2);


if($app_modulo == '') $app_modulo = "home";
// inserindo arquivo de modulo
$arquivo = "modulos/" . $app_modulo . "/modulo." . $app_modulo . ".php";
if (file_exists($arquivo) && !is_dir($arquivo)) {
    include($arquivo);
}
$temp = URL_FILE . "modulos/" . $app_modulo . "/template/" . $template;
// inserindo topo
include_once("template/topo.php");
//verificando para inserir o aquivos de template
if (file_exists($temp) && !is_dir($temp)) {
    include($temp);
} else {
    include "template/404_not_found.php";
}
//inserindo rodape
if($app_modulo != "simulador"){
    include_once("template/baixo.php");
}else{
    include_once("template/baixo2.php");
}
