<script>
    $(function() {

    $('.select-data-maratona').change(function (e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        var txt_option_date = return_html_txt_date(id);
        $("#table_date_"+id).html(txt_option_date);
    });

    $('.select-vagas-maratona').change(function (e) {
    e.preventDefault();
    var vagas = $(this).val();
    var id = $(this).attr("data-id");
    var valor = '100.00';
    var total = eval(valor) * eval(vagas);
    var txt = vagas+' x R$ '+valor+' = R$ <span class="mask-money">'+total+'.00</span>';
    var id_seletor_html = '#valor_maratona_item_'+id;
    $(id_seletor_html).html(txt);

    // var txt_option_date = return_html_txt_date(id);
    // $("#table_date_"+id).html(txt_option_date);

    $("#table_vagas_"+id).html(vagas);
    $("#table_total_"+id).html('R$: <span class="mask-money">'+total+'.00</span>');

    $('.mask-money').mask('#.##0,00', {reverse: true});

});

    function return_html_txt_date(id){

    var id_select = '#data_maratona'+id;
    var retorno = "";

    $(id_select+" option:selected").each(function() {
    retorno = $(this).html();
});
    return retorno;
}

});
</script>