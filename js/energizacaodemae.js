$(function() {



    $( "#frm_contato" ).submit(function( event ) {

        var formData = new FormData(this);
        swal("AGUARDE...","Estamos enviando o seu contato.");
        $.ajax({
            type: "POST",
            url: "ajax.php?acao=envia_email",
            data: formData,
            success: function(data)
            {

               if(data.codigo == 1){
                   swal("SUCESSO!","Recebemos o seu contato, responderemos o mais breve possível","success");
               }

            },
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json'
        });

        return false;

    });
    $('#btn_send_login').click(function (e) {
        $(this).prop("disabled",true).html("Aguarde...");
        $.getJSON( "ajax.php?acao=login",{'email': $("#email_login").val(), "senha": $("#senha_login").val()}, function(data) {
            if(data.codigo == 0){
                location.href = 'minhas_inscricoes.php';
            }else if(data.codigo == 1){
                swal("ERRO","Não foi possível fazer o login com os dados informados","error");
                $('#btn_send_login').prop("disabled",false).html('<i class="fa fa-lock-open"></i> FAZER LOGIN');
            }
        });

    });

    $('.btn_sair').click(function (e) {
        e.preventDefault();
        $.getJSON( "ajax.php?acao=logout", function() {
            location.href = 'index.php';
        });
    });

    $('button[name="btn_pagamento"]').click(function (e) {
        e.preventDefault();
        var token_pagseguro = $(this).attr("data-code-ps");

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            location.href="https://pagseguro.uol.com.br/v2/checkout/payment.html?code="+ token_pagseguro;
        }else{
            abreModal(token_pagseguro);
        }
    });

    $('.btn_login').click(function (e) {
        e.preventDefault();
        $('#modal_login').modal("show");
    });

    $('#frm_compra').submit(function(){
        var formData = new FormData(this);

        if($("#evento").val()==0 || $("#regiao").val()==0 ){
            swal("Atenção!","Selecione uma data e local!","warning");
            return false;
        }

        if($("#senha1").val() != $("#senha2").val()){
            swal("Atenção!","As duas senhas não conhecidem","warning");
            return false;
        }

        swal("AGUARDE...","Estamos processando o seu cadastro.");
        $('#btn_compra_submit').addClass('btn-warning').html('AGUARDE, ESTAMOS PROCESSANDO...').prop("disabled",true);
        $.ajax({
            type: "POST",
            url: "ajax.php?acao=add_compra",
            data: formData,
            success: function(data)
            {

                if(data.codigo == 0){
                    $('#btn_compra_submit').addClass('btn-info').removeClass('btn-warning').html('ABRINDO CONEXÃO COM O PAGSEGURO');
                    swal("SUCESSO!","Aguarde, Estamos abrindo a conexão com o PagSeguro.","success");
                    var code_ps = data.checkout_pagseguro[0];
                    setTimeout(function(){
                        swal.close();
                        $('#btn_compra_submit').addClass('btn-success').removeClass('btn-info btn-warning').html('<i class="fa fa-card"></i> EFETUAE O APGAMENTO');



                        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                            location.href="https://pagseguro.uol.com.br/v2/checkout/payment.html?code="+ code_ps;
                        }else{
                            abreModal(code_ps);
                        }

                     }, 1000);

                }else if(data.codigo == 2){
                    swal("OPS!", "Este evento não tem mais vagas disponíveis, escolha outra data!!","warning");
                    $('#btn_compra_submit').removeClass('btn-warning btn-info btn-error').addClass('btn').html('<i class="fa fa-check-circle"></i> CONCLUIR A COMPRA E PAGAR').prop("disabled",false);
                }else if(data.codigo == 3){
                    swal.close();
                    $('#email_login').val($("#email").val());
                    $('#modal_login').modal("show");
                    $('#btn_compra_submit').addClass('btn-error').removeClass('btn-warning btn-info').html('ERRO! FAÇA O SEU LOGIN');
                }

            },
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json'
        });

        return false;

    });

    $('#regiao').change(function (e) {
        var id_regiao = $(this).val();
        $("#evento").html('<option>AGUARDE...</option>');
        $.getJSON( "ajax.php",{"acao":"get_eventos_regiao","regiao": id_regiao}, function( data ) {
            var items = [];
            items.push( "<option id='0'>--SELECIONE--</li>" );
            $("#evento").prop("disabled",false);
            $.each( data.data, function( key, val ) {
                var data =  moment(new Date(val.data)).format("DD/MM/YYYY")
                var cidade = val.cidade;
                var estado = val.estado;
                var hotel = val.hotel;
                var text = data+" - "+cidade+" - "+estado+" | HOTEL "+hotel;
                items.push( "<option class='text-uppercase' value='" + val.id + "'>" + text + "</li>" );
            });

            $("#evento").html(items);
        });

    });

    $('#evento').change(function () {
        var id = $(this).val();
        var vagas = $("#vagas").val();
        $("#load_info_hotel_valor").html("<p class='lead text-theme'>Carregando..</p>");
        var url = 'load_info_hotel_valor.php?evento='+id+'&vagas='+vagas;
        $("#load_info_hotel_valor").load(url);

    });

    $('#vagas').change(function () {
        $("#load_info_hotel_valor").html("<p class='lead text-theme'>Carregando..</p>");
        var id = $('#evento').val();
        var vagas = $(this).val()
        var url = 'load_info_hotel_valor.php?evento='+id+'&vagas='+vagas;
        $("#load_info_hotel_valor").load(url);
    });
});

function mascaraTexto(evento, mascara){
    var campo, valor, i, tam, caracter;

    if (document.all) // Internet Explorer
        campo = evento.srcElement;
    else
        campo= evento.target;
    valor = campo.value;
    tam = valor.length;
    for(i=0;i<mascara.length;i++){
        caracter = mascara.charAt(i);
        if(caracter!="9")
            if(i<tam & caracter!=valor.charAt(i))
                campo.value = valor.substring(0,i) + caracter + valor.substring(i,tam);
    }
}

$("#telefone, #telefone2").keydown(function () {
    //Recebe o elemento ativo
    var focus = $(document.activeElement);
    //Timeout para pegar o valor do campo depois do evento, sem ele, o valor é testado antes do evento ser finalizado
    setTimeout(function () {
        //Se o campo focado é algum dos 3 campos de telefone, aplica a máscara de acordo
        if (focus.attr('id') == "telefone" || focus.attr('id') == "telefone2") {
            if (focus.val().length <= 14) {
                focus.unmask();
                focus.mask("(00) 0000-00009");
            }
            else {
                focus.unmask();
                focus.mask("(00) 00000-0000");
            }
        }
    }, 10);
});


function abreModal(code){
    var callback = {
        success : function(transactionCode) {
            //Insira os comandos para quando o usuário finalizar o pagamento.
            //O código da transação estará na variável "transactionCode"
            console.log("Compra feita com sucesso, código de transação: " + transactionCode);
            swal("SUCESSO!","Recebemos a interação com o pagseguro, após a confirmação do pagamento você irá receber um e-mail de confirmação da sua inscrição.","success");
            setTimeout(function(){
                swal("ATENÇÀO","Você será redircionado para a página do usuário!","success");
            }, 2000);
            setTimeout(function(){
                window.location = 'minhas_inscricoes.php';
            }, 2600);
        },
        abort : function() {
            swal("SESSÃO DE PAGAMENTO FINALIZADA", "Você encerrou a sessão de pagamento!","error");
            setTimeout(function(){
                swal("ATENÇÀO","Você será redircionado para a página do usuário!","info");
            }, 1000);
            setTimeout(function(){
                window.location = 'minhas_inscricoes.php';
            }, 2000);
        }
    };
    //Chamada do lightbox passando o código de checkout e os comandos para o callback
    var isOpenLightbox = PagSeguroLightbox(code, callback);
    // Redireciona o comprador, caso o navegador não tenha suporte ao Lightbox
    if (!isOpenLightbox){
        location.href="https://pagseguro.uol.com.br/v2/checkout/payment.html?code="+ code;
        //window.location  = 'https://pagseguro.uol.com.br/v2/checkout/payment.html?code='+ code;
    }
}

$('a[name="btn_menu"]').click(function(event){
    event.preventDefault();
    var href = $(this).attr('href');
    $([document.documentElement, document.body]).animate({
        scrollTop: ($(href).offset().top) - 30
    }, 1000);
});

// PagSeguroLightbox({
//     code: return_checkout_pagseguro
// }, {
//     success : function(transactionCode) {
//         swal("SUCESSO!","Recebemos a interação com o pagseguro, após a confirmação do pagamento você irá receber um e-mail de confirmação da sua inscrição.","success");
//     },
//     abort : function() {
//         swal("SESSÃO DE PAGAMENTO FINALIZADA", "Você encerrou a sessão de pagamento!","error");
//         setTimeout(function(){
//             swal("ATENÇÀO","Você será redircionado para a página do usuário!","info");
//         }, 1000);
//         setTimeout(function(){
//             window.location = 'minhas_inscricoes.php';
//         }, 2000);
//     }
// });