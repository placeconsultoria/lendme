

$(function() {

    $('#vagas_maratona1').val(1);
    $('.mask-date').mask("99/99/9999");
    $('.telefone').mask('(00) 0000-00009');

    $("#table_date_1").html($("#value_data_maratona_").html());

    var txt_option_date = return_html_txt_date(1);
    $("#table_date_1").html(txt_option_date);

    $( "#frm_contato" ).submit(function( event ) {

        var formData = new FormData(this);
        swal("AGUARDE...","Estamos enviando o seu contato.");
        $.ajax({
            type: "POST",
            url: "ajax.php?acao=envia_email",
            data: formData,
            success: function(data)
            {
                if(data.codigo == 1){
                    swal("SUCESSO!","Recebemos o seu contato, responderemos o mais breve possÃ­vel","success");
                }

            },
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json'
        });

        return false;

    });


    $('#btn_send_login').click(function (e) {
        $(this).prop("disabled",true).html("Aguarde...");
        $.getJSON( "ajax.php?acao=login",{'email': $("#email_login").val(), "senha": $("#senha_login").val()}, function(data) {
            if(data.codigo == 0){
                location.href = 'minhas_inscricoes.php';
            }else if(data.codigo == 1){
                swal("ERRO","NÃ£o foi possÃ­vel fazer o login com os dados informados","error");
                $('#btn_send_login').prop("disabled",false).html('<i class="fa fa-lock-open"></i> FAZER LOGIN');
            }
        });

    });

    $('.btn_sair').click(function (e) {
        e.preventDefault();
        $.getJSON( "ajax.php?acao=logout", function() {
            location.href = 'index.php';
        });
    });

    $('button[name="btn_pagamento"]').click(function (e) {
        e.preventDefault();
        var token_pagseguro = $(this).attr("data-code-ps");

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            location.href="https://pagseguro.uol.com.br/v2/checkout/payment.html?code="+ token_pagseguro;
        }else{
            abreModal(token_pagseguro);
        }
    });

    $('.btn_login').click(function (e) {
        e.preventDefault();
        $('#modal_login').modal("show");
    });

    $('#frm_comprar').submit(function(){
        var formData = new FormData(this);
        alert("oi");

        if($("#senha1").val() != $("#senha2").val()){
            swal("Atenção!","As duas senhas não conhecidem","warning");
            return false;
        }

        swal("AGUARDE...","Estamos processando o seu cadastro.");
        $('#btn_compra_submit').addClass('btn-warning').html('AGUARDE, ESTAMOS PROCESSANDO...').prop("disabled",true);
        $.ajax({
            type: "POST",
            url: "ajax.php?acao=action_efetuar_compra",
            data: formData,
            success: function(data)
            {
                if(data.codigo == 0){
                    $('#btn_compra_submit').addClass('btn-info').removeClass('btn-warning').html('ABRINDO CONEXÇÃO COM O PAGSEGURO');
                    swal("SUCESSO!","Aguarde, Estamos abrindo a conexÃ£o com o PagSeguro.","success");
                    var code_ps = data.checkout_pagseguro[0];
                    setTimeout(function(){
                        swal.close();
                        $('#btn_compra_submit').addClass('btn-success').removeClass('btn-info btn-warning').html('<i class="fa fa-card"></i> EFETUAE O APGAMENTO');
                        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                            location.href="https://pagseguro.uol.com.br/v2/checkout/payment.html?code="+ code_ps;
                        }else{
                            abreModal(code_ps);
                        }

                    }, 1000);

                }else if(data.codigo == 2){
                    swal("OPS!", "Este evento não tem mais vagas disponíveis, escolha outra data!!","warning");
                    $('#btn_compra_submit').removeClass('btn-warning btn-info btn-error').addClass('btn').html('<i class="fa fa-check-circle"></i> CONCLUIR A COMPRA E PAGAR').prop("disabled",false);
                }else if(data.codigo == 3){
                    swal.close();
                    $('#email_login').val($("#email").val());
                    $('#modal_login').modal("show");
                    $('#btn_compra_submit').addClass('btn-error').removeClass('btn-warning btn-info').html('ERRO! FAÇA O SEU LOGIN');
                }

            },
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json'
        });

        return false;

    });

});

function mascaraTexto(evento, mascara){
    var campo, valor, i, tam, caracter;

    if (document.all) // Internet Explorer
        campo = evento.srcElement;
    else
        campo= evento.target;
    valor = campo.value;
    tam = valor.length;
    for(i=0;i<mascara.length;i++){
        caracter = mascara.charAt(i);
        if(caracter!="9")
            if(i<tam & caracter!=valor.charAt(i))
                campo.value = valor.substring(0,i) + caracter + valor.substring(i,tam);
    }
}

$("#telefone, #telefone2").keydown(function () {
    //Recebe o elemento ativo
    var focus = $(document.activeElement);
    //Timeout para pegar o valor do campo depois do evento, sem ele, o valor Ã© testado antes do evento ser finalizado
    setTimeout(function () {
        //Se o campo focado Ã© algum dos 3 campos de telefone, aplica a mÃ¡scara de acordo
        if (focus.attr('id') == "telefone" || focus.attr('id') == "telefone2") {
            if (focus.val().length <= 14) {
                focus.unmask();
                focus.mask("(00) 0000-00009");
            }
            else {
                focus.unmask();
                focus.mask("(00) 00000-0000");
            }
        }
    }, 10);
});


function abreModal(code){
    var callback = {
        success : function(transactionCode) {
            //Insira os comandos para quando o usuÃ¡rio finalizar o pagamento.
            //O cÃ³digo da transaÃ§Ã£o estarÃ¡ na variÃ¡vel "transactionCode"
            console.log("Compra feita com sucesso, cÃ³digo de transaÃ§Ã£o: " + transactionCode);
            swal("SUCESSO!","Recebemos a interaÃ§Ã£o com o pagseguro, apÃ³s a confirmaÃ§Ã£o do pagamento vocÃª irÃ¡ receber um e-mail de confirmaÃ§Ã£o da sua inscriÃ§Ã£o.","success");
            setTimeout(function(){
                swal("ATENÃ‡Ã€O","VocÃª serÃ¡ redircionado para a pÃ¡gina do usuÃ¡rio!","success");
            }, 2000);
            setTimeout(function(){
                window.location = 'minhas_inscricoes.php';
            }, 2600);
        },
        abort : function() {
            swal("SESSÃƒO DE PAGAMENTO FINALIZADA", "VocÃª encerrou a sessÃ£o de pagamento!","error");
            setTimeout(function(){
                swal("ATENÃ‡Ã€O","VocÃª serÃ¡ redircionado para a pÃ¡gina do usuÃ¡rio!","info");
            }, 1000);
            setTimeout(function(){
                window.location = 'minhas_inscricoes.php';
            }, 2000);
        }
    };
    //Chamada do lightbox passando o cÃ³digo de checkout e os comandos para o callback
    var isOpenLightbox = PagSeguroLightbox(code, callback);
    // Redireciona o comprador, caso o navegador nÃ£o tenha suporte ao Lightbox
    if (!isOpenLightbox){
        location.href="https://pagseguro.uol.com.br/v2/checkout/payment.html?code="+ code;
        //window.location  = 'https://pagseguro.uol.com.br/v2/checkout/payment.html?code='+ code;
    }
}

$('a[name="btn_menu"]').click(function(event){
    event.preventDefault();
    var href = $(this).attr('href');
    $([document.documentElement, document.body]).animate({
        scrollTop: ($(href).offset().top) - 30
    }, 1000);
});


$('.add_linha_form').click(function (event) {
    event.preventDefault();
    var count = 1;
    $('.select-data-maratona').each(function() {
        count++;
    });
    $('#load_form_clone').append($('<div>').load('inc_form.php?id='+count));
    $('#remover_data').show();

    setTimeout(function () {
        calcula_valor_total();
    },500);

});

$('.remove_linha_form').click(function (event) {
    event.preventDefault();
    var id = 0;
    $(".select-vagas-maratona").each(function() {
        id = $(this).attr('data-id');
    });
    $("#row_event_form_"+id).html("");
    $("#row_table_"+id).html("");
    calcula_valor_total();

});


$('.select-data-maratona').change(function (e) {
    e.preventDefault();
    var id = $(this).attr("data-id");
    var txt_option_date = return_html_txt_date(id);
    $("#table_date_"+id).html(txt_option_date);
});

// $('.select-vagas-maratona').change(function (e) {


$('.select-vagas-maratona').change(function (e) {
    e.preventDefault();
    var vagas = $(this).val();
    var id = $(this).attr("data-id");
    var valor = '100.00';
    var total = eval(valor) * eval(vagas);
    var txt = vagas+' x R$ '+valor+' = R$ <span class="mask-money">'+total+'.00</span>';
    var id_seletor_html = '#valor_maratona_item_'+id;
    $(id_seletor_html).html(txt);

    // var txt_option_date = return_html_txt_date(id);
    // $("#table_date_"+id).html(txt_option_date);

    $("#table_vagas_"+id).html(vagas+" VAGA(S)");
    $("#table_total_"+id).html('R$: <span class="mask-money">'+total+'.00</span>');

    calcula_valor_total();

    $('.mask-money').mask('#.##0,00', {reverse: true});



});

function return_html_txt_date(id){

    var id_select = '#data_maratona'+id;
    var retorno = "";

    $(id_select+" option:selected").each(function() {
        retorno = $(this).html();
    });
    return retorno;
}


function calcula_valor_total(){
    var total_vagas = 0;
     var vagas = 0;
    $(".select-vagas-maratona option:selected").each(function() {
        vagas = $(this).val();
        total_vagas = eval(total_vagas) + eval(vagas);
    });
    var total = eval(total_vagas) * eval(100);
    $('#total_geral').html('R$: <span class="mask-money">'+total+'.00</span>');
}

