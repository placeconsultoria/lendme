$(document).ready(function() {

    // $("#simulador_step_1").hide();
    // $("#simulador_step_5_ligar_depois").show();

    $("#btn_simulador_direct").click(function(e) {
        e.preventDefault();
        var Amortizacao = retornaStatusTipoParcela();
        var prazo =  parseFloat($('#range_prazo').val()) * 12;
        var param = {
            "ValorImovel": $("#valor_imovel").val(),
            "ValorFinanciamento": $("#valor_desejado").val(),
            "Idade": $("#range_idade").val(),
            "Prazo": prazo,
            "Amortizacao": Amortizacao
        };
        var json_param = JSON.stringify(param);

        $.ajax({
            url: "ajax.php?acao=simulador",
            headers: {
                "Content-Type": "application/json"
            },
            type: "POST",
            data: json_param ,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.status == true){
                    window.location = 'simulador.php';
                }
            },
            error: function(e) {

            }

        });

    });


    $('#frm_operacoes_estruturadas').submit(function(){
        swal("Recebemos o seu contato!","Em breve, nosso time ligará para você.","success");
        return false;
    });

    $("#btn_fazer_login").click(function (e) {
        e.preventDefault();
        $(this).prop("disabled",true).html("<i class='fa fa-spinner fa-spin'></i> PROCESSANDO...");
        $('.box_error_login').hide();

        var param = {
            "Email": $("#frm_login_email").val(),
            "Senha": ($("#frm_login_senha").val())
        };
        var json_param = JSON.stringify(param);
        $.ajax({
            url: "https://189.7.89.173:8107/api/Proposta/LoginExterno",
            headers: {
                "Content-Type": "application/json"
            },
            type: "POST",
            data: json_param ,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (data) {
                $(this).prop("disabled",true).html("SUCESSO!").addClass("btn-success");
                setTimeout(function(){
                    window.open(data.retorno.Link, '_blank');
                }, 3000);

            },
            error: function(e) {
                $('.box_error_login').show();
                $("#btn_fazer_login").prop("disabled",false).html("FAZER LOGIN").removeClass("btn-success");
            }
        });

    });


    $('#frm_seja_agente').submit(function(){

        var param = {
            "nome": $("#parceiro_empresa").val(),
            "CNPJ": ($("#parceiro_cnpj").val()),
            "NomeRepresentante": $("#parceiro_representante").val(),
            "CPF": ($("#parceiro_cpf").val()),
            "CEP": ($("#parceiro_cep").val()),
            "Email": $("#parceiro_email").val(),
            "celular": ($("#parceiro_celular").val()),
            "ComoConheceu": $("#parceiro_conheceu").val()
        };

        var json_param = JSON.stringify(param);

        $.ajax({
            url: "https://189.7.89.173:8107/api/Proposta/CadastroParceiro",
            headers: {
                "Content-Type": "application/json"
            },
            type: "POST",
            data: json_param ,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (data) {
                swal("Agradecemos o seu interesse!","Em breve, nosso time entrará em contato com você.","success");
            },
            error: function(e) {
                console.log(JSON.stringify(e));
                var erros = e.responseText;
                var obj = JSON.parse(erros);
                console.log(JSON.stringify(obj.Erros));
                $('.box_error_login').
                $('.text_error_login').html(obj.Erros);
            }

        });

        // {
        //
        //     "Nome" : "Parceiro Nome",
        //
        //     "CNPJ" : 84576011000104,
        //
        //     "NomeRepresentante" : "Representante Nome",
        //
        //     "CPF" : 56681262007,
        //
        //     "CEP" : 04547005,
        //
        //     "Email" : "cliente@teste.com",
        //
        //     "Telefone" : 11999999999,
        //
        //     "celular" : 11999999999,
        //
        //     "ComoConheceu" : "Redes Sociais"
        //
        // }

        return false;

    });

    $('a[name="btn_login"]').click(function (e) {
        e.preventDefault();
        $("#modal_login").modal("show");
    });

    $("#btn_termos_de_uso_show").click(function (e) {
        e.preventDefault();
        $("#termos_de_uso").show();
    });

    $(".mask-cpf").mask("999.999.999-99");
    $(".mask-date").mask("99/99/9999");
    $(".mask-cep").mask("99999-999");
    $(".mask-cnpj").mask("99.999.999/9999-99");

    $('#link_redirect').click(function (e) {
        e.preventDefault();
        var href = $(this).attr("href");
        window.open(href, '_blank');
    });

    $('a[name="btn_step_1"]').click(function (e) {
        e.preventDefault();
        $("#simulador_step_1").hide();
        $("#simulador_step_2").show();
    });

    $('#btn_step_2').click(function (e) {
        e.preventDefault();
        //frm_pre_cadastro_nome // frm_pre_cadastro_email // frm_pre_cadastro_celular
        var nome = $("#frm_pre_cadastro_nome").val();
        var email = $("#frm_pre_cadastro_email").val();
        var celular = $("#frm_pre_cadastro_celular").val();
        var erro = 0;
        var valida_email = emailIsValid(email);

        if(nome == "" || nome.length <= 10 || nome == null){
             erro = 1;
             $(".text_info_frm_pre_cadastro_nome").html("<i class='fa fa-info-circle'></i> Você precisa inserir um nome.").addClass("text-danger");
        }
        if(email == "" || email == null || valida_email == false){
            erro = 1;
            $(".text_info_frm_pre_cadastro_email").html("<i class='fa fa-info-circle'></i> Você precisa inserir um e-mail válido.").addClass("text-danger");
        }
        if(celular == "" || celular.length < 15 || celular == null){
            erro = 1;
            $(".text_info_frm_pre_cadastro_celular").html("<i class='fa fa-info-circle'></i> Você precisa inserir um celular válido.").addClass("text-danger");
        }

        if(erro == 1){
            return false;
        }else{
            $("#simulador_step_2").hide();
            $("#simulador_step_3").show();
        }

    });

    $('#btn_step_3').click(function (e) {
        e.preventDefault();
        var cpf = $("#frm_pre_cadastro_cpf").val();
        var nascimento = $("#frm_pre_cadastro_nascimento").val();
        var cep = $("#frm_pre_cadastro_cep").val();
        var validaCpf = cpfIsValid(cpf);
        var erro = 0;

        nascimento = moment(nascimento, "DD/MM/YYYY", true);


        if(cpf == "" || cpf == null || validaCpf == false){
            erro = 1;
            $(".text_info_frm_pre_cadastro_cpf").html("<i class='fa fa-info-circle'></i> Você precisa inserir um CPF válido.").addClass("text-danger");
        }
        if(nascimento == "" || nascimento == null || nascimento.isValid() == false){
            erro = 1;
            $(".text_info_frm_pre_cadastro_nascimento").html("<i class='fa fa-info-circle'></i> Você precisa informar uma data de nascimento válida.").addClass("text-danger");
        }
        if(cep == "" || cep.length < 9){
            erro = 1;
            $(".text_info_frm_pre_cadastro_cep").html("<i class='fa fa-info-circle'></i> Você precisa inserir um CEP valído.").addClass("text-danger");
        }
        if(erro == 1){
            return false;
        }else{
            $("#simulador_step_3").hide();
            $("#simulador_step_4").show();
        }

    });

    //

    $('#btn_step_4').click(function (e) {
        e.preventDefault();
        var imovel = $("#frm_pre_cadastro_imovel").val();
        var erro = 0;

        if(imovel == 0){
            erro = 1;
            $(".text_info_frm_pre_cadastro_imovel").html("<i class='fa fa-info-circle'></i> Você precisa selecionar o tipo do imóvel.").addClass("text-danger");
        }

        if(erro == 1){
            return false;
        }else{
            $('#btn_step_4').prop("disabled",true).html('<i class="fa fa-spinner fa-spin"></i> AGUARDE, PROCESSANDO...');
            EnviaApiPreCadastro();
        }

    });

    $("#frm_pre_cadastro_renda").ionRangeSlider({
        type: "single",
        grid: true,
        min: 1000,
        max: 60000,
        from: 100,
        to: 100,
        prefix: "R$"
    });


});


$('a[name="btn_menu"]').click(function(e){
    e.preventDefault();
    var href = $(this).attr('href');
    $([document.documentElement, document.body]).animate({
        scrollTop: ($(href).offset().top) - 30
    }, 1000);
});

function SimuladorApi(){
    var Amortizacao = retornaStatusTipoParcela();
    var prazo =  parseFloat($('#range_prazo').val()) * 12;
    var param = {
        "ValorImovel": $("#valor_imovel").val(),
        "ValorFinanciamento": $("#valor_desejado").val(),
        "Idade": $("#range_idade").val(),
        "Prazo": prazo,
        "Amortizacao": Amortizacao
    };
    var json_param = JSON.stringify(param);

    $.ajax({
        url: "https://189.7.89.173:8107/Api/Proposta/Simulador",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: json_param ,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (data) {

            data = data.retorno;
            $("#r_v_p").html(numberToReal(data.ValorParcela));
            $("#r_cte").html(data.CET.toFixed(2)+'%');
            $(".valores_parcela").show();
            $(".erro_simulador").hide();
        },
        error: function(e) {
            console.log(JSON.stringify(e));
            var erros = e.responseText;
            var obj = JSON.parse(erros);
            // alert(obj.Erros);
            // console.log(JSON.stringify(obj.Erros));
            var text_erro = obj.Erros;
            $(".valores_parcela").hide();
            $(".erro_simulador").show();
            $("#text_error_api").html(obj.Erros);
        }

    });
}

function retornaStatusTipoParcela(){
    var status = "";
    if($("#forma_parcela").prop("checked") == true){
        status = "price"
    }else{
        status = "sac";
    }
    return status
}



function EnviaApiPreCadastro(){

    var nome = $("#frm_pre_cadastro_nome").val();
    var email = $("#frm_pre_cadastro_email").val();
    var celular = $("#frm_pre_cadastro_celular").val();
    var cpf = $("#frm_pre_cadastro_cpf").val();
    var nascimento = $("#frm_pre_cadastro_nascimento").val();
    nascimento = moment(nascimento).format('YYYY-MM-DD');
    var cep = $("#frm_pre_cadastro_cep").val();
    var imovel = $("#frm_pre_cadastro_imovel").val();
    var renda = $("#frm_pre_cadastro_renda").val();
    var Amortizacao = retornaStatusTipoParcela();
    var prazo =  parseFloat($('#range_prazo').val()) * 12;

    var param = {
        "ValorImovel": $("#valor_imovel").val(),
        "ValorFinanciamento": $("#valor_desejado").val(),
        "Idade": $("#range_idade").val(),
        "Prazo": prazo,
        "Amortizacao": Amortizacao,
        "Nome" : nome,
        "Email" : email,
        "Celular" : retornaNumeros(celular),
        "CPF" : retornaNumeros(cpf),
        "CNPJParceiro" : null,
        "CEP" : retornaNumeros(cep),
        "TipoImovel" : imovel,
        "DataNascimento" : nascimento,
        "RendaMensal" : renda,
        "AceiteTermo" : true,
        "ChecagemIdentidade" : true

    }

    var json_param = JSON.stringify(param);
    $.ajax({
        url: "https://189.7.89.173:8107/Api/Proposta/PreCadastro",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: json_param ,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (data) {

            var sucesso = data.retorno.Sucesso;
            var status = data.retorno.Status;

            $("#simulador_step_4").hide();

            if(status == 2 && sucesso == true){
                $('.input-nome').html(nome);
                $("#simulador_step_5_ligar_depois").show();
            }else if(status == 3 && sucesso == true){
                $('.input-nome').html(nome);
                $("#simulador_step_5_reprovado").show();
            }else if(status == 1 && sucesso == true) {
                $('.input-nome').html(nome);
                $("#simulador_step_5_aprovado").show();
                $("#email_senha_cad").val(email);

                var token_resp = data.retorno.Token;
                var valor_parcela_resp =  data.retorno.DadosSimulacao.ValorParcela;
                var valor_imovel_resp =  data.retorno.DadosSimulacao.ValorImovel;
                var valor_financamento_resp =  data.retorno.DadosSimulacao.ValorFinanciamento;
                var prazo_resp =  data.retorno.DadosSimulacao.Prazo;
                var idade_resp =  data.retorno.DadosSimulacao.Idade;
                var juros_mensal_resp =  data.retorno.DadosSimulacao.JurosMensal;
                var amortizacao_resp =  data.retorno.DadosSimulacao.Amortizacao;
                var ltv_resp =  data.retorno.DadosSimulacao.LVT;
                var iof_resp =  data.retorno.DadosSimulacao.IOF;
                var prestacaoTotal_resp =  data.retorno.DadosSimulacao.PrestacaoTotal;

                $("#token_response").val(token_resp);
                $("#token_resp").html(token_resp);
                $("#valor_parcela_resp").html(numberToReal2(valor_parcela_resp));
                $("#valor_imovel_resp").html(numberToReal2(valor_imovel_resp));
                $("#valor_financamento_resp").html(numberToReal2(valor_financamento_resp));
                $("#prazo_resp").html(prazo_resp);
                $("#idade_resp").html(idade_resp);
                $("#juros_mensal_resp").html(juros_mensal_resp);
                $("#amortizacao_resp").html(amortizacao_resp);
                $("#ltv_resp").html(ltv_resp.toFixed(2));
                $("#iof_resp").html((numberToReal2(iof_resp)));
                $("#prestacaoTotal_resp").html(numberToReal2(prestacaoTotal_resp));
            }else if(sucesso != true){

                $("#simulador_step_4").hide();
                $("#simulador_cadastro_error").show();
                $('#btn_step_4').prop("disabled",false).html('Prosseguir <i class="fa fa-arrow-right"></i>');
            }

        },
        error: function(e) {
            console.log(JSON.stringify(e));
            var erros = e.responseText;
            var obj = JSON.parse(erros);
            console.log(JSON.stringify(obj.Erros));
            $("#simulador_step_4").hide();
            $("#simulador_cadastro_error").show();
            $('.text_error_cadastro').html(obj.Erros);
            $('#btn_step_4').prop("disabled",false).html('Prosseguir <i class="fa fa-arrow-right"></i>');

        }

    });

    $(".btn_simulador_prosseguir_1").click(function (e) {
        e.preventDefault();
        $("#simulador_step_5_aprovado").hide();
        $("#simulador_step_5_aprovado_dados").show();
    });

    $(".btn_simulador_prosseguir_2").click(function (e) {
        e.preventDefault();
        $("#simulador_step_5_aprovado_dados").hide();
        $("#simulador_step_5_senha").show();
    });

    $("#btn_simulador_prosseguir_3").click(function (e) {
        e.preventDefault();
        var email  = $("#email_senha_cad").val();
        var senha1 = $("#senha_1_cad").val();
        var senha2 = $("#senha_2_cad").val();
        var token  = $("#token_response").val();

        if(senha1 != senha2){
            swal("Atenção","As duas senhas não conferem.","warning");
            return false;
        }else if(senha1.length < 6 || senha2.length < 6 ){
            swal("Atenção","A senha precisa ter no mínimo 8 dígitos.","warning");
            return false;
        }else{
            var param = {
                "Email": email,
                "Token": token,
                "Senha": senha1
            }
            CadastrarSenhaApi(param);
        }
    });

    $(".reset_simulador").click(function (e) {
        e.preventDefault();
        $("#simulador_cadastro_error").hide();
        $("#simulador_step_1").show();
    });

    $(".reset_simulador2").click(function (e) {
        e.preventDefault();
        $("#simulador_cadastro_error").hide();
        $("#simulador_step_2").show();
    });
}


function CadastrarSenhaApi(param){
    $("#btn_simulador_prosseguir_3").prop("disabled",true).addClass("btn-warning").html('<i class="fa fa-spinner fa-spin"></i> AGUARDE, PROCESSANDO...');
    var json_param = JSON.stringify(param);
    $.ajax({
        url: "https://189.7.89.173:8107/api/Proposta/AlterarSenha",
        headers: {
            "Content-Type": "application/json"
        },
        type: "POST",
        data: json_param ,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (data) {
            console.log(data);

            if(data.retorno.Sucesso == true){
                $("#btn_simulador_prosseguir_3").hide();
                $(".link_biometria").attr("href",data.retorno.Link);
                $("#simulador_step_5_senha").hide();
                $("#simulador_step_5_senha_resp").show();

                setTimeout(function(){
                    window.open(data.retorno.Link, '_blank');
                }, 3000);
            }

        },
        error: function(e) {
            console.log(JSON.stringify(e));
            var erros = e.responseText;
            var obj = JSON.parse(erros);
            console.log(JSON.stringify(obj.Erros));
            // var text_erro = obj.Erros;
            // $(".valores_parcela").hide();
            // $(".erro_simulador").show();
            // $("#text_error_api").html(obj.Erros);
        }

    });
}


$("#frm_pre_cadastro_celular, #telefone2").keydown(function () {
    //Recebe o elemento ativo
    var focus = $(document.activeElement);
    //Timeout para pegar o valor do campo depois do evento, sem ele, o valor Ã© testado antes do evento ser finalizado
    setTimeout(function () {
        //Se o campo focado Ã© algum dos 3 campos de telefone, aplica a mÃ¡scara de acordo
        if (focus.attr('id') == "frm_pre_cadastro_celular" || focus.attr('id') == "telefone2") {
            if (focus.val().length <= 14) {
                focus.unmask();
                focus.mask("(00) 0000-00009");
            }
            else {
                focus.unmask();
                focus.mask("(00) 00000-0000");
            }
        }
    }, 10);
});

function emailIsValid (email) {
    return /\S+@\S+\.\S+/.test(email)
}

function cpfIsValid(cpf){
    cpf = cpf.replace(/\D/g, '');
    if(cpf.toString().length != 11 || /^(\d)\1{10}$/.test(cpf)) return false;
    var result = true;
    [9,10].forEach(function(j){
        var soma = 0, r;
        cpf.split(/(?=)/).splice(0,j).forEach(function(e, i){
            soma += parseInt(e) * ((j+2)-(i+1));
        });
        r = soma % 11;
        r = (r <2)?0:11-r;
        if(r != cpf.substring(j, j+1)) result = false;
    });
    return result;
}

function retornaNumeros(str){
    return str.replace(/[^\d]+/g,'');
}


function numberToReal2(numero) {
    var numero = numero.toFixed(2).split('.');
    numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
}

function numberToReal(numero) {
    var numero = numero.toFixed(2).split('.');
    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
}