<?php
/**
 * Created by PhpStorm.
 * User: squal
 * Date: 03/04/2017
 * Time: 14:24
 */
class TopoHtml
{
    public $titulo;
    public $chaves;
    public $descricao;
    public $imagem;
    public $url;

    public function CriarHeadHtml()
    {
        $html = ' <!doctype html>
        <html lang="pt-BR">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="Content-Language" content="pt-br" />
            <meta http-equiv="Cache-Control" content="no-cache, no-store" />
            <meta http-equiv="Pragma" content="no-cache, no-store" />
            <meta http-equiv="expires" content=-1>
            <meta http-equiv="content-type" content="text/html; charset=utf-8" />
            <meta name="author" content="QEASY" />
            <meta name="description" content="PEDALE BIKES - CURITIBA">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="keywords" content="bicicletas curitiba, bicicletas alta performance curitiba, biclicletas specialized curitiba, bike, bikes, biciclieta, bicicletas, bike road, bikes road, motainbikes, montain bikes, montaibike, bike mtb, bikes mtb, bicicleta road, bicicletas road, road bike, road bikes, bike urbana, bikes urbana, bicicleta urbana, bicicletas urbanas, bike hibrida, bikes hibridas, bicicleta hibrida, bicicletas hibridas, bike eletrica, bikes eletrica, bicicleta eletrica, bicicletas eletrica, bike gravel, bikes gravel, bike full supension, bikes full suspension, bike full, bike tt, bikes tt, bike time trial, bikes time trial, tarmac sl4, tarmac sl5, tarmac sl6, roubaix, ruby,allez, allez sprint, venge, diverge, cross trail, crux, s-works, specialized, specialized s-works, levo, turbo levo, creo, turbo creo, epic, epic ht, chisel, rock hooper, Shimano, sram, fsa, tourney, altus, alivio, acera, deore, deore xt, slx, xtr, claris, sora, tiagra, 105, ultegra, dura ace, di2, x5, x7, x9 ,nx, gx, gx eagle, x1, xx, xx1, xx1 eagle, roupa ciclismo, roupas ciclismo, bretelle, bretelles, jersey, jerseys, sapatilha, sapatilhas, luva, luvas, capacete, selim, capacetes, capacete asian size, capacete asiatico, meia, meias, palmilha, palmilhas, acessorios bike, acessorio bike, suporte de caramanhola, camara, valvula presta, ciclocomputador, ciclo gps, computador bike, computador bicicletas, assitencia bike, assistencia tecnica bike, assistencia tecnica bikes, assistencia brain, manutenção brain, manutenção specialized brain, specialized brain, suspensão brain, brain, specialized, brain specialized, regulagem bike, regulagem bikes, ajuste bike, ajuste bikes, regulagem bicicleta, regulagem bicicletas, ajuste bicicleta, ajuste bicicletas, vestuario ciclistico, retul, bike fit retul, melhor bike fit curitiba, curitiba bike fit, ivo siebert, melhor bike fiter curitiba, bike portella, melhor bike fir em curitiba, retul, agencia bicicleta, bike alla carte, pedale bikes, bike tech, muller bikes curitiba, hunger bikes, brychta bikes,hunger bikes, bike4u, bravo bikes, heuse bikes,bike batel,ziraldo bikejamur bikes, decathlon barigui,multiciclo bike, centauro, baron bikes giros bike shop,cicles jaime, strong bike, gtes ciclo, cicles jahn, bike brothers, hain e cia ltda,bk bikes, velo club curitiba,kf bikes,cernunnos bikes, tribo esporte, ciclies carmon, euro bike magazine, klemba bikes, cicles lago, Nova bike, Suspensão fox, Suspensões fox, suspensão rock shox, suspensões rock shox, future shock, future shock specialized, future shock rear, suspensão FSR, FSR Specialized, para specialized, peças para specialized, peças de reposição specialized, manutenção specialized, revisão specialized, assitencia specialized, manutenção specialized brain, brain manutenção, rolo para treino ciclismo, rolo para treino bike,bravo bikes, avenida sete de setembro 6200, bravobikes, av. sete de setembro 6200, tel: 3343-6200, 33426200, 80250-205, bike4u, alexandre berger, alessandro D\'Andrea, Giu Barros, #ridewithgiu, bebravo, oculos %100, bebravobrasil, bravobikesbrasil, bravo bikes telefone, João_Lucas_mtb, João Lucas, revisão bravo premium">
            <!-- Document title -->
            <title>PEDALE BIKES - CURITIBA</title>
           
    
        ';
        return $html;
    }

    static public function PreparaHead($arg)
    {
        $objConteudo = new Conteudo();
        $objConteudo->setId($arg);
        $dadosTag = $objConteudo->Editar();
        $topo_modulo = new TopoHtml();
        $topo_modulo->titulo = $dadosTag["titulo"];
        $topo_modulo->descricao = $dadosTag["descricao"];
        $topo_modulo->chaves = $dadosTag["tags"];
        $topo_modulo->imagem = URL_SITE .'admin/'. $dadosTag["imagem"];
        $topo_modulo->url = URL::getBase();
        echo $imprimir_topo = $topo_modulo->CriarHeadHtml();
    }



}
