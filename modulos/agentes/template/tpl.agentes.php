<section class="halfscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE ?>images/bg-b2b.jpg">
    <div class="container-fluid">
        <div class="container-fullscreen">
            <div class="text-middle text-center ">
                <h2  data-animate="fadeInUp" data-animate-delay="300"  class="m-t-0 font-title-1 text-white text-lg">SEJA NOSSO AGENTE!</h2>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-20">
                    Tenha acesso a nossa <strong>plataforma digital, com toda a infraestrutura tecnológica, financeira e operacional para gestão de suas indicações e  contratos</strong> (simulações, portal, relatórios detalhados, serviços internos de despachantes e gestão de comissionamento online). Além de um suporte especializado de toda equipe comercial da LendMe, com <strong>treinamentos, materiais de prospecção e consultoria de vendas regionais</strong>. Aqui você faz a diferença! Temos um time dedicado para acompanhamento contínuo de todos os processos e atendimento personalizado.
                </p>
            </div>
        </div>
        <div class="row p-t-50">
            <div class="col-12 text-center">
                <div class="call-to-action call-to-action-border btn-roxo" style="height: 350px;">
                    <h2 class="font-title-1 color-azul-b2b text-uppercase">
                        REGISTRE SEU INTERESSE!
                    </h2>
                    <h3 class="text-white font-weight-normal">Cresça conosco! Com o objetivo de potencializar o acesso dos nossos produtos ao mercado, queremos gerar grandes oportunidades de negócio em home equity junto aos nossos parceiros. Um deles pode ser você!</h3>
                    <a class="btn btn-azul" href="#seja-nosso-agente" name="btn_menu">TORNE-SE UM AGENTE!</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section  class="background-grey p-b-100 ">
    <div class="container">
        <div class=" text-center">
            <h1 class="font-title-1 text-medium text-uppercase color-azul-b2b"  >Benefícios em ser um Agente LendMe</h1>
        </div>
        <div class="row p-t-100">

            <div class="col-md-4 m-b-20">
                <h3><img src="<?= $URL_SITE ?>images/icon-bullet.jpg"> Acesso à plataforma digital e toda infraestrutura tecnológica</h3>
            </div>
            <div class="col-md-4  m-b-20">
                <h3><img src="<?= $URL_SITE ?>images/icon-bullet.jpg"> Acompanhamento "full time" da proposta do cliente: status, fases e pendências</h3>
            </div>
            <div class="col-md-4  m-b-20">
                <h3><img src="<?= $URL_SITE ?>images/icon-bullet.jpg"> Atendimento personalizado pelo consultor da sua região</h3>
            </div>
            <div class="col-md-4  m-b-20">
                <h3><img src="<?= $URL_SITE ?>images/icon-bullet.jpg"> Consultas detalhadas aos relatórios e gestão de comissionamentos online</h3>
            </div>
            <div class="col-md-4  m-b-20">
                <h3> <img src="<?= $URL_SITE ?>images/icon-bullet.jpg"> Treinamentos e materiais de prospecção</h3>
            </div>
            <div class="col-md-4  m-b-20">
                <h3><img src="<?= $URL_SITE ?>images/icon-bullet.jpg"> Suporte comercial especializado</h3>
            </div>
            <div class="col-md-4  m-b-20">
                <h3><img src="<?= $URL_SITE ?>images/icon-bullet.jpg">  Serviços internos de despachantes</h3>
            </div>
            <div class="col-md-4  m-b-20">
                <h3><img src="<?= $URL_SITE ?>images/icon-bullet.jpg">  Taxas transparentes e competitivas</h3>
            </div>
        </div>
    </div>
</section>
<section class="parallax background-overlay-dark p-t-50 p-b-50" style="background-image:url(<?= $URL_SITE ?>images/bg-b2b-full.jpg);" data-stellar-background-ratio="0.7" >
    <div class="container xs-text-center sm-text-center text-light">
        <div class="row">
            <div class="col-12 text-center ">
                <h2  class="font-title-1 m m-b-0 p-b-0 text-uppercase color-azul-b2b" style="   color: #FE691F !important;">ÁREA RESTRITA</h2>
                <h2>Faça o seu login na área restrita!</h2>
                <a class="btn btn-sm btn-outline btn-light btn-azul" href="#" name="btn_login">FAZER LOGIN</a>
            </div>
        </div>
    </div>
</section>


<section id="seja-nosso-agente" class="background-white p-b-50 ">
    <div class="container">
        <div class=" text-center">
            <h1 style="font-size: 30px" class="font-title-1 color-azul-b2b">SEJA NOSSO AGENTE</h1>
            <h3>PREENCHA OS DADOS E FAÇA SUA SOLICITAÇÃO</h3>
        </div>
        <div class="row">
            <div class="col-lg-10 p-t-50 center">
                <form id="frm_seja_agente" method="post">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="name" >NOME DA EMPRESA</label>
                            <input type="text" aria-required="true" name="empresa" id="parceiro_empresa" class="form-control required " placeholder="" required>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="email" >CNPJ</label>
                            <input type="text" aria-required="true" name="cnpj" id="parceiro_cnpj" class="form-control required mask-cnpj" placeholder="" required>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="email" >NOME DO REPRESENTANTE</label>
                            <input type="text" aria-required="true" name="representante" id="parceiro_representante" class="form-control required " placeholder="" required>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="subject" >CPF</label>
                            <input type="text" name="cpf" id="parceiro_cpf" class="form-control mask-cpf" placeholder="" required>
                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-3">
                            <label for="telefone" >TELEFONE / CELULAR</label>
                            <input type="text" id="parceiro_telefone" name="telefone" class="form-control" placeholder="" required>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="subject" >E-MAIL</label>
                            <input type="email" id="parceiro_email" name="email" class="form-control" placeholder="" required>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="subject" >CEP</label>
                            <input type="text"  name="parceiro_cep" id="parceiro_cep" class="form-control mask-cep" placeholder="" required>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="subject" >COMO NOS CONHECEU?</label>
                            <select id="parceiro_conheceu" name="conheceu" class="form-control">
                                <option value="SITE">SITE</option>
                                <option value="REDES SOCIAIS">REDES SOCIAIS</option>
                                <option value="INDICAÇÃO">INDICAÇÃO</option>
                                <option value="OUTROS">OUTROS</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">

                    </div>
                    <div class="row">
                        <div class="form-group center p-t-40">
                            <button i class="btn btn-roxo" type="submit" id="form-submit-agente"><i class="fa fa-handshake"></i> Enviar pré cadastro</button>
                        </div>
                    </div>

                    <div class="row erro_cadastro_parceiro" style="display: none;">
                        <div class="col-lg-12">
                            <div role="alert" class="alert alert-danger text-left alert-dismissible">
                                <i class="fa fa-warning"></i> Atenção: <span id="texto_error_parceiro"></span>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>