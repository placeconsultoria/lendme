<section class="background-white">
    <div class="container">
       <a href="<?= $URL_SITE?>home/"><img src="<?= $URL_SITE ?>images/logo.png" alt="lendme"></a>
    </div>
    <div class="row p-t-50">
        <div class="col-md-6 center">

            <form id="frm_pre_cadastro_cliente" class="p-r-30 p-l-30">

                <input id="valor_desejado" name="valor_desejado"  type="hidden" value="<?= $_SESSION["simulador"]["ValorFinanciamento"] ?>" />
                <input id="range_idade" name="range_idade" type="hidden"  value="<?= $_SESSION["simulador"]["Idade"] ?>" >
                <input id="range_prazo" name="range_prazo" type="hidden"  value="<?= $_SESSION["simulador"]["Prazo"] ?>" >
                <input id="valor_imovel" name="valor_imovel" type="hidden" value="<?= $_SESSION["simulador"]["ValorImovel"] ?>" />
                <input id="forma_parcela" name="form_parcela" type="hidden" value="<?= $_SESSION["simulador"]["Amortizacao"] ?>" >

                <div id="simulador_step_2" >
                    <div class="row">
                        <div class="col-lg-12 m-b-20">
                            <h3 class="font-title-1 color-roxo">MELHORES CONDIÇÕES PARA VOCÊ</h3>
                            <p class="lead font-paragrafo-1">Preencha os dados para uma simulação personalizada:</p>
                        </div>
                        <div class="col-lg-12 m-b-20">
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label for="frm_pre_cadastro_nome">NOME COMPLETO:</label>
                                    <input type="text" class="form-control" name="frm_pre_cadastro_nome" id="frm_pre_cadastro_nome" >
                                    <span class="text_info_frm_pre_cadastro_nome"></span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="frm_pre_cadastro_email">E-MAIL:</label>
                                    <input type="text" class="form-control" name="frm_pre_cadastro_email" id="frm_pre_cadastro_email" >
                                    <span class="text_info_frm_pre_cadastro_email"></span>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="frm_pre_cadastro_celular">CELULAR:</label>
                                    <input type="text" class="form-control" name="frm_pre_cadastro_celular" id="frm_pre_cadastro_celular" >
                                    <span class="text_info_frm_pre_cadastro_celular"></span>
                                </div>
                                <div class="form-group col-md-12 text-center">
                                    <button id="btn_step_2" type="button" class="btn text-uppercase">prosseguir <i class="fa fa-arrow-circle-right"></i> </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="simulador_step_3" style="display: none;">
                    <div class="row">
                        <div class="col-lg-12 m-b-20">
                            <h3 class="font-title-1 color-roxo">MELHORES CONDIÇÕES PARA VOCÊ</h3>
                            <p class="lead font-paragrafo-1">Preencha os dados para uma simulação personalizada:</p>
                        </div>
                        <div class="col-lg-12 m-b-20">
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label for="frm_pre_cadastro_cpf">CPF:</label>
                                    <input type="text" class="form-control mask-cpf" name="frm_pre_cadastro_cpf" id="frm_pre_cadastro_cpf" >
                                    <span class="text_info_frm_pre_cadastro_cpf"></span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="frm_pre_cadastro_nascimento">DATA DE NASCIMENTO:</label>
                                    <input type="text" class="form-control mask-date" name="frm_pre_cadastro_nascimento" id="frm_pre_cadastro_nascimento" placeholder="dd/mm/aaaa" >
                                    <span class="text_info_frm_pre_cadastro_nascimento"></span>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="frm_pre_cadastro_cep">CEP:</label>
                                    <input type="text" class="form-control mask-cep" name="frm_pre_cadastro_cep" id="frm_pre_cadastro_cep" >
                                    <span class="text_info_frm_pre_cadastro_cep"></span>
                                </div>


                            </div>
                            <div id="aviso_erro_idade" class="row" style="display: none;">
                                <div class="col-lg-12">
                                    <div role="alert" class="alert alert-danger text-left alert-dismissible">
                                        <i class='fa fa-info-circle'></i> Atenção:  A data de nascimento informada não corresponde com a idade informada. Levamores em consideração a data de nascimento informada para a base de cálculo
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12 text-center">
                                <button id="btn_step_3" type="button" class="btn text-uppercase">prosseguir <i class="fa fa-arrow-circle-right"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="simulador_step_4" style="display: none;">
                    <div class="col-lg-12 m-b-20">
                        <h3 class="font-title-1 color-roxo">MELHORES CONDIÇÕES PARA VOCÊ</h3>
                        <p class="lead font-paragrafo-1">Preencha os dados para uma simulação personalizada:</p>
                    </div>
                    <div class="col-lg-12 m-b-20">
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <!-- casa / apartamento/ comercial -->
                                <label for="frm_pre_cadastro_imovel">TIPO DO IMÓVEL:</label>
                                <select name="frm_pre_cadastro_imovel" id="frm_pre_cadastro_imovel">
                                    <option value="0">--SELECIONE--</option>
                                    <option value="apartamento">Apartamento Residencial</option>
                                    <option value="casa">Casa Residencial</option>
                                    <option value="comercial">Comercial</option>
                                </select>
                                <span class="text_info_frm_pre_cadastro_imovel"></span>
                            </div>
                            <div class="form-group col-md-7">
                                <label for="frm_pre_cadastro_renda">RENDA MENSAL:</label>
                                <input id="frm_pre_cadastro_renda" name="frm_pre_cadastro_renda" type="text" value="" />
                                <p>Renda Bruta familiar (você pode somar a sua renda ao rendimento de seus familiares)</p>
                            </div>
                        </div>
                        <div class="form-group col-md-12 text-center">
                            <button id="btn_step_4" type="button" class="btn text-uppercase">FINALIZAR <i class="fa fa-check-circle"></i> </button>
                        </div>
                        <div class="line m-b-20 m-t-10"></div>
                        <div class="form-row">
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input m-r-10" id="termo1" name="termo2" type="checkbox" checked="checked" >
                                    <label class="form-check-label p-l-10" for="gridCheck">
                                        Declaro que Li e aceito os termos de uso da LendMe.  <a href="https://lendme.com.br/termos-de-uso/" class="text-theme" target="_blank">Ler os termos de uso.</a>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input m-r-10" id="termo2" name="termo2" type="checkbox" checked="checked" >
                                    <label class="form-check-label p-l-10" for="gridCheck" style="text-transform: none!important; color: #d9d9d9">
                                    Autorizo a Instituição *e a LendMe a consultar os órgãos específicos de proteção ao crédito (Associações Comerciais, Serasa e demais órgão públicos ou privados e demais empresas de natureza similiar), central de risco de crédito do Bacen, em meu nome, inclusive CNPJ´s que estejam atrelados ao meu CPF, quando houver a minha participação na qualidade de sócio ou administrador de empresa, bem como a fornecer as informações de minhas operações de crédito, quando necessário e enquanto perdurar a minha relação com essa Instituição ou com a LendMe ou pelo período autorizado por Lei.
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input m-r-10" id="termo2" name="termo2" type="checkbox" checked="checked" >
                                    <label class="form-check-label p-l-10" for="gridCheck" style="text-transform: none!important; color: #d9d9d9">
                                    Autorizo a confecção de um cadastro com atualização periódica e guarda do histórico.<br>*BMP MONEY PLUS SOCIEDADE DE CRÉDITO DIRETO S.A., Instituição financeira, inscrita no CNPJ/MF sob n° 34.337.707/0001-00, e “BMP MONEY PLUS”; MONEY PLUS SCMEPP LTDA, Instituição financeira, inscrita no CNPJ sob o nº 11.581.339/0001-45.
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input m-r-10" id="termo2" name="termo2" type="checkbox" checked="checked" >
                                    <label class="form-check-label p-l-10" for="gridCheck" style="text-transform: none!important; color: #d9d9d9">
                                        Autorizo contatar o(s) Proponente(s) utilizando-se de e-mails, SMS, whatsapp, cartas e telefones acerca das informações envolvidas no processo de contratação de empréstimo com imóvel em garantia.
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input m-r-10" id="termo2" name="termo2" type="checkbox" checked="checked" >
                                    <label class="form-check-label p-l-10" for="gridCheck" style="text-transform: none!important;color: #d9d9d9">
                                        Aceito receber materiais publicitários e ofertas da LendMe ou de parceiros por whatsapp, email ou SMS.
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>


                <div id="simulador_cadastro_error" style="display: none;">
                    <div class="col-lg-12 text-center p-t-50">
                        <div class="icon-box text-center effect large error p-0 m-0">
                            <div class="icon"> <a href="#"  class="" target="_blank"><i class="fa fa-times"></i></a> </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center p-t-0 m-t-0">
                        <h3 class="font-paragrafo-1 text-uppercase"><span class="badge badge-danger"><i class="fa fa-info-circle"></i> OCORREU UM ERRO NO CADASTRO:</span> </h3>
                    </div>
                    <div class="col-lg-12 p-t-50 p-b-50">
                        <h4 class="font-paragrafo-1">
                            Erro(s) aparesentado(s):
                        </h4>
                        <p class="lead font-paragrafo-1  text-danger text_error_cadastro">

                        </p>
                    </div>

                    <div class="col-lg-12 text-center  p-b-50">
                        <a href="#" class="reset_simulador2 btn btn-warning">COMEÇAR DE NOVO</a>
                    </div>

                </div>

                <div id="simulador_step_5_aprovado" style="display: none;">
                    <div class="col-lg-12 text-center p-0 m-0">
                        <div class="icon-box text-center effect large light p-0 m-0">
                            <div class="icon"> <a href="#" class="btn_simulador_prosseguir_1"><i class="fa fa-check"></i></a> </div>
                        </div>
                    </div>
                    <div class="col-lg-12 m-b-20  text-center">
                        <h3 class="font-title-1 text-success m-0 p-0" data-animate="fadeInUp" data-animate-delay="200">PARABÉNS!</h3>
                        <h3 class="font-title-1 text-success m-0 p-0" data-animate="fadeInUp" data-animate-delay="400">VOCÊ FOI PRÉ SELECIONADO!</h3>
                    </div>

                    <div class="col-lg-12 p-b-10">
                        <p class="lead" data-animate="fadeInUp" data-animate-delay="400">
                            Parabéns, <span class="input-nome"></span>! Sua análise foi pré-aprovada e você pode receber a simulação com as condições completas e ainda mais adequadas ao seu perfil. Quer ver?
                        </p>
                    </div>

                    <div class="col-lg-12 text-center p-b-20" data-animate="fadeInUp" data-animate-delay="600">
                        <a href="#" id="btn_simulador_prosseguir_1" class="btn btn-success text-uppercase btn_simulador_prosseguir_1" >Prosseguir <i class="fa fa-arrow-right"></i> </a>
                    </div>

                </div>

                <div id="simulador_step_5_aprovado_dados" style="display: none;" data-animate="fadeInUp" data-animate-delay="400" >
                    <input type="hidden" id="token_response" />
                    <div class="col-lg-12 p-b-10">
                        <p class="lead text-center font-paragrafo-1">
                            <strong><span class="input-nome"></span></strong>, <br /> aqui estão os informações completas da sua simulação. Clique em prosseguir para iniciarmos o seu processo.
                        </p>
                    </div>
                    <div class="col-lg-12 text-center">
                        <a href="#" id="btn_simulador_prosseguir_2" class="btn btn-success text-uppercase btn_simulador_prosseguir_2" >Prosseguir <i class="fa fa-arrow-right"></i> </a>
                    </div>
                    <div class="line m-t-10 m-b-10" ></div>
                    <div class="col-lg-12 p-b-10">
                        <p class="font-paragrafo-1">
                            Valor informado do imóvel: R$: <span id="valor_imovel_resp"></span> <br />
                            Valor solicitado do crédito: R$: <span id="valor_financamento_resp"></span> <br />
                            Prazo para pagamento: <span id="prazo_resp"></span> Meses<br />
                            Taxa de Juros Mensal da Operação considerada:  <span id="juros_mensal_resp"></span> %<br />
                            Sistema de amortização: <span id="amortizacao_resp"></span><br />
                            LTV (Máx %): <span id="ltv_resp"></span><br />
                            IOF: <span id="iof_resp"></span><br />
                            Prestação Total: R$: <span id="prestacaoTotal_resp"></span><br />
                            CET: <span id="cet_final"></span> %<br />
                        </p>
                    </div>
                    <div class="line m-t-10 m-b-10" ></div>
                    <div class="col-lg-12 p-b-0 text-center">
                        <p class="lead font-paragrafo-1" >
                            Se tiver alguma dúvida, é só entrar em contato com nossos consultores
                        </p>
                    </div>
                    <div class="col-lg-12 text-center p-t-0 m-t-0" >
                    <!--<script type="text/javascript" src="https://app.contako.com.br/WidgetJSFixo.sikoni/?cadastro=403A34291B"> </script>-->
                    <a onclick="window.open('https://app.contako.com.br/Atendimento/?cadastro=4DCD58CD44', '_blank', 'width=900, height=640', 'false');" class="btn btn-warning btn-sm text-uppercase">quero falar com um consultor</a>
                    </div>
                    <div class="line m-t-10 m-b-10" ></div>

                    <div class="col-lg-12 p-b-0 text-center" >
                        <p class="lead font-paragrafo-1">
                            Vamos dar seguimento ao seu processo?
                        </p>
                    </div>
                    <div class="col-lg-12 text-center">
                        <a href="#" class="btn btn-success text-uppercase btn_simulador_prosseguir_2" >Prosseguir <i class="fa fa-arrow-right"></i> </a>
                    </div>
                </div>

                <div id="simulador_step_5_senha" style="display: none;">

                    <h3 class="color-roxo text-uppercase font-paragrafo-1">Cadastrando o seu acesso:</h3>
                    <p class="lead font-paragrafo-1">
                        <span class="input-nome"></span>, você tomou uma ótima decisão! <br />
                        Para que o processo aconteça o mais rápido possível, fique sempre atento às nossas comunicações, inclusive verificando sua caixa de spam para não correr o risco de perder nenhuma mensagem. <br />
                        Vamos agora ao próximo passo que é criar o seu acesso em nossa plataforma!
                    </p>
                    <div class="row">
                        <div class="line m-t-10 m-b-10"></div>
                        <div class="form-group col-md-4">
                            <label for="frm_pre_cadastro_cpf">E-MAIL:</label>
                            <input type="email" class="form-control"  id="email_senha_cad" >
                        </div>
                        <div class="form-group col-md-4">
                            <label for="frm_pre_cadastro_cpf">SENHA:</label>
                            <input type="password" class="form-control"  id="senha_1_cad" aria-describedby="senha" style="position:relative">
                            <small id="senha" class="text-muted" style="position:absolute; right:25px; bottom:8px">
                            <i class="fa fa-eye" id="eye" style="cursor:pointer"></i>
                            </small>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="frm_pre_cadastro_cpf">CONFIRME A SENHA:</label>
                            <input type="password" class="form-control"  id="senha_2_cad" aria-describedby="repete" style="position:relative">
                            <small id="repete" class="text-muted" style="position:absolute; right:25px; bottom:8px">
                            <i class="fa fa-eye" id="eye_2" style="cursor:pointer"></i>
                            </small>
                        </div>
                        <p class="lead font-paragrafo-1 center" style="font-size:10px; padding-bottom:10px" >
                            Senha com mínimo de 6 caracteres, pelo menos 1 letra maiúscula e 1 caractere especial
                        </p>
                        <div class="col-lg-12 text-center" data-animate="fadeInUp" data-animate-delay="200">
                            <a href="#" id="btn_simulador_prosseguir_3" class="btn btn-success text-uppercase" >Prosseguir <i class="fa fa-arrow-right"></i> </a>
                        </div>
                    </div>

                </div>

                <div id="simulador_step_5_senha_resp" class="p-t-50 p-b-50" style="display: none;">

                    <div class="col-lg-12 text-center p-0 m-0">
                        <div class="icon-box text-center effect large light p-0 m-0">
                            <div class="icon"> <a href="#"  class="btn_simulador_prosseguir_1 link_biometria" target="_blank"><i class="fa fa-user-check"></i></a> </div>
                        </div>
                    </div>
                    <div class="col-lg-12 m-b-20  text-center">
                        <h3 class="font-title-1 text-success m-0 p-0" data-animate="fadeInUp" data-animate-delay="200">PARABÉNS!</h3>
                        <h3 class="font-title-1 text-success m-0 p-0" data-animate="fadeInUp" data-animate-delay="400">CADASTRO REALIZADO COM SUCESSO!</h3>
                    </div>

                    <div class="col-lg-12 p-b-10">
                        <p class="lead text-center font-paragrafo-1" data-animate="fadeInUp" data-animate-delay="600">
                            Você será redirecionado para a etapa de biometria.
                        </p>
                    </div>

                    <div class="col-lg-12 text-center" data-animate="fadeInUp" data-animate-delay="800">
                        <a href="#" class="btn btn-success text-uppercase link_biometria" target="_blank"> <i class="fa fa-user-edit"></i> ACESSAR A PÁGINA DA BIOMETRIA </a>
                    </div>

                </div>


                <div id="simulador_step_5_ligar_depois" style="display: none;">

                    <div class="col-lg-12 text-center p-t-50">
                        <div class="icon-box text-center effect large warning p-0 m-0">
                            <div class="icon"> <a href="#"  class="" target="_blank"><i class="fa fa-phone"></i></a> </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center p-t-0 m-t-0">
                        <h3 class="font-paragrafo-1 text-uppercase"><span class="badge badge-warning"><i class="fa fa-info-circle"></i> Precisamos de algumas confirmações.</span> </h3>
                    </div>
                    <div class="col-lg-12 p-b-100">
                        <p class="lead font-paragrafo-1 ">
                            Oi, <span class="input-nome"></span>.
                            <br />
                            Precisamos tirar apenas algumas dúvidas antes de prosseguirmos, ok? <br />
                            Já já o nosso consultor entrará em contato com você.
                        </p>
                    </div>
                </div>

                <div id="simulador_step_5_reprovado" style="display: none;">
                    <div class="col-lg-12 text-center p-t-50">
                        <div class="icon-box text-center effect large error p-0 m-0">
                            <div class="icon"> <a href="#"  class="" target="_blank"><i class="fa fa-times"></i></a> </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center p-t-0 m-t-0">
                        <h3 class="font-paragrafo-1 text-uppercase"><span class="badge badge-danger"><i class="fa fa-times-circle"></i> Não podemos prosseguir</span> </h3>
                    </div>
                    <div class="col-lg-12 p-t-20 p-b-50">
                        <p class="lead font-paragrafo-1">
                            Oi, <span class="input-nome"></span>. Foram verificadas algumas pendências aqui. <br />
                            Em geral, os principais motivos de não prosseguimento costumam ser: região, renda mínima, idade  comportamento de crédito. <br />
                            Novas solicitações podem ser feitas dentro de 90 dias, desde que as eventuais pendências sejam sanadas. <br />

                        </p>
                    </div>
                </div>

                <div id="simulador_step_5_status4" style="display: none;">

                    <div class="col-lg-12 text-center p-t-50">
                        <div class="icon-box text-center effect large warning p-0 m-0">
                            <div class="icon"> <a href="#"  class="" target="_blank"><i class="fa fa-phone"></i></a> </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center p-t-0 m-t-0">
                        <h3 class="font-paragrafo-1 text-uppercase"><span class="badge badge-warning"><i class="fa fa-info-circle"></i> Precisamos de algumas confirmações.</span> </h3>
                    </div>
                    <div class="col-lg-12 p-b-100 p-t-50">
                        <p class="lead font-paragrafo-1 ">
                            Olá, <span class="input-nome"></span>.
                        </p>
                        <p class="lead font-paragrafo-1 ">
                            Obrigado por confiar na LendMe.
                        </p>
                        <p class="lead font-paragrafo-1 ">
                            Nossa equipe recebeu e já está analisando suas informações.
                        </p>
                        <p class="lead font-paragrafo-1 ">
                            Em breve, nós entraremos em contato para informar o resultado e as melhores condições para o seu empréstimo.
                        </p>
                        <p class="lead font-paragrafo-1 ">
                            Mas se, enquanto espera, você quiser falar com a gente, pode entrar em contato por email ou ligando para o telefone 0800 759 1000.
                        </p>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>
<script type="text/javascript">

function show() {
        var p = document.getElementById('senha_1_cad');
        p.setAttribute('type', 'text');
    }

    function hide() {
        var p = document.getElementById('senha_1_cad');
        p.setAttribute('type', 'password');
    }

    function show2() {
        var p = document.getElementById('senha_2_cad');
        p.setAttribute('type', 'text');
    }

    function hide2() {
        var p = document.getElementById('senha_2_cad');
        p.setAttribute('type', 'password');
    }

    var pwShown = 0;

    document.getElementById("eye").addEventListener("click", function () {
        if (pwShown == 0) {
            pwShown = 1;
            show();
        } else {
            pwShown = 0;
            hide();
        }
    }, false);

    var pwShown2 = 0;

    document.getElementById("eye_2").addEventListener("click", function () {
        if (pwShown2 == 0) {
            pwShown2 = 1;
            show2();
        } else {
            pwShown2 = 0;
            hide2();
        }
    }, false);
</script>

<!-- Event snippet for Website traffic conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-623082136/jq50CL_h0fIBEJj1jakC'});
</script>