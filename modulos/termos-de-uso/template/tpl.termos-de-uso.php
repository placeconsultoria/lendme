<section class="halfscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE ?>images/lendme-bg-2.jpg">
    <div class="container-fluid">
        <div class="container-fullscreen">
            <div class="text-middle text-center text-light">
                <h2  data-animate="fadeInUp" data-animate-delay="300"  class=" m-t-0 font-title-1 text-lg">TERMOS DE USO</h2>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 m-t-20"  data-animate="fadeInUp" data-animate-delay="400" >
                <h2  class="font-title-1 m-b-0 p-b-0 text-uppercase color-roxo">TERMOS E CONDIÇÕES DE USO DA PLATAFORMA LENDME – T&C</h2>
                <h3 class="font-title-1 m-b-0 p-b-0 color-roxo">Última atualização em 13 de agosto de 2020</h3>
                <div class="line"></div>

                <h2 style="font-size: 18px;" class="font-paragrafo-1 m-b-20">
                    Seja muito bem-vindo à plataforma LendMe!
                </h2>

                <p class="lead font-paragrafo-1">
                    Estes Termos e Condições de Uso – T&C constituem a expressão das disposições contratuais da LendMe.com (“Plataforma”, ou “Plataforma LendMe” ou “Website”) pertencente e operada pela Himov Negócios e Participações S/A. (“LendMe”), inscrita no CNPJ sob o nº 36.275.316/0001-33, com sede na Rua Gomes de Carvalho, 1.629, 1º andar, sala 103, Vila Olímpia, São Paulo/SP, CEP 04547-006.
                </p>

                <p class="lead font-paragrafo-1">
                    Para facilitar sua navegação em nossos T&C e garantir sua ampla compreensão e entendimento, logo abaixo está um índice bem bacana de cada um dos tópicos abordados em nossos T&C:
                </p>

                <p class="lead font-paragrafo-1">
                    1. PLATAFORMA <br />
                    2. ACESSO À PLATAFORMA, DISPONIBILIDADE DO SITE E CONTEÚDO<br />
                    3. PRIVACIDADE E SEGURANÇA<br />
                    4. ACEITAÇÃO DOS TERMOS DE USO<br />
                    5. CONDIÇÕES GERAIS<br />
                    6. CADASTRO E REGISTRO<br />
                    7. CONFIDENCIALIDADE<br />
                    8. COMUNICAÇÕES<br />
                    9. ALTERAÇÃO DOS TERMOS DE USO<br />
                    10. PROPRIEDADE INTELECTUAL<br />
                    11. RESCISÃO<br />
                    12. SOLICITAÇÃO DE PROPOSTAS OU COTAÇÕES<br />
                    13. DISPOSIÇÕES GERAIS

                </p>

                <p class="lead font-paragrafo-1">
                    <strong>POR FAVOR, LEIA ESTES TERMOS E CONDIÇÕES DE USO CUIDADOSAMENTE, COM ATENÇÃO, POIS O USO DA PLATAFORMA DEMONSTRA A SUA CONCORDÂNCIA COM ESTES TERMOS.</strong> Estes T&C, juntamente com a Política de Privacidade, descrevem os Termos e as Condições aplicáveis ao acesso e uso da Plataforma LendMe pelo usuário.
                </p>

                <p class="lead font-paragrafo-1">
                    Ao clicar em “Eu Aceito” ou ao navegar na Plataforma, você concorda com estes T&C. Fique ciente que as disposições aqui presentes regulamentarão a relação entre a Plataforma e o usuário. Caso você não concorde com todos os termos que seguem, não poderá utilizar ou acessar a Plataforma.
                </p>

                <p class="lead font-paragrafo-1">
                    A Plataforma LendMe poderá modificar estes T&C a qualquer momento, a seu exclusivo critério, mediante a publicação de T&C atualizados. Para sua comodidade, a data da última revisão é incluída em 13 de agosto de 2020. Recomendamos que sempre que acessar a Plataforma, fique atento às novas atualizações, pois o seu acesso e uso da Plataforma estarão vinculados por quaisquer alterações destes T&C.
                </p>

                <p class="lead font-paragrafo-1">
                    Sempre que houver menção aos termos “<u>Nós</u>” ou "<u>Nossos</u>” estaremos nos referindo à LendMe; bem como toda vez que houver menção aos termos “<u>Você</u>”, “<u>Usuário</u>”, “<u>seu</u>”, “<u>sua</u>”, estaremos nos referindo a você usuário, que está consentindo com estes T&C e com a Política de Privacidade para fazer uso e acesso à Plataforma.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">1. PLATAFORMA</h2>

                <p class="lead font-paragrafo-1">
                    1.1  A LendMe atua como correspondente bancário e possui uma plataforma online que facilita o acesso de produtos e serviços ofertados por instituições financeiras parceiras, para que os Usuários possam encontrá-los com mais facilidade (“Serviço” ou “Serviços”). A LendMe atua seguindo as diretrizes do disposto na Resolução CMN nº 3.954, de 24 de novembro de 2011, conforme alterada.
                </p>

                <p class="lead font-paragrafo-1">
                    1.2 A LendMe declara que não é uma instituição financeira, companhia de seguros, operadora de meios de pagamento ou bandeira de cartão de crédito.
                </p>

                <p class="lead font-paragrafo-1">
                    1.3  Atualmente, a LendMe atua como correspondente bancária da BMP MONEY PLUS SOCIEDADE DE CRÉDITO DIRETO S.A., Instituição financeira, inscrita no CNPJ/MF sob n° 34.337.707/0001-00, com sede na Avenida Paulista, nº 1.765, 1º andar, CEP 01311-200, São Paulo – SP (“<u>BMP MONEY PLUS</u>”) e MONEY PLUS SCMEPP LTDA., Instituição financeira, inscrita no CNPJ sob o nº 11.581.339/0001-45, com sede na Av. Paulista, nº 1.765, 1º andar, CEP nº 01311-200, São Paulo – SP (“<u>Money Plus Ltda</u>”) (“<u>Instituições Parceiras</u>”).
                </p>

                <p class="lead font-paragrafo-1">
                    1.4 Uma vez aceitos estes T&C pelos usuários, será realizado o cadastro. Apenas os usuários cadastrados poderão ter acesso aos Serviços da LendMe. Após o seu cadastro, a LendMe lhe fará algumas perguntas e solicitará algumas informações para que seja possível intermediar a sua solicitação de crédito junto à Instituição Financeira Parceira. No entanto, a operação de crédito é firmada diretamente com a Instituição Financeira Parceira, na qualidade de instituição autorizada a conceder crédito nos termos da legislação e regulamentação aplicáveis (“<u>Operação de Crédito</u>”). A LendMe não é parte da Operação de Crédito.
                </p>

                <p class="lead font-paragrafo-1">
                    1.5 A LendMe, desde já, se reserva ao direito de atualizar a Plataforma e suas aplicações correspondentes, a seu exclusivo critério e discricionariedade, podendo, eventualmente, modificar a dinâmica de utilização da Plataforma, bem como formalizar novas parcerias com outras instituições financeiras.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">2. ACESSO À PLATAFORMA, DISPONIBILIDADE DO SITE E CONTEÚDO</h2>

                <p class="lead font-paragrafo-1">
                    2.1 A LendMe empreenderá seus melhores esforços para garantir a maior disponibilidade possível de seu Website e seus Serviços, no entanto, a LendMe não é responsável por qualquer indisponibilidade do Website ou de seus Serviços, por qualquer período ou momento.
                </p>

                <p class="lead font-paragrafo-1">
                    2.2 O acesso ao Website pode ser interrompido ou ficar intermitente temporariamente, sem qualquer aviso prévio, em caso de falha de sistema, manutenção, atualização, alteração de sistemas, ou por motivos que escapem ao controle da LendMe.
                </p>

                <p class="lead font-paragrafo-1">
                    2.3 As ferramentas utilizadas para acessar à plataforma (computadores, smartphones, tablets, etcs) e programa (navegadores) necessários à navegação do Website, bem como o provimento adequado de todos os recursos de internet (como a conexão), sem exceção, bem como manter o ambiente do computador seguro, com uso de ferramentas disponíveis como antivírus e firewall, entre outras, atualizadas, de modo a contribuir na prevenção de riscos eletrônicos, são de inteira responsabilidade do Usuário.
                </p>

                <p class="lead font-paragrafo-1">
                    2.4 A LendMe, a seu exclusivo critério, poderá decidir interromper permanentemente o acesso ao Website e os Serviços ofertados. Nesta hipótese, porém, a LendMe se compromete a tomar providências para que os Serviços já iniciados sejam devidamente concluídos ou sejam adequadamente migrados, sem prejuízo às partes envolvidas.
                </p>

                <p class="lead font-paragrafo-1">
                    2.5 A LendMe, a seu exclusivo critério, poderá alterar o conteúdo do Website a qualquer momento, sem prévio aviso, bem como se esforçará para manter o conteúdo do Website atualizado e completo, livre de quaisquer defeitos ou vírus. Contudo, não se responsabiliza por esses e outros possíveis problemas.
                </p>

                <p class="lead font-paragrafo-1">
                    2.6 Para a utilização dos Serviços, o Usuário terá que fornecer informações adicionais, conforme descrito na Política de Privacidade, disponível em <a href="https://www.lendme.com.br/politica-de-privacidade">https://www.lendme.com.br/politica-de-privacidade</a> ) (“<u>Política de Privacidade</u>”).
                </p>

                <p class="lead font-paragrafo-1">
                    2.7 Dessa forma, ao utilizar o Website, o Usuário expressamente declara, garante e concorda, sob pena de cancelamento do cadastro e das demais providências, conforme legislação vigente, que não utilizará o Website ou os Serviços de forma que:
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (I) sejam tecnicamente danosos, incluindo, mas não se limitando, a vírus de computador, macros, “cavalos de Tróia”, worms, spywares, componentes maliciosos, dados corrompidos e outros programas ou dados de computador maliciosos ou que sejam projetados para interferir, interromper ou derrubar as operações normais de um computador ou do nosso sistema e das Instituições Parceiras.
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (II) infrinja ou viole os direitos de propriedade intelectual ou outros direitos da LendMe ou de qualquer outra pessoa ou entidade;
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (III) não enviará qualquer informação pessoal se for menor de 18 anos de idade;
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (IV) viole a segurança de qualquer rede de computador, ou quebre quaisquer senhas ou códigos de criptografia de segurança;
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (I) não transmitirá, carregará ou enviará do Website ou para o Website qualquer material que seja de cunho violento ou ameaçador, difamatório, obsceno, ofensivo, pornográfico, abusivo, passível de incitar qualquer forma de ódio racial, discriminatório, em violação de privacidade ou de dados pessoais de terceiro ou, ainda, para o qual não tenham sido obtidas pelo usuário todas as licenças e/ou aprovações necessárias;
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (II) constitua ou incite condutas que possam ser consideradas ilícitos criminais ou civis, que violem direitos de terceiros seja no Brasil ou no exterior ou que sejam ilegais sob qualquer outra forma;
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (V) infrinja qualquer lei, norma ou regulamento aplicável, ou coloque em risco a sua segurança ou de outros usuários.
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (VI) faça engenheira reversa, decompile, ou de outra forma tente obter o código-fonte ou ideias ou dados subjacentes do Website ou relacionados a este.
                </p>

                <p class="lead font-paragrafo-1">
                    2.8 Do mesmo modo, o Usuário declara que:
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (I) é responsável por providenciar conexão estável e suficiente por provedor de Internet de sua escolha e responsabilidade, assim como por manter seu computador seguro, através de ferramentas como antivírus, firewall, dentre outras, contribuindo, assim, para a prevenção de riscos cibernéticos e para o bom uso do Website e dos Serviços, reconhecendo que a contratação por meio eletrônico necessita de cuidados próprios.
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (II) está ciente de que todos e quaisquer conteúdos enviados e/ou transmitidos ao Website ou para nossas outras plataformas por qualquer outro usuário e/ou terceiros não representam, de nenhuma forma, a opinião ou visão da LendMe.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">3. PRIVACIDADE E SEGURANÇA</h2>

                <p class="lead font-paragrafo-1">
                    3.1 A LendMe se compromete a respeitar a privacidade dos Usuários nos termos de sua Política de Privacidade.
                </p>

                <p class="lead font-paragrafo-1">
                    3.2 Ao acessar o Website ou contratar os Serviços da LendMe, o Usuário declara estar ciente de que a LendMe poderemos coletar e armazenar informações sobre o Usuário para as finalidades informadas na Política de Privacidade, que incluem informações:
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (I) Do cadastro do Usuário realizado no Website ou das solicitações de cotações e propostas de Instituições Parceiras; e
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    (II) De Cookies. Cookies são identificações da interação com o Website ou publicidades que são transferidas para o aparelho do Usuário, visando reconhecê-lo na próxima navegação. Os cookies são utilizados para proporcionar uma melhor experiência no Website e viabilizar recursos personalizados como recomendações de infográficos, artigos, calculadoras, produtos e serviços financeiros, publicidades e informações adicionais de interesse do Usuário.
                </p>

                <p class="lead font-paragrafo-1">
                    3.3 A LendMe compromete-se por si e por seus colaboradores, sempre que aplicável, a atuar em conformidade com a Legislação vigente sobre proteção de dados relativos a uma pessoa física (“<u>Titular</u>”) identificada ou identificável (“<u>Dados Pessoais</u>”) e as determinações de órgãos reguladores/fiscalizadores sobre a matéria, em especial a Lei 13.709/2018 (“<u>Lei Geral de Proteção de Dados</u>”). Conforme informado na Cláusula 3.2. (ii) acima, a Plataforma utiliza o uso de Cookies e dá a seus Usuários a opção de aceitar expressamente, bem como ter acesso a mais informações através das informações descritas na Política de Privacidade.
                </p>
                <p class="lead font-paragrafo-1">
                    3.4 A LendMe também poderá fornecer os dados dos Usuários, obtidos por meio da utilização do Website, sempre que estiver obrigado a revelá-los, seja em virtude de disposição legal, ato de autoridade competente ou ordem judicial.
                </p>
                <p class="lead font-paragrafo-1">
                    3.5 Ressaltamos que a LendMe não solicita senha, dados de cartão de crédito ou dados bancários de seus clientes por e-mail, telefone ou qualquer outro canal de atendimento personalizado. Portanto, caso o Usuário receba qualquer comunicação com esse tipo de abordagem e conteúdo, solicitamos que não responda, desconsidere-o e, se possível, encaminhe o seu relato para [meajuda@lendme.com.br].
                </p>
                <p class="lead font-paragrafo-1">
                    3.6 O Usuário deve manter sua senha em segurança e não deve compartilhá-la com ninguém. O Usuário é o responsável por qualquer atividade associada à sua própria conta. Caso o Usuário tenha ciência de qualquer uso não autorizado, este se compromete a notificar a LendMe imediatamente.
                </p>
                <p class="lead font-paragrafo-1">
                    3.7 A LendMe conta com a conscientização de seus Usuários para a prática da navegação segura na internet e combate à fraude eletrônica.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">
                    ACEITAÇÃO DOS TERMOS DE USO
                </h2>
                <p class="lead font-paragrafo-1">
                    4.1 A utilização desta Plataforma implica na plena, total e geral compreensão, aceitação e vinculação da Plataforma e Serviços oferecidos a estes T&C. A utilização da Plataforma está condicionada a sua aceitação incondicional e irrevogável às regras destes T&C. Ao utilizar a Plataforma, o Usuário concorda em respeitar e seguir todas e quaisquer diretrizes dispostas nestes T&C, as quais podem sofrer alterações a qualquer tempo e a exclusivo critério da LendMe.
                </p>
                <p class="lead font-paragrafo-1">
                    4.2 Ao aceitar estes T&C, declara ser civilmente capaz de realizar negócios jurídicos, conforme a legislação brasileira, perante a LendMe e perante a Instituição Financeira responsável pelo crédito,  bem como declara que domina a língua portuguesa e possui conhecimento suficiente relacionado ao ambiente do Website, dos produtos e Serviços ofertados pela LendMe.
                </p>
                <p class="lead font-paragrafo-1">
                    4.3 A LendMe ressalta que não coleta ou solicita dados de identificação pessoal de pessoas menores de 18 (dezoito) anos. Se o Usuário possuir menos de 18 (dezoito) anos, por favor, não prossiga com o aceite e o cadastro na Plataforma, bem como que se abstenha de acessá-la e utilizá-la. Se acreditar que uma pessoa menor de 18 (dezoito) anos pode ter nos fornecido seus Dados Pessoais, entre em contato conosco em meajuda@lendme.com.br.
                </p>

                <h2 class="font-paragrafo-1 m-t-50"> CONDIÇÕES GERAIS</h2>
                <p class="lead font-paragrafo-1">
                    5.1 A LendMe esclarece que não cobrará taxas extras para a liberação de empréstimo, nos termos da Resolução 3954/11, do Banco Central, conforme alterada.
                </p>
                <p class="lead font-paragrafo-1">
                    5.2 Em virtude das parcerias ou por acreditar que possa ser de interesse dos Usuários, a LendMe, a seu exclusivo critério, poderá disponibilizar no Website links para outros Websites de terceiros e funcionalidades da internet, sem que isso signifique que esses websites sejam de propriedade ou operados pela LendMe.
                </p>
                <p class="lead font-paragrafo-1">
                    5.3 O uso dessas páginas é de total responsabilidade do próprio Usuário, que deverá atentar-se aos termos de uso e de privacidade de cada uma dessas páginas. No caso de contratação de Serviços junto às Instituições Parceiras, estas poderão cobrar taxas do usuário, previamente informadas, sem qualquer responsabilidade ou envolvimento da LendMe.
                </p>
                <p class="lead font-paragrafo-1">
                    5.4 A LendMe não assume qualquer responsabilidade em relação ao acesso e ao uso dessas páginas por usuários. Referidos links não implicam endosso em relação a quaisquer terceiros, quaisquer informações, materiais ou conteúdos nessas páginas, quaisquer candidatos a cargos políticos identificados em páginas de terceiros ou em qualquer página de internet, ou a produtos ou serviços oferecidos por terceiros.
                </p>
                <p class="lead font-paragrafo-1">
                    5.5 Ao utilizar o Website, o Usuário reconhece expressamente que:
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    (I) a LendMe não é responsável pelo cumprimento das normas e requisitos legais por parte das Instituições Parceiras, anunciantes e outros parceiros indicados no Website, sendo os usuários responsáveis pela verificação prévia da regularidade e idoneidade dessas instituições;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    (II) a LendMe não atua e/ou substitui consultores e assessores especializados na contratação de serviços financeiros, mesmo que tais serviços sejam indicados no Website;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    (III) a LendMe não é responsável pelas decisões financeiras que os Usuários possam vir a fazer por meio da Plataforma.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    (IV) a LendMe não garante que as Instituições Parceiras cumpram suas obrigações contratual e legalmente estabelecidas.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    (V) a LendMe não assegura a continuidade dos Serviços ou a manutenção das ofertas e cotações, sem prejuízo da boa conclusão dos Serviços já iniciados; e
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    (VI) a LendMe declara e garante que não atua em seu nome de seus Usuários, em nome das Instituições Parceiras ou de quaisquer terceiros e que sua atuação consiste apenas na disponibilização de plataforma para facilitar a contratação de produtos financeiros oferecidos pelas Instituições Parceiras.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">6. CADASTRO E REGISTRO</h2>

                <p class="lead font-paragrafo-1">
                    6.1 Ao se cadastrar no Website, o Usuário atesta que todos os dados fornecidos são verdadeiros, completos e precisos, e concorda com as disposições específicas às suas Informações Pessoais, conforme disposto na Política de Privacidade. Caso seja constatado que as informações são incompletas, imprecisas ou falsas, fraudulentas ou violar ou tentar violar os presentes T&C, a Política de Privacidade, outras políticas ou qualquer documento legal da LendMe, o Usuário poderá ter sua seu acesso suspenso ou cancelado, a exclusivo critério da LendMe, sem prejuízos de outras medidas que sejam aplicáveis, sem prejuízo do disposto na Cláusula 11 abaixo.
                </p>
                <p class="lead font-paragrafo-1">
                    6.2 A LendMe poderá precisar de informações e/ou documentos adicionais sobre o Usuário cadastrado a qualquer tempo, seja para melhor identificá-lo ou para conduzir diligências internas, caso em que o Usuário será instado a fornecê-las. Não fornecer prontamente tais informações e documentos quando requisitado constituirá violação destes Termos.
                </p>
                <p class="lead font-paragrafo-1">
                    6.3 O cadastro do Usuário no Website é pessoal e intransferível.
                </p>
                <p class="lead font-paragrafo-1">
                    6.4  É responsabilidade do Usuário manter suas informações devidamente atualizadas. Ressaltamos que a LendMe poderá verificar, a qualquer tempo, se as informações fornecidas por seus Usuários são verdadeiras.
                </p>
                <p class="lead font-paragrafo-1">
                    6.5 A LendMe empregará seus melhores esforços para garantir a segurança do cadastro de seus Usuários. Contudo, o sigilo e a segurança do nome de usuário e senha são de única e exclusiva responsabilidade de cada Usuário, nos termos da Cláusula 3 acima.
                </p>
                <h2 class="font-paragrafo-1 m-t-50">7. CONFIDENCIALIDADE</h2>
                <p class="lead font-paragrafo-1">
                    7.1 A LendMe considera todas as informações coletadas por meio da Plataforma como confidenciais. Portanto, somente as utilizará de acordo com as autorizações correspondentes. Todas as informações cadastradas e coletadas na Plataforma são utilizadas para a execução das suas atividades, para melhorar a experiência de navegação dos Usuários na Plataforma.
                </p>
                <p class="lead font-paragrafo-1">
                    7.2 Importante atentar que a Plataforma pode conter links para outras páginas, inclusive de parceiros, que possuem Políticas de Privacidade com previsões diversas do disposto nestes T&C. A LendMe não se responsabiliza pela coleta, utilização, compartilhamento e armazenamento de seus dados pelos responsáveis por tais páginas fora de sua Plataforma, conforme descrito nas Cláusulas 5.3 e 5.4.
                </p>
                <p class="lead font-paragrafo-1">
                    7.3  A LendMe poderá compartilhar os dados coletados por meio da Plataforma: (i) com empresas parceiras, para negociação de melhorias na Plataforma; (ii) quando necessário às atividades comerciais da LendMe; (iii) para proteção dos interesses da LendMe em qualquer tipo de conflito, incluindo ações judiciais; (iv) no caso de transações e alterações societárias envolvendo a LendMe, hipótese em que a transferência das informações será necessária para a continuidade da Plataforma; e/ou (v) por ordem judicial ou pelo requerimento de autoridades administrativas que detenham competência legal para sua requisição.
                </p>
                <p class="lead font-paragrafo-1">
                    7.4  Qualquer titular poderá solicitar à LendMe que as informações por esta eventualmente coletadas sejam excluídas. A LendMe empreenderá os melhores esforços para atender a todos os pedidos de exclusão, no menor espaço de tempo possível e respeitará o prazo de armazenamento mínimo de determinadas informações, conforme determinado pela legislação brasileira.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">8. COMUNICAÇÕES</h2>
                <p class="lead font-paragrafo-1">
                    8.1 Toda e qualquer notificação a qualquer das partes poderá ser feita por escrito, via correio eletrônico [meajuda@lendme.com.br]. Contudo, a principal via de informação será a Plataforma, o Usuário deve se atentar para quaisquer mudanças, alterações nos regulamentos e avisos disponibilizados no Website.
                </p>
                <p class="lead font-paragrafo-1">
                    8.2 Ao aceitar estes T&C, o Usuário concorda e autoriza que a LendMe entre em contato através de quaisquer meios eletrônicos, como telefone celular, torpedo (mensagem de texto ou SMS), correio eletrônico, bem como correspondência física.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">9. ALTERAÇÃO DOS TERMOS DE USO</h2>
                <p class="lead font-paragrafo-1">
                    9.1 A Plataforma LendMe poderá modificar estes T&C a qualquer momento, a seu exclusivo critério, mediante a publicação de T&C atualizados no endereço eletrônico (https://www.lendme.com.br/termos-de-uso), visando o aprimoramento e a melhoria dos Serviços oferecidos. Caso haja alteração substancial, a LendMe se compromete a avisar seus usuários, seja por meio da disponibilização de um aviso em seu Website, através de envio de e-mail aos seus usuários cadastrados, ou ainda, por quaisquer outros meios, nos termos da Cláusula 8.2.
                </p>
                <p class="lead font-paragrafo-1">
                    9.2 As alterações aos T&C serão aplicáveis e poderão ocorrer a qualquer momento quando da disponibilização dos Serviços no Website. Algumas das condições específicas para determinados serviços poderão substituir ou complementar estes T&C, conforme divulgado nos respectivos avisos legais disponíveis em outras áreas do Website. Desse modo, recomendamos que sempre que acessar a Plataforma, fique atento às novas atualizações, pois o seu acesso e uso da Plataforma estarão vinculados por quaisquer alterações destes T&C.
                </p>
                <p class="lead font-paragrafo-1">
                    9.3 Caso não concorde com as alterações realizadas, alertamos que o Usuário não deverá começar ou continuar a acessar o Website ou utilizar os Serviços oferecidos pela LendMe, tendo em vista que a continuidade na utilização dos Serviços e acesso ao Website serão entendidas como concordância tácita dos novos T&C, estando os usuários submetidos a eles e estando a LendMe isenta de responsabilidade e desobrigada a qualquer reparação perante seus Usuários nesse caso.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">10. PROPRIEDADE INTELECTUAL</h2>
                <p class="lead font-paragrafo-1">
                    10.1 O uso comercial do nome, dos desenhos e da expressão ‘LendMe’ como nome empresarial, marca, ou nome de domínio, bem como os conteúdos das telas relativas aos nossos serviços assim como os infográficos, artigos, colunas, widgets, programas, bancos de dados, documentos e demais utilidades e aplicações que permitem ao usuário acessar e usar sua conta de usuário são de titularidade da LendMe ou a ela licenciados, os quais ficam sujeitos aos direitos intelectuais de acordo com as leis brasileiras e tratados e convenções internacionais dos quais o Brasil seja signatário. Para os fins destes T&C, conteúdos, elementos e/ou ferramentas encontrados na Plataforma compreendem, sem limitação, textos, códigos-fonte, softwares, scripts, imagens gráficas, fotos, sons, músicas, vídeos, arquivos, ícones, desenhos, layouts, algoritmos, recursos interativos e similares, marcas, marcas de serviços, logotipos, conjunto-imagem, “look and feel” etc.
                </p>
                <p class="lead font-paragrafo-1">
                    10.2 O Usuário está ciente de que não adquire, em virtude da utilização da Plataforma, nenhum direito de propriedade intelectual ou outros direitos exclusivos, incluindo patentes, desenhos, marcas, direitos autorais ou quaisquer direitos sobre informações confidenciais ou segredos de negócio, sobre ou relacionados à Plataforma  ou parte dela, sendo todos os direitos de exploração comercial da Plataforma reservados à LendMe.
                </p>
                <p class="lead font-paragrafo-1">
                    10.3 O Usuário está ciente de que em nenhuma hipótese lhe é permitido copiar, ceder, sublicenciar, vender, dar em locação ou em garantia, reproduzir, doar, alienar de qualquer forma, transferir total ou parcialmente, sob quaisquer modalidades, gratuita ou onerosamente, provisória ou permanentemente, a Plataforma, assim como seus módulos, partes, manuais ou quaisquer informações a ele relativas. Fica proibido ao Usuário retirar ou alterar, total ou parcialmente, os avisos de reserva de direito existente na Plataforma, bem como praticar engenharia reversa, descompilação ou desmontagem da Plataforma.
                </p>
                <p class="lead font-paragrafo-1">
                    10.4 Caso deseje utilizar algum dos conteúdos da LendMe, solicitamos que entre em contato conosco antes de fazê-lo. Usar qualquer conteúdo aqui mencionado sem a prévia e expressa autorização da LendMe poderá acarretar responsabilizações legais. O Usuário deve estar ciente de que qualquer utilização indevida da Plataforma poderá representar violação de direitos de propriedade intelectual da LendMe, sendo de responsabilidade exclusiva do Usuário qualquer reparação de danos e despesas incorridas nesse sentido pela LendMe e seus parceiros.
                </p>


                <h2 class="font-paragrafo-1 m-t-50">11. SOLICITAÇÃO DE PROPOSTAS OU COTAÇÕES</h2>
                <p class="lead font-paragrafo-1">
                    11.1 Ao requerer que as Instituições Parceiras enviem cotações ou propostas relativas a serviços e produtos financeiros através do Website, o Usuário expressamente reconhece que:
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    a) A LendMe não garante que receberá respostas ou cotações das Instituições Parceiras;
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    b) A LendMe poderá enviar as informações fornecidas dos Usuários para as Instituições Parceiras, em conformidade a Política de Privacidade. Tais informações poderão ser usadas pelas Instituições Parceiras e por outras instituições a elas ligadas para determinarem o perfil de consumidor do Usuário, seu histórico de crédito e outros fatores relevantes para que possam enviar uma proposta adequada. As Instituições Parceiras poderão entrar em contato com os Usuários, através das formas de contato dadas à LendMe ao se cadastrar como usuário do Website;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    c) As Instituições Parceiras poderão solicitar diretamente outras informações sobre o Usuário, cuja coleta, guarda e tratamento não serão de responsabilidade da LendMe.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    d) Alguns serviços oferecidos pelas Instituições Parceiras poderão gerar impostos, tarifas, emolumentos, custas, taxas e cobranças ou retenções específicas, legal ou contratualmente previstas, as quais não são de responsabilidade da LendMe. Caberá ao Usuário verificar as leis e contratos aplicáveis, para que tenha inteiro conhecimento de quaisquer retenções ou cobranças previstas e aplicáveis e impostas a quaisquer partes, sendo de exclusiva responsabilidade da parte a quem a lei ou o contrato atribuir responsabilidade;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    e) Cada Serviço oferecido ou por Instituições Parceiras possui termos contratuais e legais próprios, complementares aos presentes Termos, que deverão ser cuidadosamente verificados pelo Usuário antes da contratação.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    f) A LendMe não é parte no contrato ou na relação entre os Usuários e as Instituições Parceiras, não podendo ser responsabilizada por obrigações ali assumidas. Tais obrigações serão de responsabilidade exclusiva das partes do contrato, na medida e forma em que tais responsabilidades forem alocadas.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    g) Caso o Usuário deseje, deverá manifestar, por conta e risco, sua vontade em contratar um dos Serviços ou produtos financeiros oferecidos pelas Instituições Parceiras, dentre as opções disponibilizadas pelas Instituições Parceiras na Plataforma, conforme o valor, condições de prazo e pagamento aplicáveis. Em seguida, o Usuário deverá fornecer as informações solicitadas, que, juntamente com informações extraídas de bases de dados públicas e/ou privadas disponíveis, servirão de fundamento para a análise de risco de crédito pelas Instituições Parceiras.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    h) Caso o Usuário queira obter uma proposta de crédito com a Instituição Parceira por meio da Plataforma, deverá (i) autorizar o acesso da Instituição Parceira às informações que lhe são relativas e que constem do Sistema de Informações de Crédito do Banco Central (“SCR”), dos órgãos de proteção ao crédito, de bases de dados públicas e privadas, conforme aplicável, e demais fontes que possam auxiliar na definição de seu risco de crédito; (ii) autorizar o compartilhamento das informações coletadas pela Instituição Parceira conosco e com as demais Instituições Parceiras, conforme previsto nestes Termos; e (iii) autorizar a execução de procedimentos de Know Your Client (“KYC”), inclusive aqueles dispostos nos termos das leis anticorrupção, antiterrorismo, contra a lavagem de capitais e a evasão de divisas, nos termos da regulamentação bancaria aplicável e sempre que necessário ou adequado.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">12. DISPOSIÇÕES GERAIS</h2>
                <p class="lead font-paragrafo-1">
                    12.1 Estes Termos de Uso constituem o acordo integral estabelecido entre a LendMe seus Usuários, para utilização da Plataforma.
                </p>
                <p class="lead font-paragrafo-1">
                    12.2 A LendMe é uma empresa brasileira e o Website e seus Serviços foram criados e mantidos em fiel cumprimento às leis brasileiras e demais tratados que são incorporados ao ordenamento jurídico brasileiro. Caso você esteja usufruindo de nossos serviços fora do Brasil, você será responsável pelo cumprimento das leis locais, na medida em que forem aplicáveis.
                </p>
                <p class="lead font-paragrafo-1">
                    12.3 Em caso de dúvidas em relação ao uso e/ou acesso da Plataforma, os Usuários poderão acessar o <a href="https://www.lendme.com.br/contato/fale-conosco" class="theme-text">FAQ</a>. Dúvidas, críticas e/ou comentários sobre a Plataforma também podem ser encaminhados para meajuda@lendme.com.br ou pelo telefone 0800 759 1000. Nossas redes sociais como Facebook, Twitter, Instagram, LinkedIn e outros não são meios próprios para prestação de suporte e, portanto, não poderão ser usados para isto. Nosso suporte funciona de segunda a sexta-feira, de 09:00 às 18:00, exceto em feriados e feriados prolongados.
                </p>
                <p class="lead font-paragrafo-1">
                    12.4 Caso qualquer disposição contida nestes T&C seja considerada inválida, ilegal ou inexequível sob qualquer aspecto, a validade, a legalidade ou a exequibilidade das demais disposições não serão de forma alguma afetadas ou prejudicadas em decorrência de tal fato, ficando a LendMe obrigada à revisão da referida disposição, de modo a torná-la válida, sem que se desvirtue o objetivo destes T&C.
                </p>
                <p class="lead font-paragrafo-1">
                    12.5 A novação, quitação ou renúncia de qualquer obrigação decorrente destes T&C de Uso somente serão consideradas válidas se realizadas por escrito. O não exercício de qualquer direito oriundo destes T&C, na primeira ocasião em que seria possível fazê-lo, não implicará a renúncia a tais direitos, nem sua preclusão, salvo se disposto de forma diversa nestes T&C. A eventual tolerância de qualquer infração a estes T&C não significará que qualquer infração posterior, ainda que da mesma natureza, será tolerada.
                </p>
                <p class="lead font-paragrafo-1">
                    12.6  Nenhuma das disposições aqui estabelecidas deve ser considerada ou interpretada como se conferissem a terceiro, que não seja a LendMe ou seus Usuários, qualquer direito ou prerrogativa por força deste Contrato.
                </p>
                <p class="lead font-paragrafo-1">
                    12.7 Estes T&C serão regidos e interpretados de acordo com as leis brasileiras. A comarca da Capital do Estado de São Paulo é a eleita para dirimir quaisquer conflitos relacionados a estes T&C, por mais privilegiado que outro foro possa parecer ser.
                </p>
            </div>

        </div>
    </div>
</section>
