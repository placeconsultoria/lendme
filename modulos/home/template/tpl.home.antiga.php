<section class="fullscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE ?>images/lendme-bg-2.jpg">
    <div class="container container-fullscreen">
        <div class="text-middle text-center">
            <h1 class="text-uppercase text-lg text-light font-title-1 f-70 "  data-animate="fadeInUp" data-animate-delay="400"> BENVINDO A UMA NOVA ERA</h1>
            <h1 class=" text-light m-b-0" data-animate="fadeInUp" data-animate-delay="800">Plataforma de Home Equity </h1>
            <h1 class=" text-light m-t-0" data-animate="fadeInUp" data-animate-delay="1200">segura baseada em Blockchain</h1>
            <!--                <h3 class=" color-laranja" data-animate="fadeInUp" style="margin-top: 100px;" data-animate-delay="1600">Plataforma de Home Equity simples, rápida e segura baseada em Blockchain.</h3>-->
            <div id="scroll-down" class="scrolldown-animation scrolldown-bottom">
                <a href="#about" name="btn_menu" class="scroll-to"><img alt="" src="<?= $URL_SITE ?>images/scrolldown.png"></a>
            </div>
        </div>
    </div>
</section>
<?php require_once("template/simulador.php"); ?>
<section class="parallax background-overlay-dark p-t-50 p-b-50" style="background-image:url(<?= $URL_SITE ?>images/bg-2.jpg);" data-stellar-background-ratio="0.7">
    <div class="container xs-text-center sm-text-center text-light">
        <div class="row">
            <div class="col-lg-5 p-b-60">
                <h4 class="font-title-1">A evolução do</h4>
                <h1 class="font-title-1">Home Equity</h1>
                <p class="lead font-paragrafo-1">
                    Com o Home Equity (crédito com garantia de imóvel) você dá liquidez ao seu patrimônio imobiliário. Essa modalidade de crédito ajuda a solucionar suas questões financeiras, acessando recursos de forma ágil e segura.
                </p>
                <p class="lead font-paragrafo-1">
                    Você pode pagar suas dívidas, retomando o controle da sua vida financeira, abrir ou expandir um negócio, reformar sua casa ou pagar estudos dos filhos.  <strong><a href="#">Descubra as melhores condições para você</a> </strong>.
                </p>
            </div>
            <div class="col-lg-7">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="text-center">
                            <div class="counter text-lg"><small style="font-size: 40px;">R$ 3.3 bilhões</small></div>
                            <div class="seperator seperator-small"></div>
                            <p class="lead font-paragrafo-1">CRÉDITO CONCEDIDO</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="text-center">
                            <div class="counter text-lg"> <span data-speed="4500" data-refresh-interval="3" data-to="58" data-from="0" data-seperator="true"></span><small style="font-size: 40px;">%</small> </div>
                            <div class="seperator seperator-small"></div>
                            <p class="lead font-paragrafo-1">CRESCIMENTO 2019 X 2018</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="text-center">
                            <div class="counter text-lg"><small style="font-size: 40px;">R$</small> <span data-speed="3000" data-refresh-interval="1" data-to="500" data-from="15" data-seperator="true"></span> <small style="font-size: 30px;">bilhões</small> </div>
                            <div class="seperator seperator-small"></div>
                            <p class="lead font-paragrafo-1">NOS PRÓXIMOS 20 ANOS</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="text-center">
                            <div class="counter text-lg"><small style="font-size: 40px;">2.25%</small></div>
                            <div class="seperator seperator-small"></div>
                            <p class="lead font-paragrafo-1 text-uppercase">Taxa Selic</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 text-center p-t-0 m-t-0">
                <a href="#simulador" name="btn_menu" class="btn m-t-40 btn-lg"><i class="fa fa-calculator"></i> FAÇA A SIMULAÇÃO ONLINE</a>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 center text-center" data-animate="fadeInUp" data-animate-delay="200">
                <h4 class="font-title-1 text-uppercase color-roxo">o que é?</h4>
                <h1 style="font-size: 60px;" class="font-title-1  text-uppercase color-roxo">Home Equity</h1>

            </div>
        </div>
        <div class="row p-t50">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12 m-t-20"  data-animate="fadeInUp" data-animate-delay="400" >
                        <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-40">
                            O crédito concedido é garantido por um imóvel em seu nome (pf) e você permanece como proprietário.
                            O home equity permite que você e sua família permaneçam na sua casa, sem precisar mudar, evitando custos e transtornos. Além disso, você pode usar outros imóveis de sua propriedade para obter o crédito desejado. <strong>Novas possibilidades com comodidade, segurança e agilidade</strong>.
                        </p>
                    </div>
                    <div class="col-lg-12 m-t-50">
                        <div class="call-to-action btn-roxo call-to-action-colored text-center p-t-20"  data-animate="fadeInUp" data-animate-delay="600">
                            <img src="<?= $URL_SITE ?>images/logo-dark3.png" class="img-fluid m-b-20" style="max-width: 200px;" />
                            <h2 class="font-title-1 m-b-10 text-white color-azul-b2b text-uppercase" data-animate="fadeInUp" data-animate-delay="800" >Home Equity</h2>
                            <h3 class="font-title-1 m-b-10 text-white  text-uppercase" data-animate="fadeInUp" data-animate-delay="1000">crédito de até 50% do valor do seu imóvel e 15 anos para quitar</h3>
                            <a href="#simulador" name="btn_menu" class="btn btn-lg" data-animate="fadeInLeft" data-animate-delay="1200"><i class="fa fa-calculator"></i> SIMULADOR ONLINE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="slider" class="p-t-100 p-b-100" data-vide-bg="<?= $URL_SITE ?>homepages/portfolio/video/Head-or-Tails">
    <div class="container">
        <div class="container-fullscreen">
            <div class="text-middle text-dark text-center">
                <h2 class="text-medium-light font-title-1 color-roxo text-uppercase" data-animate="fadeInDown" data-animate-delay="500">Home Equity</h2>
                <h3 data-animate="fadeInDown" data-animate-delay="1000" class="m-b-50 font-paragrafo-1 color-roxo">Assista o vídeo</h3>
                <a href="https://www.youtube.com/watch?v=tDBLPbYDbdY" data-lightbox="iframe" class="play-button dark"><i class="fa fa-play"></i></a>
            </div>
        </div>
    </div>
</section>


<section id="image-block" class="image-block no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6" style="height:609px;background:url(<?= $URL_SITE ?>banner-new.jpeg) 60% 50% / cover no-repeat;"></div>
            <div class="col-lg-6">
                <div class="heading-text heading-section">
                    <h2 data-animate="fadeInDown" data-animate-delay="100" class="font-title-1 color-roxo">PRA QUEM?</h2>
                    <p data-animate="fadeInDown" data-animate-delay="200" class="lead font-paragrafo-1">
                        Para pessoas físicas que (mesmo com dividas), tenham imóvel quitado ou não, podem obter o empréstimo, com as melhores taxas e prazo mais longo.
                    </p>
                    <p data-animate="fadeInDown" data-animate-delay="300" class="lead font-paragrafo-1">
                        Pessoas jurídicas também podem obter o crédito, bastando que o imóvel garantidor esteja em nome de pessoa física.
                    </p>
                    <p data-animate="fadeInDown" data-animate-delay="400" class="lead font-paragrafo-1">
                        Para você empresário do mercado imobiliário, conheça também a nossa área de operações estruturadas.
                    </p>
                    <p data-animate="fadeInDown" data-animate-delay="500" class="lead font-paragrafo-1">
                        A proposta da Lendme é ser uma ONE-STOP-SHOP, reunindo um portfolio diversificado de produtos de base imobiliária, para todos os tipos de clientes e necessidades.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="image-block2" class="image-block no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="heading-text heading-section">
                    <h2 data-animate="fadeInDown" data-animate-delay="100" class="font-title-1 color-roxo">QUANDO USAR?</h2>
                    <p data-animate="fadeInDown" data-animate-delay="200" class="lead font-paragrafo-1">
                        Tem dívidas? Quer reorganizar a vida financeira? Esse é o momento de usar um home equity. Obtenha crédito com taxas mais baixas e prazos mais longos.
                    </p>
                    <p data-animate="fadeInDown" data-animate-delay="300" class="lead font-paragrafo-1">
                        Tem outros sonhos para você e sua família? Quer empreender, financiar os estudos dos filhos ou reformar a casa? Tenha acesso às taxas mais baixas do mercado. Simule e consulte nossos especlialistas!
                    </p>
                    <p data-animate="fadeInDown" data-animate-delay="400" class="lead font-paragrafo-1">
                        Ou ainda, é empresário e está em busca de capital de giro? O home equity também está disponível para você (desde que o imóvel garantidor esteja em nome de pessoa física).
                    </p>
                </div>
                <a data-animate="fadeInRight" data-animate-delay="800" href="#simulador" name="btn_menu" class="btn btn-lg"><span>Fazer simulação <i class="fa fa-calculator"></i> </span></a>
            </div>
            <div class="col-lg-6" style="height:609px;background:url(<?= $URL_SITE ?>img-1.png) 50% 50% / cover no-repeat;">
            </div>
        </div>
    </div>
</section>
