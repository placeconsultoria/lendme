<section class="halfscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE?>images/lendme-bg-2.jpg">
    <div class="container-fluid">
        <div class="container-fullscreen">
            <div class="text-middle text-center ">
                <h2  class="m-t-0 font-title-1 text-white text-lg">FALE CONOSCO</h2>
            </div>
        </div>
    </div>
</section>
<section class="background-white p-b-0 m-b-0">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center p-20" >
                <div class="row">
                    <div class="col-md-12 p-20" style="border: 1px solid #281943; width: 100%;">
                        <div class="icon"> <a href="#"><i class="fa fa-5x fa-phone color-roxo fa-rotate-90"></i></a> </div>
                        <h3 style="font-size: 18px;">TELEFONE</h3>
                        <h3 style="font-size: 16px;" class="color-roxo">0800 759 1000</h3>
                    </div>
                </div>

            </div>

            <div class="col-md-3 text-center p-20" >
                <div class="row">
                    <div class="col-md-12 p-20" style="border: 1px solid #281943; width: 100%;">
                        <div class="icon"> <a onclick="window.open('https://app.contako.com.br/Atendimento/?cadastro=4DCD58CD44', '_blank', 'width=900, height=640', 'false');" style="cursor:pointer" ><i class="fas fa-5x fa-comment-dots color-roxo"></i></a></div>
                        <h3 style="font-size: 18px;">ATENDIMENTO VIA</h3>
                        <h3 style="font-size: 16px;" class="color-roxo">CHAT</h3>
                    </div>
                </div>

            </div>

            <div class="col-md-3 text-center p-20" >
                <div class="row">
                    <div class="col-md-12 p-20" style="border: 1px solid #281943; width: 100%;">
                        <div class="icon"> <a href="mailto:contato@lendme.com.br"><i class="fas fa-5x fa-at color-roxo"></i></a></div>
                        <h3 style="font-size: 18px;">E-MAIL</h3>
                        <h3 style="font-size: 16px;" class="color-roxo"><a href="mailto:meajuda@lendme.com.br">meajuda@lendme.com.br</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center p-20" >
                <div class="row">
                    <div class="col-md-12 p-20" style="border: 1px solid #281943; width: 100%;">
                        <div class="icon"> <a href="#faq"><i class="fas fa-5x fa-question color-roxo"></i></a></div>
                        <h3 style="font-size: 18px;">AJUDA </h3>
                        <h3 style="font-size: 16px;" class="color-roxo">PERGUNTAS FREQUENTES</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="contato" class="background-white  p-t- 0 m-t-0 p-b-50 ">
    <div class="container">
        <div class=" text-center">
            <h1 class="font-title-1 text-medium">ENTRE EM CONTATO</h1>
            <h3>Mande uma mensagem para nós responderemos o mais breve possível!</h3>
        </div>
        <div class="row">
            <div class="col-lg-8 p-t-50 center">
                <form id="frm_contato" method="post">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name" >Nome</label>
                            <input type="text" aria-required="true" name="nome" class="form-control required " placeholder="" required>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="email" >E-mail</label>
                            <input type="email" aria-required="true" name="email" class="form-control required " placeholder="" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="telefone" >Telefone</label>
                            <input type="text" id="telefone2" name="telefone" class="form-control" placeholder="" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="subject">CPF/CNPJ</label>
                            <input type="text"  data-mask-for-cpf-cnpj name="cnpj" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message" >Mensagem</label>
                        <textarea type="text" name="mensagem" rows="8" class="form-control required" placeholder="Como podemos lhe ajudar?" required></textarea>
                    </div>
                    <div class="row">
                        <div class="form-group center">
                            <button class="btn btn-roxo" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i> Enviar mensagem</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>
<section class="background-overlay-dark p-t-50 p-b-60" style="background-image:url(<?= $URL_SITE?>images/bg-2.jpg);" data-stellar-background-ratio="0.7" id="faq">
    <div class="container xs-text-center sm-text-center text-light">
        <div class="row">
            <div class="col-12 text-center ">
                <img src="<?= $URL_SITE?>images/logo-dark3.png" />
                <h2 class="text-white font-title-1">Empowering your financial life</h2>
            </div>
        </div>
    </div>
</section>

<section class="background-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 center text-center" data-animate="fadeInUp" data-animate-delay="200">
                <h3 style="font-size: 60px;" class="font-title-1  text-uppercase color-roxo">PERGUNTAS FREQUENTES</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-t-100" data-animate="fadeInUp" data-animate-delay="600">
                <!--                   <h3 class="font-paragrafo-1 color-roxo">SOBRE A LENDME</h3>-->
                <div class="accordion accordion-shadow">

                    <h3 class="font-paragrafo-1 color-roxo">SOBRE A LENDME</h3>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">O que é a LendMe?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            A LendMe é uma Fintech que tem por objetivo ofertar crédito com garantia de imóvel de forma saudável e 100% digital.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">O que é uma Fintech?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            A definição Fintech surgiu da combinação de duas palavras em inglês: <em>financial</em> (financeiro) e <em>technology</em> (tecnologia) e é usada para definir empresas que desenvolvem e ofertam produtos financeiros por meio de plataformas digitais. Mas é importante observar que não basta ter tecnologia para ser uma Fintech.<br> As Fintechs oferecem uma nova experiência com um produto ou serviço.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Por que escolher a LendMe?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            Mais do que crédito, a LendMe trabalha para que você tenha controle da sua vida financeira e, dessa forma, você e sua família possam ter tranquilidade e estabilidade.   Além disso, trabalhamos com as melhores taxas de mercado e de maneira rápida, segura e responsável.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Onde fica o escritório da LendMe?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Em São Paulo, Capital, situado à Rua Gomes de Carvalho, 1629 – Vila Olímpia.
                            </p>
                        </div>
                    </div>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Como sei que a LendMe é confiável?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            A LendMe é composta por profissionais que estão no mercado há mais de 20 anos e que lançaram o produto Home Equity no Brasil. É a primeira Fintech de Home Equity no Brasil a utilizar Blockchain em seus processos. Dessa forma, todas as informações dos clientes e dos parceiros de negócios são criptografadas com total segurança e sigilo.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Vocês são um banco?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            Não. A LendMe é uma Fintech - empresa que disponibiliza produtos financeiros, com total segurança e sigilo, atuando como correspondente bancária de uma instituição financeira. A empresa tem por objetivo ofertar crédito com garantia de imóvel (Home Equity) de forma saudável e 100% digital.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Qual a experiência de vocês?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            A LendMe foi criada por profissionais especialistas em Crédito e Mercado Financeiro. Seus executivos foram os pioneiros no segmento de Home Equity no Brasil e já realizaram mais de 12 mil contratos nesse segmento, alcançando mais de R$ 2.5 bilhões em créditos concedidos.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Como sei que o site da LendMe é seguro?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            Nosso site tem certificado de segurança (https). Além disso, todas as informações são imputadas dentro do nosso sistema. A partir daí, todo o processo fica criptografado em Blockchain - ambiente onde os dados ficam armazenados de forma imutável e com total segurança.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">O que é Blockchain?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            O Blockchain é um protocolo de confiabilidade. Por meio de um sistema tecnológico de registro distribuído, as informações são armazenadas de forma segura e inviolável. Neste formato, é possível rastrear o envio e recebimento de alguns dados pela internet. Sua principal vantagem é permitir o funcionamento e transação das chamadas criptomoedas (moedas digitais) em uma espécie de livro de registro público, com chaves de segurança confiáveis e imutáveis.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo"> A LendMe é uma empresa estrangeira?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            Não. A LendMe é uma empresa 100% nacional, com sócios brasileiros e sede em São Paulo (SP), Brasil.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Home equity é a mesma coisa que empréstimo com garantia de imóvel?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Como posso falar com alguém da LendMe diretamente?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            A LendMe é prioritariamente digital e no site (www.lendme.com.br) você encontra todas as informações sobre a empresa, nossos produtos, como fazer simulações, conhecer nossas taxas e condições de crédito. Você também pode enviar suas dúvidas no Fale Conosco ou fazer contato pelo 0800 759 1000 da LendMe. Uma vez aprovado seu crédito, você também passa a contar com uma equipe especializada para melhor orientá-lo em tudo que precisar.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Quero fazer uma reclamação, sugestão, elogio. Como faço?</h5>
                        <div class="ac-content">
                            <p class="lead">
                            A LendMe disponibiliza os seguintes canais:  chat no site, e-mail (meajuda@lendme.com.br), telefone (0800 759 1000) e nossas redes sociais (disponíveis da página inicial do site).
                            </p>
                        </div>
                    </div>



                    <div class="line"></div>

                    <h3 class="font-paragrafo-1 color-roxo">PERFIL</h3>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">  Quem é autônomo pode solicitar  crédito na LendMe?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim. Basta comprovar renda e estar enquadrado na nossa política de crédito.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Negativado pode solicitar essa forma de empréstimo?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim. Ter dívidas não é um impeditivo. A aprovação de sua solicitação passará por uma análise de crédito que considera vários aspectos, incluindo suas dívidas atuais.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">O que eu preciso para pedir um empréstimo na LendMe?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Você precisa ter entre 18 anos e 72 anos e ter o seu perfil aprovado pela LendMe.Também precisa  comprovar renda compatível com o valor solicitado e ofertar um imóvel como garantia (de propriedade da própria pessoa ou de um parente de 1º grau).
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Ter um imóvel já viabiliza o crédito?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. O imóvel em garantia nos permite trabalhar com taxas menores e prazos maiores de parcelamento, mas como toda operação de crédito cada solicitação passa por uma análise. Somente ter um imóvel não garante a concessão do crédito.
                            </p>
                        </div>
                    </div>

                    <div class="line"></div>

                    <h3 class="font-paragrafo-1 color-roxo">IMÓVEL</h3>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Posso vender o imóvel que coloquei em garantia depois de conseguir o empréstimo?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim, desde que o empréstimo seja quitado.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">O imóvel em garantia continua em meu nome?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Você manterá a posse direta do imóvel e a instituição financeira terá a posse indireta.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Posso financiar um imóvel?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não, a LendMe não trabalha com financiamento para a aquisição de imóvel.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Que tipo de imóvel comercial vocês aceitam em garantia?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Aceitamos salas e escritórios comerciais.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Posso transferir o imóvel que coloquei em garantia para outra pessoa?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. Qualquer alteração na matrícula do imóvel somente poderá ser realizada após a quitação do empréstimo.

                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Eu posso perder meu imóvel?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim. Pode acontecer em último caso, se as parcelas deixarem de ser pagas e o empréstimo não for quitado. Contudo, não temos nenhum interesse no seu imóvel e sim, em ajudá-lo a reorganizar a sua vida financeira.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Posso fazer o empréstimo se meu imóvel estiver com usufruto?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Antes de fazer o empréstimo, é necessário baixar a cláusula de usufruto.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Posso colocar o imóvel que eu moro como garantia?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim. Você manterá a posse direta do imóvel e a instituição financeira terá a posse indireta. Assim, você poderá continuar morando nele.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Posso colocar outro imóvel que possuo como garantia?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Posso trocar o imóvel que coloquei em garantia depois que recebi o empréstimo?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não é possível fazer a alteração do imóvel em garantia antes da quitação do empréstimo.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">O imóvel tem que estar 100% quitado?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. O imóvel a ser disponibilizado em garantia precisa ter o mínimo de 50% de seu valor quitado.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">São aceitas propostas de inventário?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. A documentação do imóvel precisa estar completa e regularizada para que seja possível a análise da solicitação do empréstimo.

                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Até que percentual do valor do imóvel consigo de empréstimo?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Até 50% do valor do imóvel. Entretanto, o percentual a ser concedido considera outros aspectos, como a localização e o tipo do imóvel, além de dados do laudo de avaliação.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Posso compor renda com mais alguém?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim. Você pode compor renda com mais uma pessoa, que pode ser seu marido ou sua esposa, pai, mãe e irmão (parentes de 1º grau).
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Posso ter mais de um empréstimo usando o mesmo imóvel em garantia?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. No Brasil, a legislação permite que um imóvel seja garantia em uma única operação de crédito, mas já há estudos do Banco Central para que isso mude e o mesmo imóvel seja oferecido em garantia para mais de um empréstimo.
                            </p>
                        </div>
                    </div>

                    <div class="line"></div>

                    <h3 class="font-paragrafo-1 color-roxo">OUTRAS GARANTIAS</h3>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">É possível outras garantias como carros, motos,jóias ou ouro?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. A LendMe trabalha somente com garantia de imóveis.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Terreno pode servir de garantia?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. A LendMe trabalha apenas com imóveis residenciais e comerciais em áreas urbanas.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo"> O imóvel pode estar localizado em qualquer lugar do país?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. No momento, a LendMe está trabalhando com imóveis localizados no Estado de São Paulo.
                            </p>
                        </div>
                    </div>

                    <div class="line"></div>

                    <h3 class="font-paragrafo-1 color-roxo">PROCESSOS</h3>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">Quais são os principais motivos de rejeição?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Idade ou renda fora dos parâmetros necessários, pendências jurídicas que possam recair sobre o imóvel, documentação irregular do imóvel  ou não enquadramento do seu perfil à  nossa política de crédito.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">A LendMe solicita algum tipo de adiantamento para liberação do empréstimo?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. Nem a LendMe, ou quaisquer  dos seus parceiros comerciais, está autorizado a cobrar qualquer valor para a liberação do empréstimo.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo"> Há despesas/ custos para eu fazer a operação?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim, mas eles podem ser incluídos no valor do empréstimo.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo"> São cobradas taxas depois da análise jurídica?</h5>
                        <div class="ac-content">
                            <p class="lead">
                                Após a aprovação da análise jurídica, será agendada a visita técnica presencial realizada por um profissional especializado para avaliar as condições do imóvel.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            O que acontece depois que meu imóvel foi analisado?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Assim que aprovado o laudo de avaliação do imóvel, o cliente pode assinar o contrato de empréstimo e realizar o registro para obter a liberação do recurso na conta. Todos esses procedimentos são acompanhados pela equipe da LendMe.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            E depois que o contrato for enviado para o cartório?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Acontece o registro do contrato e a liberação do recurso na conta do solicitante.
                            </p>
                        </div>
                    </div>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Quanto tempo leva o processo de home equity?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                A LendMe é uma plataforma online pensada para oferecer segurança e agilidade no processo. Caso  seu crédito seja aprovado, o processo pode durar cerca de 15 dias (desde a análise de crédito até a liberação  do empréstimo).
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Quanto tempo leva para o dinheiro estar em conta?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                            Em até 10 dias após o registro do contrato em cartório.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Qual o valor mínimo que posso solicitar de empréstimo?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                A LendMe trabalha com o valor mínimo de R$ 100 mil (cem mil reais).
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Só posso usar o empréstimo para quitar dívidas?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. O empréstimo com garantia é de uso livre e está entre as opções de menores juros do mercado. Portanto, ele pode ser o meio mais rápido e prático para a realização de um sonho, aquisição de outro imóvel, viagens de lazer e de estudo, entre outros.
                            </p>
                        </div>
                    </div>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            O que acontece após a quitação do empréstimo?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                O termo de quitação é expedido em até 30 dias e a alienação fiduciária é baixada na matrícula. O imóvel fica 100% liberado.
                            </p>
                        </div>
                    </div>



                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Como a LendMe  atua nos processos?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                A LendMe trabalha de forma 100% digital e com registro em Blockchain, visando agilidade e segurança dos nossos clientes.
                            </p>
                        </div>
                    </div>


                    <div class="line"></div>

                    <h3 class="font-paragrafo-1 color-roxo">SIMULADOR</h3>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Fazendo a simulação no site, já estou fechando um contrato?

                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. A partir dos dados fornecidos acontecem as análises necessárias e um analista da LendMe entrará em contato para apresentar as melhores condições de empréstimos personalizadas para o seu perfil. A escolha e decisão é sempre do cliente.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Quantas simulações posso fazer?

                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Vocêpode fazer quantas simulações quiser. Não solicitamos dados para você fazer uma simples simulação.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Fui reprovado. Quando posso fazer uma nova solicitação?

                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Três meses após o pedido anterior ter sido reprovado.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Fiz a simulação no site, mas ainda não recebi resposta.

                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Retornamos em menos de 24 horas. Caso você não tenha retorno durante este período, entre em contato conosco através do 0800 759 1000.
                            </p>
                        </div>
                    </div>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Minha simulação de crédito foi aprovada. Qual a próxima etapa?

                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Após a aprovação das condições, você fará a biometria e uma lista com os documentos necessários será enviada para o seu e-mail. Nosso time de consultores especializados também entrará em contato e fará o seu acompanhamento durante todo o processo até a liberação do recurso.
                            </p>
                        </div>
                    </div>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Coloquei informação errada no simulador. Como edito?

                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                A simulação pode ser modificada na hora, através do cursor, ou seja, se você errar, pode ir realizando as alterações arrastando o cursor para as novas informações desejadas.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Não gosto de colocar informações online. Posso fazer a simulação de outra forma?

                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Caso você ainda não se sinta seguro em uma plataforma que armazena seus dados de forma sigilosa (100% registrados em blockchain), você pode entrar em contato com nossos especialistas via chat, e-mail (comercial@lendme.com.br)e/ou telefone(0800 759 1000) ou ainda pelo <a href="https://lendme.com.br/contato/fale-conosco/" class="text-theme">Fale Conosco</a>.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Como sei que meu e-mail está cadastrado?

                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                No site da LendMe, você tem a indicação de ‘Esqueci minha senha - Como faço para recuperá-la?’ -  Basta seguir as instruções.
                            </p>
                        </div>
                    </div>

                    <div class="line"></div>
                    <h3 class="font-paragrafo-1 color-roxo">PARCELAS</h3>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Vocês trabalham com carência de pagamento?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não. No período de carência, os juros se acumulam ao saldo devedor do cliente e são cobrados nas parcelas seguintes. A LendMe atua com crédito saudável e responsável e, por isso, prefere ser transparente com você.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Posso adiantar parcelas do empréstimo?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim.Você pode quitar seu empréstimo antes do prazo se desejar.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Onde acompanho as parcelas em aberto?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Na área de login do sistema. No início do processo, cada cliente recebe um login e senha de acesso para acompanhamento.
                            </p>
                        </div>
                    </div>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            O que compõe o valor da minha parcela?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                As parcelas são compostas de: amortização, juros, seguro de morte e invalidez permanente, seguro de danos físicos ao imóvel e tarifa mensal de manutenção da sua conta.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Como faço para tirar a segunda via do boleto?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Basta entrar em contato por qualquer um dos nossos canais de atendimento.
                            </p>
                        </div>
                    </div>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Como faço pagamentos em atraso?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Entre em contato com nossa equipe através do meajuda@lendme.com.br ou pelo 0800 759 1000.
                            </p>
                        </div>
                    </div>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Tem valor mínimo e máximo para o empréstimo?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                A LendMe trabalha com empréstimos entre R$ 100mil e R$ 1,5 milhão, que correspondam a até 50% do valor do imóvel.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Qual o valor das parcelas?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Para obter as melhores condições é preciso fazer uma simulação no próprio site da <a href="https://lendme.com.br" class="text-theme">LendMe</a>. Dessa forma, os analistas poderão realizar uma análise prévia de acordo com o seu perfil e informar sobre os valores de crédito e parcelas. Entretanto, as prestações não podem ultrapassar o limite de 30% da renda  bruta familiar.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            O que é Renda Bruta Familiar?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Na LendMe, aceitamos que você comprove sua renda mensal incluindo mais uma pessoa com parentesco de primeiro grau. A Renda total dessas pessoas será a renda considerada para calcular o valor das prestações.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Em quantas parcelas posso pagar o empréstimo se ele for aprovado?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Trabalhamos com prazos para pagamentos de 5 (cinco) a 15 (quinze) anos.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Consigo antecipar as parcelasdo empréstimo?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim. Nesse caso, suas parcelas e juros serão readequados.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Como é feito o pagamento das parcelas do empréstimo?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Pagamento mensal, com boleto bancário, até o lançamento do PIX do Banco Central, quando, certamente, iremos oferecer a opção através deste sistema, eliminando custos e burocracia.
                            </p>
                        </div>
                    </div>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Como são as taxas de juros? Já ouvi que são maiores que cartão de crédito. É verdade?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não é verdade. Essa é uma das vantagens dessa modalidade de empréstimo. No Home Equity, o crédito é vinculado à garantia de um imóvel e isso permite a oferta de taxas menores que todas as demais modalidades disponíveis no mercado  (cartão de crédito, CDC, cheque especial etc.).O Home Equity, segundo o próprio Banco Central, é o produto com as menores taxas de juros (para pessoas físicas) no Brasil.
                            </p>
                        </div>
                    </div>


                    <div class="line"></div>
                    <h3 class="font-paragrafo-1 color-roxo">DOCUMENTAÇÃO</h3>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Quais documentos vocês pedem?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                O processo segue um checklist personalizado. Em geral, é solicitada a documentação de identificação, de renda e do imóvel (de todas as pessoas envolvidas na operação).
                            </p>
                        </div>
                    </div>
                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            A LendMe faz contato ou solicita documentos pelo WhatsApp?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Você poderá receber avisos pelo WhatsApp, mas todo o envio de documentação acontece na nossa plataforma, dentro da sua área logada.
                            </p>
                        </div>
                    </div>
                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Onde encontro a matrícula do imóvel?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Caso você não possua os documentos do seu imóvel, você poderá encontrar a matrícula no cartório de registro de imóvel da sua cidade, distrito ou sub-região. Vale ressaltar quea LendMe  também  consegue auxiliá-lo com essa questão.Basta falar com um dos nossos consultores especializados.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Consigo solicitar um empréstimo sem a matrícula do imóvel?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não é possível.Este é um documento fundamental para a conclusão do processo.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Consigo fazer o empréstimo sem comprovação de renda?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Não é possível.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            E se eu ficar sem renda depois de conseguir o empréstimo?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Neste caso,entre em contato conosco. Estaremos sempre à disposição para situações e imprevistos como este.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Preciso declarar imposto de renda para conseguir o empréstimo?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Sim.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Como declarar o empréstimo no Imposto de Renda?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Deve ser apontado/declarado na parte de dívidas e ônus.

                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            O que é alienação fiduciária?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                            A Alienação Fiduciária de coisa imóvel é instituída pela Lei 9514/97 que permite ao tomador do empréstimo permanecer com a posse direta do bem, que é a garantia do pagamento da dívida contraída.
                            </p>
                        </div>
                    </div>


                    <div class="line"></div>
                    <h3 class="font-paragrafo-1 color-roxo">TAXAS</h3>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            O que é CET?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                CET é o Custo Efetivo Total da operação, ou seja, é a taxa que inclui todos os custos envolvidos na operação, tais como juros, seguros, tarifas e demais despesas. A divulgação da CET é obrigatória pelo Banco Central. Não compare as taxas de juros cobradas, compare sempre a CET. A menor CET garante a melhor condição do empréstimo.
                            </p>
                        </div>
                    </div>




                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            O que significa taxa pós-fixada?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                É uma modalidade de empréstimo em que, além da taxa de juros mensal, o saldo devedor é corrigido por um indexador monetário contratado. No nosso caso, utilizamos o IPCA (inflação).
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            O que é IPCA?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                O IPCA é o Índice de Preços para o Consumidor Amplo, medido mensalmente pelo IBGE (Instituto Brasileiro de Geografia e Estatística). Foi criado com o objetivo de oferecer a variação dos preços no comércio para o público final, sendo considerado, pelo Banco Central, o índice brasileiro oficial da inflação ou deflação.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            O que é SAC e Price?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                São sistemas de amortização. Os mais comuns são: Sistema de Amortização Constante (SAC) ou a Tabela Price.  No sistema SAC,o que se mantém fixo é o valor da amortização. Como os juros vão diminuindo com o passar do tempo, consequentemente a prestação também diminuirá. Já na Tabela Price, a somatória entre juros e amortização é sempre igual. Entretanto, como o saldo devedor decresce, os juros também decrescem ao passo que a amortização aumenta, ou seja, amortiza-se mais no final quando comparado ao início do contrato.
                            </p>
                            <p class="lead">
                                Vale lembrar que é sempre importante você ficar atento ao valor das parcelas e de como os juros incidem sobre o saldo devedor.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Qual a taxa de juros de vocês para o empréstimo com garantia de imóvel?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                A simulação é personalizada e depende de vários fatores, mas nossas taxas começam 0,95% ao mês. As variações da Selic têm sido constantemente variadas pelo Copom, portanto, essa informação pode sofrer alterações.

                            </p>
                        </div>
                    </div>

                    <div class="line"></div>
                    <h3 class="font-paragrafo-1 color-roxo">CESSÃO</h3>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            O beneficiário dos meus boletos mudou. O que isso significa?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Os créditos que originamos podem ser cedidos para outras instituições. Para você, não há qualquer alteração nas condições de empréstimo.
                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo">
                            Meu empréstimo foi cedido. O que isso significa?
                        </h5>
                        <div class="ac-content">
                            <p class="lead">
                                Significa que seu crédito foi adquirido por outra Instituição, tornando-se a sua nova credora. Vale ressaltar que nenhuma condição contratada será alterada. Tudo será honrado como descrito em contrato previamente  estabelecido.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>