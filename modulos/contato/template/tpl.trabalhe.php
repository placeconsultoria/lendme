<section class="halfscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE?>images/lendme-bg-2.jpg">
    <div class="container-fluid">
        <div class="container-fullscreen">
            <div class="text-middle text-center ">
                <h2  class="m-t-0 font-title-1 text-white text-lg">TRABALHE CONOSCO</h2>
            </div>
        </div>
    </div>
</section>
<section class="background-white p-b-0 m-b-0">
    <div class="container">
        
        <div class="row">
            <div class="col-md-12 p-t-100 p-b-100">
                <!--                   <h3 class="font-paragrafo-1 color-roxo">SOBRE A LENDME</h3>-->
                <!--<div class="accordion accordion-shadow">

                    <h3 class="font-paragrafo-1 color-roxo">Nossas Vagas</h3>


                    <div class="ac-item">
                        <h5 class="ac-title color-roxo"><strong>Consultor Comercial</strong></h5>
                        <div class="ac-content">
                            <p>
                                <strong>Requisitos Obrigatórios:</strong> Experiência no gerenciamento de carteira, gestão de equipe comercial, comercialização de linhas de crédito tais como: Operações Estruturadas, Home Equity e Financiamento de Imóveis Residenciais e Comerciais com desenvolvimento de parcerias estratégicas para alavancar resultados.<br><br>
                                <strong>Requisitos desejáveis:</strong> Experiência na área de crédito. Pós-graduação e Cursos específicos (imobiliário e mercado de capitais) serão considerados diferenciais.<br><br>
                                <strong>Benefícios e Remuneração:</strong> a combinar <br><br>
                                <strong>Horário:</strong> comercial <br><br>
                                <strong>Tipo de contratação:</strong> CLT

                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo"><strong>Analista Jurídico Junior</strong></h5>
                        <div class="ac-content">
                            <p>
                                <strong>Requisitos Obrigatórios:</strong> Redação clara e objetiva; Hands On; Comprometimento; Dinamismo; Ownership; OAB ativa e experiência na em direito imobiliário.<br><br>
                                <strong>Requisitos desejáveis:</strong> Experiência na área imobiliária. Pós-graduação e Cursos específicos (imobiliário e mercado de capitais) serão considerados diferenciais.<br><br>
                                <strong>Benefícios e Remuneração:</strong> a combinar <br><br>
                                <strong>Horário:</strong> comercial<br><br> 
                                <strong>Tipo de contratação:</strong> CLT<br><br>
                                <strong>Atividades iniciais a serem desenvolvidas:</strong> (i) Análise documental para concessão de empréstimo com garantia imobiliária, (ii) Elaboração de minutas contratuais e Pareceres Legais; (iii) gestão de workflow para originação de empréstimos com garantia.

                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo"><strong>Analista Crédito Junior</strong></h5>
                        <div class="ac-content">
                            <p>
                                <strong>Requisitos Obrigatórios:</strong> Redação clara e objetiva; Hands On; Comprometimento; Dinamismo; Ownership; Experiência em avaliação de crédito.<br><br>
                                <strong>Requisitos desejáveis:</strong> Experiência na área imobiliária. Pós-graduação e Cursos específicos (imobiliário e mercado de capitais) serão considerados diferenciais.<br><br>
                                <strong>Benefícios e Remuneração:</strong> a combinar<br><br>
                                <strong>Horário:</strong> comercial<br><br>
                                <strong>Tipo de contratação:</strong> CLT<br><br>
                                <strong>Atividades iniciais a serem desenvolvidas:</strong> (i) Análise de crédito de pessoa física, (ii) Identificação de renda através de documentação pessoal e bancária; (iii) Pesquisa de informações cadastrais em sites oficiais.

                            </p>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title color-roxo"><strong>Engenheiro/Técnico de Avaliação Imobiliária</strong></h5>
                        <div class="ac-content">
                            <p>
                                <strong>Requisitos Obrigatórios:</strong> Redação clara e objetiva; Hands On; Comprometimento; Dinamismo; Ownership; Experiência em avaliação imobiliária.<br><br>
                                <strong>Requisitos desejáveis:</strong> Experiência na área imobiliária. Pós-graduação e Cursos específicos (imobiliário e mercado de capitais) serão considerados diferenciais.<br><br>
                                <strong>Benefícios e Remuneração:</strong> a combinar<br><br>
                                <strong>Horário:</strong> comercial<br><br>
                                <strong>Tipo de contratação:</strong> CLT<br><br>
                                <strong>Atividades iniciais a serem desenvolvidas:</strong> (i) Controle e monitoramento de avaliação de imóvel, (ii) Controle de empresas terceirizadas de avaliação de imóvel; (iii) Pesquisa de de valores de imóveis em sites especializados.

                            </p>
                        </div>
                    </div>

                    

                </div>-->
                
                <div class=" text-center">
            <h3>No momento não temos vagas disponíveis.</h3>
        </div>
                
            </div>
        </div>

    </div>
</section>

<!--<section class="background-overlay-dark p-t-50 p-b-60" style="background-image:url(<?= $URL_SITE?>images/bg-2.jpg);" data-stellar-background-ratio="0.7" id="faq">
    <div class="container xs-text-center sm-text-center text-light">
        <div class="row">
            <div class="col-12 text-center ">
                <img src="<?= $URL_SITE?>images/logo-dark3.png" />
                <h2 class="text-white font-title-1">Empowering your financial life</h2>
            </div>
        </div>
    </div>
</section>-->
<section id="contato" class="background-white  p-t- 0 m-t-0 p-b-50 ">
    <div class="container">
        <div class=" text-center">
            <h1 class="font-title-1 text-medium">ENVIE SEU CURRÍCULO</h1>
            <h3>Entraremos em conato o mais breve possível!</h3>
        </div>
        <div class="row">
            <div class="col-lg-8 p-t-50 center">
                <form id="frm_vaga" method="post">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name" >Nome</label>
                            <input type="text" aria-required="true" name="nome" class="form-control required " placeholder="" required>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="email" >E-mail</label>
                            <input type="email" aria-required="true" name="email" class="form-control required " placeholder="" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="telefone" >Telefone</label>
                            <input type="text" id="telefone2" name="telefone" class="form-control" placeholder="" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="oportunidade">Vaga</label>
                            <select id="oportunidade" name="oportunidade" class="form-control" required>
                                <option value="">Selecione</option>
                                <option value="Consultor Comercial">Consultor Comercial</option>
                                <option value="Analista Jurídico Junior">Analista Jurídico Junior</option>
                                <option value="Analista Crédito Junior">Analista Crédito Junior</option>
                                <option value="Engenheiro/Técnico de Avaliação Imobiliária">Engenheiro/Técnico de Avaliação Imobiliária</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="linkedin" >Linkedin</label>
                        <input type="text" id="linkedin" name="linkedin" class="form-control required" required>
                    </div>
                    <div class="form-group">
                        <label for="file" >Seu CV.</label>
                        <input type="file" name="file">
                    </div>
                    <div class="row">
                        <div class="form-group center">
                            <button class="btn btn-roxo" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i> Enviar informações</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>


