<section class="halfscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE ?>images/lendme-bg-2.jpg">
    <div class="container-fluid">
        <div class="container-fullscreen">
            <div class="text-middle text-center text-light">
                <h2  data-animate="fadeInUp" data-animate-delay="300"  class=" m-t-0 font-title-1 text-lg">POLÍTICA DE PRIVACIDADE</h2>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 m-t-20"  data-animate="fadeInUp" data-animate-delay="400" >

                <p class="lead font-paragrafo-1">
                    Esta Política de Privacidade estabelece o compromisso da Himov Negócios e Participações S/A. (“LendMe”) com a privacidade dos usuários do website www.lendme.com.br (“Website”). Você, enquanto usuário, deve ler e compreender esta Política de Privacidade antes de começar a utilizar o Website. Ao utilizar o Website Você automaticamente concorda com todos os termos de nossa Política de Privacidade e com os nossos Termos e Condições de Uso, e, ainda, manifesta o seu consentimento livre, expresso e informado com relação à coleta de tais informações, para fins do disposto no artigo 7º, inciso IX, da Lei 12.965/2014 (Marco Civil da Internet).
                </p>

                <p class="lead font-paragrafo-1">
                    Esta Política de Privacidade pode ser alterada a qualquer momento, sem prévio aviso por parte da LendMe. A última versão desta Política sempre estará disponível no Website: (www.lendme.com.br), e sugerimos veementemente que Você consulte eventuais atualizações de tempos em tempos. Salientamos que tais alterações serão aplicáveis desde o momento em que forem disponibilizadas no Website.
                </p>


                <p class="lead font-paragrafo-1">
                    Caso Você não concorde com os termos dessa Política de Privacidade, recomendamos que Você não utilize o Website.
                </p>

                <p class="lead font-paragrafo-1">
                    Nesta Política, Você encontrará as informações necessárias para entender sobre:
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    &bull; As informações coletadas pela LendMe;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    &bull; Forma de coleta das informações;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    &bull; Interrupção da coleta de informações;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    &bull; Utilização das informações coletadas pela LendMe;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    &bull; Casos de cessão e/ou divulgação das informações;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    &bull; Exclusão das informações.
                </p>


                <h2 class="font-paragrafo-1 m-t-50">1. Informações Coletadas</h2>

                <p class="lead font-paragrafo-1">
                    Para aprimorarmos nossos serviços e melhorarmos continuamente a experiência de navegação, coletamos informações importantes de nossos clientes. As informações que coletamos são as seguintes:
                </p>

                <p class="lead font-paragrafo-1 p-l-40">
                    a) Informações Pessoais: Ao se cadastrar, coletamos seu nome, e-mail, endereço, telefone de contato, CPF, RG, data de nascimento e sexo. Quando Você solicita cotações ou propostas, também iremos solicitar informações financeiras relevantes para que possam ser oferecidos produtos ou serviços adequados às suas necessidades. Tais informações poderão incluir sua renda, suas despesas, bens de sua propriedade e outros dados do seu patrimônio.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    b) Informações Não-Pessoais: Poderemos coletar informações e dados sobre a sua experiência de navegação em nosso Website. Ao longo de sua visita no Website ou Aplicativo, poderão ser automaticamente coletados o seu Internet Protocol (IP, com data e hora), sua localização, seu tipo de computador, tipo de celular, tipo de navegador, páginas e serviços acessados, informações sobre cliques, termos de busca digitados no Website, dentre outros.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">2. Forma de coleta das informações</h2>
                <p class="lead font-paragrafo-1">
                    Nós poderemos coletar e armazenar suas informações da seguinte forma:
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    a) A partir de seu cadastro no Website ou das suas solicitações de cotações e propostas de instituições financeiras, companhias de seguros, operadoras de meios de pagamento ou bandeira de cartão de crédito (“Instituições Parceiras”).
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    b) De Cookies, identificados a partir da interação com nosso site ou nossas publicidades que são transferidas para o aparelho do cliente visando reconhecê-lo na próxima navegação. Utilizamos cookies para proporcionar uma melhor experiência em nosso Website e viabilizar recursos personalizados como recomendações de infográficos, artigos, calculadoras, produtos e serviços financeiros, publicidades e informações adicionais do seu interesse.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    c)  De outras origens, tais como pixel tags, beacons e local shared objects.
                </p>
                <p class="lead font-paragrafo-1">
                    A LendMe poderá utilizar todas as informações sobre Você obtidas no Website, a qualquer tempo e a seu exclusivo critério, seja para estudos de caso, análise de dados, estatísticas, definição de estratégias de mercado ou para qualquer outra finalidade necessária à consecução dos objetivos do negócio da LendMe ou para garantir uma melhor experiencia no Website, ficando a LendMe expressamente autorizada por Você a veicular esses dados, estudos, análises, estatísticas e pesquisas a terceiros.
                </p>
                <p class="lead font-paragrafo-1">
                    Você, enquanto usuário, entende e concorda que o Website e outros sites da LendMe são acessíveis através de sites de busca, podendo localizar páginas com nome de usuários e/ou conteúdos publicados por Você, se aplicável.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">3. Interrupção da coleta de informações</h2>
                <p class="lead font-paragrafo-1">
                    Caso não deseje ter suas informações pessoais coletadas, como mencionamos no item “1.a”, acima, recomendamos que Você não as forneça. Neste caso, advertimos a Você, enquanto usuário, que certas funcionalidades do Website não poderão ser utilizadas. Você está ciente de que, para continuar utilizando o Website com a melhor experiência, a LendMe poderá não apagar suas informações pessoais após Você tê-las fornecido.
                </p>
                <p class="lead font-paragrafo-1">
                    Caso deseje interromper a coleta de informações não pessoais como mencionamos no item “1.b”, acima, Você deverá desabilitar o salvamento de cookies, pixel tags, beacons, local shared objects e outros, em seu navegador de internet, apagá-los e gerenciar sua utilização por meio da configuração do navegador que utiliza para acessar o Website, ou Você deverá sair da sua sessão do Aplicativo e não o utilizar mais.
                </p>
                <p class="lead font-paragrafo-1">
                    Os principais navegadores de internet possibilitam ao cliente gerenciar a utilização dos cookies em sua máquina. A nossa recomendação é que Você mantenha o salvamento de cookies, pixel tags, beacons, local shared objects e outros, ligados. Desta forma, é possível explorar todos os recursos de navegação personalizada oferecidos no Website, tendo em vista que, caso essas configurações sejam inabilitadas, é possível que algumas das funcionalidades oferecidas pela Website deixem de funcionar corretamente.
                </p>


                <h2 class="font-paragrafo-1 m-t-50">4. Armazenamento e proteção das informações coletadas</h2>
                <p class="lead font-paragrafo-1">
                    Após a coleta dos dados e informações mencionados nessa Política de Privacidade, a LendMe envidará seus melhores esforços para armazená-los sob as mais rígidas práticas de segurança de informação. Nosso banco de dados terá seu acesso criteriosamente restrito somente a alguns de nossos funcionários autorizados, que estão contratualmente obrigados a preservar a confidencialidade dos dados e informações dos usuários do Website.
                </p>
                <p class="lead font-paragrafo-1">
                    A LendMe envidará os melhores esforços razoáveis de mercado para garantir que suas informações serão sempre manipuladas de acordo com o estabelecido nesta Política de Privacidade. Ainda assim, Você deve saber que nós não somos responsáveis pela hipótese de quebra de segurança de nossa base de dados que cause a divulgação, uso ou acesso indevido de informações do usuário. Em tal caso, nenhuma compensação por parte da LendMe será devida a Você.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">5. Utilização das informações coletadas pela LendMe</h2>
                <p class="lead font-paragrafo-1">
                    Todas as informações coletadas pela LendMe no Website são utilizadas para permitir uma prestação de serviços e garantir uma experiência cada vez melhores ao usuário.
                </p>
                <p class="lead font-paragrafo-1">
                    Nós poderemos utilizar dados e informações (pessoais ou não) principalmente para:
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    a) Completar as solicitações de cotações dos usuários, propostas de produtos ou serviços oferecidos pelas Instituições Parceiras;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    b) Traçar perfis, estratégias de mercado e tendências demográficas de uso do Website;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    c) Entrar em contato com Você para confirmar ou verificar os dados e/ou informações fornecidos por Você;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    d) Garantir que o Website esteja sempre interessante e útil para Você e lhe proporcione, sempre, uma experiência única, o que poderá incluir a personalização de anúncios e sugestões de conteúdos, produtos ou serviços.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    e) Cruzar informações através do Google Analytics para analisar os perfis dos clientes e oferecer serviços mais personalizados;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    f) Proteger a segurança e a integridade da base de dados da LendMe;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    g) Conduzir diligências internas relativas aos negócios da LendMe;
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    h) Desenvolver, melhorar e oferecer serviços e produtos de terceiros
                </p>

                <h2 class="font-paragrafo-1 m-t-50">5.  Casos de cessão e/ou divulgação das informações</h2>

                <p class="lead font-paragrafo-1">
                    A viabilidade de certos serviços prestados pela LendMe só ocorre pelo compartilhamento de alguns dados e/ou informações, o que fazemos com responsabilidade e seguindo parâmetros rigorosos, cujos casos de compartilhamento são os seguintes:
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    a) Instituições Parceiras: para que Você possa receber cotações e propostas de produtos e serviços financeiros tais como cartões de crédito, seguros, empréstimos, financiamentos, investimentos e outros, as Instituições Parceiras necessitam ter acesso a informações pessoais e não pessoais. Apenas assim é possível que Você receba propostas adequadas ao que Você procura. As Instituições Parceiras terão acesso a essas informações apenas na medida em que forem necessárias para que elas possam oferecer produtos e serviços que Você solicitou ou manifestou interesse. Você também deve estar ciente de que no momento em que Você solicita um produto ou serviço das Instituições Parceiras por meio do nosso Website , as suas informações serão tratadas por essas instituições de acordo as suas próprias políticas. Não submeta pedidos de propostas e cotações caso não concorde com a forma com a qual as Instituições Parceiras tratam as suas informações.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    b) Novos negócios: no contínuo desenvolvimento do nosso negócio, processos de aquisição e fusão de empresas, estabelecimento de parcerias comerciais, joint ventures e outros negócios podem ocorrer. Nesses negócios, informações dos respectivos clientes também são transferidas, mas ainda assim, será mantida a Política de Privacidade.
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    c) Ordem judicial: a LendMe pode compartilhar dados pessoais em caso de requisição judicial
                </p>
                <p class="lead font-paragrafo-1 p-l-40">
                    d) Com a autorização do cliente: em demais casos, havendo a necessidade de compartilhamento das informações, enviaremos ao cliente uma notificação solicitando sua aprovação ou reprovação.
                </p>
                <p class="lead font-paragrafo-1">
                    Ao solicitar uma simulação de crédito no Website, Você, enquanto usuário, concorda e autoriza que o Banco ou a instituição parceira tenha acesso e/ou consulte seus dados financeiros, especialmente aqueles relacionados a depósitos, aplicações em outras instituições financeiras e informações a seu respeito constantes junto ao aos órgãos de proteção ao crédito tais como SPC e Serasa.
                </p>
                <p class="lead font-paragrafo-1">
                    As informações armazenadas no Website poderão ser fornecidas a terceiros, tais como instituições financeiras e demais agentes de mercado, para fins de análise, elaboração e gerenciamento de Propostas de Captação de Recursos, com o objetivo de gerar uma Proposta de Crédito, em consonância com a Política de Avaliação de Crédito. As informações submetidas poderão ser utilizadas para manutenção dos Serviços de Cobrança, gerenciamento de aplicações, recomendação de investimentos e manutenção de um banco de dados, de acordo com a legislação sobre proteção de dados e regulamentos do Banco Central, conforme aplicável.
                </p>

                <h2 class="font-paragrafo-1 m-t-50">7.Exclusão das Informações</h2>

                <p class="lead font-paragrafo-1">
                    Você poderá solicitar que as informações coletadas, conforme mencionado na presente Política de Privacidade, sejam excluídas, por meio da solicitação de descadastramento, diretamente no Website. A LendMe empreenderá os melhores esforços para atender a todos os pedidos de exclusão, no menor espaço de tempo possível. Tal exclusão impossibilitará novos acessos pelo usuário e/ou diminuir a qualidade de sua experiência no Website.
                </p>
                <p class="lead font-paragrafo-1">
                    Você está ciente e concorda que a LendMe respeitará o prazo de armazenamento mínimo de determinadas informações, conforme determinado pela legislação brasileira, ainda que Você solicite a exclusão de tais informações.
                </p>

            </div>
        </div>
    </div>
</section>