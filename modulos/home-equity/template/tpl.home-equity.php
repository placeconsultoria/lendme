<section id="slider">

    <div id="rev_slider_24_1_wrapper" class="rev_slider_wrapper fullscreen-container background-overlay-dark" data-alias="website-intro" data-source="gallery" style="background:#000000;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
        <div id="rev_slider_24_1" class="rev_slider fullscreenbanner tiny_bullet_slider" style="display:none;" data-version="5.4.1">
            <ul>

                <li data-index="rs-66" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="js/plugins/revolution/assets/images/deskbg-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="" data-slicey_shadow="0px 0px 0px 0px transparent">
                    <img src="<?= $URL_SITE ?>images/home_equity_lendme_banner_home.png"  alt="LendMe é a única fintech especializada em home equity que faz registros em Blockchain, trazendo muito mais segurança e tecnologia para seus empréstimos."  data-bgposition="center center" data-kenburns="on" data-duration="0" data-ease="Power2.easeInOut" data-scalestart="100" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                    <div class="tp-caption  font-title-1  tp-resizeme"
                         id="slide-66-layer-2"
                         data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['180','180','180','20']"
                         data-fontsize="['50','50','45','45']"
                         data-lineheight="['65','65','55','50']"
                         data-width="['95%','95%','95%','95%']"
                         data-height="none"
                         data-whitespace="['nowrap','nowrap','normal','normal']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":1000,"speed":800,"frame":"0","from":"sX:0.9;sY:0.9;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"sX:0.9;sY:0.9;opacity:0;fb:20px;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[20,20,20,20]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[20,20,20,20]"
                         style="z-index: 19; white-space: nowrap; font-weight: 500; color: #ffffff; letter-spacing: -2px;font-family:Rubik;">Conheça o Home Equity da LendMe</div>
                    <div class="tp-caption font-paragrafo-1 tp-resizeme"
                         id="slide-66-layer-3"
                         data-x="['left','left','left','left']" data-hoffset="['1','1','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['260','260',260','260']"
                         data-fontsize="['30','30','30','26']"
                         data-lineheight="['40','40','40','26']"
                         data-width="['none','none','800','500']"
                         data-height="none"
                         data-whitespace="['nowrap','nowrap','normal','normal']"
                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:0.9;sY:0.9;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"sX:0.9;sY:0.9;opacity:0;fb:20px;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[20,20,20,20]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[20,20,20,20]"
                         style="z-index: 20; white-space: normal; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Rubik;"> Empréstimo com garantia de imóvel com a agilidade que você precisa.
                    </div>
                    <div class="tp-caption font-paragrafo-1 tp-resizeme"
                         id="slide-66-layer-3"
                         data-x="['left','left','left','left']" data-hoffset="['1','1','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['320','320',320','340']"
                         data-fontsize="['26','26','26','20']"
                         data-lineheight="['44','44','44','22']"
                         data-width="['none','none','800','500']"
                         data-height="none"
                         data-whitespace="['nowrap','nowrap','normal','normal']"
                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:0.9;sY:0.9;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"sX:0.9;sY:0.9;opacity:0;fb:20px;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[20,20,20,20]"
                         data-paddingbottom="[40,40,40,40]"
                         data-paddingleft="[20,20,20,20]"
                         style="z-index: 20; white-space: normal; font-weight: 300; color: #ffffff; letter-spacing: 0px;font-family:Rubik;">
                            
                            <i class="fa fa-check" style="font-size: 20px; color: #27b6b0"></i> Taxas a partir de 0,81% a.m. + IPCA<br>
                            <i class="fa fa-check" style="font-size: 20px; color: #27b6b0"></i> Até 20 anos para pagar<br>
                            <i class="fa fa-check" style="font-size: 20px; color: #27b6b0"></i> Processo inteiro feito de forma online<br>
                            <i class="fa fa-check" style="font-size: 20px; color: #27b6b0"></i> Pré análise imediata<br>
                            <i class="fa fa-check" style="font-size: 20px; color: #27b6b0"></i> Assessoria personalizada<br>
                            <i class="fa fa-check" style="font-size: 20px; color: #27b6b0"></i> Plataforma segura baseada em Blockchain
                    </div>
                    <div class="tp-caption text-center Fashion-BigDisplay   tp-resizeme" id="slide-130-layer-11" 
                    data-x="['left','left','left','left']" 
                    data-hoffset="['0','0','0','-3']" 
                    data-y="['top','top','top','top']" 
                    data-voffset="['620','620','620','480']" 
                    data-width="none" 
                    data-height="none" 
                    data-whitespace="nowrap" 
                    data-transform_idle="o:1;" 
                    data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1290;e:Power3.easeInOut;" 
                    data-transform_out="opacity:0;s:300;s:300;" 
                    data-start="2340" 
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on" 
                    style="z-index: 8; white-space: nowrap;" 
                    data-fontsize="['26','26','26','20']"
                    data-paddingtop="[0,0,0,20]"
                    data-paddingright="[20,20,20,20]"
                    data-paddingbottom="[40,40,40,40]"
                    data-paddingleft="[20,20,20,20]">
                    <a href="#simulador" name="btn_menu" class="btn btn-outline btn-light btn-lg">FAÇA A SIMULAÇÃO ONLINE</a>
                            </div>
                         

                </li>
                <!-- SLIDE 2 
                <li data-index="rs-68" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="600"  data-thumb="js/plugins/revolution/assets/images/reachout_bg-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="" data-slicey_shadow="0px 0px 0px 0px transparent">
                    <img src="<?= $URL_SITE?>images/1920-ex-c2.jpg"  alt="LendMe é a única fintech especializada em home equity que faz registros em Blockchain, trazendo muito mais segurança e tecnologia para seus empréstimos."  data-bgposition="center center" data-kenburns="on" data-duration="5000" data-ease="Power2.easeInOut" data-scalestart="100" data-scaleend="150" data-rotatestart="0" data-rotateend="0" data-blurstart="20" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                    <div class="tp-caption  font-title-1  tp-resizeme"
                         id="slide-68-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['1','1','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-70','-70','-70','-70']"
                         data-fontsize="['90','90','70','50']"
                         data-lineheight="['90','90','70','50']"
                         data-width="['none','none','481','360']"
                         data-height="none"
                         data-whitespace="['nowrap','nowrap','normal','normal']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:0.9;sY:0.9;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"sX:0.9;sY:0.9;opacity:0;fb:20px;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 19; white-space: nowrap; font-size: 60px; line-height: 90px; font-weight: 500; color: #ffffff; letter-spacing: -2px;font-family:Rubik;">BEM-VINDO A UMA NOVA ERA</div>

                    <div class="tp-caption font-paragrafo-1 tp-resizeme"
                         id="slide-68-layer-3"
                         data-x="['center','center','center','center']" data-hoffset="['1','1','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-0','100',100','100']"
                         data-fontsize="['40','40','30','30']"
                         data-lineheight="['65','65','55','45']"
                         data-width="['none','none','481','360']"
                         data-height="none"
                         data-whitespace="['nowrap','nowrap','normal','normal']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:0.9;sY:0.9;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"sX:0.9;sY:0.9;opacity:0;fb:20px;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 20; min-width: 480px; max-width: 480px; white-space: normal; font-size: 60px; line-height: 45px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Rubik;"> Plataforma de Home Equity segura baseada em Blockchain</div>
                </li>-->
            </ul>
            <!--<div class="tp-bannertimer tp-bottom" style="height: 5px; background: rgb(254, 105, 31);"></div></div>-->
    </div>
    <script type="text/javascript">
        var tpj=jQuery;
        var revapi24;
        tpj(document).ready(function() {
            if(tpj("#rev_slider_24_1").revolution == undefined){
                revslider_showDoubleJqueryError("#rev_slider_24_1");
            }else{
                revapi24 = tpj("#rev_slider_24_1").show().revolution({
                    sliderType:"standard",
                    jsFileLocation:"revolution/js/",
                    sliderLayout:"fullscreen",
                    dottedOverlay:"none",
                    delay:9000,
                    navigation: {
                        keyboardNavigation:"off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                        mouseScrollReverse:"default",
                        onHoverStop:"off",
                        bullets: {
                            enable:false,
                            hide_onmobile:false,
                            style:"bullet-bar",
                            hide_onleave:false,
                            direction:"horizontal",
                            h_align:"center",
                            v_align:"bottom",
                            h_offset:0,
                            v_offset:50,
                            space:5,
                            tmp:''
                        }
                    },
                    responsiveLevels:[1240,1024,778,480],
                    visibilityLevels:[1240,1024,778,480],
                    gridwidth:[1240,1024,778,480],
                    gridheight:[868,768,960,720],
                    lazyType:"none",
                    shadow:0,
                    spinner:"off",
                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,
                    shuffle:"off",
                    autoHeight:"off",
                    fullScreenAutoWidth:"off",
                    fullScreenAlignForce:"off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "0px",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        nextSlideOnWindowFocus:"off",
                        disableFocusListener:false,
                    }
                });
            }

            if(revapi24) revapi24.revSliderSlicey();
        });	/*ready*/
    </script></section>

<?php require_once("template/simulador-separado.php"); ?>

<section class="parallax background-overlay-dark p-t-50 p-b-50" style="background-image:url(<?= $URL_SITE ?>images/bg-2.jpg);" data-stellar-background-ratio="0.7">
    <div class="container xs-text-center sm-text-center text-light">
        <div class="row">
            <div class="col-lg-5 p-b-60">
                <h4 class="font-title-1">A evolução do</h4>
                <h1 class="font-title-1">Home Equity</h1>
                <p class="lead font-paragrafo-1">
                    Com o Home Equity (crédito com garantia de imóvel) você dá liquidez ao seu patrimônio imobiliário. Essa modalidade de crédito ajuda a solucionar suas questões financeiras, acessando recursos de forma ágil e segura.
                </p>
                <p class="lead font-paragrafo-1">
                    Você pode pagar suas dívidas, retomando o controle da sua vida financeira, abrir ou expandir um negócio, reformar sua casa ou pagar estudos dos filhos.  <br><br><strong>Descubra as melhores condições para você</strong>.
                </p>
            </div>
            <div class="col-lg-7">
             <h3 class="font-title-2 text-center">DADOS DO MERCADO DE HOME EQUITY:</h3>
                <div class="row">
               
                    <div class="col-lg-6">
                        <div class="text-center">
                            <div class="counter text-lg"><small style="font-size: 40px;">R$ 3.3 bilhões</small></div>
                            <div class="seperator seperator-small"></div>
                            <p class="lead font-paragrafo-1">CRÉDITO CONCEDIDO</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="text-center">
                            <div class="counter text-lg"> <span data-speed="4500" data-refresh-interval="3" data-to="58" data-from="0" data-seperator="true"></span><small style="font-size: 40px;">%</small> </div>
                            <div class="seperator seperator-small"></div>
                            <p class="lead font-paragrafo-1">CRESCIMENTO 2019 X 2018</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="text-center">
                            <div class="counter text-lg"><small style="font-size: 40px;">R$</small> <span data-speed="3000" data-refresh-interval="1" data-to="500" data-from="15" data-seperator="true"></span> <small style="font-size: 30px;">bilhões</small> </div>
                            <div class="seperator seperator-small"></div>
                            <p class="lead font-paragrafo-1">NOS PRÓXIMOS 20 ANOS</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="text-center">
                            <div class="counter text-lg"><small style="font-size: 40px;">2.0%</small></div>
                            <div class="seperator seperator-small"></div>
                            <p class="lead font-paragrafo-1 text-uppercase">Taxa Selic</p>
                        </div>
                        
                    </div>
                    
                </div>
                <div class="text-right" style="margin-top: 10px">
                    <h5 class="font-title-2 text-right">Fonte: GEO Digital</h5>
                </div>
            </div>
            <div class="col-lg-12 text-center p-t-0 m-t-0">
                <a href="#simulador" name="btn_menu" class="btn m-t-0 btn-lg"><i class="fa fa-calculator"></i> FAÇA A SIMULAÇÃO ONLINE</a>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 center text-center">
                <h4 class="font-title-1 text-uppercase color-roxo">o que é?</h4>
                <h1 style="font-size: 60px;" class="font-title-1  text-uppercase color-roxo">Home Equity</h1>

            </div>
        </div>
        <div class="row p-t50">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12 m-t-20">
                        <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-40">
                            O crédito concedido é garantido por um imóvel em seu nome (pessoa física) e você permanece como proprietário.
                            O home equity permite que você e sua família permaneçam na sua casa, sem precisar mudar, evitando custos e transtornos. Além disso, você pode usar outros imóveis de sua propriedade para obter o crédito desejado. <strong>Novas possibilidades com comodidade, segurança e agilidade</strong>.
                        </p>
                    </div>
                    <div class="col-lg-12 m-t-50">
                        <div class="call-to-action btn-roxo call-to-action-colored text-center p-t-20">
                            <img src="<?= $URL_SITE ?>images/logo-dark3.png" class="img-fluid m-b-20" style="max-width: 200px;" />
                            <h2 class="font-title-1 m-b-10 text-white color-azul-b2b text-uppercase">Home Equity</h2>
                            <h3 class="font-title-1 m-b-10 text-white  text-uppercase">crédito de até 50% do valor do seu imóvel e 20 anos para pagar</h3>
                            <a href="#simulador" name="btn_menu" class="btn btn-lg"><i class="fa fa-calculator"></i> SIMULADOR ONLINE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!--<section id="slider" class="p-t-100 p-b-100" data-vide-bg="<?= $URL_SITE ?>homepages/portfolio/video/Head-or-Tails">
    <div class="container">
        <div class="container-fullscreen">
            <div class="text-middle text-dark text-center">
                <h2 class="text-medium-light font-title-1 color-roxo text-uppercase" data-animate="fadeInDown" data-animate-delay="500">Home Equity</h2>
                <h3 data-animate="fadeInDown" data-animate-delay="1000" class="m-b-50 font-paragrafo-1 color-roxo">Assista o vídeo</h3>
                <a href="https://www.youtube.com/watch?v=WFvKwE_Azbg" data-lightbox="iframe" class="play-button dark"><i class="fa fa-play"></i></a>
            </div>
        </div>
    </div>
</section>-->



<section id="image-block" class="image-block no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6" style="height:609px;background:url(<?= $URL_SITE ?>images/home_equity_pra_quem.png) 60% 50% / cover no-repeat;"></div>
            <!--<div class="col-lg-6" style="height:609px;background:url(<?= $URL_SITE ?>banner-new.jpeg) 60% 50% / cover no-repeat;"></div>-->
            <div class="col-lg-6">
                <div class="heading-text heading-section">
                    <h2 class="font-title-1 color-roxo">PARA QUEM?</h2>
                    <p class="lead font-paragrafo-1">
                        Pessoas físicas (mesmo com dívidas), que tenham imóvel quitado ou não, podem obter o empréstimo, com as melhores taxas e prazo mais longo.
                    </p>
                    <p class="lead font-paragrafo-1">
                        Pessoas jurídicas também podem obter o crédito, bastando que o imóvel garantidor esteja em nome de pessoa física.
                    </p>
                    <p class="lead font-paragrafo-1">
                        Para você, empresário, conheça também a nossa área de operações estruturadas.
                    </p>
                    <p class="lead font-paragrafo-1">
                        A proposta da LendMe é ser uma ONE-STOP-SHOP, reunindo um portfolio diversificado de produtos de base imobiliária para todos os tipos de clientes e necessidades.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="image-block2" class="image-block no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="heading-text heading-section">
                    <h2 class="font-title-1 color-roxo">QUANDO USAR?</h2>
                    <p class="lead font-paragrafo-1">
                        Tem dívidas? Quer reorganizar a vida financeira? Esse é o momento de usar um home equity. Obtenha crédito com taxas mais baixas e prazos mais longos.
                    </p>
                    <p class="lead font-paragrafo-1">
                        Tem outros sonhos para você e sua família? Quer empreender, financiar os estudos dos filhos ou reformar a casa? Tenha acesso às taxas mais baixas do mercado. Simule e consulte nossos especlialistas!
                    </p>
                    <p class="lead font-paragrafo-1">
                        Ou ainda, é empresário e está em busca de capital de giro? O home equity também está disponível para você (desde que o imóvel garantidor esteja em nome de pessoa física).
                    </p>
                </div>
                <a href="#simulador" name="btn_menu" class="btn btn-lg"><span>Fazer simulação <i class="fa fa-calculator"></i> </span></a>
            </div>
            <div class="col-lg-6" style="height:609px;background:url(<?= $URL_SITE ?>images/home_equity_como_usar.png) 60% 50% / cover no-repeat;"></div>
            <!--<div class="col-lg-6" style="height:609px;background:url(<?= $URL_SITE ?>img-1.png) 50% 50% / cover no-repeat;">-->
            </div>
        </div>
    </div>
</section>
<section class="p-t-60">
            <div class="container">
            
            <div class="row">
            <div class="col-lg-10 center text-center">
                <h1 style="font-size: 60px;" class="font-title-1  text-uppercase color-roxo">A LendMe na Mídia</h1>

            </div>
        </div>
            
                <div class="carousel m-t-60 p-b-60" data-items="3" data-items-sm="4" data-items-xs="3" data-items-xxs="2" data-margin="20" data-arrows="true" data-autoplay="true" data-autoplay-timeout="3000" data-loop="true">
                    <div>
                        <a href="https://valorinveste.globo.com/produtos/credito/noticia/2020/08/03/nova-fintech-chega-ao-mercado-de-emprestimo-com-garantia-de-imovel.ghtml" target="_blank"><img alt="" src="<?= $URL_SITE ?>images/logo-fb-investe.jpg"> </a>
                    </div>
                    <div>
                        <a href="https://www.idinheiro.com.br/lend-me/" target="_blank"><img alt="" src="<?= $URL_SITE ?>images/logo-idinheiro.jpg"> </a>
                    </div>
                    <div>
                        <a href="https://www.abecip.org.br/imprensa/noticias/nova-fintech-chega-ao-mercado-de-emprestimo-com-garantia-de-imovel" target="_blank"><img alt="" src="<?= $URL_SITE ?>images/logo-abecip.jpg"> </a>
                    </div>
                    <div>
                        <a href="https://finsiders.medium.com/ex-brazilian-mortgages-monta-fintech-de-home-equity-2620e82ba5cf" target="_blank"><img alt="" src="<?= $URL_SITE ?>images/logo-finsiders.jpg"> </a>
                    </div>
                    <div>
                        <a href="https://jornalempresasenegocios.com.br/negocios/cinco-vantagens-dos-emprestimos-que-tem-imoveis-como-garantia/" target="_blank"><img alt="" src="<?= $URL_SITE ?>images/logo-empresas-e-negocios.jpg"> </a>
                    </div>
                    <div>
                        <a href="http://www.portalradarimobiliario.com.br/noticia/29219,home-equity-fintech-oferece-modalidade-com-juros-de-113-ao-mes-.html" target="_blank"><img alt="" src="<?= $URL_SITE ?>images/logo-radar-imobiliario.jpg"> </a>
                    </div>
                </div>
            </div>

        </section>
    <section class="background-grey p-b-100">
    <div class="container">
            <div class="row text-center align-itens-center">
                <div class="col-lg-8 center">
                    <h3 class="m-b-50 font-paragrafo-1 color-roxo text-center">Saiba mais sobre o Home Equity</h3>
                
                   
                    <div class="shadow ml-5 text-center align-items-center">
                    <div class="embed-responsive embed-responsive-9by16 text-center align-items-center"><iframe width="1280" height="720" src="https://www.youtube.com/embed/WFvKwE_Azbg?rel=0&amp;showinfo=0" allowfullscreen=""></iframe>
                    </div>
                    </div>
                
            </div>
    </div>
</section>
