<section class="halfscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE ?>images/lendme-bg-2.jpg">
    <div class="container-fluid">
        <div class="container-fullscreen">
            <div class="text-middle text-center text-light">
                <h1 class="font-title-1 text-lg">UMA FINTECH ÁGIL E TRANSPARENTE</h1>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
     <div class="heading-text heading-section text-center m-b-40">
            <h2 class="p-t-0 m-t-0 font-title-1">Muito prazer! Somos a LendMe.</h2>
             <h3>E estamos aqui para te ajudar a reorganizar a sua vida financeira.</h3>
        </div>
        <div class="row">
            <div class="col-lg-12 m-t-20" >
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-20">
                    Criada no crescente mercado de home equity – crédito com garantia de imóvel - a LendMe  é uma fintech que foca em inovação e tecnologia, solucionando de modo personalizado a necessidade de cada cliente.
                </p>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-20">
                De maneira transparente, ética e confiável, nascemos para ajudar pessoas e empresas, proprietárias de imóveis que precisam de crédito, a captar
recursos de forma fácil, rápida e segura, evitando que percam tempo, passem constrangimentos ou tenham custos excessivos e desnecessários.
                </p>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-20">
                    
                    Com um time de executivos experientes, somos uma <em>ONE STOP SHOP</em> que oferece um portfolio diversificado de produtos de base imobiliária, operando com processos 100% registrados em Blockchain sistema que mais confere segurança e confiabilidade às transações financeiras.
                    
                </p>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-20">
                    Pautada na melhor experiência, a LendMe prioriza a rapidez na aprovação de crédito para quem busca estabilizar as finanças, consolidar dívidas, obter capital de giro ou garantir recursos para empreender. Por meio de nossa plataforma digital, o processo é todo feito de forma online, com recursos seguros, que tornam a operação mais ágil e confortável para nossos clientes. Sim, agora você pode ampliar suas possibilidades!
                </p>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-20">
                    Atuando de modo transparente e com o desafio de atender o mercado de home equity de forma efetiva, criativa e confiável, atualmente estamos
operando no Estado de São Paulo.
                </p>
                <p>
                <em><strong>EMPOWERING YOUR FINANCIAL LIFE</strong></em>
                </p>
            </div>
        </div>
    </div>
</section>

<!--<section class=" background-overlay-dark p-t-50 p-b-60" style="background-image:url(<?= $URL_SITE ?>images/bg-2.jpg);" data-stellar-background-ratio="0.7" >
    <div class="container xs-text-center sm-text-center text-light">
        <div class="row">
            <div class="col-12 text-center text-white m-b-50">
                <img src="<?= $URL_SITE ?>images/logo-dark3.png" class="img-fluid m-b-20" data-animate="fadeInUp" data-animate-delay="150" />
                <h1  class="font-title-1 text-medium m-b-0 p-b-0 text-uppercase " data-animate="fadeInUp" data-animate-delay="250">RETOME O CONTROLE DA SUA VIDA FINANCEIRA!</h1>
                <h2 class="font-title-1 m-t-20" data-animate="fadeInUp" data-animate-delay="350"> Um novo formato de crédito chegou</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="shadow ml-5">
                    <div class="embed-responsive embed-responsive-9by16">
                        <iframe width="1280" height="720" src="https://www.youtube.com/embed/5N_QT6r_4Eo?rel=0&amp;showinfo=0" allowfullscreen=""></iframe>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>-->

<!--<section id="slider" class="p-t-100 p-b-100" data-vide-bg="<?= $URL_SITE ?>homepages/portfolio/video/Head-or-Tails">
    <div class="container">
        <div class="container-fullscreen">
            <div class="text-middle text-dark text-center">
                <h2 class="text-medium-light font-title-1 color-roxo text-uppercase" data-animate="fadeInDown" data-animate-delay="500">Home Equity</h2>
                <h3 data-animate="fadeInDown" data-animate-delay="1000" class="m-b-50 font-paragrafo-1 color-roxo">Assista o vídeo</h3>
                <a href="https://www.youtube.com/watch?v=5N_QT6r_4Eo" data-lightbox="iframe" class="play-button dark"><i class="fa fa-play"></i></a>
            </div>
        </div>
    </div>
</section>-->

<section class=" background-overlay-dark p-t-50 p-b-60" style="background-image:url(<?= $URL_SITE ?>images/bg-2.jpg);" data-stellar-background-ratio="0.7" >
<div class="container">
<div class="container-fullscreen">
<div class="text-middle">
<div class="row justify-content-between">
<div class="col-md-5">
<div class="heading-text  text-light">
<img src="<?= $URL_SITE ?>images/logo-dark3.png" class="img-fluid m-b-20"/>
<h1 style="font-size:36px!important"><span>RETOME O CONTROLE DA SUA VIDA FINANCEIRA!</span></h1>
<p> Um novo formato de crédito chegou.</p>
<!--<a href="#" class="btn btn-light btn-outline btn-rounded">Read More</a>-->
</div>
</div>
<div class="col-md-7">
<div class="shadow ml-5">
<div class="embed-responsive embed-responsive-9by16"><iframe width="1280" height="720" src="https://www.youtube.com/embed/5N_QT6r_4Eo?rel=0&amp;showinfo=0" allowfullscreen=""></iframe>
</div>

</div>


</div>
</div>
</div>

</div>
</div>
</div>

<section class="background-grey">
    <div class="container">
        <div class="heading-text heading-section text-center m-b-40">
            <h2 class="p-t-0 m-t-0 font-title-1">CONHEÇA A NOSSA EQUIPE</h2>
        </div>

        <div id="blog" class="grid-layout post-3-columns m-b-0 p-t-0" data-item="post-item">

            <!-- Post item-->
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a name="saiba_mais_time" href="1">
                            <img alt="Elyseu Mardegan Jr., sócio-fundador e CEO na LendMe" src="<?= $URL_SITE?>equipe/equipe-elyseu.jpg">
                        </a>
                    </div>
                    <div class="post-item-description" >
                        <h2><a href="https://www.linkedin.com/in/elyseu-mardegan-jr/" target="_blank">Elyseu Mardegan</a></h2>
                        <p style="font-size: 12px;" class="lead font-weight-600">Fundador</p>
                        <p class=""  style="min-height: 120px; font-size: 12px;">
                            <?php echo substr("Elyseu Mardegan Jr, Engenheiro e Mestre em Administração de Empresas pela FGV, com cursos de especialização na UCLA e Kellogg, nos EUA. Possui mais de 40 anos de experiência no mercado financeiro",0,150)."..." ?>
                        </p>

                        <div class="row">
                            <div class="col-md-12">
                                <a name="saiba_mais_time" href="1" class="m-b-20 btn btn-secondary">SAIBA MAIS <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>

                        <div class="align-center">
                            <a class="btn btn-xs btn-slide btn-light" href="https://www.linkedin.com/in/elyseu-mardegan-jr/" target="_blank">
                                <i class="fab fa-linkedin"></i>
                                <span>LINKEDIN</span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>

            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a name="saiba_mais_time" href="2" target="_blank">
                            <img alt="Analu Nogueira, sócia-executiva e head de operações na LendMe" src="<?= $URL_SITE?>equipe/equipe-analu.jpg">
                        </a>
                    </div>
                    <div class="post-item-description" >
                        <h2><a href="https://www.linkedin.com/in/analu-nogueira-nascimento-80206b25/">Analu Nogueira</a></h2>
                        <p style="font-size: 12px;" class="lead font-weight-600">Operações</p>
                        <p class=""  style="min-height: 120px; font-size: 12px;">
                            <?php echo substr("Formada em direito OAB/SP, pós-graduada em Direito Civil e Processo Civil pela Escola Paulista de Direito e especialista em Direito Empresarial Imobiliário pelo Secovi, Analu possui curso de extensão em administração de empresa pela FGV/SP",0,150)."..." ?>
                        </p>

                        <div class="row">
                            <div class="col-md-12">
                                <a name="saiba_mais_time" href="2" class="m-b-20 btn btn-secondary">SAIBA MAIS <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>

                        <div class="align-center">
                            <a class="btn btn-xs btn-slide btn-light" href="https://www.linkedin.com/in/analu-nogueira-nascimento-80206b25/" target="_blank">
                                <i class="fab fa-linkedin"></i>
                                <span>LINKEDIN</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a name="saiba_mais_time" href="3">
                            <img alt="Felipe Leite, sócio-executivo e head de planejamento na LendMe" src="<?= $URL_SITE?>equipe/equipe-felipe.jpg">
                        </a>
                    </div>
                    <div class="post-item-description">
                        <h2><a href="https://www.linkedin.com/in/felipe-leite-70173a70/" target="_blank">Felipe Leite</a></h2>
                        <p style="font-size: 12px;" class="lead font-weight-600">Planejamento</p>
                        <p class=""  style="min-height: 120px; font-size: 12px;">
                            <?php echo substr("Bacharel em Administração pela PUC/São Paulo, pós-graduado em Controladoria pelo Mackenzie (SP) e com especialização em Contabilidade pelo Ibmec (SP). Profissional com 15 anos de experiência em instituições financeiras (BankBoston, Itaú, BFRE, PRB, Creditas)",0,150)."..." ?>
                        </p>

                        <div class="row">
                            <div class="col-md-12">
                                <a name="saiba_mais_time" href="3" class="m-b-20 btn btn-secondary">SAIBA MAIS <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>

                        <div class="align-center">
                            <a class="btn btn-xs btn-slide btn-light" href="https://www.linkedin.com/in/felipe-leite-70173a70/" target="_blank">
                                <i class="fab fa-linkedin"></i>
                                <span>LINKEDIN</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
        <div id="blog" class="grid-layout post-3-columns m-b-0 p-t-0" data-item="post-item">
        <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a name="saiba_mais_time" href="4">
                            <img alt="Jefferson Pavarin, sócio-executivo e diretor de operações estruturadas na LendMe" src="<?= $URL_SITE?>equipe/equipe-jefferson.jpg">
                        </a>
                    </div>
                    <div class="post-item-description" >
                        <h2><a href="https://www.linkedin.com/in/jefferson-pavarin/" target="_blank">Jefferson Pavarin</a></h2>
                        <p style="font-size: 12px;" class="lead font-weight-600">Operações Estruturadas</p>
                        <p class=""  style="min-height: 120px; font-size: 12px;">
                            <?php echo substr("Formado em Administração de Empresas e com MBA em Desenvolvimento Imobiliário pela FUPAM (FAU-USP), desde 1997 desenvolve suas atividades profissionais em grandes e conceituadas empresas do Mercado Financeiro e de Capitais",0,150)."..." ?>
                        </p>

                        <div class="row">
                            <div class="col-md-12">
                                <a name="saiba_mais_time" href="4" class="m-b-20 btn btn-secondary">SAIBA MAIS <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>

                        <div class="align-center">
                            <a class="btn btn-xs btn-slide btn-light" href="https://www.linkedin.com/in/jefferson-pavarin/" target="_blank">
                                <i class="fab fa-linkedin"></i>
                                <span>LINKEDIN</span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a name="saiba_mais_time" href="6">
                            <img alt="Luis Christiano, sócio-executivo e chief technology officer na LendMe" src="<?= $URL_SITE?>equipe/equipe-luis.jpg">
                        </a>
                    </div>
                    <div class="post-item-description">
                        <h2><a href="https://www.linkedin.com/in/luis-r-sambiase-christiano/" target="_blank">Luís Christiano</a></h2>
                        <p style="font-size: 12px;" class="lead font-weight-600">Tecnologia</p>
                        <p class=""  style="min-height: 120px; font-size: 12px;">
                            <?php echo substr(" Formado em Turismo, pela Escola de Comunicações e Artes – USP, com formação em Marketing pela New York University e MBA –  Marketing de Serviços, FIA – USP. Começou sua carreira no Banco Real como Trainee.",0,150)."..." ?>
                        </p>

                        <div class="row">
                            <div class="col-md-12">
                                <a name="saiba_mais_time" href="6" class="m-b-20 btn btn-secondary">SAIBA MAIS <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>

                        <div class="align-center">
                            <a class="btn btn-xs btn-slide btn-light" href="https://www.linkedin.com/in/luis-r-sambiase-christiano/" target="_blank">
                                <i class="fab fa-linkedin"></i>
                                <span>LINKEDIN</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a name="saiba_mais_time" href="7">
                            <img alt="Mauricio Silva, sócio-executivo e chief financial officer na LendMe" src="<?= $URL_SITE?>equipe/equipe-mauricio.jpg">
                        </a>
                    </div>
                    <div class="post-item-description">
                        <h2><a href="https://www.linkedin.com/in/mauricio-p-silva/" target="_blank">Maurício Silva</a></h2>
                        <p style="font-size: 12px;" class="lead font-weight-600">Financeiro</p>
                        <p class=""  style="min-height: 120px; font-size: 12px;">
                            <?php echo substr("Formado em Ciências Econômicas e com MBA em Finanças Empresariais pela FGV, atua há 30 anos no mercado financeiro: Operações de financiamento, empréstimos imobiliários, consultoria de planejamento financeiro; e Mercado de Capitais",0,150)."..." ?>
                        </p>
                        <div class="row">
                            <div class="col-md-12">
                                <a name="saiba_mais_time" href="7" class="m-b-20 btn btn-secondary">SAIBA MAIS <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>
                        <div class="align-center">
                            <a class="btn btn-xs btn-slide btn-light" href="https://www.linkedin.com/in/mauricio-p-silva/" target="_blank">
                                <i class="fab fa-linkedin"></i>
                                <span>LINKEDIN</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
        <!--<div data-animate="fadeInUp" class="heading-text heading-section text-center m-b-40 animated fadeInUp visible">
            <h3 class="p-t-0 m-t-0 font-title-1">NOSSO TIME COMERCIAL</h3>
        </div>-->
        <div id="blog" class="grid-layout post-3-columns m-b-0 p-t-0" data-item="post-item">
        
            
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a name="saiba_mais_time" href="5">
                            <img alt="Lucia Iwami, sócia-executiva e head comercial na LendMe" src="<?= $URL_SITE?>equipe/equipe-lucia.jpg">
                        </a>
                    </div>
                    <div class="post-item-description" >
                        <h2><a href="https://www.linkedin.com/in/luciaharumiakamatsuiwami/" target="_blank"> Lúcia Iwami</a></h2>
                        <p style="font-size: 12px;" class="lead font-weight-600">Comercial</p>
                        <p class=""  style="min-height: 120px; font-size: 12px;">
                            <?php echo substr("Lúcia Harumi Akamatsu Iwami, formada em Administração de Empresas na Universidade São Judas Tadeu, profissional com mais de 20 anos de experiência na área comercial, vivenciada em empresas renomadas. Especialista em produtos",0,150)."..." ?>
                        </p>
                        <div class="row">
                            <div class="col-md-12">
                                <a name="saiba_mais_time" href="5" class="m-b-20 btn btn-secondary">SAIBA MAIS <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>

                        <div class="align-center">
                            <a class="btn btn-xs btn-slide btn-light" href="https://www.linkedin.com/in/luciaharumiakamatsuiwami/" target="_blank">
                                <i class="fab fa-linkedin"></i>
                                <span>LINKEDIN</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a name="saiba_mais_time" href="10">
                            <img alt="Andressa Gasques, consultora comercial na LendMe" src="<?= $URL_SITE?>equipe/equipe-andressa.jpg">
                        </a>
                    </div>
                    <div class="post-item-description" >
                        <h2><a href="https://www.linkedin.com/in/andressa-gasques-a6004670/" target="_blank">Andressa Gasques</a></h2>
                        <p style="font-size: 12px;" class="lead font-weight-600">Comercial</p>
                        <p class=""  style="min-height: 120px; font-size: 12px;">
                            <?php echo substr("Profissional com vasta experiência em comercialização do Home Equity. Já trabalhou nas empresas BFRE, Banco Itaú, Banco BMG, Domus Cia Hipotecária, Sabemi e Banco Bari",0,150)."..." ?>
                        </p>
                        <div class="row">
                            <div class="col-md-12">
                                <a name="saiba_mais_time" href="10" class="m-b-20 btn btn-secondary">SAIBA MAIS <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>
                        <div class="align-center">
                            <a class="btn btn-xs btn-slide btn-light" href="https://www.linkedin.com/in/andressa-gasques-a6004670/" target="_blank">
                                <i class="fab fa-linkedin"></i>
                                <span>LINKEDIN</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a name="saiba_mais_time" href="11">
                            <img alt="Maria Julia Rostitucci, head de marketing na LendMe" src="<?= $URL_SITE?>equipe/equipe-mj.jpg">
                        </a>
                    </div>
                    <div class="post-item-description" >
                        <h2><a href="https://www.linkedin.com/in/andressa-gasques-a6004670/" target="_blank">Maria Julia Rostitucci</a></h2>
                        <p style="font-size: 12px;" class="lead font-weight-600">Marketing</p>
                        <p class=""  style="min-height: 120px; font-size: 12px;">
                            <?php echo substr("Profissional de Marketing com mais de 15 anos de experiência, atuando diretamente em projetos de comunicação e marca para diversos públicos, nos segmentos da Construção Civil",0,150)."..." ?>
                        </p>
                        <div class="row">
                            <div class="col-md-12">
                                <a name="saiba_mais_time" href="11" class="m-b-20 btn btn-secondary">SAIBA MAIS <i class="fa fa-plus"></i> </a>
                            </div>
                        </div>
                        <div class="align-center">
                            <a class="btn btn-xs btn-slide btn-light" href="https://www.linkedin.com/in/mariajuliarostitucci/" target="_blank">
                                <i class="fab fa-linkedin"></i>
                                <span>LINKEDIN</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-3" tabindex="-1" role="modal" aria-labelledby="modal-label-3"   aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title font-paragrafo-1 color-roxo">CONHEÇA O NOSSO TIME</h4>
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 p-20">
                        <h2 id="modal_nome" class="font-paragrafo-1 color-roxo"></h2>
                        <h3 id="modal_cargo" class="font-paragrafo-1 color-roxo"></h3>
                        <p  id="modal_texto" class="lead"></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn" type="button">FECHAR</button>
            </div>
        </div>
    </div>
</div>