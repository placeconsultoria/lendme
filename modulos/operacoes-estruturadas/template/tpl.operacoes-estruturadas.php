<section class="halfscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE ?>images/bg-b2b.jpg">
    <div class="container-fluid">
        <div class="container-fullscreen">
            <div class="text-middle text-center ">
                <h2  data-animate="fadeInUp" data-animate-delay="300"  class="m-t-0 font-title-1 text-white text-lg">OPERAÇÕES ESTRUTURADAS</h2>
                <h2  data-animate="fadeInUp" data-animate-delay="400" class="font-title-1"></h2>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-20">
                    Na LendMe, você encontra atendimento para empresas de médio e grande porte e obtém diversas formas de operações de crédito para transformar seus ativos de base imobiliária em recursos financeiros. Sejam imóveis residenciais, comerciais, loteamentos ou industriais, produtos específicos estarão disponíveis em operações desenhadas para as necessidades e perfil de cada cliente.
                </p>
            </div>
            <div class="col-lg-12 text-left">
                <a href="#contato_agente" class="btn" name="btn_menu"> Fale com a gente</a>
            </div>
        </div>
    </div>
</section>

<section class="roxo-b2b background-overlay-dark p-t-60 p-b-60" style="background-image:url(<?= $URL_SITE ?>images/bg-b2b-full.jpg);" data-stellar-background-ratio="0.7" >
    <div class="container xs-text-center sm-text-center text-light">
        <div class="row">
            <div class="col-12 text-center ">
                <img src="<?= $URL_SITE ?>images/logo-dark.png" class="img-fluid" />
                <h3  class="font-title-1 m m-b-30 p-b-0 text-uppercase color-laranja" >Tem uma empresa e quer entender como podemos te ajudar?</h3>
                <a href="#contato_agente" name="btn_menu" class="btn btn-light btn-azul">Fale conosco</a>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center"   >
                <h2 class="font-title-1">Produtos inteligentes para públicos exigentes</h2>
            </div>
        </div>
        <div class="row p-t-100">
            <div class="col-lg-12" >
                <h2 class="font-title-1 color-laranja">CRI</h2>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-10">
                    Instrumento dedicado a carregar as garantias de todos os produtos desse segmento e que viabiliza a venda dos mesmos ao mercado.
                </p>
            </div>
            <div class="line"></div>
            <div class="col-lg-12">
                <h2 class="font-title-1 color-laranja">OPERAÇÃO DE ESTOQUE</h2>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-10">
                    Voltado às incorporadoras, construtoras e loteadoras que disponham de estoques de imóveis prontos, que servirão como garantia para monetização de capital, com taxas mais acessíveis e prazos de pagamentos interessantes. Está aí uma oportunidade para reforçar o caixa da empresa.

                </p>
            </div>
            <div class="line"></div>
            <div class="col-lg-12">
                <h2 class="font-title-1 color-laranja"> BUILT TO SUIT (BTS)</h2>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-10">
                    Operação de longo prazo desenhada sob medida para terrenos que possam comportar os interesses de locatário pré-determinado, como centros de distribuição, fábricas e similares. Trata-se de um contrato de locação no qual o investidor viabiliza um empreendimento imobiliário segundo os interesses de um futuro usuário, que irá utilizá-lo por um período pré-estabelecido, garantindo o retorno do investimento e a remuneração pelo uso do imóvel.

                </p>
            </div>
            <div class="line"></div>
            <div class="col-lg-12">
                <h2 class="font-title-1 color-laranja">SALE LEASE BACK (SLB)</h2>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-10">
                    Aquisição do imóvel do cliente e concomitante locação ao próprio ocupante por meio de contrato atípico de locação. O principal benefício da desmobilização para as empresas é a possibilidade de reinvestir o capital da venda do imóvel na sua própria operação. Por meio dessa operação, a empresa ganha fluxo de caixa e passa a pagar aluguel pelo próprio imóvel. Produto voltado a empresas que queiram focar no seu core business, sem a preocupação de gerenciar ativos imobiliários.
                </p>
            </div>
            <div class="line"></div>
            <div class="col-lg-12">
                <h2 class="font-title-1 color-laranja">SECURITIZAÇÃO DE RECEBÍVEIS</h2>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-10">
                    Prática financeira que agrupa e converte recebíveis futuros em crédito com recebimento à vista, permitindo que as empresas recebam valores à vista que só entrariam em caixa no futuro.
                </p>
            </div>
            <div class="line"></div>
            <div class="col-lg-12">
                <h2 class="font-title-1 color-laranja"> TÉRMINO DA OBRA</h2>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-10">
                    Produto criado para atender incorporadoras, construtoras e loteadoras que muitas vezes começam suas obras com recursos próprios e/ou fluxo de pagamento dos clientes e acabam tendo pouco fôlego. Desta maneira, as empresas podem antecipar o resultado do projeto por meio da venda da carteira de recebíveis (créditos originados a mutuários), o que possibilita o término da obra em questão.
                </p>
            </div>
            <div class="line"></div>
            <div class="col-lg-12">
                <h2 class="font-title-1 color-laranja"> DESCONTO DE CONTRATO DE LOCAÇÃO</h2>
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-10">
                    Imóveis alugados nos quais o proprietário quer antecipar os valores do aluguel por alguns anos.
                </p>
            </div>

        </div>
    </div>
</section>

<section class="parallax background-overlay-dark p-t-50 p-b-50" style="background-image:url(<?= $URL_SITE ?>images/1920-ex-c2.jpg);" data-stellar-background-ratio="0.7">
    <div class="container xs-text-center sm-text-center text-light">
        <div class="row">
            <div class="col-12 text-center ">
                <img src="<?= $URL_SITE ?>images/logo-dark.png" class="img-fluid" />
                <h2 class="font-title-1 m m-b-30 m-t-20 p-b-0 text-uppercase color-laranja">Empowering your financial life</h2>
            </div>
        </div>
    </div>
</section>

<section id="contato_agente" class="background-white p-b-50 ">
    <div class="container">
        <div class=" text-center">
            <h1 class="font-title-1 text-medium color-laranja">ENTRE EM CONTATO</h1>
            <h3>Ligue no 4210.3208 ramal 2963 ou envie sua mensagem:</h3>
        </div>
        <div class="row">
            <div class="col-lg-8 p-t-50 center">
                <form id="frm_operacoes_estruturadas" method="post">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name" >Nome</label>
                            <input type="text" aria-required="true" name="nome" class="form-control required " placeholder="" required>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="email" >E-mail</label>
                            <input type="email" aria-required="true" name="email" class="form-control required " placeholder="" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="telefone" >Telefone</label>
                            <input type="text" id="telefone2" name="telefone" class="form-control" placeholder="" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="subject" >CPF/CNPJ</label>
                            <input type="text" data-mask-for-cpf-cnpj  name="cp" class="form-control" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message">Mensagem</label>
                        <textarea type="text" name="mensagem" rows="8" class="form-control required" placeholder="Como podemos lhe ajudar?" required></textarea>
                    </div>
                    <div class="row">
                        <div class="form-group center">
                            <button class="btn btn-roxo" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i> Enviar mensagem</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>