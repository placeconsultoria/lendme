<section class="halfscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE ?>images/bg-b2b.jpg">
    <div class="container-fluid">
        <div class="container-fullscreen">
            <div class="text-middle text-center ">
                <h2  data-animate="fadeInUp" data-animate-delay="300"  class="m-t-0 font-title-1 text-white text-lg">AQUISIÇÃO E PORTABILIDADE</h2>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p style="font-size: 18px;" class="font-paragrafo-1 lead m-b-20">
                    Pela LendMe você tem acesso ao melhor financiamento imobiliário para aquisição residencial e comercial. Em uma parceria com o Banco Santander, indicamos as melhores taxas e prazos para você concretizar seus planos.<br><br>

Caso já tenha um financiamento e queira adequá-lo às suas necessidades financeiras atuais, você também pode fazer a portabilidade de forma simples e rápida, transferindo a sua operação em outra instituição financeira para o Santander.<br><br>

Financiamento imobiliário descomplicado, online e com as melhores condições para você. Confira!

                </p>
                <p align="right">
                <img src="<?= $URL_SITE ?>images/logo-lendme.png" style="padding-right: 10px" >
                <img src="<?= $URL_SITE ?>images/logo-santander.png">
                </p>
            </div>
        </div>
        <div class="row p-t-50">
            
                <div class="col-md-12 btn-roxo">
                <div class="row p-t-50 p-b-50">
                <div class="col-md-6">
                <img src="<?= $URL_SITE ?>images/icon-home-aquisicao.png" >
                    <h3 class="font-title-1 color-azul-b2b text-uppercase">
                        AQUISIÇÃO OU PORTABILIDADE RESIDENCIAL PESSOA FÍSICA  
                    </h3>
                    <p class="text-white font-weight-normal">
                    
                    - Taxas a partir de 6,99% ao ano (0,56% ao mês) + TR Financiamento de até 80% do valor do imóvel<br>
                    - Imóveis a partir de R$ 90 mil<br>
                    - Composição de renda com qualquer pessoa<br>
                    - Em até 35 anos para pagar
                    </p>
                    
                </div>
                
                <div class="col-md-6">
                <img src="<?= $URL_SITE ?>images/icon-comercial-aquisicao.png" >
                    <h3 class="font-title-1 color-azul-b2b text-uppercase">
                        AQUISIÇÃO COMERCIAL PESSOA FÍSICA 
                    </h3>
                    <p class="text-white font-weight-normal">
                    
                        - Taxas a partir de 13% ao ano (1,02% ao mês) + TR<br>
                        - Financiamento de até 70% do valor do imóvel<br>
                        - Imóveis a partir de  R$ 90 mil<br>
                        - Salas comerciais, lojas e prédios comerciais não operacionais<br>
                        - Em até 30 anos para pagar

                    
                    </p>
                    
                </div>
                <div class="col-md-12  text-center p-t-20">
                <a class="btn btn-lg btn-azul" href="#seja-nosso-agente" name="btn_menu">SOLICITE AQUI!</a>
                </div>
                </div>
                
                
            </div>
        </div>
    </div>
</section>
<section id="seja-nosso-agente" class="background-white p-b-50 ">
    <div class="container">
        <div class=" text-center">
            <h1 style="font-size: 30px" class="font-title-1 color-azul-b2b">SIMULAÇÃO</h1>
            <h3>PREENCHA OS DADOS E FAÇA SUA SOLICITAÇÃO</h3>
        </div>
        <div class="row">
            <div class="col-lg-10 p-t-50 center">
                <form id="frm_aquisicao" method="post">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="name" >NOME</label>
                            <input type="text" aria-required="true" name="nome" id="nome_aquisicao" class="form-control required " placeholder="" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="email" >NASCIMENTO</label>
                            <input type="text" aria-required="true" name="nascimento" id="aquisicao_nascimento" class="form-control required mask-date" placeholder="" required>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="subject" >CPF</label>
                            <input type="text" name="cpf" id="aquisicao_cpf" class="form-control mask-cpf" placeholder="" required>
                        </div>
                    </div>
                    <div class="row">
                    <div class="form-group col-md-3">
                            <label for="imovel" >VALOR DO IMÓVEL</label>
                            <input type="text" aria-required="true" name="imovel" id="imovel_aquisicao" class="form-control required mask-money" placeholder="" required>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="financiamento" >VALOR DO FINANCIAMENTO</label>
                            <input type="text" aria-required="true" name="financiamento" id="aquisicao_financiamento" class="form-control mask-money" placeholder="" required>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="prazo" >PRAZO DESEJADO</label>
                            <input type="number" max="35" name="prazo" id="aquisicao_prazo" class="form-control" placeholder="" required>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="renda" >RENDA BRUTA</label>
                            <input type="text" name="renda" id="aquisicao_renda" class="form-control mask-money" placeholder="" required>
                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-4">
                            <label for="telefone" >TELEFONE / CELULAR</label>
                            <input type="text" id="telefone2" name="telefone" class="form-control" placeholder="" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="subject" >E-MAIL</label>
                            <input type="email" id="aquisicao_email" name="email" class="form-control" placeholder="" required>
                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="subject" >COMO NOS CONHECEU?</label>
                            <select id="aquisicao_conheceu" name="conheceu" class="form-control">
                                <option value="SITE">SITE</option>
                                <option value="REDES SOCIAIS">REDES SOCIAIS</option>
                                <option value="INDICAÇÃO">INDICAÇÃO</option>
                                <option value="GOOGLE">GOOGLE</option>
                                <option value="OUTROS">OUTROS</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">

                    </div>
                    <div class="row">
                        <div class="form-group center p-t-40">
                            <button i class="btn btn-roxo" type="submit" id="form-submit-agente"><i class="fa fa-handshake"></i> Enviar solicitação</button>
                        </div>
                    </div>

                    <div class="row erro_cadastro_parceiro" style="display: none;">
                        <div class="col-lg-12">
                            <div role="alert" class="alert alert-danger text-left alert-dismissible">
                                <i class="fa fa-warning"></i> Atenção: <span id="texto_error_parceiro"></span>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>