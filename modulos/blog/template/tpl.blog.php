<section class="halfscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE ?>images/lendme-bg-2.jpg">
    <div class="container-fluid">
        <div class="container-fullscreen">
            <div class="text-middle text-center ">
                <h2  data-animate="fadeInUp" data-animate-delay="300"  class="m-t-0 font-title-1 text-white text-lg">BLOG</h2>
            </div>
        </div>
    </div>
</section>
<section class="background-white p-b-0 m-b-0">
    <div class="container">
        <div class="page-title">
            <!--                <h1 class="font-paragrafo-1 color-roxo text-uppercase">Conheça o blog da LendMe</h1>-->
            <div class="breadcrumb float-left">
                <ul>
                    <li><a href="index.php">Home</a>
                    </li>
                    <li><a href="<?= $URL_SITE ?>blog/materias">Blog</a>
                    </li>
                    <li class="active"><a href="#">Listagem completa</a>
                    </li>
                </ul>
            </div>
        </div>

        <div id="blog" class="grid-layout post-3-columns m-b-30 p-t-30" data-item="post-item">
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a href="#">
                            <img alt="" src="<?= $URL_SITE ?>img-1.png">
                        </a>
                        <span class="post-meta-category"><a href="">Home Equity</a></span>
                    </div>
                    <div class="post-item-description">
                        <span class="post-meta-date"><i class="fa fa-calendar-o"></i>13/07/2020</span>
                        <h2><a href="<?= $URL_SITE ?>blog/postagem-completa/"> A aprovação de medidas simples pode contribuir para
                                destravar a economia na pós-pandemia</a></h2>
                        <p> Um 2020 que ninguém esperava e que a cada dia nos apresenta novos desafios. Vivemos um cenário impensável e com questões complexas, sem precedentes na história recente. É difícil prever hoje o que acontecerá depois que superarmos essa pandemia, mas é claro que o mundo não será mais o mesmo. A busca é - e será por longo tempo - pela sobrevivência, com o menor índice de dano colateral possível.
                            Segundo projeções do Ibevar (Instituto Brasileiro de Executivos de Varejo & Mercado de Consumo) a taxa de inadimplência da pessoa física deve crescer na ordem de 5,96% em julho e 6,09% em agosto, na comparação com os mesmos períodos de 2019.</p>
                        <a href="<?= $URL_SITE ?>blog/postagem-completa/" class="item-link">Continuar lendo <i class="fa fa-arrow-right"></i></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>