<section class="halfscreen background-overlay-dark" data-parallax-image="<?= $URL_SITE ?>images/lendme-bg-2.jpg">
    <div class="container-fluid">
        <div class="container-fullscreen">
            <div class="text-middle text-center ">
                <h2  data-animate="fadeInUp" data-animate-delay="300"  class="m-t-0 font-title-1 text-white text-lg">BLOG</h2>
                <h2  data-animate="fadeInUp" data-animate-delay="400" class="font-title-1 text-white">POSTAGEM COMPLETA</h2>
            </div>
        </div>
    </div>
</section>
<section class="background-white p-b-0 m-b-0">
    <div class="container">

        <div class="content col-lg-12">
            <!-- Blog -->
            <div id="blog" class="single-post">
                <!-- Post single item-->
                <div class="post-item">
                    <div class="post-item-wrap">
                        <div class="post-image">
                            <a href="#">
                                <img alt="" src="img-1.png">
                            </a>
                        </div>
                        <div class="post-item-description">
                            <h2> A aprovação de medidas simples pode contribuir para  destravar a economia na pós-pandemia</h2>
                            <div class="post-meta">

                                <span class="post-meta-date"><i class="fa fa-calendar-o"></i>13/07/2020</span>
                                <span class="post-meta-category"><a href=""><i class="fa fa-tag"></i>Home Equity, LendMe</a></span>
                                <div class="post-meta-share">
                                    <a class="btn btn-xs btn-slide btn-facebook" href="#">
                                        <i class="fab fa-facebook-f"></i>
                                        <span>Facebook</span>
                                    </a>
                                    <a class="btn btn-xs btn-slide btn-twitter" href="#" data-width="100">
                                        <i class="fab fa-twitter"></i>
                                        <span>Twitter</span>
                                    </a>
                                    <a class="btn btn-xs btn-slide btn-instagram" href="#" data-width="118">
                                        <i class="fab fa-instagram"></i>
                                        <span>Instagram</span>
                                    </a>
                                    <a class="btn btn-xs btn-slide btn-googleplus" href="mailto:#" data-width="80">
                                        <i class="far fa-envelope"></i>
                                        <span>Mail</span>
                                    </a>
                                </div>
                            </div>

                            <p>
                                Um 2020 que ninguém esperava e que a cada dia nos apresenta novos desafios. Vivemos um cenário impensável e com questões complexas, sem precedentes na história recente. É difícil prever hoje o que acontecerá depois que superarmos essa pandemia, mas é claro que o mundo não será mais o mesmo. A busca é - e será por longo tempo - pela sobrevivência, com o menor índice de dano colateral possível. <br />
                                Segundo projeções do Ibevar (Instituto Brasileiro de Executivos de Varejo & Mercado de Consumo) a taxa de inadimplência da pessoa física deve crescer na ordem de 5,96% em julho e 6,09% em agosto, na comparação com os mesmos períodos de 2019.<br />
                                O dinheiro está escasso em todos os setores. Do poder público ao privado; da pessoa jurídica à pessoa física. A procura por crédito já aumentou e só vai crescer.<br />
                                Auxílio emergencial, linhas de crédito do sistema bancário tradicional, programas voltados às pequenas empresas. Uma gama diversificada para um público também heterogêneo. A pós-pandemia vai acentuar e muito a procura por dinheiro.<br />
                                Recente pesquisa do BTG, conduzida pelo Instituto FSB, aponta o céu cinzento do momento, traduzido pela evidente perda de renda, pelo aumento das despesas, a dificuldade de retomar as atividades profissionais e o medo de um futuro incerto.<br />
                                Na cesta de produtos dos bancos, os empréstimos já precificam os riscos, com aumento das taxas de juros e prazos mais curtos para pagamento. Mesmo abastecidos na outra ponta por recursos do governo, com taxa Selic bastante atraente e no menor patamar da história -, o crédito no sistema bancário não é acessível a todos.<br />
                                Com projeções tão desanimadoras, como ter a sua paz de volta? Para quem já pensa em meios de suprir suas necessidades nos próximos meses, o Home Equity pode ser uma saída inteligente. Com taxas menores que as praticadas em outras modalidades de empréstimos e prazos maiores - podendo chegar a 15 anos, o empréstimo com garantia de imóvel é  um produto que já faz parte do portfólio de muitas instituições financeiras e da vida do brasileiro.<br />
                                Temos no Brasil uma imensa capacidade de recursos que não dependem de programas do governo, não oneram o déficit público e que têm força para impulsionar, de forma rápida e positiva, a economia.<br />
                                A última Pesquisa Nacional por Amostra de Domicílios (PNAD) apontou que cerca de 70% das propriedades imobiliárias no Brasil estão pagas e quitadas, em nome de seus proprietários. Estima-se que essas propriedades representem um valor potencial de R$ 1 trilhão. E, para acessar esse dinheiro, precisamos de muito pouco.<br />
                                Por meio do Home Equity é possível injetar uma enorme quantidade de recursos na economia, sem onerar as contas públicas, uma vez que este produto se utiliza do mercado de Securitização Imobiliária oferecendo produtos atraentes de investimentos - LCIs, CRIs, FIDC, FII, LIG e outros. Estabelece-se assim um fluxo virtuoso de dinheiro entre os proprietários de imóveis (PF ou PJ)- que necessitam de recursos -, e o mercado de investimentos e títulos privados.<br />
                                Em uma outra modalidade, a da hipoteca reversa, podemos ajudar a imensa e crescente população de idosos na utilização do patrimônio imobiliário adquirido em vida em uma fonte extra de renda, que complemente seu recebimento da previdência, no momento que mais necessitam de recursos para suprir o aumento de despesas que a idade traz.<br />
                                A solução está nas mãos do governo e para destravar esta enorme quantidade de capital, basta acelerar algumas medidas que já estão na pauta do Banco Central e do Ministério da Economia. Regulamentar a múltipla hipoteca e aprovar o projeto de lei da hipoteca reversa (PL 55) , deveria ser pauta urgente nesse momento, abrindo caminho para uma alternativa de crédito rápida, com dinheiro barato e para uma parte da população que, provavelmente, não terá acesso fácil em outros canais.
                            </p>
                            <h3>Postado por: Elyseu Mardegan Jr</h3>
                        </div>
                        <div class="post-tags">
                            <a href="#">Home Equity</a>
                            <a href="#">LendMe</a>

                        </div>

                    </div>
                </div>
                <!-- end: Post single item-->
            </div>

        </div>

    </div>
</section>